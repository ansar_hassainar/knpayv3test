# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django import forms
from django.utils.translation import ugettext_lazy as _

from .models import NexmoSMSAuthConfig, TwilioSMSAuthConfig, MsgQ8Config


class NexmoSMSAuthConfigForm(forms.ModelForm):
    class Meta:
        model = NexmoSMSAuthConfig
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(NexmoSMSAuthConfigForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = _('API Key')
        self.fields['password'].label = _('API Secret')


class TwilioSMSAuthConfigForm(forms.ModelForm):
    class Meta:
        model = TwilioSMSAuthConfig
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(TwilioSMSAuthConfigForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = _('Account SID')
        self.fields['password'].label = _('Auth Token')


class SMSAuthConfigForm(forms.ModelForm):
    class Meta:
        fields = "__all__"
        widgets = {
            'password': forms.PasswordInput(render_value=True),
        }


class MPPConfigForm(SMSAuthConfigForm):
    def __init__(self, *args, **kwargs):
        super(MPPConfigForm, self).__init__(*args, **kwargs)
        self.fields['api_url'].initial = 'http://api.mpp-sms.com/api/send.aspx'
        self.fields['source'].label = _('Sender')


class MsgQ8ConfigForm(SMSAuthConfigForm):
    def __init__(self, *args, **kwargs):
        super(MsgQ8ConfigForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = _('API Key')
        self.fields['password'].label = _('Private Key')
        self.fields['source'].label = _('Sender ID')
        self.fields['api_url'].initial = 'http://msgq8.com/api/Service.svc/Send'


class FCCConfigForm(SMSAuthConfigForm):
    def __init__(self, *args, **kwargs):
        super(FCCConfigForm, self).__init__(*args, **kwargs)
        self.fields['password'].required = False
        self.fields['source'].label = _('Sender ID')
        self.fields['api_url'].label = _('URL')
        self.fields['api_url'].initial = 'http://62.215.226.164/'


class FCCPuffinConfigForm(SMSAuthConfigForm):
    def __init__(self, *args, **kwargs):
        super(FCCPuffinConfigForm, self).__init__(*args, **kwargs)
        self.fields['password'].required = False
        self.fields['source'].label = _('Sender ID')
        self.fields['api_url'].label = _('URL')
        self.fields['api_url'].initial = 'https://trxnc.future-club.com/'
