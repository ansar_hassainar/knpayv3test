# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from urllib import urlopen, urlencode
from json import loads
import logging

from utils.funcs import only_roman_chars
from ...models import NexmoSMSAuthConfig

logger = logging.getLogger('communications')


def send(sms_to, sms_body, **kwargs):
    config = NexmoSMSAuthConfig.objects.current()
    
    if not config:
        return

    params = {
        'api_key': config.username,
        'api_secret': config.password,
        'from': config.source,
        'to': sms_to.replace(' ', '').replace('+', ''),
        'text': sms_body.encode('utf-8')
    }

    if not only_roman_chars(unicode(sms_body)):
        params['type'] = 'unicode'

    if kwargs:
        params.update(**kwargs)
    try:
        url = urlopen('%s?%s' % ('https://rest.nexmo.com/sms/json', urlencode(params)))
    except Exception as e:
        logger.exception("Error while sending SMS %s" % e)
        return False
    messages = loads(url.read())

    response = messages.get('messages')

    if response and response[0].get('error-text'):
        logger.exception("Error while sending SMS %s" % messages['messages'][0]['error-text'])
    elif 'status' in messages and messages.get('status') != '0':
        logger.exception("Error while sending SMS %s" % messages.get('error-text'))
    return messages
