# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import logging
from httplib import HTTPSConnection
from urllib import urlencode
from base64 import b64encode
from json import loads
from dbmail.providers.prowl.push import from_unicode
from dbmail import get_version
from ...models import TwilioSMSAuthConfig

logger = logging.getLogger('communications')


def send(sms_to, sms_body, **kwargs):
    """
    Site: https://www.twilio.com/
    API: https://www.twilio.com/docs/api/rest/sending-messages
    """
    config = TwilioSMSAuthConfig.objects.current()

    if not config:
        return
        
    headers = {
        "Content-type": "application/x-www-form-urlencoded",
        "User-Agent": "DBMail/%s" % get_version(),
        'Authorization': 'Basic %s' % b64encode(
            "%s:%s" % (
                config.username, config.password
            )).decode("ascii")

    }

    kwargs.update({
        'From': config.source,
        'To': sms_to,
        'Body': from_unicode(sms_body)
    })

    http = HTTPSConnection('api.twilio.com')
    http.request(
        "POST",
        "/2010-04-01/Accounts/%s/Messages.json" % config.username,
        headers=headers,
        body=urlencode(kwargs))

    response = http.getresponse()
    if response.status != 201:
        logger.exception("Error while sending SMS %s" % response.reason)
        return

    return loads(response.read()).get('sid')