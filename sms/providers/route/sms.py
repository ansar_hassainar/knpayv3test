# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging
from urllib import urlencode
import requests

from utils.funcs import only_roman_chars
from ...models import RouteSMSAuthConfig

logger = logging.getLogger('communications')


def send(sms_to, sms_body, **kwargs):
    config = RouteSMSAuthConfig.objects.current()
    _success = '1701'
    _unicode = 2
    _plain_text = 0
    
    if not config:
        return

    message_type = _plain_text
    if not only_roman_chars(unicode(sms_body)):
        message_type = _unicode

    dict_param = dict(username=config.username, password=config.password,
                      sender=config.source, destination=sms_to.replace(' ', '').replace('+', ''),
                      message=sms_body.encode('utf-8'), dlr=0,
                      type=message_type)
    
    url = '{}?{}'.format(config.api_url, urlencode(dict_param))
    try:
        response = requests.get(url)
    except Exception as e:
        logger.exception("Error while sending SMS %s" % e)
        return False

    if _success in response.content:
        code, msg_id = response.content.split(
            '|')[0], response.content.split('|')[2]
    else:
        code = response.content.split('|')[0]
    provider_response = code

    return provider_response == _success
