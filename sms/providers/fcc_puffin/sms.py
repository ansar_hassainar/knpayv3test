# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

from urllib import urlencode
import requests

from utils.funcs import str_to_hex, only_roman_chars
from ...models import FCCPuffinSMSAuthConfig

logger = logging.getLogger('communications')


def send(sms_to, sms_body, **kwargs):
    config = FCCPuffinSMSAuthConfig.objects.current()
    success_code = '00'

    if not config:
        return

    dict_param = dict(UID=config.username, S=config.source, P=config.password,
                      G=sms_to.replace(' ', '').replace('+', ''))

    if not only_roman_chars(unicode(sms_body)):
        dict_param['M'] = str_to_hex(sms_body.encode('utf-8'))
        dict_param['L'] = 'U'
    else:
        dict_param['M'] = sms_body.encode('utf-8')
        dict_param['L'] = 'L'

    url = '{}fccsms.aspx?{}'.format(config.api_url, urlencode(dict_param))
    try:
        response = requests.get(url)
    except Exception as e:
        logger.exception("Error while sending SMS %s" % e)
        return False
    provider_response = response.content.split()[0]
    return success_code == provider_response
