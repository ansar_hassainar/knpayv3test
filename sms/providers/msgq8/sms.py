# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import logging
import requests
from ...models import MsgQ8Config

logger = logging.getLogger('communications')


def send(sms_to, sms_body, **kwargs):
    config = MsgQ8Config.objects.current()
    
    if not config:
        raise Exception('No configuration found')

    sms_payload = {
        'apiKey': config.username,
        'privateKey': config.password,
        'SenderID': config.source,
        'numbers': sms_to.replace(' ', '').replace('+', ''),
        'msg': sms_body.encode('utf-8')
    }
    logger.info("SMS payload %s" % sms_payload)

    try:
        r = requests.post(config.api_url, json=sms_payload)
    except Exception as e:
        logger.exception("Error while sending SMS %s" % e)
        return

    if r.status_code == requests.codes.ok:
        content = r.json()
        if content.get('isSuccess', False):
            return True
        else:
            logger.info("Error while sending SMS %s" % content.get("errorMsg"))

    logger.info(r.content)
    return False
