# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

from urllib import urlencode
import requests

from utils.funcs import str_to_hex, only_roman_chars
from ...models import MPPSMSAuthConfig

logger = logging.getLogger('communications')


def send(sms_to, sms_body, **kwargs):
    config = MPPSMSAuthConfig.objects.current()
    _success = '1701'
    _unicode = 2
    _char_type = 0
    
    if not config:
        return

    dict_param = dict(username=config.username, password=config.password,
                      sender=config.source, mobile=sms_to.replace(' ', '').replace('+', ''))
    
    if not only_roman_chars(unicode(sms_body)):
        _char_type = _unicode
        
    if _char_type == _unicode:
        dict_param['message'] = str_to_hex(sms_body.encode('utf-8'))
        dict_param['language'] = 3
    else:
        dict_param['message'] = sms_body.encode('utf-8')
        dict_param['language'] = 1
    url = '{}?{}'.format(config.api_url, urlencode(dict_param))
    try:
        response = requests.get(url)
    except Exception as e:
        logger.exception("Error while sending SMS %s" % e)
        return False

    if _success in response.content:
        code, msg_id = response.content.split(
            '|')[0], response.content.split('|')[2]
    else:
        code = response.content.split('|')[0]
    provider_response = code

    return provider_response == _success
