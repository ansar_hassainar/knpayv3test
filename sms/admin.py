# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from polymorphic.admin import (PolymorphicParentModelAdmin,
                               PolymorphicChildModelAdmin, PolymorphicChildModelFilter)

from .models import (SMSAuthConfig, RouteSMSAuthConfig, MPPSMSAuthConfig,
                     NexmoSMSAuthConfig, TwilioSMSAuthConfig, MsgQ8Config,
                     FCCSMSAuthConfig, FCCPuffinSMSAuthConfig)
from .forms import (NexmoSMSAuthConfigForm, TwilioSMSAuthConfigForm,
                    SMSAuthConfigForm, MsgQ8ConfigForm, MPPConfigForm,
                    FCCConfigForm, FCCPuffinConfigForm)


class ProviderPolymorphicChildModelFilter(PolymorphicChildModelFilter):
    title = _("Provider")


@admin.register(RouteSMSAuthConfig)
class RouteSMSAuthConfigAdmin(PolymorphicChildModelAdmin):
    base_model = RouteSMSAuthConfig
    save_as = True
    form = SMSAuthConfigForm
    fieldsets = (
        (None, {
            'fields': ('is_current', 'username', 'password', 'source', 'api_url')
        }),
    )


@admin.register(MPPSMSAuthConfig)
class MPPSMSAuthConfigAdmin(PolymorphicChildModelAdmin):
    base_model = MPPSMSAuthConfig
    save_as = True
    form = MPPConfigForm
    fields = ('is_current', 'username', 'password', 'source', 'api_url')


@admin.register(NexmoSMSAuthConfig)
class NexmoSMSAuthConfigAdmin(PolymorphicChildModelAdmin):
    base_model = NexmoSMSAuthConfig
    save_as = True
    form = NexmoSMSAuthConfigForm


@admin.register(TwilioSMSAuthConfig)
class TwilioSMSAuthConfigAdmin(PolymorphicChildModelAdmin):
    base_model = TwilioSMSAuthConfig
    save_as = True
    form = TwilioSMSAuthConfigForm


@admin.register(MsgQ8Config)
class MsqQ8SMSAuthConfigAdmin(PolymorphicChildModelAdmin):
    base_model = MsgQ8Config
    save_as = True
    form = MsgQ8ConfigForm
    fields = ('is_current', 'username', 'password', 'source', 'api_url')


@admin.register(FCCSMSAuthConfig)
class FCCSMSAuthConfigAdmin(PolymorphicChildModelAdmin):
    base_model = FCCSMSAuthConfig
    save_as = True
    form = FCCConfigForm
    fields = ('is_current', 'username', 'source', 'password', 'api_url')


@admin.register(FCCPuffinSMSAuthConfig)
class FCCPuffinSMSAuthConfigAdmin(PolymorphicChildModelAdmin):
    base_model = FCCPuffinSMSAuthConfig
    save_as = True
    form = FCCPuffinConfigForm
    fields = ('is_current', 'username', 'source', 'password', 'api_url')


@admin.register(SMSAuthConfig)
class SMSAuthConfigAdmin(PolymorphicParentModelAdmin):
    base_model = SMSAuthConfig
    child_models = (RouteSMSAuthConfig, MPPSMSAuthConfig, NexmoSMSAuthConfig,
                    TwilioSMSAuthConfig, MsgQ8Config, FCCSMSAuthConfig,
                    FCCPuffinSMSAuthConfig)
    list_filter = (ProviderPolymorphicChildModelFilter,)
    list_display = ('username', 'is_current', )
    list_editable = ('is_current', )
