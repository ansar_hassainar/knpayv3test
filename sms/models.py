from __future__ import unicode_literals

from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.db import models
from django.utils.translation import ugettext_lazy as _

from polymorphic.models import PolymorphicModel
from polymorphic.query import PolymorphicQuerySet

from utils.db.models import TimeStampedModel


class SMSAuthConfigQuerySet(PolymorphicQuerySet):
    def current(self):
        try:
            return self.get(is_current=True)
        except ObjectDoesNotExist:
            return None


class SMSApiURLMixin(models.Model):
    api_url = models.URLField(_("API url"),
                              help_text=_("The url where to send the sms payload for "
                                          "being broadcasted")
                              )

    class Meta:
        abstract = True


class SMSAuthConfig(PolymorphicModel, TimeStampedModel):
    provider = 'default'

    is_current = models.BooleanField(
        _("Is current"), default=False,
        help_text=_("Designates if this provider shall be used "
                    "for sending SMS from the platform"))
    username = models.CharField(_("Username"), max_length=64)
    password = models.CharField(_("Password"), max_length=64)
    source = models.CharField(
        _("Source"), max_length=64,
        help_text=_("The source address that should appear in the message"))

    objects = SMSAuthConfigQuerySet.as_manager()

    def __unicode__(self):
        return "%s" % self.username

    def get_path(self):
        return 'sms.providers.%s.sms' % self.provider

    class Meta:
        verbose_name = _("SMS auth config")
        verbose_name_plural = _("SMS auth config")

    def clean(self):
        if self.is_current and SMSAuthConfig.objects.exclude(pk=self.pk).filter(is_current=True).exists():
            provider = SMSAuthConfig.objects.exclude(pk=self.pk).filter(is_current=True).get().provider
            raise ValidationError(_("%s is active. Disable it first "
                                    "and then enable %s.") %
                                  (provider.upper(), self.provider))

    @classmethod
    def get_path_for_current(cls):
        try:
            return cls.objects.get(is_current=True).get_path()
        except cls.DoesNotExist:
            return None


class RouteSMSAuthConfig(SMSAuthConfig, SMSApiURLMixin):
    provider = 'route'

    class Meta:
        verbose_name = _("Route")
        verbose_name_plural = _("Route")


class MPPSMSAuthConfig(SMSAuthConfig, SMSApiURLMixin):
    provider = 'mpp'

    class Meta:
        verbose_name = _("MPP")
        verbose_name_plural = _("MPP")


class FCCSMSAuthConfig(SMSAuthConfig, SMSApiURLMixin):
    provider = 'fcc'

    class Meta:
        verbose_name = _("FCC")
        verbose_name_plural = _("FCC")


class FCCPuffinSMSAuthConfig(SMSAuthConfig, SMSApiURLMixin):
    provider = 'fcc_puffin'

    class Meta:
        verbose_name = _("FCC Puffin")
        verbose_name_plural = _("FCC Puffin")


class NexmoSMSAuthConfig(SMSAuthConfig):
    provider = 'nexmo'

    class Meta:
        verbose_name = _("Nexmo")
        verbose_name_plural = _("Nexmo")


class TwilioSMSAuthConfig(SMSAuthConfig):
    provider = 'twilio'

    class Meta:
        verbose_name = _("Twilio")
        verbose_name_plural = _("Twilio")


class MsgQ8Config(SMSAuthConfig, SMSApiURLMixin):
    provider = 'msgq8'

    class Meta:
        verbose_name = _("MsgQ8")
        verbose_name_plural = _("MsgQ8")

