from decimal import Decimal
import unittest

from django.test import TestCase

import pytest
from mixer.backend.django import mixer

from utils.tests import knet_settings_factory
from ..models import ExchangeConfig
pytestmark = pytest.mark.django_db


class ExchangeCurrencyCase(TestCase):

    def setUp(self):
        self.amount = Decimal(12)
        self.default_currency = mixer.blend('currency.Currency', code='KWD')
        self.exchange_currency = mixer.blend('currency.Currency', code='USD')

        mixer.blend('currency.CurrencyExchange', from_code='USD',
                    to_code='KWD', rate=Decimal(0.305))
        mixer.blend('currency.CurrencyExchange', from_code='KWD',
                    to_code='USD', rate=Decimal(3.269))

        self.exchange_rate_with_fix_fee = mixer.blend('currency.ExchangeConfig',
                                                      default_currency=self.default_currency,
                                                      charge_default_currency=True,
                                                      fee_type='fee_fix',
                                                      work_as=ExchangeConfig.LOCAL,
                                                      fee_fix=Decimal(8))
        self.exchange_rate_with_percent_fee = mixer.blend('currency.ExchangeConfig',
                                                          default_currency=self.default_currency,
                                                          charge_default_currency=True,
                                                          fee_type='fee_percent',
                                                          work_as=ExchangeConfig.LOCAL,
                                                          fee_percent=Decimal(0.5))

    def test_compute_fee_with_fix_fee_and_same_currency_charge_default_currency_method(self):
        fee = self.exchange_rate_with_fix_fee.compute_fee(
            self.amount,  self.default_currency.code)
        exchange_rate_fee = self.exchange_rate_with_fix_fee.fee_fix

        assert fee == exchange_rate_fee, 'values should be equal'

    def test_compute_fee_with_fix_fee_and_same_currency_dont_charge_same_currency_method(self):
        self.exchange_rate_with_fix_fee.charge_default_currency = False
        self.exchange_rate_with_fix_fee.save()

        fee = self.exchange_rate_with_fix_fee.compute_fee(
            self.amount,  self.default_currency.code)
        exchange_rate_fee = self.exchange_rate_with_fix_fee.fee_fix

        assert fee == Decimal(0), 'the fee should not be applied'

    def test_compute_fee_with_fix_fee_from_different_currency_method(self):
        self.exchange_rate_with_fix_fee.charge_default_currency = True
        self.exchange_rate_with_fix_fee.save()

        fee = self.exchange_rate_with_fix_fee.compute_fee(
            self.amount,  self.exchange_currency.code)
        exchange_rate_fee = self.exchange_rate_with_fix_fee.fee_fix

        assert fee == exchange_rate_fee, 'values should be equal'

    def test_compute_fee_with_percent_fee_and_same_currency_charge_default_currency_method(self):
        fee = self.exchange_rate_with_percent_fee.compute_fee(
            self.amount,  self.default_currency.code)
        exchange_rate_fee = self.exchange_rate_with_percent_fee.fee_percent * \
            self.amount

        assert fee == exchange_rate_fee, 'values should be equal'

    def test_compute_fee_with_percent_fee_and_same_currency_dont_charge_same_currency_method(self):
        self.exchange_rate_with_percent_fee.charge_default_currency = False
        self.exchange_rate_with_percent_fee.save()

        fee = self.exchange_rate_with_percent_fee.compute_fee(
            self.amount,  self.default_currency.code)
        exchange_rate_fee = self.exchange_rate_with_percent_fee.fee_percent * \
            self.amount

        assert fee == Decimal(0), 'the fee should not be applied'

    def test_compute_fee_with_percent_fee_from_different_currency_method(self):
        self.exchange_rate_with_percent_fee.charge_default_currency = True
        self.exchange_rate_with_percent_fee.save()

        fee = self.exchange_rate_with_percent_fee.compute_fee(
            self.amount,  self.exchange_currency.code)
        currency_bool, converted_amount = self.exchange_rate_with_percent_fee._compute_amount_from_currency(
            self.amount, self.exchange_currency.code)
        exchange_rate_fee = self.exchange_rate_with_percent_fee._quantize_value(
            self.exchange_rate_with_percent_fee.fee_percent * converted_amount)

        assert fee == exchange_rate_fee, 'values should be equal'

    def test_compute_amount_with_same_default_currency_method(self):
        computed_amount = self.exchange_rate_with_percent_fee.compute_amount(
            self.amount,  self.default_currency.code)
        assert self.amount == computed_amount, 'values should be equal'

    def test_compute_amount_with_different_currency_method(self):
        currency_bool, converted_amount = self.exchange_rate_with_percent_fee._compute_amount_from_currency(
            self.amount, self.exchange_currency.code)
        amount = self.exchange_rate_with_percent_fee.compute_amount(
            self.amount,  self.exchange_currency.code)

        assert amount == self.exchange_rate_with_percent_fee._quantize_value(
            converted_amount), 'values should be equal'

    def test_is_currency_code_accepted(self):
        assert self.exchange_rate_with_fix_fee.is_currency_code_accepted('USD') is False
        assert self.exchange_rate_with_fix_fee.is_currency_code_accepted('KWD') is True

    def test_quantize_value(self):
        self.exchange_rate_with_fix_fee.default_currency = self.exchange_currency
        self.exchange_rate_with_fix_fee.save()
        assert Decimal(10.00) == self.exchange_rate_with_fix_fee._quantize_value(Decimal(10.001))

    def test_has_fee(self):
        assert self.exchange_rate_with_fix_fee.has_fee(), 'it should have fee'
        assert self.exchange_rate_with_percent_fee.has_fee(), 'it should have fee'
        config = mixer.blend('currency.ExchangeConfig', default_currency=self.default_currency, fee_type='',)
        assert config.has_fee() is False, 'should\'t have fee'
