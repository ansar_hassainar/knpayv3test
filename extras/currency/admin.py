# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib import admin

from import_export.admin import ImportExportModelAdmin

from .models import ExchangeConfig, CurrencyExchange, Currency


@admin.register(Currency)
class CurrencyAdmin(admin.ModelAdmin):
    list_filter = ('is_active',)
    list_display = ('code', 'is_active')


@admin.register(CurrencyExchange)
class CurrencyExchangeAdmin(ImportExportModelAdmin):
    list_display = ('__unicode__', 'id', 'from_code', 'to_code', 'rate')
    search_fields = ('from_code', 'to_code')
    readonly_fields = ('id',)
    list_editable = ('from_code', 'to_code', 'rate')
    fields = ('id', 'from_code', 'to_code', 'rate')
    save_as = True


@admin.register(ExchangeConfig)
class ExchangeConfigAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'name', 'default_currency', 'work_as')
    list_editable = ('name', 'default_currency', 'work_as')
    list_filter = ('default_currency', 'work_as')
    save_as = True
    filter_horizontal = ('currencies',)
