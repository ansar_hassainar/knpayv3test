# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging
from decimal import Decimal

from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import ugettext_lazy as _, ugettext as _f

from utils.db.managers import StatusQuerySet
from utils.db.models import StatusModel
from utils.funcs import correct_decimals
from .data import CURRENCY_CHOICES, KWD, USD, currency_decimal_places


logger = logging.getLogger('currency')


class CurrencyManager(models.Manager):
    def get_queryset(self):
        return StatusQuerySet(self.model, using=self._db)

    def active(self):
        return self.get_queryset().active()

    def choices(self):
        return self.active().values_list('code', 'code')


class Currency(StatusModel):
    code = models.CharField(_("Code"), choices=CURRENCY_CHOICES,
                            default=KWD, max_length=4, unique=True)
    decimal_places = models.IntegerField(
        _("Decimal places"), default=2, editable=False,
        help_text=_("The number of decimal places to round the currency"))
    objects = CurrencyManager()

    def __unicode__(self):
        return self.get_code_display()

    def save(self, *args, **kwargs):
        self.decimal_places = currency_decimal_places.get(self.code)
        return super(Currency, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _("Currency")
        verbose_name_plural = _("Currencies")


class ExchangeConfig(models.Model):
    """
    Exchange settings model
    """
    LOCAL = 'local'
    FEE_FIXED, FEE_PERCENT = 'fee_fix', 'fee_percent'
    FEE_CHOICES = (
        (FEE_FIXED, _("Fix fee")),
        (FEE_PERCENT, _("Percent fee"))
    )
    WORK_CHOICES = (
        (LOCAL, _("Local")),
    )
    name = models.CharField(
        _("Name"),
        max_length=32,
        help_text=_("Used only for informational purposes"))
    default_currency = models.ForeignKey(
        Currency,
        verbose_name=_("Default currency"),
        related_name='+',
        on_delete=models.SET_NULL,
        null=True,
    )
    currencies = models.ManyToManyField(
        Currency,
        verbose_name=_("Currencies"),
        help_text=_("Select currencies for which conversion shall be available"),
        blank=True
    )
    fee_type = models.CharField(
        _("Fee type"),
        choices=FEE_CHOICES,
        default='',
        blank=True,
        max_length=20)
    work_as = models.CharField(
        _("Work as"),
        max_length=24,
        default=LOCAL,
        choices=WORK_CHOICES,
        help_text=_("Select how the conversion should work. "
                    "Local - Takes rates defined manually."))
    fee_fix = models.DecimalField(
        _("Fix fee"),
        max_digits=8,
        decimal_places=3,
        blank=True,
        null=True,
        help_text=_("Fixed amount fee (using default currency) to be "
                    "applied on currency conversion result.")
    )
    fee_percent = models.DecimalField(
        _("Percent fee"),
        null=True,
        blank=True,
        decimal_places=3,
        max_digits=8,
        help_text=_("Percent fee to be applied on currency "
                    "conversion result. Required to be decimal "
                    "value, ie. 0.250 represents 25%."))
    charge_default_currency = models.BooleanField(
        _("Charge default currency"),
        default=False,
        help_text=_("Apply fee on conversion from default "
                    "currency to the same currency. ie. KWD to KWD"))
    round_last_decimal = models.BooleanField(
        _("Round the last decimal"),
        default=False,
        help_text=_("Enable to round the last decimal")
    )

    def has_fee(self):
        return self.fee_type != ''

    def _get_from_local(self, from_code=None, to_code=None):
        # be sure CurrencyExchange existence is validated before reaching this stage!
        currency = CurrencyExchange.objects.get(from_code=from_code, to_code=to_code)
        return currency.rate
        # try:
        #     currency = CurrencyExchange.objects.get(from_code=from_code, to_code=to_code)
        # except CurrencyExchange.DoesNotExist:
        #     return self._get_from_yahoo(from_code=from_code, to_code=to_code)
        # return Decimal(currency.rate)

    def _quantize_value(self, value):
        decimal_places = Decimal(10) ** -self.default_currency.decimal_places

        return Decimal(value).quantize(decimal_places)

    def _compute_amount_from_currency(self, amount, from_code):
        from_currency = Currency.objects.get(code=from_code)

        if not isinstance(amount, Decimal):
            amount = Decimal(amount)
        currency_bool = self.default_currency != from_currency
        if currency_bool:
            rate = self.get_rate(from_code)
            amount = amount * rate

        return currency_bool, amount

    def get_rate(self, from_code):
        to_code = self.default_currency.code
        logger.info('Currency codes: %s - %s' % (from_code, to_code))
        return getattr(self, '_get_from_%s' % self.work_as)(from_code, to_code)

    def compute_fee(self, amount, from_code):
        currency_bool, amount = self._compute_amount_from_currency(amount, from_code)

        if (self.charge_default_currency or currency_bool) and bool(self.fee_type):
            if self.fee_type == self.FEE_FIXED:
                amount = self.fee_fix
            else:
                amount = amount * self.fee_percent

        else:
            amount = 0
        return self._quantize_value(amount)

    def compute_amount(self, amount, from_code):
        currency_bool, amount = self._compute_amount_from_currency(amount, from_code)
        return self._quantize_value(amount)

    def compute_total(self, amount, from_code):
        fee = self.compute_fee(amount, from_code)
        amount = self.compute_amount(amount, from_code)
        total = correct_decimals(sum([fee, amount])) if self.round_last_decimal else sum([fee, amount])
        total = self._quantize_value(total)
        return str(total)

    def compute_total_with_quantity(self, amount, from_code, quantity=1):
        """
        Fixed fee type: Add fee only once
        Percentage type: Fee will be calculated as per amount*quantity
        :return: fee+(amount*(quantity)) and fee
        """
        fee = self.compute_fee((amount*quantity), from_code)
        amount = self.compute_amount(amount, from_code)
        total = correct_decimals(sum([fee, (amount*quantity)])) if self.round_last_decimal else sum([fee, (amount * quantity)])
        total = self._quantize_value(total)
        return str(total), fee

    def is_currency_code_accepted(self, currency_code):
        return any([currency_code == self.default_currency.code,
                    self.currencies.filter(code=currency_code).exists()])

    def clean(self):
        if self.fee_type:
            value = getattr(self, self.fee_type)
            if not value >= Decimal(0):
                raise ValidationError(_f('Add a value for %s') % self.get_fee_type_display())

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _("Exchange config")
        verbose_name_plural = _("Exchanges config")


class CurrencyExchange(models.Model):
    """
    Currency entry
    """
    from_code = models.CharField(
        _("From"),
        max_length=3,
        choices=CURRENCY_CHOICES,
        default=KWD,
        help_text=_("ISO 4217 Currency Code")
    )
    to_code = models.CharField(
        _("To"),
        max_length=3,
        choices=CURRENCY_CHOICES,
        default=USD,
        help_text=_("ISO 4217 Currency Code")
    )
    rate = models.DecimalField(
        _("Rate"),
        decimal_places=3,
        max_digits=10
    )

    def __unicode__(self):
        return '%s - %s' % (self.from_code, self.to_code)

    def save(self, *args, **kwargs):
        self.from_code, self.to_code = self.from_code.upper(), self.to_code.upper()
        return super(CurrencyExchange, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _("Currency exchange")
        verbose_name_plural = _("Currency exchanges")
        unique_together = ('from_code', 'to_code')


