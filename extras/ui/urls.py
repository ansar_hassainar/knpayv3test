# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^attempt-detail/(?P<reference_number>[0-9A-Za-z_\-]+)/$',
        views.PaymentAttemptDetailView.as_view(), name='attempt_detail'),
    url(r'^pay/$',
        views.CustomerPaymentView.as_view(), name='customer_request'),
    url(r'^calculate-payment-fee/(?P<pk>\d+)/(?P<gateway>[\w-]+)$',
        views.PaymentTransactionFeeView.as_view(), name='calculate_payment_fee'),
    url(r'^pay/(?P<encrypted_pk>.+)/$',
        views.PaymentRequestCustomerView.as_view(), name='payment_request'),
]
