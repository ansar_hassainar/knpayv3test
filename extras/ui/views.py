# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging
from collections import OrderedDict

from django.db import transaction
from django.http import JsonResponse
from django.utils.translation import ugettext_lazy as _, get_language, \
    force_text as _ft
from django.utils.decorators import method_decorator
from django.views import generic

from rest_framework import status

from api.func import get_knpay_url
from config.models import Config
from extras.dashboard.models import CustomerPaymentFormConfig, \
    PaymentRequestFormConfig, Field
from plugins.catalogue.models import Product
from gateway.models import PaymentTransaction, PaymentAttempt, GatewaySettings
from utils.views.mixins import PageTitleMixin, EncryptedPKMixin
from .forms import PaymentRequestCustomerForm, CustomerPaymentForm

logger = logging.getLogger('ui')


class PaymentAttemptDetailView(PageTitleMixin, generic.DetailView):
    """
    Payment attempt details shown upon payment stage completes
    """
    queryset = PaymentAttempt.objects.all()
    template_name = 'ui/attempt_detail_new.html'
    slug_url_kwarg = 'reference_number'
    slug_field = 'reference_number'
    context_object_name = 'attempt'

    def get_page_title(self):
        return _("Transaction %s details") % \
               self.object.transaction.order_no or self.object.transaction.bulk_id

    def get_context_data(self, **kwargs):
        context = super(PaymentAttemptDetailView, self).get_context_data(**kwargs)
        config = PaymentRequestFormConfig.get_solo()
        atmp_obj = context['attempt']

        if atmp_obj.transaction.is_catalogue():
            context['try_again_link'] = atmp_obj.transaction.get_product_url()
        else:
            context['try_again_link'] = atmp_obj.transaction.get_payment_request_url()
        
        if atmp_obj.state == 'success':
            context['message'] = _('Thank you! Payment completed Successfully.')
            context['try_again_link'] = None
        elif atmp_obj.state == 'failed':
            context['message'] = _('Sorry, Payment got Failed.')
            context['attempt_state'] = 'Failed'
        elif atmp_obj.state == 'canceled':
            context['message'] = _('Oops, You clicked on Cancel button')

        customer_data = OrderedDict()
        extra = atmp_obj.transaction.extra
        remarks = extra.pop('remarks', None)
        order_id = None

        product_id = extra.pop('product', None)
        quantity = extra.pop('quantity', None)
        if product_id:
            try:
                context['product'] = Product.objects.get(pk=product_id)
                context['quantity'] = quantity
            except Product.DoesNotExist:
                pass

        # Complex logic for showing FUll Name
        # checks built in fields and custom fields
        full_name = extra.pop('full_name', None)
        first_name = getattr(atmp_obj.transaction, 'customer_first_name')
        last_name = getattr(atmp_obj.transaction, 'customer_last_name')
        customer_full_name = ' '.join([first_name, last_name])
        if customer_full_name.strip():
            customer_data[_('Customer Name')] = customer_full_name
        elif full_name:
            customer_data[_('Customer Name')] = full_name

        # Adding customer builtin fields in the customer details
        for field in PaymentTransaction.CUSTOMER_FIELDS:
            if field in ['customer_first_name', 'customer_last_name']:
                continue
            val = getattr(atmp_obj.transaction, field)
            # first check and collect order_no from builtin field
            if field == 'order_no' and val:
                order_id = val
                continue
            if val:
                customer_data[PaymentTransaction._meta.get_field(field).verbose_name] = val

        # Adding extra data in customer details
        for key, value in extra.items():
            # if builtin field doesn't have order no then try to get it from
            # custom field order_id
            if key == 'order_id' and not order_id:
                order_id = value
                continue
            if not value:
                continue
            if key == 'has_default_email':
                customer_data['has_default_email'] = value
                continue
            field = Field.objects.get_for_name(key, config)
            if field is not None and field.display_to_customer():
                label = field.get_label() if field is not None else key
                customer_data[_(label)] = value

        # Remove customer_email if it is set to use default email
        if 'has_default_email' in customer_data:
            customer_data.pop(PaymentTransaction._meta.get_field(
                'customer_email').verbose_name, None)
            customer_data.pop('has_default_email', None)

        # remove attachment from customer data
        customer_data.pop('Attachment', None)
        if Config.get_solo().attach_invoice_pdf:
            context['invoice_url'] = atmp_obj.transaction.get_invoice_url()
        else:
            context['invoice_url'] = None
        context['customer_details'] = customer_data

        if atmp_obj.transaction.attachment and not atmp_obj.transaction.is_paid():
            context['attachment'] = atmp_obj.transaction.attachment

        context['reference_date'] = atmp_obj.state_changed_at
        context['remarks'] = remarks
        # will show order_id as a reference number
        context['order_id'] = order_id

        return context


class PaymentRequestCustomerView(PageTitleMixin,
                                 EncryptedPKMixin,
                                 generic.UpdateView):
    """
    Page where customer can see details of the payment
    he has to make
    """
    queryset = PaymentTransaction.objects.catalogue() | PaymentTransaction.objects.payment_requests()
    form_class = PaymentRequestCustomerForm
    page_title = _("Payment request")

    def get_template_names(self):
        config = Config.get_solo()
        if self.object.is_canceled():
            return ['ui/canceled_payment_new.html']
        elif config.is_paused:
            return ['ui/paused_new.html']
        return ['ui/payment_request_customer_new.html']

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.concur_customer_view(request)
        return super(PaymentRequestCustomerView, self).get(request, *args, **kwargs)

    def form_invalid(self, form):
        data = {'errors': form.errors_as_dict()}
        return JsonResponse(data, status=status.HTTP_400_BAD_REQUEST)

    def form_valid(self, form):
        _status = status.HTTP_400_BAD_REQUEST
        try:
            # url can be None also here and in Exception
            url = form.get_url()
        except Exception as e:
            logger.exception("Following error occurred while "
                             "trying to generate a payment link %s" % e)
            url = None

        if url is not None:
            _status = status.HTTP_200_OK
            data = {
                'message': _ft(_("You are being redirected to payment page...")),
                'redirect_url': url
            }
        else:
            data = {
                'errors': {
                    '__all__': _ft(_("There was an error while connecting to payment server. "
                                     "Please try again later."))
                }
            }
        return JsonResponse(data, status=_status)

    def get_form_kwargs(self):
        kwargs = super(PaymentRequestCustomerView, self).get_form_kwargs()
        kwargs.update({'config': PaymentRequestFormConfig.get_solo()})
        return kwargs

    def get_selected_gateway_payment_info(self, transaction_amount,
                                        transaction_currency_code):
        data = {'gateway_code': self.object.gateway_code}
        gateway = GatewaySettings.objects.get_for_code(self.object.gateway_code)
        gateway_fee = gateway.currency_config.compute_fee(
            amount=transaction_amount,
            from_code=transaction_currency_code)
        total = gateway.currency_config.compute_total(amount=transaction_amount,
                                                      from_code=transaction_currency_code)
        data['fee'] = gateway_fee
        data['total'] = total
        data['currency_code'] = gateway.currency_config.default_currency.code
        if gateway.gateway.logo:
            data['url'] = '{}{}'.format(get_knpay_url(), gateway.gateway.logo.url)
        return data

    def get_all_gateway_payment_info(self, transaction_amount,
                                          transaction_currency_code):
        gateway_choices = GatewaySettings.objects.get_for_user(
            self.object.initiator)
        choices = []
        for gateway in gateway_choices:
            if gateway.is_active and self.object.is_valid_for_payment(
                    gateway_code=gateway.code):
                if self.object._currency_exchange_is_available(
                        gateway_code=gateway.code):
                    total = gateway.currency_config.compute_total(
                        amount=transaction_amount,
                        from_code=transaction_currency_code)
                    currency_code = gateway.currency_config.default_currency.code
                    choice = dict(total=str(total),
                                  code=gateway.code,
                                  value=gateway.name,
                                  currency_code=currency_code)
                    if gateway.gateway.logo:
                        choice['url'] = '{}{}'.format(get_knpay_url(),
                                                      gateway.gateway.logo.url)
                    if gateway.has_fee():
                        gateway_fee = gateway.currency_config.compute_fee(
                            amount=transaction_amount,
                            from_code=transaction_currency_code)
                        choice['fee'] = gateway_fee
                    choices.append(choice)

        data = {'gateway_choices': choices}
        return data

    def get_context_data(self, **kwargs):
        context = super(PaymentRequestCustomerView, self).get_context_data(**kwargs)
        config = PaymentRequestFormConfig.get_solo()
        transaction_amount = self.object.get_amount()
        transaction_currency_code = self.object.currency_code
        context['amount'] = self.object.amount

        # If initiator has selected gateway code while generating
        # payment-request then provide that option only
        # else give all active gateway choices to customer
        if self.object.gateway_code:
            context.update(self.get_selected_gateway_payment_info(transaction_amount, transaction_currency_code))
        else:
            context.update(self.get_all_gateway_payment_info(transaction_amount, transaction_currency_code))

        # Building customer data
        customer_data = OrderedDict()
        extra = self.object.extra
        remarks = extra.pop('remarks', None)
        order_id = None

        # Complex logic for showing FUll Name
        # checks built in fields and custom fields
        full_name = extra.pop('full_name', None)
        first_name = getattr(self.object, 'customer_first_name')
        last_name = getattr(self.object, 'customer_last_name')
        customer_full_name = ' '.join([first_name, last_name])
        if customer_full_name.strip():
            customer_data[_('Customer Name')] = customer_full_name
        elif full_name:
            customer_data[_('Customer Name')] = full_name

        # Adding customer builtin fields in the customer details
        for field in PaymentTransaction.CUSTOMER_FIELDS:
            if field in ['customer_first_name', 'customer_last_name']:
                continue
            val = getattr(self.object, field)
            if val:
                customer_data[PaymentTransaction._meta.get_field(field).verbose_name] = val

        # Adding extra data in customer details
        for key, value in extra.items():
            if key == 'order_id':
                order_id = value
                continue
            if not value:
                continue
            field = Field.objects.get_for_name(key, config)
            if field is not None and field.display_to_customer():
                label = field.get_label() if field is not None else key
                customer_data[_(label)] = value
            
        # Remove customer_email if it is set to use default email
        if 'has_default_email' in customer_data:
            customer_data.pop(PaymentTransaction._meta.get_field(
                'customer_email').verbose_name, None)
            customer_data.pop('has_default_email', None)

        # Already providing downloadable attachment link
        customer_data.pop('Attachment', None)
        context['customer_details'] = customer_data

        if self.object.attachment:
            context['attachment'] = self.object.attachment

        context['reference_date'] = self.object.created
        context['remarks'] = remarks

        # will show order_id as a reference number
        context['order_id'] = order_id
        return context


@method_decorator(transaction.atomic, name='form_valid')
class CustomerPaymentView(PageTitleMixin, generic.FormView):
    """
    Page where customer can pay invoice by himself
    """
    form_class = CustomerPaymentForm
    page_title = _("Pay")

    def get_template_names(self):
        config = Config.get_solo()
        if config.is_paused:
            return ['ui/paused_new.html']
        return ['ui/customer_payment.html']

    def get_initial(self):
        initial = super(CustomerPaymentView, self).get_initial()
        initial.update({'language': get_language()})
        return initial

    def get_form_kwargs(self):
        kwargs = super(CustomerPaymentView, self).get_form_kwargs()
        kwargs.update({
            'request': self.request,
            'config': CustomerPaymentFormConfig.get_solo(),
        })
        return kwargs

    def form_invalid(self, form):
        data = {'errors': form.errors_as_dict()}
        return JsonResponse(data, status=status.HTTP_400_BAD_REQUEST)

    def form_valid(self, form):
        self.object = form.save()
        return JsonResponse({'redirect_url': self.object.get_payment_url()})


class PaymentTransactionFeeView(generic.DetailView):
    """
    Used to calculate fee and total of a given transaction and gateway setting code.
    """
    model = PaymentTransaction
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        # ToDO: use encryption_key mixin here
        gateway_code = kwargs.get('gateway')
        gateway = GatewaySettings.objects.get_for_code(gateway_code)
        error_payload = dict(message=_ft(_('There was an error for selected gateway. '
                                           'Please try another one.')))

        if not request.is_ajax() or gateway is None:
            logger.exception('Gateway does not exist for code %s' % gateway_code)
            return JsonResponse(error_payload, status=400)

        self.object = self.get_object()
        transaction_amount = self.object.get_amount()
        transaction_currency_code = self.object.currency_code
        try:
            if self.object._currency_exchange_is_available(gateway_code=gateway_code):
                if gateway.has_fee():
                    gateway_fee = gateway.currency_config.compute_fee(amount=transaction_amount,
                                                                      from_code=transaction_currency_code)

                    total = gateway.currency_config.compute_total(amount=transaction_amount,
                                                                  from_code=transaction_currency_code)
                    currency_code = gateway.currency_config.default_currency.code
                    total_label = _ft(_("Total in %s")) % currency_code
                    fee_label = _ft(_("Fee in %s")) % currency_code
                    return JsonResponse(dict(fee=str(gateway_fee), total=str(total),
                                             total_label=total_label, fee_label=fee_label))
                else:
                    return JsonResponse(dict())
            else:
                return JsonResponse(error_payload, status=400)
        except Exception as e:
            logger.exception("Error occurred while adding trying to calculate the currency exchange %s" % e)
            return JsonResponse(error_payload, status=400)
