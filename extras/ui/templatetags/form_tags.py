# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django import template

from gateway.models import GatewaySettings

register = template.Library()


@register.assignment_tag
def get_gateway_logo(gateway_code):
    settings = GatewaySettings.objects.get_for_code(gateway_code)
    if settings:
        try:
            return settings.gateway.logo.url
        except:
            return None
    return None