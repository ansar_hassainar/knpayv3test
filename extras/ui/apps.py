# -*- coding: utf-8 -*-
from django.apps import AppConfig


class FormsConfig(AppConfig):
    name = 'forms'
