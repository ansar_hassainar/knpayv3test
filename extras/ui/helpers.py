# -*- coding: utf-8 -*-

from __future__ import unicode_literals


from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field


def gateway_helper_factory(form, **kwargs):
    helper = FormHelper()
    helper.layout = Layout()
    for key, value in kwargs.items():
        setattr(helper, key, value)
    for index, field in enumerate(form.fields):
        if field == 'gateway_code':
            field = Field('gateway_code', template='dashboard/fields/gateway_radio.html')
        helper.layout.insert(index, field)
    return helper
