# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

from django import forms
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _

from intl_tel_input.widgets import IntlTelInputWidget

from config.models import Config
from gateway.models import PaymentTransaction, GatewaySettings
from utils.forms import (AjaxBaseForm, InstancePaymentTransactionForm,
                         AbstractBasePaymentRequestForm, AttemptURLMixin)
from extras.dashboard.models import CustomerPaymentFormConfig
from .helpers import gateway_helper_factory

logger = logging.getLogger('ui')


class TermsAndConditionsForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(TermsAndConditionsForm, self).__init__(*args, **kwargs)
        config = Config.get_solo()
        if config.terms:
            label = format_html(
                '%s<a data-toggle="modal" data-target="#Modalterms"> %s</a>'
                % (_("I accept"), _("Terms & Conditions")))
            self.fields['terms'] = forms.BooleanField(required=True, initial=False,
                                                      label=label)


class PaymentRequestCustomerForm(forms.ModelForm, AjaxBaseForm, AttemptURLMixin):
    """
    Process payment created from dashboard
    """
    class Meta:
        model = PaymentTransaction
        fields = (
            'amount',
            'currency_code',
            'order_no',
            'customer_first_name',
            'customer_last_name',
            'customer_email',
            'customer_phone',
            'customer_address_line1',
            'customer_address_line2',
            'customer_address_city',
            'customer_address_state',
            'customer_address_country',
            'customer_address_postal_code',
        )

    def __init__(self, config, *args, **kwargs):
        self.config = config
        super(PaymentRequestCustomerForm, self).__init__(*args, **kwargs)
        # remove fields without value
        for field in self.fields:
            if self[field].value() in ['', None]:
                del self.fields[field]
        # check if there is a gateway specified
        # if not, let the user choose the payment method
        if not self.instance.has_gateway():
            self.fields['gateway_code'] = forms.ChoiceField(
                choices=GatewaySettings.objects.get_for_section(
                    PaymentTransaction.PAYMENT_REQUEST).get_for_user(self.instance.initiator).choices(),
                widget=forms.RadioSelect)
            self.fields['gateway_code'].label = _("Payment Method")
            self.fields['gateway_code'].help_text = _(
                "Choose the payment method")

        self.helper = gateway_helper_factory(self)

    def clean(self):
        if not self.instance.is_valid_for_payment():
            raise forms.ValidationError(
                _("This transaction cannot be paid anymore."))


class CustomerPaymentForm(TermsAndConditionsForm,
                          AbstractBasePaymentRequestForm):
    """
    Allow to pay user an invoice by himself
    """
    # not a field!
    transaction_type = PaymentTransaction.CUSTOMER_PAYMENT
    blank_gateway = False
    check_permission = False
    knpay_section = PaymentTransaction.CUSTOMER_PAYMENT

    field_order = ('amount', 'customer_email',
                   'currency_code', 'language', 'gateway_code')

    def __init__(self, *args, **kwargs):
        super(CustomerPaymentForm, self).__init__(*args, **kwargs)
        self.fields['language'].widget = forms.HiddenInput()

        for field in self.config.fields.active():
            self.add_defined_field(field.get_name(), label=field.get_label(), required=field.required,
                                   field=field)

        if 'customer_phone' in self.fields:
            self.fields['customer_phone'].widget = IntlTelInputWidget(default_code='kw',
                                                                      preferred_countries=['kw', 'ro', 'in'])

        # Incase customer don't have email then we can set default_email
        self.fields['customer_email'].required = False
        self.helper = gateway_helper_factory(self)
        self.helper.include_media = False

    def get_data_for_api(self):
        data = super(CustomerPaymentForm, self).get_data_for_api()
        customer_form_config = CustomerPaymentFormConfig.get_solo()
        # if customer_email is populated from the default_email then set flag
        # in extra data
        if data['customer_email'] == customer_form_config.default_email:
            # Fail safe in case there is no extra data
            if data.get('extra', None):
                data['extra'].update({'has_default_email': 'true'})
            else:
                data['extra'] = {'has_default_email': 'true'}

        return data

    def clean(self):
        if not self.cleaned_data['customer_email']:
            customer_form_config = CustomerPaymentFormConfig.get_solo()
            self.cleaned_data['customer_email'] = customer_form_config.default_email
        return super(CustomerPaymentForm, self).clean()

    def save(self):
        return self.serializer.save()
