# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.core.management.base import BaseCommand

from utils.funcs import update_email_context


class Command(BaseCommand):
    help = "update email context of existing MailTemplates"

    def handle(self, **options):
        update_email_context()
        print('Email context updated')
