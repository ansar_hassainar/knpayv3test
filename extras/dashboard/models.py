# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import When, Case
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from dbmail.models import MailTemplate
from django_extensions.db.fields import AutoSlugField
from modeltranslation.manager import MultilingualManager, MultilingualQuerySet
from solo.models import SingletonModel

from utils.db.models import (StatusModel, EmailMailTemplate, MerchantURLConfig,
                             SMSMailTemplate, NotificationMailTemplate)

logger = logging.getLogger('dashboard')

CUSTOMER_PHONE = 'customer_phone'
FIELD_CHOICES = (
    ('order_no', _("Order no")),
    ('customer_first_name', _("First Name")),
    ('customer_last_name', _("Last Name")),
    (CUSTOMER_PHONE, _("Phone")),
    ('customer_address_line1', _("Address line 1")),
    ('customer_address_line2', _("Address line 2")),
    ('customer_address_city', _("City")),
    ('customer_address_state', _("State")),
    ('customer_address_country', _('Country')),
    ('customer_address_postal_code', _("Postal Code")),
    ('bulk_id', _("Bulk ID")),
    ('email_recipients', _('Email Recipients')),
)


class BaseFormConfig(SingletonModel):
    related_query_name = None

    currency = models.ForeignKey(
        'currency.Currency', verbose_name=_("Default currency"),
        null=True, blank=True,
        help_text=_("If completed, this currency will be used "
                    "for all initialized payments and the currency field "
                    "will not be mandatory anymore, nor appear in the "
                    "transaction form.")
    )

    sms_payment_details = models.BooleanField(
        _("SMS payment details"), default=False,
        help_text=_("If checked, customer will get the payment confirmation "
                    "via SMS. To work properly `phone` field has to be present "
                    "on the form"))

    class Meta:
        abstract = True

    def extra_fields(self):
        """
        :return: extra fields to be used in the transaction listing
        """
        return self.fields.filter(listing_display=True)


class PaymentRequestFormMixin(models.Model):
    bcc_initiator = models.BooleanField(
        _("BCC initiator"),
        default=True,
        help_text=_("If checked, the initiator of the transaction will "
                    "get BCCed on the payment confirmation email.")
    )

    class Meta:
        abstract = True


class PaymentRequestFormConfig(EmailMailTemplate,
                               SMSMailTemplate,
                               NotificationMailTemplate,
                               PaymentRequestFormMixin,
                               MerchantURLConfig,
                               BaseFormConfig):
    related_query_name = 'config_payment_request'
    fields = GenericRelation('dashboard.Field',
                             related_query_name=related_query_name)

    default_email = models.EmailField(_("default email"), max_length=120)

    # Social media whatsapp share only for payment-request at the moment
    whatsapp_notification_template = models.ForeignKey(
        'dbmail.MailTemplate', blank=True, null=True, related_name='+')

    def __unicode__(self):
        return 'payment request form config'

    class Meta:
        verbose_name = _("Payment request form configuration")
        app_label = 'dashboard'


class CustomerPaymentFormConfig(EmailMailTemplate, SMSMailTemplate, BaseFormConfig):
    related_query_name = 'config_customer_payment'
    fields = GenericRelation('dashboard.Field',
                             related_query_name=related_query_name)

    default_email = models.EmailField(_("default email"), max_length=120)

    def __unicode__(self):
        return 'customer payment form config'

    class Meta:
        verbose_name = _("Customer payment form configuration")
        app_label = 'dashboard'


class FieldQuerySet(MultilingualQuerySet):
    def active(self):
        return self.filter(is_active=True)

    def required(self):
        return self.filter(required=True)

    def custom(self):
        return self.filter(type=self.model.CUSTOM)

    def builtin(self):
        return self.filter(type=self.model.BUILTIN)

    def itinerary(self):
        return self.filter(itinerary_display=True)

    def names_list(self):
        return list(self.annotate(
            field_name=Case(
                When(name__regex='[0-9a-zA-Z]', then='name'),
                When(field__regex='[0-9a-zA-Z]', then='field')
            )
        ).values_list('field_name', flat=True))


class FieldManager(MultilingualManager):
    def get_queryset(self):
        return FieldQuerySet(self.model, using=self._db)

    def active(self):
        return self.get_queryset().active()

    def required(self):
        return self.get_queryset().required()

    def custom(self):
        return self.get_queryset().custom()

    def builtin(self):
        return self.get_queryset().builtin()

    def itinerary(self):
        return self.get_queryset().itinerary()

    def names_list(self):
        return self.get_queryset().names_list()

    def get_for_name(self, field_name, config):
        filter_params = {config.related_query_name: config}
        try:
            return self.active().filter(**filter_params).filter(models.Q(field=field_name) |
                                                                models.Q(name=field_name)).get()
        except Field.DoesNotExist:
            return None


class Field(StatusModel):
    FIELD_CHOICES += (('attachment', _('Payment Request Attachment')),)
    BUILTIN, CUSTOM = 'builtin', 'custom'
    TYPE_CHOICES = (
        (BUILTIN, _("Builtin")),
        (CUSTOM, _("Custom"))
    )
    type = models.CharField(_("Type"), choices=TYPE_CHOICES, default=BUILTIN,
                            max_length=10)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    config = GenericForeignKey('content_type', 'object_id')
    required = models.BooleanField(
        _("Required?"), default=True,
        help_text=_("Designates if this field is required")
    )
    field = models.CharField(_("Field"), blank=True, max_length=32,
                             choices=FIELD_CHOICES, default='',
                             help_text=_("Builtin fields. Do no create same fields "
                                         "as custom fields."))
    label = models.CharField(
        _("Label"), max_length=32,
        blank=True, default='',
        help_text=_("Add a custom label if any from the builtin fields "
                    "does not satisfy the needs.")
    )
    name = AutoSlugField(
        _("Name"), populate_from='label', editable=True,
        separator='_', default='', allow_duplicates=True,
        help_text=_("HTML field name. Used only for backend validation. "
                    "Will not be visible anywhere."))

    listing_display = models.BooleanField(
        _("Listing display"), default=False,
        help_text=_("Designates if this field shall appear in transaction "
                    "table list."))

    itinerary_display = models.BooleanField(
        _("Itinerary Display"), default=False,
        help_text=_("Designate if this field shall appear in the itinerary "
                    "table list at the invoice pdf.")
    )

    customer_display = models.BooleanField(
        _("Display to customer"), default=False,
        help_text=_("Designates if the field will be displayed to customer.")) 

    objects = FieldManager()

    def __unicode__(self):
        return self.get_field_display() or self.label

    def get_name(self):
        return self.field or self.name

    def get_label(self):
        return self.get_field_display() or self.label

    def display_to_customer(self):
        return self.customer_display

    def clean(self):
        # raise ValidationError('sss')
        if all([self.field, any([self.name, self.label])]):
            raise ValidationError(_("Either select a field from dropdown or "
                                    "add your custom one."))
        # if no label selected
        if (self.label and not self.name) or (self.name and not self.label):
            raise ValidationError(_("Fill both label and name"))

        # if nothing selected
        if not (bool(self.field) or (self.name or self.label)):
            raise ValidationError(_("Either select a field from dropdown or "
                                    "add your custom one."))

    def is_builtin(self):
        return self.type == self.BUILTIN

    class Meta:
        verbose_name = _("Field")
        verbose_name_plural = _("Fields")
        app_label = 'dashboard'

    # def validate_unique(self, exclude=None):
    #     if self.name:
    #         filter_params = {self.config.related_query_name: self.config, 'name': self.name}
    #         if Field.objects.exclude(models.Q(pk=self.pk) | models.Q(field__regex='[0-9a-zA-Z]')).filter(**filter_params).exists():
    #             raise ValidationError(
    #                 _("%s Custom field name already exists!") % self.name)


@receiver(post_save, sender=Field)
@receiver(post_save, sender=MailTemplate)
@receiver(post_delete, sender=Field)
def update_email_context(sender, instance, **kwargs):
    from utils.funcs import update_email_context
    update_email_context()
