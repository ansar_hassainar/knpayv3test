# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf.urls import url, include

from . import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^auth-settings/$', views.AuthSettingsView.as_view(),
        name='auth_settings'),
    url(r'^backup-tokens/$', views.BackUpTokensView.as_view(),
        name='backup_tokens'),
    url(r'^create-payment-request/$', views.CreatePaymentRequestView.as_view(),
        name='create_payment_request'),
    url(r'^help/$', views.HelpSupportView.as_view(),
        name='help'),
    url(r'^load-currencies/', views.LoadCurrency.as_view(), name='load_currency'),
    url(r'^payment-request/(?P<pk>\d+)/$', views.PaymentRequestDetailView.as_view(),
        name='payment_request_detail'),
    url(r'^payment-invoice/(?P<encrypted_pk>.+)/$$', views.PaymentInvoiceTemplateView.as_view(),
        name='payment_invoice'),
    url(r'^transactions/(?P<month>[0-9]{2})/(?P<day>[0-9]{2})/(?P<year>[0-9]{4})/$',
        views.AllPaymentListView.as_view(), name='all_payment_list'),
    url(r'^transactions/payment-requests/$',
        views.PaymentRequestListView.as_view(), name='payment_requests'),
    url(r'^transactions/customer-payments/$',
        views.CustomerPaymentListView.as_view(), name='customer_payments'),
    url(r'^transactions/deleted-transactions/$',
        views.DeletedTransactionsListView.as_view(), name='deleted_transactions'),
    url(r'^transactions/catalogue/$',
        views.CataloguePaymentListView.as_view(), name='catalogue_payments'),
    url(r'^transactions/e-commerce/$',
        views.ECommerceListView.as_view(), name='e_commerce_list'),
    url(r'^transactions/shopify/$',
        views.ThirdPartyListView.as_view(), name='third_party_list'),
    url(r'^actions/$', views.ActionListView.as_view(),
        name='actions'),
    url(r'^transactions/actions/$', views.TransactionActionView.as_view(),
        name='transaction_actions'),
    # url(r'^transactions/export/(?P<file_format>\w+)/$', views.TransactionListView.as_view(), name='export'),
    url(r'^transactions/cancel/(?P<pk>\d+)/$', views.CancelTransactionView.as_view(),
        name='cancel_transaction'),
    url(r'^send-communication-event/(?P<transaction_pk>\d+)/(?P<event_type>\w+)$',
        views.send_communication_event, name='send_communication_event'),
    url(r'^transactions-graph/$',
        views.PaymentTransactionGraphView.as_view(), name='transaction_graph'),
    url(r'^push-transactions/$',
        views.PushPaymentTransactionViewSet.as_view(), name='push_transactions'),
    url(r'^bulk/', include('extras.dashboard.bulk.urls', namespace='bulk')),

]
