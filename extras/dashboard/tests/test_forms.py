# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import json
import unittest
from decimal import Decimal
from django import forms
from django.conf import settings
from django.test import TestCase, RequestFactory
from django.contrib.auth.models import User, Permission
import pytest
from mixer.backend.django import mixer
from mock import patch
from gateway.models import PaymentTransaction

from utils.tests import knet_settings_factory

from extras.dashboard.models import CUSTOMER_PHONE
from sms.models import SMSAuthConfig

pytestmark = pytest.mark.django_db


class TestCreatePaymentRequestForm(TestCase):
    def setUp(self):
        from ..forms import CreatePaymentRequestForm
        self.form_class = CreatePaymentRequestForm
        self.form_config = mixer.blend('dashboard.PaymentRequestFormConfig')
        self.kwd_currency = mixer.blend('currency.Currency', code='KWD')
        self.exchange_config = mixer.blend('currency.ExchangeConfig', 
            default_currency=self.kwd_currency, fee_type='', name='default',currencies=self.kwd_currency )

    def test_amount(self):
        request = RequestFactory().get('/')
        user = mixer.blend('auth.User', is_superuser=True)
        request.user = user
        form = self.form_class(config=self.form_config, request=request)
        assert form.fields['amount'].required is True, 'Amount is mandatory'

    def test_invalid_amount(self):
        request = RequestFactory().get('/')
        user = mixer.blend('auth.User', is_superuser=True)
        request.user = user
        form = self.form_class(config=self.form_config, data={'amount': 'foo'}, request=request)
        assert form.is_valid() is False
        assert 'amount' in form.errors

    def test_gateway_code_with_one_gateway(self):
        """
        rules for gateway:
        1. If only one gateway is available, select it by default and hide it from the form
        2. If multiple gateway available, list all of them
        """

        # test with one gateway settings available
        knpay_section = mixer.blend('config.KNPaySection',
                                    name=PaymentTransaction.PAYMENT_REQUEST)
        gw_one = mixer.blend('knet.KnetSettings', code='knet-demo', is_active=True, currency_config=self.exchange_config)
        gw_one.sections.add(knpay_section)
        request = RequestFactory().get('/')
        user = mixer.blend('auth.User', is_superuser=True)
        request.user = user
        form = self.form_class(config=self.form_config, request=request)
        field = form.fields['gateway_code']
        assert len(field.choices) == 2, 'Should be 2 because of one existing gw + empty choice'
        assert field.required is False, 'Should be optional'
        assert field.initial == gw_one.code
        assert isinstance(field.widget, forms.HiddenInput)

    def test_gateway_code_with_multiple_gateways(self):
        """
        rules for gateway:
        1. If only one gateway is available, select it by default and hide it from the form
        2. If multiple gateway available, list all of them
        """

        # test with multiple gateways
        knpay_section = mixer.blend('config.KNPaySection',
                                    name=PaymentTransaction.PAYMENT_REQUEST)
        gw_one = mixer.blend('knet.KnetSettings', code='knet-demo', is_active=True, currency_config=self.exchange_config)
        gw_one.sections.add(knpay_section)
        gw_two = mixer.blend(
            'knet.KnetSettings', code='knet-class', is_active=True, gateway=gw_one.gateway, currency_config=self.exchange_config)
        gw_two.sections.add(knpay_section)
        request = RequestFactory().get('/')
        user = mixer.blend('auth.User', is_superuser=True)
        request.user = user
        form = self.form_class(config=self.form_config, request=request)
        field = form.fields['gateway_code']
        # pytest.set_trace()
        assert len(field.choices) == 3, 'Should be 3 because of 2 gateways + empty choice'
        assert field.required is False, 'Should be optional'
        assert isinstance(field.widget, forms.Select), "Select type, so the user can choose the gw"
        assert field.initial is None, "No initial value"

    def test_currency_code_with_one_entry(self):
        """
        Rules for currencies:
        -------------------
        1. currency will be changable upon the gateway selection by front-end code
        2. if gateway has more than one currency then we will display the list of the currencies
        3. if the gateway has one currency also it should be the default cur then it will
            be selected with hidden dropdown.
        4. if no selected gatway then currency will be selected with the form config currency
        5. if only one currency is available, select it by default and hide it from the form
        6. if multiple currencies are available, list all of them
        7. should be mandatory
        """


        # test one currency in DB
        # currency_one = mixer.blend('currency.Currency')
        request = RequestFactory().get('/')
        user = mixer.blend('auth.User', is_superuser=True)
        request.user = user
        form = self.form_class(config=self.form_config, request=request)
        field = form.fields['currency_code']
        assert len(field.choices) == 1
        assert field.required is True
        assert field.initial == None
        assert isinstance(field.widget, forms.Select)

    def test_currency_code_with_multiple_entries(self):
        """
        Rules for currencies:
        -------------------
        1. currency will be changable upon the gateway selection by front-end code
        2. if gateway has more than one currency then we will display the list of the currencies
        3. if the gateway has one currency also it should be the default cur then it will
            be selected with hidden dropdown.
        4. if no selected gatway then currency will be selected with the form config currency
        5. if only one currency is available, select it by default and hide it from the form
        6. if multiple currencies are available, list all of them
        7. should be mandatory
        """

        # test multiple currencies in DB
        mixer.blend('currency.Currency', code='USD')
        mixer.blend('currency.Currency', code='RON')
        request = RequestFactory().get('/')
        user = mixer.blend('auth.User', is_superuser=True)
        request.user = user
        form = self.form_class(config=self.form_config, request=request)
        field = form.fields['currency_code']

        assert len(field.choices) == 3
        assert field.required is True
        assert field.initial is None
        assert isinstance(field.widget, forms.Select)

    def test_currency_code_with_admin_override(self):
        """
        Rules for currencies:
        -------------------
        1. currency will be changable upon the gateway selection by front-end code
        2. if gateway has more than one currency then we will display the list of the currencies
        3. if the gateway has one currency also it should be the default cur then it will
            be selected with hidden dropdown.
        4. if no selected gatway then currency will be selected with the form config currency
        5. if only one currency is available, select it by default and hide it from the form
        6. if multiple currencies are available, list all of them
        7. should be mandatory
        """

        # mixer.blend('knet.KnetSettings', code='knet-demo', is_active=True)
        # test one currency in DB
        # mixer.blend('currency.Currency')
        # currency_one = mixer.blend('currency.Currency', code='EUR')

        # test admin override
        self.form_config.currency = self.kwd_currency
        self.form_config.save()
        request = RequestFactory().get('/')
        user = mixer.blend('auth.User', is_superuser=True)
        request.user = user
        form = self.form_class(config=self.form_config, request=request)
        field = form.fields['currency_code']
        assert field.required is True
        assert isinstance(field.widget, forms.Select)
        assert field.initial == self.kwd_currency.code

    def test_language(self):
        request = RequestFactory().get('/')
        user = mixer.blend('auth.User', is_superuser=True)
        request.user = user
        form = self.form_class(config=self.form_config, request=request)
        field = form.fields['language']
        assert field.required is True
        assert len(field.choices) == len(settings.LANGUAGES)

    def test_customer_email(self):
        request = RequestFactory().get('/')
        user = mixer.blend('auth.User', is_superuser=True)
        request.user = user
        form = self.form_class(config=self.form_config, request=request)
        field = form.fields['customer_email']
        assert field.required is True

    def test_sms_payment_details(self):
        request = RequestFactory().get('/')
        user = mixer.blend('auth.User')
        request.user = user
        self.form_config.sms_payment_details = False
        self.form_config.save()
        form = self.form_class(config=self.form_config, request=request)
        field = form.fields['sms_notification']
        assert field.required is False
        assert field.initial is None
        if not SMSAuthConfig.objects.current():
            assert isinstance(field.widget, forms.HiddenInput)
        else:
            assert isinstance(field.widget, forms.CheckboxInput)

    def test_add_defined_field_builtin(self):
        # test with builtin field
        form_field = mixer.blend(
            'dashboard.Field', field='customer_phone', is_active=True)
        request = RequestFactory().get('/')
        user = mixer.blend('auth.User', is_superuser=True)
        request.user = user
        form = self.form_class(config=self.form_config, request=request)
        form.add_defined_field(form_field.get_name(), label=form_field.get_label(), required=form_field.required)
        field = form.fields[form_field.get_name()]
        assert field.label == form_field.get_label()
        assert field.required == form_field.required

    def test_add_defined_field_custom(self):
        form_field = mixer.blend(
            'dashboard.Field', name='custom', label='custom', is_active=True)
        request = RequestFactory().get('/')
        user = mixer.blend('auth.User', is_superuser=True)
        request.user = user
        form = self.form_class(config=self.form_config, request=request)
        form.add_defined_field(form_field.get_name(), label=form_field.get_label(), required=form_field.required)
        field = form.fields[form_field.get_name()]
        assert field.label == form_field.get_label()
        assert field.required == form_field.required

    @patch('extras.dashboard.forms.CreatePaymentRequestForm.add_defined_field')
    def test_init(self, mock_one):
        request = RequestFactory().get('/')
        user = mixer.blend('auth.User', is_superuser=True)
        request.user = user

        # if no dynamic fields added, add_defined_field method shall not be called
        self.form_class(config=self.form_config, request=request)

        assert mock_one.call_count == 0

        # check if dynamic fields are being added
        mixer.blend('dashboard.Field', is_active=True, config=self.form_config)
        self.form_class(config=self.form_config, request=request)

        assert mock_one.call_count == 1

        mixer.blend('dashboard.Field', is_active=True, config=self.form_config)
        self.form_class(config=self.form_config, request=request)
        assert mock_one.call_count == 3, '1 from previous form + 2 from new form'

    def test_get_api_data_with_default_fields(self):

        gateway_settings =knet_settings_factory(self.kwd_currency)
        amount = '23'
        customer_email = 'foo@example.com'

        data = {'amount': amount,
                'currency_code': gateway_settings.currency_config.default_currency.code,
                'language': settings.LANGUAGE_CODE,
                'customer_email': customer_email
                }
        expected = {
            'amount': Decimal(amount),
            'gateway_code': gateway_settings.code,
            'currency_code': gateway_settings.currency_config.default_currency.code,
            'language': settings.LANGUAGE_CODE,
            'customer_email': customer_email,
            'type': PaymentTransaction.PAYMENT_REQUEST,
            'sms_notification': True,
            'sms_payment_details': True,
            'generate_qr_code': False,
            'default_email': False
        }

        request = RequestFactory().get('/')
        user = mixer.blend('auth.User', is_superuser=True)
        request.user = user
        form = self.form_class(config=self.form_config, data=data, request=request)
        assert form.is_valid() is True
        assert set(expected.keys()) == set(form.get_data_for_api().keys())

    def test_get_api_data_with_sms_and_builtin_fields(self):
        request = RequestFactory().get('/')
        user = mixer.blend('auth.User', is_superuser=True)
        request.user = user
        gateway_settings = knet_settings_factory(self.kwd_currency)
        amount = '23'
        customer_email = 'foo@example.com'
        mixer.blend('dashboard.Field', field=CUSTOMER_PHONE, is_active=True, config=self.form_config)

        data = {'amount': amount,
                'currency_code': gateway_settings.currency_config.default_currency.code,
                'language': settings.LANGUAGE_CODE,
                'customer_email': customer_email,
                'sms_notification': True,
                'sms_payment_details': True,
                'customer_phone': '+40749622278'
                }
        expected = {
            'amount': Decimal(amount),
            'gateway_code': gateway_settings.code,
            'currency_code': gateway_settings.currency_config.default_currency.code,
            'language': settings.LANGUAGE_CODE,
            'customer_email': customer_email,
            'type': PaymentTransaction.PAYMENT_REQUEST,
            'sms_notification': True,
            'sms_payment_details': True,
            'customer_phone': '123456789',
            'generate_qr_code': False,
            'default_email': False
        }
        form = self.form_class(config=self.form_config, request=request, data=data)
        # pytest.set_trace()
        form.is_valid()
        assert form.is_valid() is True
        assert set(expected.keys()) == set(form.get_data_for_api().keys())

        obj = form.save()
        if not SMSAuthConfig.objects.current():
            assert obj.sms_payment_details is False
        else:
            assert obj.sms_payment_details is True

        assert obj.sms_notification is True

    def test_get_api_data_with_custom_fields(self):
        gateway_settings = knet_settings_factory(self.kwd_currency)
        amount = '23'
        customer_email = 'foo@example.com'
        mixer.blend('dashboard.Field', name='name', label='name', is_active=True, config=self.form_config)

        data = {'amount': amount,
                'currency_code': gateway_settings.currency_config.default_currency.code,
                'language': settings.LANGUAGE_CODE,
                'customer_email': customer_email,
                'name': 'FOO'
                }
        expected = {
            'amount': Decimal(amount),
            'gateway_code': gateway_settings.code,
            'currency_code': gateway_settings.currency_config.default_currency.code,
            'language': settings.LANGUAGE_CODE,
            'customer_email': customer_email,
            'type': PaymentTransaction.PAYMENT_REQUEST,
            'extra': {'name': 'FOO'},
            'sms_notification': True,
            'sms_payment_details': True,
            'generate_qr_code': False,
            'default_email': False
        }
        request = RequestFactory().get('/')
        user = mixer.blend('auth.User', is_superuser=True)
        request.user = user
        form = self.form_class(config=self.form_config, data=data, request=request)
        assert form.is_valid() is True
        assert set(expected.keys()) == set(form.get_data_for_api().keys())

    def test_get_api_data_for_merchant_notification(self):

        request = RequestFactory().get('/')
        user = mixer.blend('auth.User', is_superuser=True)
        request.user = user
        gateway_settings = knet_settings_factory(self.kwd_currency)
        amount = '23'
        customer_email = 'foo@example.com'
        self.form_config.disclosure_url = 'http://example.com'
        self.form_config.redirect_url = 'http://example.com'
        self.form_config.disclose_to_merchant_url = True
        self.form_config.save()
        data = {'amount': amount,
                'currency_code': gateway_settings.currency_config.default_currency.code,
                'language': settings.LANGUAGE_CODE,
                'customer_email': customer_email,
                }
        expected = {
            'amount': Decimal(amount),
            'gateway_code': gateway_settings.code,
            'currency_code': gateway_settings.currency_config.default_currency.code,
            'language': settings.LANGUAGE_CODE,
            'customer_email': customer_email,
            'type': PaymentTransaction.PAYMENT_REQUEST,
            'disclosure_url': 'http://example.com',
            'redirect_url': 'http://example.com',
            'sms_payment_details': False,
            'sms_notification': False,
            'generate_qr_code': False,
            'default_email': False
        }
        form = self.form_class(config=self.form_config, request=request, data=data)
        assert form.is_valid() is True
        assert set(expected.keys()) == set(form.get_data_for_api().keys())
        obj = form.save()
        assert obj.disclosure_url != ''

    def test_manage_gateway(self):
        request = RequestFactory().get('/')
        user = mixer.blend('auth.User', is_superuser=True)
        request.user = user
        gateway_settings = knet_settings_factory(self.kwd_currency)
        amount = '23'
        customer_email = 'foo@example.com'
        self.form_config.disclose_to_merchant_url = True
        self.form_config.disclosure_url = 'http://example.com'
        self.form_config.save()
        data = {'amount': amount,
                'currency_code': gateway_settings.currency_config.default_currency.code,
                'language': settings.LANGUAGE_CODE,
                'customer_email': customer_email,
                }
        form = self.form_class(config=self.form_config, request=request, data=data)
        assert form.is_valid() is True
        # test method
        form.manage_gateway(blank=False)
        form.manage_gateway(blank=True)

    @unittest.skip('WIP')
    def test_save(self):
        pass


class TestTransactionForm(TestCase):
    def setUp(self):
        # Create required configs
        self.form_config = mixer.blend('dashboard.PaymentRequestFormConfig')
        self.form_config.save()
        # Create transactions

    def test_statistics_permissions(self):
        # User credentials
        username = 'test_statistics_permissions'
        email = 'test@statistics.permissions'
        password = 'UserPassword12!@'

        # Create user
        user = User.objects.create_user(username, email)
        user.set_password(password)
        # Set permissions to view transactions statistics
        can_view_transaction = Permission.objects.get(codename='view_paymenttransaction')
        can_view_transaction_statistics = Permission.objects.get(codename='view_paymenttransaction_statistics')
        user.user_permissions.set([can_view_transaction, can_view_transaction_statistics])
        user.save()
        # Authorize user
        self.client.force_login(user=user, backend='django.contrib.auth.backends.ModelBackend')
        # Check if transactions statistics is present
        response = self.client.get('/en/transactions/payment-requests/')
        rq = response.context
        self.assertEqual(response.status_code, 200)
        assert (
            type(rq['total_based_per_currencies']) is list and
            type(rq['total_paid_based_per_currencies']) is list and
            type(rq['total_not_paid_based_per_currencies']) is list
        )

    def test_statistics_permissions_restrictions(self):
        username = 'test_statistics_permissions_restrictions'
        email = 'test@statistics.permissions_restrictions'
        password = 'UserPassword12!@'

        # Create user
        user = User.objects.create_user(username, email)
        user.set_password(password)
        # Don't set permissions to view transactions statistics
        can_view_transaction = Permission.objects.get(codename='view_paymenttransaction')
        user.user_permissions.set([can_view_transaction])
        user.save()
        # Authorize user
        self.client.force_login(user=user, backend='django.contrib.auth.backends.ModelBackend')
        # Check if transactions statistics is absent
        response = self.client.get('/en/transactions/payment-requests/')
        rq = response.context
        self.assertEqual(response.status_code, 200)
        assert (not rq.get('total_based_per_currencies') and
                not rq.get('total_paid_based_per_currencies') and
                not rq.get('total_not_paid_based_per_currencies'))
