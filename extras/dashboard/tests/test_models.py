# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import unittest

from django.test import TestCase
from django.core.exceptions import ValidationError

import pytest
from mixer.backend.django import mixer


pytestmark = pytest.mark.django_db


class TestPaymentRequestFormConfig(TestCase):

    def test_get_name_for_builtin_field(self):
        config = mixer.blend('dashboard.PaymentRequestFormConfig')
        field = mixer.blend('dashboard.Field',
                            field='order_no',
                            config=config,
                            name='')
        assert field.get_name() == 'order_no', "Should return the name of builtin field"

    def test_get_field_label_for_builtin_field(self):
        config = mixer.blend('dashboard.PaymentRequestFormConfig')
        field = mixer.blend('dashboard.Field',
                            field='order_no',
                            config=config,
                            name=''
                            )
        assert field.get_label() == 'Order no', "Should return the label of builtin field"

    def test_get_field_name_for_custom_field(self):
        config = mixer.blend('dashboard.PaymentRequestFormConfig')
        field_name = 'birthday'
        field = mixer.blend('dashboard.Field',
                            config=config,
                            name=field_name,
                            label=field_name
                            )
        assert field.get_name() == field_name, "Should return the name of custom field"

    def test_get_label_for_custom_field(self):
        config = mixer.blend('dashboard.PaymentRequestFormConfig')
        field_name = 'birthday'
        field = mixer.blend('dashboard.Field',
                            config=config,
                            name=field_name,
                            label=field_name
                            )
        assert field.get_label() == field_name, "Should return the label of custom field"

    def test_cannot_create_with_builtin_field_and_custom_field(self):
        config = mixer.blend('dashboard.PaymentRequestFormConfig')
        field = mixer.blend('dashboard.Field',
                            field='order_no',
                            name='order_no',
                            config=config,
                            label='order_no')
        self.assertRaises(ValidationError, field.clean), 'Should forbid creating a field with duplicate field'

    # @unittest.skip('WIP')
    def test_create_with_builtin_field(self):
        field = mixer.blend('dashboard.Field', field='order_no')
        assert field.field == 'order_no'
        assert field.name == ''
        assert field.label == ''