# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db.models import Q

import django_filters

from gateway.models import PaymentTransaction


class PaymentTransactionFilter(django_filters.FilterSet):
    q = django_filters.CharFilter(method='filter_q')
    seen = django_filters.CharFilter(method='filter_seen_at')
    email_seen = django_filters.CharFilter(method='filter_email_seen_at')
    created = django_filters.DateFromToRangeFilter()
    modified = django_filters.DateFromToRangeFilter()
    gateway = django_filters.CharFilter(method='filter_gateway')
    graph = django_filters.CharFilter(method='filter_graph')
    exclude = django_filters.CharFilter(method='filter_exclude')

    start_date = None
    end_date = None
    period = 'day'

    class Meta:
        model = PaymentTransaction
        fields = ('created', 'modified', 'gateway', 'type',
                  'currency_code', 'order_no', 'bulk_id','seen','email_seen',
                  'q', 'gateway_code', 'graph', 'exclude',
                  'state', 'initiator')

    def filter_gateway(self, queryset, name, value):
        return queryset

    def filter_seen_at(self, queryset, name, value):
        if value == 'yes':
            return queryset.filter(seen_at__isnull=False)
        if value == 'no':
            return queryset.filter(seen_at__isnull=True)
        return queryset

    def filter_email_seen_at(self, queryset, name, value):
        if value == 'yes':
            return queryset.filter(email_seen_at__isnull=False)
        if value == 'no':
            return queryset.filter(email_seen_at__isnull=True)
        return queryset

    def filter_q(self, queryset, name, value):
        if not value:
            return queryset
        return queryset.filter(
            Q(order_no__icontains=value) |
            Q(bulk_id__icontains=value) |
            Q(amount__icontains=value) |
            Q(customer_first_name__icontains=value) |
            Q(customer_last_name__icontains=value) |
            Q(customer_email__icontains=value) |
            Q(customer_phone__icontains=value) |
            Q(id__icontains=value) |
            Q(extra__contains=value)
        )

    def filter_graph(self, queryset, name, value):

        if value:
            if all([self.start_date, self.end_date]):
                number_of_days = (self.end_date - self.start_date).days

                if 28 < number_of_days < 365:
                    self.period = 'month' 
                if number_of_days > 365:
                    self.period = 'year'

            return queryset.get_report(period=self.period)

        return queryset

    def filter_exclude(self, queryset, name, value):
        if value:
            return queryset.exclude(id__in=value.split(','))
        return queryset


class AllPaymentTranFilter(PaymentTransactionFilter):
    """
    Just removing created and modified
    for the specified date filter
    """
    class Meta:
        model = PaymentTransaction
        fields = ('gateway', 'type',
                  'currency_code', 'order_no', 'bulk_id',
                  'q', 'gateway_code', 'graph', 'exclude',
                  'state', 'initiator')


class OnlyBoxFilterClass(AllPaymentTranFilter):

    class Meta:
        model = PaymentTransaction
        fields = ('created', 'modified', 'gateway', 'type',
                  'gateway_code', 'initiator')
