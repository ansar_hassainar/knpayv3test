# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
from django.conf import settings
import requests


def push_payment_transactions(queryset, installation):

    def prepare_data(queryset, installation):
        data = []
        for tran in queryset:
            data.append(tran.fetch_data(installation))
        return json.dumps(data)

    url = settings.DASHBOARD_URL
    headers = {'Content-Type': 'application/json'}
    requests.post(url, data=prepare_data(queryset, installation), headers=headers)
    requests.patch(url, data=prepare_data(queryset, installation), headers=headers)
