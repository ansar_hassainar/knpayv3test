# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django import template

register = template.Library()


@register.assignment_tag(takes_context=True)
def get_values_for_extra_fields(context, payment_transaction):
    transaction_config = context['transaction_config']
    return payment_transaction.get_values_for_extra_fields(transaction_config)
