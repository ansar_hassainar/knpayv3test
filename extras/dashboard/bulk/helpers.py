# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Field, ButtonHolder, Submit, Div, HTML, Reset


class BulkFilterFormHelper(FormHelper):
    form_show_labels = False
    # disable_csrf = True
    form_class = 'bulk_filter_form'
    form_method = 'GET'
    layout = Layout(
                Row(
                    Div(HTML('<hr>'),
                        css_class='col-sm-12'
                        ),
                    Div(
                        # Do not remove these empty format string.
                        # Django translation engine is not able to parse trans tag in crispy HTML
                        # That is why empty format is added for keeping translations in po file
                        # and not getting commented.
                        HTML("{% load i18n %}<label>{% trans 'Created by' %}</label>" + "".format({'created_by': _('Created by')})),
                        Div(
                            Field('initiator'),
                            css_class='btn-group bootstrap-select'
                            ),
                        css_class='col-sm-3'
                        ),
                    Div(
                        HTML("{% load i18n %}<label>{% trans 'Uploaded by' %}</label>"),
                        Div(
                            Field('files__initiator'),
                            css_class='btn-group bootstrap-select'
                            ),
                        css_class='col-sm-3'
                        ),
                    Div(
                        HTML("{% load i18n %}<label>{% trans 'File name' %}</label>" + "".format({'file_name': _('File name')})),
                        Div(
                            Field('files__filename'),
                            css_class='btn-group bootstrap-select'
                            ),
                        css_class='col-sm-3'
                        ),
                    ),
                Row(
                    Div(
                        HTML("{% load i18n %}<label>{% trans 'Created from' %}</label>" + "".format({'created_from': _('Created from')})),
                        Div(
                            Field('created_0'),
                            css_class='btn-group bootstrap-select'
                            ),
                        css_class='col-sm-3'
                        ),
                    Div(
                        HTML("{% load i18n %}<label>{% trans 'Created to' %}</label>" + "".format({'created_to': _('Created to')})),
                        Div(
                            Field('created_1'),
                            css_class='btn-group bootstrap-select'
                            ),
                        css_class='col-sm-3'
                        ),

                    ),
                Row(
                    Div( 
                        Row(
                            Div(
                                Div(
                                    Div(
                                        Reset('reset', _('Clear')),
                                        Submit('submit', _('Submit')),
                                        css_class='col-sm-6'
                                        ),
                                    css_class='form-group row'
                                    ),
                                css_class='col-lg-12'
                                ),
                           
                            ),
                        css_class='col-lg-8',
                        ),
                    
                    ),
                )