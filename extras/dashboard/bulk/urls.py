# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^export-format/$', views.export_bulk_format, name='export_format'),
    url(r'^(?P<pk>\d+)/$', views.BulkDetailView.as_view(), name='bulk_detail'),
    url(r'^error-export/(?P<pk>\d+)/$', views.error_export, name='error_rows'),
    url(r'^(?P<bulk_pk>\d+)/(?P<pk>\d+)/$',
        views.BulkTransactionUpdateView.as_view(),
        name='transaction_detail'),
    url(r'^upload/$', views.BulkUploadView.as_view(), name='upload'),
    url(r'^process/(?P<pk>\d+)/$', views.process_bulk_file, name='process'),
    url(r'^error_report/(?P<file_pk>\d+)/$', views.error_report, name='error_report'),
    url(r'^dispatch-bulk/(?P<pk>\d+)/$', views.dispatch_bulk, name='dispatch_bulk'),
    url(r'^dispatch-transaction/(?P<pk>\d+)/$',
        views.dispatch_transaction, name='dispatch_transaction'),
]
