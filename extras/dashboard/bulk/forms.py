# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from datetime import date, datetime, timedelta
import dateutil.parser
import logging

from django import forms
from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now

import django_rq
from schedule.models import Rule, Event
from crispy_forms.helper import FormHelper

from gateway.models import PaymentTransaction
from extras.dashboard.forms import BasePaymentRequestForm, CreatePaymentRequestForm
from utils.forms import AjaxBaseForm, AbstractBasePaymentRequestConfigForm
from .models import Bulk, BulkFile, BulkTransaction
from .helpers import BulkFilterFormHelper

logger = logging.getLogger('dashboard.bulk')


class BulkFileForm(AjaxBaseForm, forms.ModelForm):

    class Meta:
        model = BulkFile
        fields = ('file', 'bulk')

    def __init__(self, request, *args, **kwargs):
        self.request = request
        super(BulkFileForm, self).__init__(*args, **kwargs)
        self.fields['bulk'].required = False

    def save(self, orphaned=False):
        if orphaned:
            self.instance = BulkFile.objects.create_from_orphaned(self.request, self.instance.file)
        else:
            self.instance.initiator = self.request.user
        # action.send(self.request.user, verb='created', action_object=obj)
        self.instance.filename = self.instance.file.name.split('/')[-1].split('.')[0]
        self.instance.save()
        return self.instance


class BulkUpdateForm(AjaxBaseForm, forms.ModelForm):
    class Meta:
        model = Bulk
        fields = ('initiator', 'name',)

    def __init__(self, *args, **kwargs):
        helper = FormHelper()
        helper.form_id = 'bulk_form'
        self.helper = helper
        super(BulkUpdateForm, self).__init__(*args, **kwargs)
        self.fields['name'].required = True
        self.fields['initiator'].disabled = True
        self.fields['initiator'].required = False


class BulkFormMixin(forms.Form):
    event_fields = ('rule', 'start', 'end', 'end_recurring_period')
    required_event_fields = ('rule', 'start', 'end_recurring_period',)
    transaction_fields = [f.name for f in BulkTransaction._meta.get_fields()]
    private_fields = ('type', 'initiator', 'bulk_id')
    rule = forms.ChoiceField(
        label=_("Scheduler rule"),
        choices=(), required=False)
    start = forms.DateTimeField(
        label=_("Scheduler start date"),
        required=False,
        widget=forms.DateTimeInput(format='%m/%d/%Y %H:%M', attrs={'class': 'datetimepicker'})
    )
    end = forms.DateTimeField(
        label=_("Scheduler end date"),
        help_text=_("The end time must be later than start time."),
        required=False,
        widget=forms.HiddenInput(),
    )
    end_recurring_period = forms.DateTimeField(label=_("Scheduler end date"),
                                               help_text=_("When recurring shall end."),
                                               required=False,
                                               widget=forms.DateTimeInput(format='%m/%d/%Y %H:%M',
                                                                          attrs={'class': 'datetimepicker'}))

    transaction_type = PaymentTransaction.PAYMENT_REQUEST

    def __init__(self, *args, **kwargs):
        super(BulkFormMixin, self).__init__(*args, **kwargs)
        self.fields['rule'].choices = [("", _('---------'))] + list(Rule.objects.all().values_list('name', 'name'))

    def clean_start(self):
        start = self.cleaned_data.get('start')
        if not start:
            return None
        return start

    def clean_rule(self):
        rule = self.cleaned_data.get('rule')
        if rule:
            try:
                return Rule.objects.get(name=rule)
            except Rule.DoesNotExist:
                return
        return

    def clean(self):
        self.cleaned_data = super(BulkFormMixin, self).clean()
        # end means when the event shall end, for our situation this has to end immediately
        start = self.cleaned_data.get('start')
        if start is not None:
            self.cleaned_data.update({'end': start + timedelta(seconds=1)})

        if self.cleaned_data.get('rule'):
            for field_name in self.required_event_fields:
                if not self.cleaned_data.get(field_name):
                    self.add_error(field_name, _("This field is required."))
        return self.cleaned_data

    def _get_model_data(self, field_list):
        model_data = dict()
        for field in field_list:
            if self.cleaned_data.get(field, False):
                model_data[field] = self.cleaned_data.get(field)
        return model_data

    @classmethod
    def _update_model_instance(cls, instance, model_data):
        for key, value in model_data.iteritems():
            setattr(instance, key, value)
        instance.save()

    # @transaction.atomic
    def save(self, row_number=None):
        transaction_data = self._get_model_data(self.transaction_fields)
        event_data = self._get_model_data(self.event_fields)
        created = True
        is_recurring = self.cleaned_data.get('rule') is not None

        # create or update
        if hasattr(self, 'instance'):
            created = False
            instance = self.instance
            self._update_model_instance(instance, transaction_data)

            if instance.event is not None:
                if 'rule' not in [key for key in event_data.iterkeys()]:
                    event_data.update({'rule': None})
                self._update_model_instance(self.instance.event, event_data)
        else:
            instance = BulkTransaction.objects.create(
                bulk=self.bulk_file.bulk,
                initiator=self.bulk_file.initiator,
                **transaction_data)

            # create event - only if the Bulk is recurring
            if is_recurring:
                event_data = {k: self.cleaned_data.get(k) for k in self.event_fields}
                event = Event.objects.create(
                    title=instance.__unicode__(),
                    creator=instance.bulk.initiator,
                    **event_data)
                instance.event = event

            self.data_for_api.update({'bulk_id': instance.id, 'initiator': self.bulk_file.initiator.id})

        # exclude fields existing on BulkTransaction model from API data
        data = {key: value for key, value in self.data_for_api.iteritems() if key not in self.transaction_fields}
        # convert native datetime objects
        data.update(
            {key: value.isoformat() for key, value in data.iteritems() if isinstance(value, (date, datetime))})
        # convert models to pks
        data.update(
            {key: value.pk for key, value in data.iteritems() if issubclass(type(value), models.Model)})
        # change amount decimal to string
        data['amount'] = str(data['amount'])

        instance.data = data
        instance.row_number = row_number if row_number is not None else instance.row_number
        instance.save()

        # schedule
        if created and is_recurring:
            # for now we need to manage the queue only on newly created instances
            scheduler = django_rq.get_scheduler('default')
            initiator_pk = instance.initiator.pk
            event = instance.event
            occurrences = event.get_occurrences(event.start, instance.event.end_recurring_period)
            for occurrence in occurrences:
                scheduler.enqueue_at(occurrence.start, instance.dispatch, initiator_pk, queue=True)

        return instance

    
class BulkForm(BulkFormMixin, AbstractBasePaymentRequestConfigForm, BasePaymentRequestForm):
    knpay_section = PaymentTransaction.PAYMENT_REQUEST
    pass


class BaseBulkImportForm(BulkFormMixin, CreatePaymentRequestForm):
    knpay_section = PaymentTransaction.PAYMENT_REQUEST
    pass


class BulkTransactionImportForm(BaseBulkImportForm):

    def __init__(self, bulk_file, *args, **kwargs):
        self.bulk_file = bulk_file
        super(BulkTransactionImportForm, self).__init__(*args, **kwargs)


class BulkTransactionUpdateForm(BulkForm,
                                forms.ModelForm):

    class Meta:
        model = BulkTransaction
        fields = '__all__'
        exclude = 'data', 'row_number'

    def __init__(self, *args, **kwargs):
        super(BulkTransactionUpdateForm, self).__init__(*args, **kwargs)
        form_fields = self.fields.keys()

        # default api data
        data = self.instance.data
        # merge default data with extra
        data.update(data.pop('extra', {}))
        fields = self.config.fields.active()

        for key, value in data.iteritems():
            # if the field is already available on the form, fill it

            if key in ['start', 'end', 'end_recurring_period'] and value:
                # convert to a readable date format
                value = datetime.strftime(dateutil.parser.parse(value), '%m/%d/%Y %H:%M')

            if key in form_fields and key != 'type':
                if key == 'rule':
                    self.fields[key].initial = self.instance.event.rule.name if \
                        getattr(self.instance.event, 'rule', False) else '---------'
                else:
                    self.fields[key].initial = value
            # else, add it
            elif key not in self.private_fields:
                try:
                    field = fields.get(models.Q(name=key) | models.Q(field=key))

                    self.add_defined_field(field.get_name(), label=field.get_label(),
                                           initial=value, required=field.required,
                                           field=field)
                except ObjectDoesNotExist:
                    self.add_defined_field(key, initial=value)


class BulkFilterForm(forms.ModelForm):
    created_0 = forms.DateField(
        widget=forms.DateInput(attrs={'class': 'form-control'}),
        required=False
    )
    created_1 = forms.DateField(
        widget=forms.DateInput(attrs={'class': 'form-control'}),
        required=False
    )

    class Meta:
        model = Bulk
        fields = ('initiator',)

    def __init__(self, *args, **kwargs):
        super(BulkFilterForm, self).__init__(*args, **kwargs)
        self.helper = BulkFilterFormHelper()
        choices_map = {
            'initiator': ('initiator__id', 'initiator__username'),
            'files__filename': ('files__filename', 'files__filename'),
            'files__initiator': ('files__initiator', 'files__initiator__username')
        }

        for field_name, field_list in choices_map.items():
            self.set_choices(field_name, field_list)

    def set_choices(self, field_name, field_list):
        widget = forms.Select(attrs={'class': 'selectpicker', 'data-live-search': "true"})
        bulks = Bulk.objects.all()
        choices = [('', _('All'))] + list(set(bulks.values_list(*field_list).distinct()))
        self.fields[field_name] = forms.ChoiceField(choices=choices,
                                                    widget=widget,
                                                    required=False)
