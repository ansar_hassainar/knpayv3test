# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from solo.admin import SingletonModelAdmin

from utils.admin import PrettyJsonAdminMixin
from extras.dashboard.admin import FieldInline as CoreFieldInline
from extras.dashboard.models import FIELD_CHOICES
from .models import Bulk, BulkTransaction, BulkFile, BulkConfig


class FieldInline(CoreFieldInline):
    def formfield_for_choice_field(self, db_field, request, **kwargs):
        if db_field.name == 'field':
            kwargs['choices'] = (('', '------'),) + FIELD_CHOICES
        return super(FieldInline, self).formfield_for_choice_field(db_field,
                                                                   request,
                                                                   **kwargs)


@admin.register(BulkConfig)
class BulkConfigAdmin(SingletonModelAdmin):
    inlines = [FieldInline]
    fieldsets = (
        (None, {
            'fields': ('disclose_to_merchant_url', 'disclosure_url',
                       'redirect_url', 'currency', 'sms_payment_details',
                       'bcc_initiator', 'default_email', 'notify_on_error', 'generate_on_error')
        }),
        (_("Communication"), {
            'fields': ('email_payment_details_success_template',
                       'email_payment_details_failure_template',
                       'sms_payment_details_success_template',
                       'sms_payment_details_failure_template',
                       'customer_payment_email_notification_template',
                       'customer_payment_sms_notification_template',
                       'bulk_upload_error_notification_template',
                       )
        }),
    )


class BulkTransactionInline(admin.StackedInline):
    model = BulkTransaction
    extra = 0


class BulkFileInline(PrettyJsonAdminMixin, admin.StackedInline):
    model = BulkFile
    readonly_fields = ('rows', 'imported_rows')
    extra = 0
    json_field = 'error_report'


@admin.register(Bulk)
class BulkAdmin(admin.ModelAdmin):
    inlines = (BulkFileInline, BulkTransactionInline,)
    list_filter = ('created', 'initiator')
    list_display = ('__unicode__', 'name', 'initiator', 'created')
    list_editable = 'name',
