# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import csv
import logging
import collections

from django.core.urlresolvers import reverse
from django.db import transaction
from django.http import JsonResponse, Http404, HttpResponse, StreamingHttpResponse
from django.views import generic
from django.utils.translation import ugettext_lazy as _, force_text as _ft
from django.shortcuts import get_object_or_404, render

import django_rq
from django_filters.views import FilterMixin
from braces.views import MultiplePermissionsRequiredMixin
from el_pagination.views import AjaxListView
from rest_framework import status

from utils.decorators import pause_restriction
from utils.views.mixins import PageTitleMixin, DashboardMixin
from tasks import import_file
from .data import SAMPLE_DATA
from .forms import BulkFileForm, BulkUpdateForm, BulkTransactionUpdateForm, BulkFilterForm
from .filters import BulkFilter
from .models import Bulk, BulkFile, BulkTransaction, BulkConfig

logger = logging.getLogger('dashboard.bulk')


class BulkMixin(object):
    def get_context_data(self, **kwargs):
        ctx = super(BulkMixin, self).get_context_data(**kwargs)
        ctx.update({
            'file_form': BulkFileForm(self.request, initial=self.get_initial_for_file_form())
        })
        return ctx

    def get_initial_for_file_form(self):
        return {}


def export_bulk_format(request):
    """
    Export empty excel with bulk required format
    """
    sms_fields = ['sms_notification']
    config = BulkConfig.get_solo()
    config_fields = config.fields.active()
    base_fields = ['dummy', 'customer_email', 'amount', 'currency_code', 'language']
    required_fields = base_fields + config_fields.required().names_list()
    all_fields = base_fields + sms_fields + config.fields.active().names_list()

    fields = {
        'required_fields': required_fields,
        'all_fields': all_fields,
        'recurrence_required_fields': required_fields + list(BulkTransactionUpdateForm.required_event_fields),
        'recurrence_all_fields': all_fields + list(BulkTransactionUpdateForm.required_event_fields)
    }
    _type = request.GET.get('type')
    if _type not in fields.keys():
        raise Http404()

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="%s.csv"' % _type
    example_values = [SAMPLE_DATA.get(field_name, '') for field_name in fields[_type]]
    writer = csv.writer(response)
    writer.writerow(fields[_type])
    writer.writerow(example_values)
    return response


class Echo(object):
    """An object that implements just the write method of the file-like
    interface.
    """
    def write(self, value):
        """Write the value by returning it, instead of storing in a buffer."""
        return value


def error_export(request, pk):
    """
    Export only rows with errors
    """
    bulk_file = get_object_or_404(BulkFile, pk=pk)
    # convert cell to ut8
    rows = []
    for row in bulk_file.get_error_rows():
        utf8_row = []
        for cell in row:
            utf8_row.append(cell.encode('utf8'))
        rows.append(utf8_row)
    pseudo_buffer = Echo()
    writer = csv.writer(pseudo_buffer)
    response = StreamingHttpResponse((writer.writerow(row) for row in rows),
                                     content_type="text/csv")
    filename = 'error_rows_bulk_%s_%s' % (bulk_file.bulk.pk, bulk_file.pk)
    response['Content-Disposition'] = 'attachment; filename="%s.csv"' % filename
    return response


class IndexView(PageTitleMixin,
                BulkMixin,
                DashboardMixin,
                MultiplePermissionsRequiredMixin,
                FilterMixin,
                AjaxListView):
    template_name = 'dashboard/bulk/index.html'
    page_title = _("Bulk")
    model = Bulk
    context_object_name = 'bulks'
    filterset_class = BulkFilter
    page_template = 'dashboard/ajax/bulks.html'
    permissions = {
        "any": ("bulk.view_bulk",
                "bulk.view_owned_bulk",
                "bulk.add_bulk",
                "bulk.change_bulk",
                "bulk.change_owned_bulk")
    }

    def get_queryset(self):
        return Bulk.objects.populated().get_for_user(self.request.user)

    def get(self, request, *args, **kwargs):
        super(IndexView, self).get(request, *args, **kwargs)

        filterset_class = self.get_filterset_class()
        self.filterset = self.get_filterset(filterset_class)
        self.object_list = self.filterset.qs
        context = self.get_context_data(
            object_list=self.object_list,
            page_template=self.page_template,
            filter=self.filterset,
        )
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['bulk_filter_form'] = BulkFilterForm()
        return context


class BulkDetailView(PageTitleMixin,
                     BulkMixin,
                     DashboardMixin,
                     MultiplePermissionsRequiredMixin,
                     generic.UpdateView,
                     AjaxListView):
    model = Bulk
    form_class = BulkUpdateForm
    page_title = _("Bulk Detail")
    template_name = 'dashboard/bulk/bulk_detail.html'
    context_object_name = 'bulk'
    page_template = 'dashboard/ajax/bulk_transactions.html'

    def get_permission_required(self, request=None):
        if request.method == 'POST':
            return {'any': ("bulk.change_bulk",)}
        return {"any": ("bulk.view_bulk", "bulk.view_owned_bulk")}

    def get(self, request, *args, **kwargs):
        super(BulkDetailView, self).get(request, *args, **kwargs)

        self.object = self.get_object()
        self.object_list = BulkTransaction.objects.filter(bulk=self.object).get_for_user(request.user)

        config = BulkConfig.get_solo()
        config_fields = config.fields.active()
        extra_fields = collections.OrderedDict()
        config_fields = [field for field in config_fields if field.listing_display]
        for obj in self.object_list:
            fields_data = {}
            data = obj.data
            data.update(obj.data.get('extra', {}))
            for field in config_fields:
                    fields_data[field] = data.get(field.field if field.field else field.name, '-')
            extra_fields[obj.id] = fields_data

        context = self.get_context_data(
            object_list=self.object_list,
            transactions=self.object_list,
            page_template=self.page_template,
            extra_fields=extra_fields,
            config_fields=config_fields
        )

        return self.render_to_response(context)

    def get_template_names(self):
        return [self.page_template] if self.request.is_ajax() else [self.template_name]

    def get_initial_for_file_form(self):
        return {'bulk': self.object.id}

    def form_valid(self, form):
        form.save()
        return JsonResponse({'message': _ft(_("Bulk name changed."))})

    def form_invalid(self, form):
        data = {'errors': form.errors_as_dict()}
        return JsonResponse(data, status=status.HTTP_400_BAD_REQUEST)


class BulkTransactionUpdateView(PageTitleMixin,
                                DashboardMixin,
                                MultiplePermissionsRequiredMixin,
                                generic.UpdateView):
    model = BulkTransaction
    context_object_name = 'transaction'
    template_name = 'dashboard/bulk/transaction_detail.html'
    form_class = BulkTransactionUpdateForm
    permissions = {
        "any": ("bulk.view_owned_bulktransaction", "bulk.view_bulktransaction",
                "bulk.change_bulktransaction")
    }

    def get_form_kwargs(self):
        kwargs = super(BulkTransactionUpdateView, self).get_form_kwargs()
        kwargs.update({
            'config': BulkConfig.get_solo(),
            'request': self.request
        })
        return kwargs

    def get_context_data(self, **kwargs):
        ctx = super(BulkTransactionUpdateView, self).get_context_data(**kwargs)
        list_form = list(ctx['form'])
        index = len(list_form) // 2 + 1
        left_form = list_form[:index]
        right_form = list_form[index:]
        ctx.update({
            'left_form': left_form,
            'right_form': right_form
        })
        return ctx

    def get_page_title(self):
        return self.object.__unicode__()

    def get_queryset(self):
        return BulkTransaction.objects.all()

    def form_valid(self, form):
        form.save()
        return JsonResponse({'message': _ft(_("Transaction updated."))})

    def form_invalid(self, form):
        data = {'errors': form.errors_as_dict()}
        return JsonResponse(data, status=status.HTTP_400_BAD_REQUEST)

    def get_object(self, queryset=None):
        bulk_pk = self.kwargs.get('bulk_pk')
        pk = self.kwargs.get('pk')
        queryset = self.get_queryset().filter(bulk__id=bulk_pk, pk=pk)
        try:
            return queryset.get()
        except BulkTransaction.DoesNotExist:
            raise Http404()


class BulkUploadView(generic.FormView):
    form_class = BulkFileForm

    def get_form_kwargs(self):
        kwargs = super(BulkUploadView, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def form_invalid(self, form):
        data = {'errors': form.errors_as_dict(error_message=False)}
        return JsonResponse(data, status=status.HTTP_400_BAD_REQUEST)

    # @method_decorator(transaction.atomic)
    # TODO: Check why saving if error!!!
    def form_valid(self, form):
        # print form.cleaned_data.get('bulk')
        obj = form.save(orphaned=not bool(form.cleaned_data.get('bulk')))
        data = {
            'message': _ft(_("File uploaded. Processing... ")),
            'process_url': reverse('dashboard:bulk:process', args=(obj.id,))
        }
        return JsonResponse(data)


@transaction.atomic
def process_bulk_file(request, pk):
    bulk_file = get_object_or_404(BulkFile, pk=pk)
    result = import_file(bulk_file, request)
    s = status.HTTP_200_OK if result['success'] else status.HTTP_400_BAD_REQUEST
    return JsonResponse(result, status=s)


@pause_restriction()
def dispatch_bulk(request, pk):
    bulk = get_object_or_404(Bulk, pk=pk)
    django_rq.enqueue(bulk.dispatch, request.user.pk, queue=True)
    return JsonResponse({'message': _ft(_("Bulk dispatching started"))})


@pause_restriction()
def dispatch_transaction(request, pk):
    bulk_transaction = get_object_or_404(BulkTransaction, pk=pk)
    # django_rq.enqueue(bulk_transaction.dispatch)
    # return JsonResponse({'message': _("Transaction dispatching started")})
    status, obj = bulk_transaction.dispatch(request.user.pk)
    if not status:
        data = {'message': _ft(_("Failed to dispatch")), 'errors': obj}
        return JsonResponse(data, status=400)
    return JsonResponse({'message': _ft(_("Transaction dispatched"))})


def error_report(request, file_pk):
    bulk_file = get_object_or_404(BulkFile, pk=file_pk)
    ctx = {
        'bulk_file': bulk_file,
        'report': bulk_file.error_report_for_html()
    }
    return render(request, 'dashboard/bulk/bulk_error.html', ctx)
