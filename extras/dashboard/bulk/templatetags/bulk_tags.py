# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django import template

register = template.Library()


@register.simple_tag(takes_context=True)
def has_bulk_transaction_perm(context, menu_item):
    user = context['request'].user
    return any([user.has_perm(perm) for perm in menu_item.perms])
