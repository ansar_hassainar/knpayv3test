# -*- coding: utf-8 -*-
from __future__ import absolute_import

import rules


def can_dispatch(user, obj, global_perm, obj_perm):
    if user.is_superuser:
        return True
    elif user.has_perm(global_perm):
        return True
    elif user.has_perm(obj_perm):
        return hasattr(obj, 'initiator') and user == obj.initiator
    else:
        return False


@rules.predicate
def can_add_bulk_file(user):
    return user.has_perm('bulk.add_bulkfile')


@rules.predicate
def can_add_bulk(user):
    return user.has_perm('bulk.add_bulk')


@rules.predicate
def can_dispatch_bulk(user, bulk):
    return can_dispatch(user, bulk, 'bulk.dispatch_bulk', 'bulk.dispatch_owned_bulk')


@rules.predicate
def can_dispatch_bulk_transaction(user, bulk_transaction):
    return can_dispatch(user, bulk_transaction,
                        'bulk.dispatch_bulktransaction',
                        'bulk.dispatch_owned_bulktransaction')


can_see_import_button = can_add_bulk & can_add_bulk_file
rules.add_rule('can_add_bulk_file', can_add_bulk_file)
rules.add_rule('can_add_bulk', can_add_bulk)
rules.add_rule('can_see_import_button', can_see_import_button)
rules.add_rule('can_dispatch_bulk', can_dispatch_bulk)
rules.add_rule('can_dispatch_bulk_transaction', can_dispatch_bulk_transaction)
