# -*- coding: utf-8 -*-

from mock import MagicMock, patch

from django.test import TestCase
from django.contrib.auth import get_user_model

from ..models import Bulk

# patch ReverseManyToOneDescriptor to use mock on 'files' field
@patch('django.db.models.fields.related.ReverseManyToOneDescriptor.__get__')
class TestBulk(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestBulk, cls).setUpClass()
        # create user object
        UserModel = get_user_model()
        cls.user = UserModel.objects.create()

    def setUp(self):
        # create new Bulk object for each test
        self.bulk = Bulk.objects.create(initiator=self.user)

    def set_files(self, *config):
        """Create mock BulkFile objects"""
        # set count to mock RelatedManager
        self.bulk.files.count = len(config)
        # create mock files using config
        files = []
        for conf in config:
            rows = conf.get("rows")
            imported_rows = conf.get("imported_rows")
            size = conf.get("size")
            mock_file = MagicMock(rows=rows, imported_rows=imported_rows)
            mock_file.file.size = size
            files.append(mock_file)
        # files.all() should return a list of mock files
        self.bulk.files.all.return_value = files

    # tests for total_rows method

    def test_total_rows_no_files(self, manager):
        """total_rows should return 0 if Bulk contains no files"""
        self.assertEqual(self.bulk.total_rows(), 0)
    
    def test_total_rows_one_file(self, manager):
        """total_rows should return rows of the file if Bulk contains one file"""
        self.set_files({"rows": 10})
        self.assertEqual(self.bulk.total_rows(), 10)

    def test_total_rows_two_files(self, manager):
        """total_rows should return sum of rows of files if Bulk contains two files"""
        self.set_files({"rows": 10}, {"rows": 20})
        self.assertEqual(self.bulk.total_rows(), 30)

    # tests for total_imported_rows method

    def test_total_imported_rows_no_files(self, manager):
        """total_imported_rows should return 0 if Bulk contains no files"""
        self.assertEqual(self.bulk.total_imported_rows(), 0)
    
    def test_total_imported_rows_one_file(self, manager):
        """total_imported_rows should return imported rows of the file if Bulk contains one file"""
        self.set_files({"imported_rows": 10})
        self.assertEqual(self.bulk.total_imported_rows(), 10)

    def test_total_imported_rows_two_files(self, manager):
        """total_imported_rows should return sum of imported rows of files if Bulk contains two files"""
        self.set_files({"imported_rows": 10}, {"imported_rows": 20})
        self.assertEqual(self.bulk.total_imported_rows(), 30)

    # tests for total_size method

    def test_total_size_no_files(self, manager):
        """total_size should return 0 if Bulk contains no files"""
        self.assertEqual(self.bulk.total_size(), 0)
    
    def test_total_size_one_file(self, manager):
        """total_size should return size of the file if Bulk contains one file"""
        self.set_files({"size": 10})
        self.assertEqual(self.bulk.total_size(), 10)

    def test_total_size_two_files(self, manager):
        """total_size should return sum of sizes of files if Bulk contains two files"""
        self.set_files({"size": 10}, {"size": 20})
        self.assertEqual(self.bulk.total_size(), 30)