# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib.auth.models import Permission
from django.core.management.base import BaseCommand


def update_perms(initial_perm_codename, new_perm_codename):
    initial = Permission.objects.get(codename=initial_perm_codename)
    Permission.objects.get(codename=new_perm_codename).user_set.add(*initial.user_set.all())


class Command(BaseCommand):
    help = "update bulk permissions for users which have view to also have " \
           "change"

    def handle(self, **options):
        # add change permission to users which are having only view
        perms = (
            ('view_bulk', 'change_bulk'),
            ('view_owned_bulk', 'change_owned_bulk'),
            ('view_bulktransaction', 'change_bulktransaction'),
            ('view_owned_bulktransaction', 'change_owned_bulktransaction'),
        )
        for perm_map in perms:
            update_perms(perm_map[0], perm_map[1])
