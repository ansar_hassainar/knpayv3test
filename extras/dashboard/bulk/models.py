# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging
from collections import OrderedDict

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _, ugettext
from django.utils.timezone import now
from django.utils.functional import cached_property
from django.utils.timezone import get_default_timezone

import django_rq
from jsonfield import JSONField
from tablib import Dataset

from utils.db.models import (TimeStampedModel, EmailMailTemplate, MerchantURLConfig,
                             SMSMailTemplate, NotificationMailTemplate)
from utils.db.managers import PermissionManager, PermissionQuerySet
from utils.validators import FileValidator
from extras.dashboard.models import BaseFormConfig, PaymentRequestFormMixin
from .data import non_field_errors

logger = logging.getLogger('dashboard.bulk')


def get_upload_path(instance, filename):
    filename, dot, extension = filename.rpartition('.')
    return 'bulk/{}.{}'.format(slugify(filename), extension)


class BulkConfig(EmailMailTemplate,
                 SMSMailTemplate,
                 NotificationMailTemplate,
                 PaymentRequestFormMixin,
                 MerchantURLConfig,
                 BaseFormConfig):
    """
    Bulk config model
    """
    related_query_name = 'config_bulk'
    fields = GenericRelation('dashboard.Field', related_query_name=related_query_name)

    default_email = models.EmailField(_("default email"), max_length=120)

    notify_on_error = models.BooleanField(
        _("Notify initiator on error"),
        default=False,
        help_text='Send email notification to user if downloaded bulk file contains errors'
    )
    generate_on_error = models.BooleanField(
        _("Generate on file error"),
        default=True,
        help_text='Create bulk from imported file even if some of its rows contain errors'
    )
    bulk_upload_error_notification_template = models.ForeignKey(
        'dbmail.MailTemplate', blank=True, null=True, related_name='+'
    )

    class Meta:
        verbose_name = _("Bulk configuration")


class BulkQuerySet(PermissionQuerySet):
    def populated(self):
        return self.filter(files__isnull=False)


class BulkManager(PermissionManager):
    def get_queryset(self):
        return BulkQuerySet(self.model, using=self._db)

    def populated(self):
        return self.get_queryset().populated()


class Bulk(TimeStampedModel):
    name = models.CharField(_("Name"), max_length=32, default='', blank=True)
    initiator = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("Initiator"), related_name='bulks'
    )
    objects = BulkManager()

    def __unicode__(self):
        tz = get_default_timezone()
        return '{} - {} - {}'.format(
            self.id,
            self.name,
            self.created.astimezone(tz).strftime('%d-%m-%Y / %H:%M')
        )

    def description(self):
        return self.__unicode__()

    @models.permalink
    def get_absolute_url(self):
        return 'dashboard:bulk:bulk_detail', (self.pk,)

    @models.permalink
    def get_dispatch_url(self):
        return 'dashboard:bulk:dispatch_bulk', (self.pk,)

    class Meta:
        verbose_name = _("Bulk")
        verbose_name_plural = _("Bulks")
        app_label = 'bulk'
        ordering = '-created',
        permissions = (
                ('view_bulk', _('Can view bulks')),
                ('view_owned_bulk', _('Can view owned bulks')),
                ('change_owned_bulk', _('Can change owned bulks')),
                ('dispatch_bulk', _('Can dispatch bulks')),
                ('dispatch_owned_bulk', _('Can dispatch owned bulks'))
            )

    def can_be_dispatched(self):
        return self.transactions.exists()

    def dispatch(self, user_pk, queue=False):
        user = get_user_model().objects.get(pk=user_pk)
        for transaction in self.transactions.all():
            transaction.dispatch(user.pk, queue=queue)

    def total_rows(self):
        if self.files.count > 0:
            rows = map(lambda f: f.rows or 0, self.files.all())
            return sum(rows)
        return 0

    def total_imported_rows(self):
        if self.files.count > 0:
            imported_rows = map(lambda f: f.imported_rows or 0, self.files.all())
            return sum(imported_rows)
        return 0

    def total_size(self):
        if self.files.count > 0:
            sizes = map(lambda f: f.file.size, self.files.all())
            return sum(sizes)
        return 0


class BulkFileManager(models.Manager):
    def create_from_orphaned(self, request, file):
        bulk = Bulk.objects.create(initiator=request.user)
        return self.create(bulk=bulk, file=file, initiator=request.user)


class BulkFile(TimeStampedModel):
    bulk = models.ForeignKey(Bulk, verbose_name=_("Bulk"),
                             related_name='files')
    file = models.FileField(
        verbose_name=_("File"), upload_to=get_upload_path,
        validators=[FileValidator(allowed_extensions=['csv'])]
    )
    filename = models.CharField(_("Filename"), max_length=255, default='')
    rows = models.IntegerField(
        _("Rows"), null=True, blank=True,
        help_text=_("How many rows are in the sheet. "
                    "Header does count.")
    )
    imported_rows = models.IntegerField(
        _("Imported rows"), null=True, blank=True,
        help_text=_("How many rows were imported from the sheet.")
    )
    error_report = JSONField(_("Error report"), default={}, blank=True)
    initiator = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("Initiator"), related_name='bulk_files')
    objects = BulkFileManager()

    def __unicode__(self):
        return self.filename

    def has_errors(self):
        return bool(self.error_report)

    def get_error_rows(self):
        row_numbers = [i['row'] for i in self.error_report]
        sheet_data = Dataset().load(open(self.file.path).read())
        rows = [sheet_data[row_number-2] for row_number in row_numbers]
        data = [sheet_data.headers]
        for row in rows:
            data.append(row)
        return data

    def error_report_for_html(self):
        report = []

        # preserve a minimal order - row and other
        for raw_row_report in self.error_report:
            row_report = OrderedDict()
            row_report['row'] = raw_row_report.pop('row')
            other = raw_row_report.pop(non_field_errors)
            row_report.update(raw_row_report)
            row_report[non_field_errors] = other
            report.append(row_report)

        header = report[0].keys()
        return {'header': header, 'report': report}

    def error_report_for_email(self, error_report=None):
        report = []
        error_report = error_report or self.error_report
        for raw_row_report in error_report:
            row_report = OrderedDict()
            row_report['row'] = raw_row_report.pop('row')
            other = raw_row_report.pop(non_field_errors)
            row_report.update(raw_row_report)
            row_report[non_field_errors] = other
            if not len(report):
                report.append(row_report.keys())
            report.append(row_report.values())
        return report

    @cached_property
    def extension(self):
        return self.file.name.split('.')[-1]

    class Meta:
        verbose_name = _("Bulk file")
        verbose_name_plural = _("Bulk files")
        app_label = 'bulk'


class BulkTransaction(TimeStampedModel):
    bulk = models.ForeignKey(
        Bulk, verbose_name=_("Bulk"), related_name='transactions',
        help_text=_("Bulk parent of this transaction")
    )

    row_number = models.PositiveIntegerField(_("Row number"), null=True)

    # scheduler options
    event = models.OneToOneField('schedule.Event', null=True, blank=True,
                                 verbose_name=_("Scheduler Event"))

    last_sent_at = models.DateTimeField(_("Last sent at"), null=True, blank=True)
    initiator = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("Initiator"), related_name='bulk_transactions'
    )

    data = JSONField(_("Data"), default={}, blank=True)
    objects = PermissionManager()

    def __unicode__(self):
        return ugettext("Bulk %s / ID %s / Row %s") % (self.bulk, self.id, self.row_number)

    class Meta:
        verbose_name = _('Bulk Transaction')
        verbose_name_plural = _('Bulk Transactions')
        app_label = 'bulk'
        ordering = 'created',
        permissions = (
                ('view_bulktransaction', _('Can view bulk transactions')),
                ('view_owned_bulktransaction', _('Can view owned bulk transactions')),
                ('change_owned_bulktransaction', _('Can change owned bulk transactions')),
                ('dispatch_bulktransaction', _('Can dispatch bulk transactions')),
                ('dispatch_owned_bulktransaction', _('Can dispatch owned bulk transactions')),
            )

    def is_recurring(self):
        return bool(self.event)

    @models.permalink
    def get_absolute_url(self):
        return 'dashboard:bulk:transaction_detail', (self.bulk.id, self.pk)

    @models.permalink
    def get_dispatch_url(self):
        return 'dashboard:bulk:dispatch_transaction', (self.pk,)

    def dispatch(self, user_pk, queue=False):
        from api.func import initialize_transaction
        data = self.data
        data['initiator'] = user_pk
        obj, status = initialize_transaction(data, queue=queue)
        self.last_sent_at = now()
        self.save()
        return status, obj

    def get_customer_first_name(self):
        return self.data.get('customer_first_name', '-')

    def get_customer_last_name(self):
        return self.data.get('customer_last_name', '-')

    def get_customer_email(self):
        return self.data.get('customer_email', '-')

    @property
    def frequency(self):
        if self.is_recurring():
            if self.event.rule:
                return self.event.rule.frequency
