# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db.models import Q

import django_filters

from .models import Bulk


class BulkFilter(django_filters.FilterSet):
    created = django_filters.DateFromToRangeFilter()

    class Meta:
        model = Bulk
        fields = ('name', 'initiator', 'created', 'files__filename')
