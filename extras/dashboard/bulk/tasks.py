# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging
import csv
from copy import deepcopy
from io import BytesIO

from django.utils.translation import ugettext_lazy as _
from django.conf import settings as django_settings

from schedule.models import Calendar, Event
from tablib import Dataset
from dbmail import send_db_mail

from .data import non_field_errors

logger = logging.getLogger('dashboard.bulk')


def import_file(bulk_file, request):
    from .models import BulkConfig
    from .forms import BulkTransactionImportForm

    success = False
    partial = False

    try:
        imported_data = Dataset().load(open(bulk_file.file.path).read())
    except Exception as e:
        logger.info(
            "error occurred during loading bulk file ID %s - %s" % (bulk_file.id, e))
        bulk_file.delete()
        return {'success': success, 'partial': partial, 'message': _("Invalid file.")}

    headers = ['row'] + imported_data.headers + [non_field_errors]
    config = BulkConfig.get_solo()
    errors = []
    response = dict()
    imported_rows = 0
    for i, data in enumerate(imported_data.dict, start=2):
        # If customer doesn't have email then we'll add default_email from
        # BulkConfig that is defined in the admin
        if 'customer_email' not in data or not data['customer_email']:
            data['customer_email'] = config.default_email
            data['default_email'] = True

        form = BulkTransactionImportForm(
            config=config, request=request, bulk_file=bulk_file, data=dict(data))
        if form.is_valid():
            form.save(row_number=i)
            imported_rows += 1
        else:
            form_errors = form.errors_as_dict(error_message=False)
            row_errors = {h: '' for h in headers}
            row_errors['row'] = i

            for key, value in form_errors.iteritems():
                # map the field error to a header value
                try:
                    headers.index(key)
                    row_errors[key] = value
                except ValueError:
                    # add global column for errors
                    row_errors[non_field_errors] += '[%s - %s] ' % (key, value)
            if row_errors:
                errors.append(row_errors)
    success = not bool(errors)
    partial = len(errors) != i

    if not success:
        if config.notify_on_error:
            try:
                error_report = deepcopy(errors)
                rows = bulk_file.error_report_for_email(error_report=error_report)
                csv_file = BytesIO()
                writer = csv.writer(csv_file)
                for row in rows:
                    utf8_row = []
                    for cell in row:
                        utf8_row.append(unicode(cell).encode('utf8'))
                    writer.writerow(utf8_row)

                filename = 'error_rows_bulk_%s_%s' % (bulk_file.bulk.pk, bulk_file.pk) + '.csv'
                attachment_content_type = "text/csv"

                attachments = [(filename,
                                csv_file.getvalue(),
                                attachment_content_type
                                )]
                send_db_mail(config.bulk_upload_error_notification_template,
                             [request.user.email],
                             {},
                             language=django_settings.LANGUAGE_CODE,
                             attachments=attachments,
                             )
            except Exception as e:
                mail_logger = logging.getLogger('communications')
                mail_logger.exception("Error while sending email %s" % e)

        # reject uploading if it specified at Bulk config
        if not config.generate_on_error:
            bulk_file.delete()
            return {'success': False, 'partial': False, 'message': _("Invalid or missing values in some of rows")}
        else:
            response.update({'success': success, 'partial': partial})
    else:
        response['success'] = success

    # create the events

    if success or (not success and partial):
        calendar, created = Calendar.objects.get_or_create(
            name='Calendar for Bulk %s' % bulk_file.bulk.__unicode__(),
            slug='bulk-id-%s' % bulk_file.bulk.id,
        )
        event_ids = bulk_file.bulk.transactions.values_list(
            'event__id', flat=True)
        Event.objects.filter(id__in=list(event_ids)).update(calendar=calendar)

    bulk_file.rows = i - 1
    bulk_file.imported_rows = imported_rows
    bulk_file.error_report = errors
    bulk_file.save()

    return response

