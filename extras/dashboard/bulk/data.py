# -*- coding: utf-8 -*-

from __future__ import unicode_literals


non_field_errors = 'other'


SAMPLE_DATA = {
    'amount': '100',
    'gateway_code': 'knet',
    'currency_code': 'KWD',
    'language': 'en',
    'order_no': 'abc123',
    'customer_email': 'john_doe@example.com',
    'customer_phone': '96597117060',
    'customer_first_name': 'John',
    'customer_last_name': 'Doe',
    'customer_address_line1': 'Dar Al Awadhi',
    'customer_address_line2': 'Ahmad Al Jaber',
    'customer_address_city': 'Sharq',
    'customer_address_state': 'Kuwait',
    'customer_address_country': 'KW',
    'customer_address_postal_code': '24514',
    'rule': 'daily',
    'start': '12/12/2017 12:50',
    'end': '12/12/2018 12:50',
    'end_recurring_period': '12/12/2017 12:50',
    'sms_notification': True,
    'dummy': 'dummy'
}