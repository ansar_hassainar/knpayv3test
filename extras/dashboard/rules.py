# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import unicode_literals

import rules

from utils.perms import perm_check, perm_codes


def can_do(user, obj, verb):
    opts = obj._meta
    if user.is_superuser:
        return True
    elif perm_check(user, opts, perm_codes[verb]['all']):
        return True
    elif perm_check(user, opts, perm_codes[verb]['one']):
        return obj.initiator == user
    return False


@rules.predicate
def can_view_obj(user, obj):
    return can_do(user, obj, 'view')


@rules.predicate
def can_change_obj(user, obj):
    return can_do(user, obj, 'change')


@rules.predicate
def has_menu_perm(user, menu_item):
    return any([user.has_perm(perm) for perm in menu_item.perms])


@rules.predicate
def can_cancel_transaction(user, transaction):
    return (
        user.is_superuser or
        user.has_perm('gateway.cancel_paymenttransaction') or
        (transaction.initiator == user and user.has_perm('gateway.cancel_owned_paymenttransaction'))
    )

rules.add_rule('has_menu_perm', has_menu_perm)
rules.add_rule('can_view_obj', can_view_obj)
rules.add_rule('can_change_obj', can_change_obj)
rules.add_rule('can_cancel_transaction', can_cancel_transaction)

