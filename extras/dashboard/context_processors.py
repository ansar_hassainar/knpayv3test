# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from .menu import get_menu_items


def menu(request):
    return {'menu_items': get_menu_items()}