# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from modeltranslation.admin import TranslationGenericStackedInline
from solo.admin import SingletonModelAdmin

from .models import PaymentRequestFormConfig, CustomerPaymentFormConfig, Field


class FieldInline(TranslationGenericStackedInline):
    model = Field
    fields = (('type', 'listing_display', 'itinerary_display','customer_display'), ('is_active', 'required'), 'field', ('label_en', 'label_ar', 'name'))
    extra = 0
    prepopulated_fields = {"name": ("label_en",)}

    class Media:
        js = ('js/admin/field.js',)


@admin.register(PaymentRequestFormConfig)
class PaymentRequestFormConfigAdmin(SingletonModelAdmin):
    inlines = [FieldInline]
    fieldsets = (
        (None, {
            'fields': ('default_email', 'disclose_to_merchant_url',
                       'redirect_url', 'disclosure_url',
                       'currency', 'sms_payment_details',
                       'bcc_initiator')
        }),
        (_("Communication"), {
            'fields': ('email_payment_details_success_template',
                       'email_payment_details_failure_template',
                       'sms_payment_details_success_template',
                       'sms_payment_details_failure_template',
                       'customer_payment_email_notification_template',
                       'customer_payment_sms_notification_template',
                       'whatsapp_notification_template',

                       )
        }),
    )


@admin.register(CustomerPaymentFormConfig)
class CustomerPaymentFormConfigAdmin(SingletonModelAdmin):
    inlines = [FieldInline]
    fieldsets = (
        (None, {
            'fields': ('currency', 'sms_payment_details', 'default_email')
        }),
        (_("Communication"), {
            'fields': ('email_payment_details_success_template',
                       'email_payment_details_failure_template',
                       'sms_payment_details_success_template',
                       'sms_payment_details_failure_template',
                       )
        }),
    )