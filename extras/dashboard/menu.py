# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.core.urlresolvers import reverse, reverse_lazy
from django.utils.encoding import smart_str
from django.utils.translation import ugettext_lazy as _

from gateway.models import PaymentTransaction,DeletedPaymentTransaction


class MenuItem(object):
    def __init__(self, code=None, icon=None, verbose_name=None,
                 url=None, children=None, perms=None, visible=True):
        self.code = code or ''
        self.icon = icon
        self.verbose_name = verbose_name
        self.url = url
        self.children = children or []
        self.perms = perms or []
        self.visible = visible

    def __repr__(self):
        return "<Menu Item: %s>" % smart_str(self.code)

    # def is_selected(self, request):
    #     return self.url == request.path

    def has_children(self):
        return bool(self.children)


menu_items = [
    MenuItem(
        code='dashboard',
        icon='fa fa-dashboard fa-fw',
        verbose_name=_("Dashboard"),
        url=reverse_lazy('dashboard:index'),
        perms=[
            'gateway.view_statistics'
        ]
    ),
    MenuItem(
        code='pos',
        icon='fa fa-files-o fa-fw',
        verbose_name=_("POS"),
        children=[
            MenuItem(
                code='payment-request',
                verbose_name=_("Payment Request"),
                url=reverse_lazy('dashboard:create_payment_request'),
                perms=[
                    'gateway.add_paymenttransaction',
                ]
            ),
            MenuItem(
                code='bulk',
                verbose_name=_("Bulk"),
                url=reverse_lazy('dashboard:bulk:index'),
                perms=[
                    "bulk.view_bulk",
                    "bulk.view_owned_bulk",
                    "bulk.add_bulk",
                    "bulk.change_bulk",
                    "bulk.change_owned_bulk"
                ]
            )
        ]
    ),
    MenuItem(
        code='transactions',
        icon='fa fa-files-o fa-fw',
        verbose_name=_("Transactions"),
        children=[
            MenuItem(
                code='payment-requests',
                verbose_name=_("Payment requests"),
                url=reverse_lazy('dashboard:payment_requests'),
                perms=[
                    'gateway.view_paymenttransaction',
                    'gateway.view_owned_paymenttransaction',
                    'gateway.view_collaborators_paymenttransaction'
                ],
                visible=PaymentTransaction.objects.payment_requests().exists()
            ),
            MenuItem(
                code='customer-payments',
                verbose_name=_("Customer payments"),
                url=reverse_lazy('dashboard:customer_payments'),
                perms=[
                    'gateway.view_paymenttransaction',
                    'gateway.view_owned_paymenttransaction',
                    'gateway.view_collaborators_paymenttransaction'
                ],
                visible=PaymentTransaction.objects.customer_requests().exists()
            ),
            MenuItem(
                code='catalogue-payments',
                verbose_name=_("Catalogue payments"),
                url=reverse_lazy('dashboard:catalogue_payments'),
                perms=[
                    'gateway.view_catalogue_paymenttransaction'
                ],
                visible=PaymentTransaction.objects.catalogue().exists()
            ),
            MenuItem(
                code='e-commerce',
                verbose_name=_("E-Commerce"),
                url=reverse_lazy('dashboard:e_commerce_list'),
                perms=[
                    'gateway.view_paymenttransaction',
                    'gateway.view_owned_paymenttransaction',
                    'gateway.view_collaborators_paymenttransaction'
                ],
                visible=PaymentTransaction.objects.e_commerce().exists()
            ),
            MenuItem(
                code='third-party',
                verbose_name=_("Shopify"),
                url=reverse_lazy('dashboard:third_party_list'),
                perms=[
                    'gateway.view_paymenttransaction',
                    'gateway.view_owned_paymenttransaction',
                    'gateway.view_collaborators_paymenttransaction'
                ],
                visible=PaymentTransaction.objects.third_parties().exists(),
            ),
            MenuItem(
                code='deletedpaymenttransactions',
                verbose_name=_("Deleted Paid Payment Transactions"),
                url=reverse_lazy('dashboard:deleted_transactions'),
                perms=[
                    'gateway.view_deletedpaymenttransaction'
                ],
                visible=DeletedPaymentTransaction.objects.exists()
            ),
        ]
    ),
    MenuItem(
        code='support',
        icon='fa fa-support',
        verbose_name=_("Help/Support"),
        url=reverse_lazy('dashboard:help'),
        perms=[
            'gateway.add_support'
        ]
    ),
]


def get_menu_items():
    for menu_item in menu_items:
        if menu_item.has_children():
            for child in menu_item.children:
                menu_item.perms += child.perms
    return menu_items
