# -*- coding: utf-8 -*-

from __future__ import unicode_literals


from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Div


def create_payment_req_helper(form, **kwargs):
    helper = FormHelper()
    helper.layout = Layout()
    for key, value in kwargs.items():
        setattr(helper, key, value)
    for index, field in enumerate(form.fields):
        if field == 'customer_email':
            helper.layout.insert(index, Div(
                Div(Field('customer_email', css_class='form-control',
                          id='id_customer_email',), css_class='col-md-7'),
                Div(Field('default_email', css_class='checkboxinput',
                          id='id_default_email'), css_class='col-md-5'),
                css_class='row'))
            continue
        if field == 'default_email':
            continue
        helper.layout.insert(index, field)
    return helper
