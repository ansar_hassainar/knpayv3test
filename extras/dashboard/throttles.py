# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from rest_framework.throttling import UserRateThrottle


class EventRateThrottle(UserRateThrottle):
    scope = 'event'

