# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from modeltranslation.translator import translator, TranslationOptions
from .models import Field


class FieldOptions(TranslationOptions):
    fields = ('label',)

translator.register(Field, FieldOptions)