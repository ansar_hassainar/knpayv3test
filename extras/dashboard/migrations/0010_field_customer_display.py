# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-10-15 11:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0009_field_itinerary_display'),
    ]

    operations = [
        migrations.AddField(
            model_name='field',
            name='customer_display',
            field=models.BooleanField(default=False, help_text='Designates, if the field will be displayed to customer.', verbose_name='Display to Customer'),
        ),
    ]
