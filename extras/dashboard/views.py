# -* coding: utf-8 -*-

from __future__ import unicode_literals

from datetime import datetime
import pytz
import logging
import tablib
import re
from collections import defaultdict

from django.core.urlresolvers import reverse
from django.conf import settings
from django.db import transaction
from django.http import JsonResponse, Http404, HttpResponse
from django.utils.translation import ugettext_lazy as _, force_text as _ft
from django.utils.decorators import method_decorator
from django.shortcuts import get_object_or_404
from django.views import generic, View
from django.utils import timezone
from django.template import Context, Template
from utils.funcs import get_class
from django.core.mail import send_mail

import django_rq
from actstream import action
from actstream.models import Action
from braces.views import LoginRequiredMixin, PermissionRequiredMixin, MultiplePermissionsRequiredMixin
from django_filters.views import FilterMixin
from el_pagination.views import AjaxListView
from rest_framework import status
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from two_factor.views import ProfileView, BackupTokensView

from config.models import ECommerceConfig, Config, MerchantDetails
from extras.dashboard.models import CustomerPaymentFormConfig, FIELD_CHOICES
from plugins.catalogue.models import CatalogueConfig, Product
from extras.dashboard.bulk.models import BulkConfig
from gateway.models import PaymentTransaction, GatewaySettings, Gateway, DeletedPaymentTransaction
from gateway.forms import PaymentTransactionFilterForm, AllPaymentTraFilterForm
from gateway.resource import PaymentTransactionResource, ExportResource
from plugins.shopify.models import ShopifyConfig
from utils.decorators import pause_restriction
from utils.views.mixins import PageTitleMixin, DashboardMixin, \
    ActionManagerMixin, EncryptedPKMixin
from utils.funcs import get_whatsapp_send_origin
from ._utils import push_payment_transactions
from .filters import PaymentTransactionFilter, AllPaymentTranFilter, OnlyBoxFilterClass
from .forms import CreatePaymentRequestForm, PaymentRequestDetailForm, CreateHelpRequestForm
from .models import PaymentRequestFormConfig
from extras.currency.models import Currency

logger = logging.getLogger('dashboard')


def calculate_total_based_per_currencies(transactions):
    """
    Helper function for calculating total transaction value
    :param self:
    :param transactions:
    :return:
    """
    total_based_on_currencies = []
    currencies = transactions.get_currencies()
    for currency in set(currencies):
        total = transactions.get_total_by_currency_code(currency)
        total_based_on_currencies.append(
            dict(currency=currency, total=total))

    return total_based_on_currencies


class IndexView(DashboardMixin,
                # PermissionRequiredMixin,
                PageTitleMixin,
                AjaxListView):
    template_name = 'dashboard/index.html'
    page_title = _("Dashboard index")
    queryset = PaymentTransaction.reports.all()
    page_template = 'dashboard/_partials/gateway_detail.html'
    # move the permission in template
    # permission_required = 'gateway.view_statistics'

    def get_queryset(self):
        self.gateway = self.request.GET.get('gateway', None)
        self.gateways = GatewaySettings.objects.all()
        queryset = self.queryset
        if self.gateway:
            queryset = queryset.get_by_gateway_code(self.gateway)
        else:
            if self.gateways.exists():
                queryset = queryset.get_by_gateway_code(
                    self.gateways.first().code)
        return queryset

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['gateway_accounts'] = self.gateways

        if self.gateway:
            context['gateway'] = self.gateways.get(code=self.gateway)
        else:
            context['gateway'] = self.gateways.first()

        transactions = PaymentTransaction.reports.all()
        context['total_based_per_currencies'] = calculate_total_based_per_currencies(
            transactions)

        paid_transactions = transactions.paid()
        context['total_paid_based_per_currencies'] = calculate_total_based_per_currencies(
            paid_transactions)

        unpaid_transactions = transactions.unpaid()

        context['total_not_paid_based_per_currencies'] = calculate_total_based_per_currencies(
            unpaid_transactions)
        return context


class LoadCurrency(View):
    """
    This API for load currencies upon the gateway

    Rules for currencies:
    -------------------
    1. currency will be changable upon the gateway selection by front-end code
    2. if gateway has more than one currency then we will display the list of the currencies
        and select the default currency from the currency config
    3. if the gateway has one currency, it should be the default cur then it will
        be selected with hidden dropdown (by the JS code).
    4. if no selected gatway then currency will be selected with the form config currency
        and if there is no deafult currency we will not set initial to the dropdown
    5. In case of no gateway selected and only one currency is available,
        then select it by default and hide it from the form
    6. In case of no gateway selected, if multiple currencies are available, 
        list all of them and set default with first choise
    """

    def get(self, request):
        gateway_code = request.GET.get('gateway_code', "")
        data = {"currencies": [], "default": ""}
        if gateway_code:
            gateway_settings = GatewaySettings.objects.get_for_code(
                gateway_code)
        if not gateway_code or gateway_settings is None:
            form_config = PaymentRequestFormConfig.get_solo()
            choices = list(Currency.objects.choices().values('code', 'code'))
            data['currencies'] = choices
            data['default'] = choices[0]['code']
            return JsonResponse(data)
        config = gateway_settings.currency_config
        currencies_list = list(
            config.currencies.filter().values('code', 'code'))
        default_currency = {"code": config.default_currency.code}
        if default_currency not in currencies_list:
            currencies_list.append(default_currency)
        data['currencies'] = currencies_list
        data['default'] = config.default_currency.code
        return JsonResponse(data)


@method_decorator(pause_restriction(), name='dispatch')
@method_decorator(transaction.atomic, name='form_valid')
class CreatePaymentRequestView(DashboardMixin,
                               PermissionRequiredMixin,
                               PageTitleMixin,
                               generic.FormView):
    template_name = 'dashboard/payment_request.html'
    page_title = _("Create payment request")
    form_class = CreatePaymentRequestForm
    permission_required = 'gateway.add_paymenttransaction'

    def get_success_url(self):
        return reverse('dashboard:payment_request_detail', args=(self.object.pk,))

    def get_form_kwargs(self):
        kwargs = super(CreatePaymentRequestView, self).get_form_kwargs()
        kwargs.update({
            'request': self.request,
            'config': PaymentRequestFormConfig.get_solo()
        })
        return kwargs

    def form_invalid(self, form):
        data = {'errors': form.errors_as_dict()}
        return JsonResponse(data, status=status.HTTP_400_BAD_REQUEST)

    def form_valid(self, form):
        self.object = form.save()
        generate_qr = self.request.POST.get('generate_qr_code', None)
        if generate_qr:
            self.object.generate_qr_code()

        return JsonResponse({'redirect_url': self.get_success_url()})

    def get_context_data(self, **kwargs):
        context = super(
            CreatePaymentRequestView, self).get_context_data(**kwargs)
        context[
            'default_email'] = PaymentRequestFormConfig.get_solo().default_email
        return context


class HelpSupportView(DashboardMixin, PageTitleMixin, generic.FormView):
    template_name = 'dashboard/help_support.html'
    page_title = _("Help/Support")
    form_class = CreateHelpRequestForm

    def form_valid(self, form):
        user_email = form.cleaned_data['email']
        from_email = settings.DEFAULT_FROM_EMAIL
        subject = dict((x, y) for x, y in form.SUBTITLE_CHOICES)[
            form.cleaned_data['subject']]
        merchant_details = MerchantDetails.get_solo()
        body = """Body:  %s
        This is submitter email: %s
        This is merchant email: %s""" % (form.cleaned_data['message'], user_email, merchant_details.email)
        recipients = settings.SUPPORT_SUBJECTS_EMAILS[form.cleaned_data['subject']]
        try:
            send_mail(subject, body, from_email, recipients,
                      fail_silently=False)
            recipients = [user_email]
            send_mail("%s Submitted !" % subject, "Thanks for your time. Our support team will reply you shortly", from_email, recipients,
                      fail_silently=False)
        except:
            return JsonResponse(dict(success=False, message=_ft(_('An error occured. Please try again'))))
        return JsonResponse(dict(success=True, message=_ft(_('Successfully sent.'))))

    def form_invalid(self, form):
        data = {'errors': form.errors_as_dict()}
        return JsonResponse(data, status=status.HTTP_400_BAD_REQUEST)


class PaymentRequestDetailView(DashboardMixin,
                               PageTitleMixin,
                               generic.UpdateView):
    template_name = 'dashboard/payment_request_detail.html'
    page_title = _("Payment request detail")
    queryset = PaymentTransaction.objects.all()
    form_class = PaymentRequestDetailForm

    def get_form_kwargs(self):
        kwargs = super(PaymentRequestDetailView, self).get_form_kwargs()
        kwargs.update({'config': PaymentRequestFormConfig.get_solo()})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(PaymentRequestDetailView,
                        self).get_context_data(**kwargs)
        context['qr_code'] = self.object.qr_code
        context['whatsapp_send_url'] = get_whatsapp_send_origin(self.request)
        if self.object.attachment:
            context['has_attachment'] = True
        return context


class PaymentInvoiceTemplateView(EncryptedPKMixin,
                                 generic.TemplateView):
    template_name = 'dashboard/payment_confirm_pdf.html'
    queryset = PaymentTransaction.objects.all()

    def get_config_fields(self, *args):
        fields_list = defaultdict()
        for field in args:
            fields_list[field.get_name()] = field.get_label()

        if 'attachment' in fields_list:
            del fields_list['attachment']
        return fields_list

    def get_context_data(self, **kwargs):
        context = super(PaymentInvoiceTemplateView,
                        self).get_context_data(**kwargs)
        transaction_obj = self.get_object()
        context.update(transaction_obj._get_notification_context())

        itinerary_fields_list = None
        itinerary_fields_data = defaultdict()
        if transaction_obj.type in [transaction_obj.CATALOGUE,
                                    transaction_obj.CUSTOMER_PAYMENT,
                                    transaction_obj.PAYMENT_REQUEST]:
            config = transaction_obj._get_transaction_config()
            itinerary_fields = config.fields.itinerary()
            itinerary_fields_list = self.get_config_fields(*itinerary_fields)

        try:
            payment_attempt = transaction_obj.get_paid_attempt()
            if payment_attempt:
                # Complex logic for showing FUll Name
                # checks built in fields and custom fields
                full_name = transaction_obj.extra.pop('full_name', None)
                first_name = getattr(payment_attempt.transaction,
                                     'customer_first_name')
                last_name = getattr(payment_attempt.transaction,
                                    'customer_last_name')
                customer_full_name = ' '.join([first_name, last_name])
                if customer_full_name.strip():
                    context['customer_name'] = customer_full_name
                elif full_name:
                    context['customer_name'] = full_name

                product_id = transaction_obj.extra.get('product', None)
                quantity = transaction_obj.extra.get('quantity', None)
                if product_id:
                    try:
                        context['product'] = Product.objects.get(pk=product_id)
                        context['quantity'] = quantity
                    except Product.DoesNotExist:
                        pass

                # reference no
                order_id = None
                for field in PaymentTransaction.CUSTOMER_FIELDS:
                    val = getattr(payment_attempt.transaction, field)
                    if field == 'order_no' and val:
                        order_id = val
                    if field in itinerary_fields_list.keys() and val:
                        itinerary_fields_data[itinerary_fields_list[field]] = val

                # default email condition
                has_default_email = False
                for key, value in transaction_obj.extra.items():
                    if key == 'order_id' and not order_id:
                        order_id = value
                    if key == 'has_default_email':
                        has_default_email = True
                    if key in itinerary_fields_list.keys() and val:
                        itinerary_fields_data[itinerary_fields_list[key]] = val

                context['order_id'] = order_id
                context['itinerary_data'] = itinerary_fields_data

                if not has_default_email:
                    context['customer_email'] = transaction_obj.customer_email

                ctx = {
                    'reference_number': payment_attempt.reference_number,
                    'state': payment_attempt.get_state_display(),
                    'gateway_response': payment_attempt.gateway_response,
                    'gateway_code': payment_attempt.settings.code if payment_attempt.settings else transaction_obj.gateway_code,
                    'attempt': payment_attempt,
                    'transaction_obj': transaction_obj
                }
                context.update(ctx)
        except Exception as e:
            logger.exception("Error while getting invoice html context %s" % e)

        c = Config.get_solo()
        if transaction_obj.type == transaction_obj.CATALOGUE and \
                c.attach_catalogue_invoice_pdf:
            catalogue_invoice_html = c.catalogue_invoice_pdf_template
            t = Template(catalogue_invoice_html)
            temp_context = Context(context)
            context['invoice_html'] = t.render(temp_context)
        elif c.attach_invoice_pdf:
            invoice_html = c.invoice_pdf_template
            t = Template(invoice_html)
            temp_context = Context(context)
            context['invoice_html'] = t.render(temp_context)

        return context


class BaseTransactionListView(DashboardMixin,
                              PageTitleMixin,
                              MultiplePermissionsRequiredMixin,
                              FilterMixin,
                              AjaxListView):
    template_name = 'dashboard/transactions.html'
    page_template = 'dashboard/ajax/transactions.html'
    resource = ExportResource
    model = PaymentTransaction
    allowed_actions = ['cancel', 'delete']
    transaction_config = None
    supported_format = {'csv': 'text/csv',
                        'xls': 'application/vnd.ms-excel'}
    box_filterset_class = OnlyBoxFilterClass
    filterset_class = PaymentTransactionFilter
    page_title = _("Transactions")
    permissions = {
        "any": ("gateway.view_paymenttransaction",
                "gateway.view_owned_paymenttransaction",
                "gateway.view_collaborators_paymenttransaction"
                )
    }
    default_export_fields = ['type', 'id', 'state', 'gateway_code', 'amount',
                             'currency_code', 'reference_number',
                             'payment_date']
    custom_fields_list = []
    builtin_fields_list = []
    other_fields = []

    def get_queryset(self):
        qs = super(BaseTransactionListView, self).get_queryset()
        return qs.get_for_user(self.request.user)

    def get(self, request, *args, **kwargs):
        # ToDO: Refactor this method

        whatsapp_send_url = get_whatsapp_send_origin(request)

        filterset_class = self.get_filterset_class()
        self.filterset = self.get_filterset(filterset_class)
        self.object_list = self.filterset.qs

        # To decide which extra fields to add in export
        payment_types = self.object_list.values_list('type', flat=True)
        self.payment_type = payment_types[0] if payment_types else PaymentTransaction.PAYMENT_REQUEST

        context = self.get_context_data(
            object_list=self.object_list,
            transaction_config=self.transaction_config.get_solo(),
            page_template=self.page_template,
            filter=self.filterset,
            transactions=self.object_list,
            transaction_filter_form=PaymentTransactionFilterForm(
                self.object_list),
            whatsapp_send_url=whatsapp_send_url,
            allowed_actions=self.allowed_actions,
        )

        context.update(self.get_builtin_custom_fields())

        # analytics boxes logic starts here
        # As per the Enhanced reporting SRS, change boxes value when
        # gateway_code or type or created_by selected
        transactions = self.get_queryset()
        gateway_code = request.GET.get('gateway_code', None)
        created_0 = request.GET.get('created_0', None)
        created_1 = request.GET.get('created_1', None)
        modified_0 = request.GET.get('modified_0', None)
        modified_1 = request.GET.get('modified_1', None)
        type = request.GET.get('type', None)
        initiator = request.GET.get('initiator', None)

        if request.user.has_perm('gateway.view_paymenttransaction_statistics'):
            box_filter_required_fields = [gateway_code, type, initiator,
                                          created_0, created_1,
                                          modified_0, modified_1]
            required_fields_sum = sum(
                [1 for d in (box_filter_required_fields) if d])

            context.update(self.get_boxes_analytics(required_fields_sum,
                                                    transactions))

        # 'csv' or 'xls' else send csv by default
        self.file_format = self.request.GET.get('file_format', 'csv')

        # can be 'selected' or 'default'
        self.export_type = request.GET.get('export_type')

        if self.export_type:
            self.other_fields.append('gateway_response')
            export_fields = self.builtin_fields_list.keys(
            ) + self.custom_fields_list.keys() + self.other_fields
            user_fields = (request.GET.get('fields')).split(',')
            valid_field_regex = re.compile('[a-zA-Z_]')
            selected_fields = [field for field in user_fields if valid_field_regex.match(
                field) and field in export_fields]
            return self.render_to_report_fields(selected_fields if self.export_type == 'selected' else export_fields)
        return self.render_to_response(context)

    def get_file_format(self):
        if self.file_format in self.supported_format.keys():
            return self.file_format, self.supported_format[self.file_format]
        else:
            raise Http404()

    def render_to_report_fields(self, fields=None):
        if 'gateway_response' in fields:
            gateway_response_fields = self.get_gateway_response_fields()
            fields.remove('gateway_response')
        else:
            gateway_response_fields = []

        fields = self.default_export_fields + fields

        # Extra is eligible for catalogue, For e-commerce, customer payment,
        # and payment-request extra fields are covered inside the custom fields
        extra_fields = []
        if 'extra' in fields and self.payment_type == PaymentTransaction.CATALOGUE:
            extra_fields += ['product', 'product_name', 'product_type',
                             'quantity']
            fields.remove('extra')
            fields += extra_fields

        # Add custom fields of configuration in extra_fields
        extra_fields += self.custom_fields_list.keys()

        # logic for adding initiator field
        if self.payment_type == PaymentTransaction.PAYMENT_REQUEST:
            fields.append('initiator')

        # To add label of fields
        fields_verbose_list = {f.name: _ft(
            f.verbose_name) for f in PaymentTransaction._meta.fields}
        custom_fields_verbose_list = {k: v for k,
                                      v in self.custom_fields_list.items()}
        fields_verbose_list.update(custom_fields_verbose_list)
        final_verbose_fields = [
            fields_verbose_list.get(key, key) for key in fields]

        file_format, content_type = self.get_file_format()
        resource = ExportResource(self.object_list, fields,
                                  final_verbose_fields,
                                  gateway_response_fields, extra_fields,
                                  file_format)
        resource.construct_dump()
        response = HttpResponse(resource.dump(), content_type=content_type)
        response['Content-Disposition'] = 'attachment; filename=%s' % (
            self.get_export_filename(file_format),
        )
        return response

    def get_export_filename(self, file_format):
        date_str = datetime.now().strftime('%Y-%m-%d')
        filename = "%s-%s.%s" % (self.model.__name__,
                                 date_str,
                                 file_format)
        return filename

    def get_builtin_custom_fields(self):
        """
        Custom fields list and built-in fields list to populate in
        Export fields popover
        """
        payment_req_fields = PaymentRequestFormConfig.get_solo().fields
        bulk_fields = BulkConfig.get_solo().fields
        customer_fields = CustomerPaymentFormConfig.get_solo().fields
        catalogue_fields = CatalogueConfig.get_solo().fields

        custom_config_data = []
        builtin_config_data = []
        if self.payment_type == PaymentTransaction.CATALOGUE:
            custom_config_data.append(catalogue_fields.custom())
            builtin_config_data.append(catalogue_fields.builtin())
            self.other_fields = ['extra']
        elif self.payment_type == PaymentTransaction.PAYMENT_REQUEST:
            builtin_config_data += [payment_req_fields.builtin(),
                                    bulk_fields.builtin()]
            custom_config_data += [payment_req_fields.custom(),
                                   bulk_fields.custom()]
        elif self.payment_type == PaymentTransaction.CUSTOMER_PAYMENT:
            custom_config_data.append(customer_fields.custom())
            builtin_config_data.append(customer_fields.builtin())

        self.builtin_fields_list = self.get_config_fields(*builtin_config_data)
        self.custom_fields_list = self.get_config_fields(*custom_config_data)

        data = {'custom_fields_list': self.custom_fields_list,
                'builtin_fields_list': self.builtin_fields_list,
                'other_fields': self.other_fields}
        return data

    def get_boxes_analytics(self, required_fields_sum, transactions):
        # logic for filtering on boxes
        if required_fields_sum >= 1:
            box_filterset_class = self.box_filterset_class
            box_filterset = self.get_filterset(box_filterset_class)
            transactions = box_filterset.qs

        paid_transactions = transactions.paid()
        unpaid_transactions = transactions.unpaid()
        data = {'total_based_per_currencies': calculate_total_based_per_currencies(transactions),
                'total_paid_based_per_currencies': calculate_total_based_per_currencies(paid_transactions),
                'total_not_paid_based_per_currencies': calculate_total_based_per_currencies(unpaid_transactions)}

        return data

    def get_config_fields(self, *args):
        fields_list = defaultdict()
        for payment_type in args:
            for field in payment_type:
                fields_list[field.get_name()] = field.get_label()

        if 'attachment' in fields_list:
            del fields_list['attachment']
        return fields_list

    def get_gateway_response_fields(self):
        """
        Return gateway_response keys from unique paid transaction of each gateway
        """
        keys = set()
        gateway_codes = list(self.object_list.values_list(
            'gateway_code', flat=True).distinct().order_by())
        for gateway in gateway_codes:
            try:
                paid_transaction = self.object_list.filter(gateway_code=gateway,
                                                           state=PaymentTransaction.PAID).last()
                if paid_transaction:
                    s = set(
                        k for k in paid_transaction.get_paid_attempt().gateway_response.keys())
                    keys = keys.union(s)

                # Below code is inspired by latest requirement of showing all
                # state's gateway_response in export report file.
                # Mostly are corner cases.
                for state in [PaymentTransaction.FAILED, PaymentTransaction.ATTEMPTED,
                              PaymentTransaction.CANCELED, PaymentTransaction.INVALID]:
                    state_transaction = self.object_list.filter(
                        gateway_code=gateway, state=state).last()

                    if state_transaction:
                        state_attempt = state_transaction.attempts
                        if state_attempt.exists() and state_attempt.last(): 
                            s = set(k for k in state_attempt.last().gateway_response.keys())
                            keys = keys.union(s)

            except Exception as e:
                logger.exception(e)
                pass
        return list(keys)


class PaymentRequestListView(BaseTransactionListView):
    queryset = PaymentTransaction.objects.payment_requests()
    transaction_config = PaymentRequestFormConfig


class CataloguePaymentListView(BaseTransactionListView):
    queryset = PaymentTransaction.objects.catalogue()
    transaction_config = CatalogueConfig
    page_template = 'dashboard/ajax/catalogue_transactions.html'
    permissions = {
        "any": ("gateway.view_catalogue_paymenttransaction",)
    }

    def get_queryset(self):
        qs = super(BaseTransactionListView, self).get_queryset()
        return qs


class CustomerPaymentListView(BaseTransactionListView):
    queryset = PaymentTransaction.objects.customer_requests()
    transaction_config = CustomerPaymentFormConfig


class ECommerceListView(BaseTransactionListView):
    queryset = PaymentTransaction.objects.e_commerce()
    transaction_config = PaymentRequestFormConfig


class ThirdPartyListView(BaseTransactionListView):
    queryset = PaymentTransaction.objects.third_parties()
    transaction_config = ShopifyConfig


class AllPaymentListView(BaseTransactionListView):
    queryset = PaymentTransaction.objects.all()
    filterset_class = AllPaymentTranFilter
    box_filterset_class = OnlyBoxFilterClass
    resource = PaymentTransactionResource
    page_template = 'dashboard/ajax/all_transactions.html'
    template_name = 'dashboard/all_transactions.html'

    def get_queryset(self):
        queryset = super(AllPaymentListView, self).get_queryset()
        year = self.kwargs.get('year')
        month = self.kwargs.get('month')
        day = self.kwargs.get('day')
        # ToDO: Add DateMixin and refactor
        try:
            start_date = datetime.now().replace(year=int(year), month=int(month), day=int(day),
                                                hour=0, minute=0, second=0,
                                                microsecond=0)
            end_date = datetime.now().replace(year=int(year), month=int(month), day=int(day),
                                              hour=23, minute=59, second=59,
                                              microsecond=59)

            queryset = queryset.filter(created__range=[start_date, end_date])
        except Exception as e:
            logger.exception(e)
            raise Http404("Looks like you've hit the wall.")

        return queryset

    def get(self, request, *args, **kwargs):
        # ToDO: Refactor this method
        filterset_class = self.get_filterset_class()
        self.filterset = self.get_filterset(filterset_class)
        self.object_list = self.filterset.qs

        # To decide which extra fields to add in export
        payment_types = self.object_list.values_list('type', flat=True)
        self.payment_type = payment_types[0] if payment_types else PaymentTransaction.PAYMENT_REQUEST

        context = self.get_context_data(
            object_list=self.object_list,
            page_template=self.page_template,
            filter=self.filterset,
            transactions=self.object_list,
            transaction_filter_form=AllPaymentTraFilterForm(self.object_list),
        )

        context.update(self.get_builtin_custom_fields())

        # current page logic starts here
        # As per the Enhanced reporting SRS, change boxes value when
        # gateway_code or type or created_by selected
        transactions = self.get_queryset()
        gateway_code = request.GET.get('gateway_code', None)
        type = request.GET.get('type', None)
        initiator = request.GET.get('initiator', None)

        required_fields_sum = sum(
            [1 for d in (gateway_code, type, initiator) if d])

        context.update(self.get_boxes_analytics(required_fields_sum,
                                                transactions))

        # 'csv' or 'xls' else send csv by default
        self.file_format = self.request.GET.get('file_format', 'csv')

        # can be 'selected' or 'default'
        self.export_type = request.GET.get('export_type', None)
        if self.export_type:
            self.other_fields.append('gateway_response')
            export_fields = self.builtin_fields_list.keys(
            ) + self.custom_fields_list.keys() + self.other_fields
            user_fields = (request.GET.get('fields')).split(',')
            valid_field_regex = re.compile('[a-zA-Z_]')
            selected_fields = [field for field in user_fields if valid_field_regex.match(
                field) and field in export_fields]
            return self.render_to_report_fields(selected_fields if self.export_type == 'selected' else export_fields)
        return self.render_to_response(context)


@pause_restriction()
@api_view(['GET'])
@renderer_classes((JSONRenderer,))
def send_communication_event(request, transaction_pk, event_type):
    """
    # Payment Request Send Notification to user #

    ** GET: /send-communication-event/{transaction_pk}/{event_type} **

        {
            'message': "Customer notified."
        }

    ** legends **

        transaction_pk: transaction object's id (integer)
        event_type: email, sms, push, all (String)

    """
    if event_type not in ('sms', 'email', 'push', 'all'):
        raise Http404()

    payment_transaction = get_object_or_404(
        PaymentTransaction, pk=transaction_pk)
    kwargs = {'sms': True, 'email': True,
              'push': True} if event_type == 'all' else {event_type: True}
    payment_transaction.send_payment_request_notifications(**kwargs)
    event = event_type if not event_type == 'all' else 'email & sms & push'
    try:
        # Added try except block because of no authentication.
        action.send(request.user, verb='sent %s' %
                    event, action_object=payment_transaction)
    except:
        pass
    return Response({"message": _("Customer notified.")})


class ActionListView(DashboardMixin,
                     PageTitleMixin,
                     AjaxListView):
    template_name = 'dashboard/actions.html'
    page_template = 'dashboard/ajax/actions.html'
    queryset = Action.objects.all()
    context_object_name = 'actions'


class PaymentTransactionGraphView(FilterMixin, generic.ListView):
    queryset = PaymentTransaction.reports.all()
    model = PaymentTransaction
    filterset_class = PaymentTransactionFilter
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):

        filterset_class = self.get_filterset_class()

        self.start_date = self.request.GET.get('created_0', None)
        self.end_date = self.request.GET.get('created_1', None)
        if all([self.start_date, self.end_date]):
            self.start_date = datetime.strptime(self.start_date, '%m/%d/%Y')
            self.end_date = datetime.strptime(self.end_date, '%m/%d/%Y')
            filterset_class.start_date = self.start_date
            filterset_class.end_date = self.end_date

        self.filterset = self.get_filterset(filterset_class)
        self.object_list = self.filterset.qs

        payload = self.get_payload(self.object_list, self.filterset.period)
        return JsonResponse(payload)

    def get_payload(self, transactions, period):
        data = {
            'ykeys': ['a'],
            'labels': [_ft(_('Transactions')), _ft(_('Total')), _ft(_('Total Generated'))],
            'data': self.create_data(transactions, period)
        }
        return data

    def create_data(self, transactions, period):
        payload = []
        if len(transactions) > 0:
            for result in transactions:
                start_time = datetime.combine(
                    result[period], timezone.now().time().replace(0, 0, 0, 0))
                end_time = datetime.combine(
                    result[period], timezone.now().time().replace(23, 59, 59, 59))
                period_transactions = self.queryset.filter(created__range=[start_time.replace(
                    tzinfo=pytz.UTC), end_time.replace(tzinfo=pytz.UTC)])
                currency_data = self.calculate_total_based_per_currencies(
                    period_transactions)
                payload.append(
                    dict(y=result[period].strftime("%m/%d/%Y"), a=result['pk__count'], b=currency_data))
        else:
            if not self.start_date:
                payload.append(
                    dict(y=datetime.now().date().strftime("%m/%d/%Y"), a=0, b=0))
            else:
                payload.append(
                    dict(y=self.start_date.strftime("%m/%d/%Y"), a=0, b=0))
        return payload

    def calculate_total_based_per_currencies(self, transactions):
        total_based_on_currencies = []
        currencies = transactions.get_currencies()
        for currency in set(currencies):
            total = transactions.get_total_by_currency_code(currency)
            total_based_on_currencies.append(str(total)+" "+currency)
        return total_based_on_currencies


class PushPaymentTransactionViewSet(generic.View):
    """
    View used to fetch the transactions for KNPay DASHBOARD
    """
    http_method_names = ['get']
    queryset = PaymentTransaction.objects.all()

    def get(self, request, *args, **kwargs):
        installation = self.request.GET.get('installation', None)
        token = self.request.GET.get('token', None)
        if not installation or token != settings.CENTRAL_AUTH_TOKEN:
            return Http404()

        django_rq.enqueue(
            push_payment_transactions, self.queryset, installation)

        return JsonResponse(dict(status=True))


class CancelTransactionView(LoginRequiredMixin, PermissionRequiredMixin, generic.DetailView):
    http_method_names = ['get']
    model = PaymentTransaction

    def check_permissions(self, request):
        return (
            request.user.is_superuser or
            request.user.has_perm("gateway.cancel_paymenttransaction") or
            (request.user.has_perm("gateway.cancel_owned_paymenttransaction")
             and self.get_object().initiator == request.user)
        )

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            self.object = self.get_object()
            if self.object.can_be_cancelled():
                self.object.cancel()
                self.object.save()
                # self.object.refresh_from_db()
                return JsonResponse(dict(status=_ft(_("CANCELED")),
                                         success=True,
                                         message=_ft(_('Successfully cancelled transaction'))))
            else:
                return JsonResponse(dict(message=_ft(_('Transaction cannot be cancelled'))))
        return Http404()


class TransactionActionView(LoginRequiredMixin, ActionManagerMixin, generic.ListView):
    http_method_names = ['get']
    queryset = PaymentTransaction.objects.all()


class AuthSettingsView(DashboardMixin, ProfileView):
    """
    2fa's profile view extended to match dashboard design
    """
    template_name = 'two_factor/profile/profile.html'


class BackUpTokensView(DashboardMixin, BackupTokensView):
    """
    2fa's backup_token view extended to match dashboard design
    """
    template_name = 'two_factor/core/backup_tokens.html'


class DeletedTransactionsListView(DashboardMixin, PageTitleMixin,
                                  MultiplePermissionsRequiredMixin,
                                  AjaxListView):
    template_name = 'dashboard/transactions.html'
    page_template = 'dashboard/ajax/transactions.html'
    model = DeletedPaymentTransaction
    page_title = _("Deleted Transactions")
    allowed_actions = ['restore']
    permissions = {
        "any": ("gateway.view_deletedpaymenttransaction",)
    }

    def get_queryset(self):
        return DeletedPaymentTransaction.objects.all()

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        context = self.get_context_data(
            object_list=self.object_list,
            page_template=self.page_template,
            transactions=self.object_list,
            allowed_actions=self.allowed_actions,
            transaction_filter_form=None,
        )
        return self.render_to_response(context)
