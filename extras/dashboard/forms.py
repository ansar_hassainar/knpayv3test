# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

from django import forms
from django.utils.translation import ugettext_lazy as _
from django.core import validators

from actstream import action
from intl_tel_input.widgets import IntlTelInputWidget

from sms.models import SMSAuthConfig
from gateway.models import PaymentTransaction
from utils.forms import BasePaymentForm, AbstractBasePaymentRequestConfigForm, AjaxBaseForm
from .models import Field
from .helpers import create_payment_req_helper

logger = logging.getLogger('dashboard')


class BasePaymentRequestForm(BasePaymentForm):
    """
    Manages sms confirmation
    Must be used in conjunction with BasePaymentForm due to self.config
    requirement.
    """
    sms_notification = forms.BooleanField(
        label=_("SMS notification"), required=False
    )
    sms_payment_details = forms.BooleanField(
        widget=forms.HiddenInput(), required=False)

    def __init__(self, *args, **kwargs):
        super(BasePaymentRequestForm, self).__init__(*args, **kwargs)

    def clean_sms_notification(self):
        sms_notification = self.cleaned_data.get('sms_notification', False)
        if sms_notification and not self.has_phone_field:
            raise forms.ValidationError(
                _("SMS cannot be sent because phone number field is missing from the form."
                  " Contact an administrator to enable it."))
        return sms_notification


class CreatePaymentRequestForm(AbstractBasePaymentRequestConfigForm,
                               BasePaymentRequestForm):
    """
    Main form for creating a payment transaction from frontend
    """
    knpay_section = PaymentTransaction.PAYMENT_REQUEST
    transaction_type = PaymentTransaction.PAYMENT_REQUEST
    generate_qr_code = forms.BooleanField(label=_('Generate QR Code'),
                                          required=False)

    def __init__(self, *args, **kwargs):
        super(CreatePaymentRequestForm, self).__init__(*args, **kwargs)

        if hasattr(self.config, 'default_email'):
            if self.config.default_email:
                self.fields['default_email'] = forms.BooleanField(
                    label=_('Use default email'), required=False)

        for field in self.config.fields.active():
            self.add_defined_field(field.get_name(), label=field.get_label(
            ), required=field.required, field=field)

        # Hide SMS Checkbox if there is no Active SMS Provider available
        if not SMSAuthConfig.objects.current():
            self.fields['sms_notification'].widget = forms.HiddenInput()
        else:
            self.fields['sms_notification'].help_text = _("Notify the customer via SMS that "
                                                          "he has to pay an invoice. Builtin "
                                                          "phone field has to be present on "
                                                          "the form.")

        if 'customer_phone' in self.fields:
            self.fields['customer_phone'].widget = IntlTelInputWidget(default_code='kw',
                                                                      preferred_countries=['kw', 'ro', 'in'])

        self.helper = create_payment_req_helper(self)
        self.helper.include_media = False

    def save(self):
        user = self.request.user
        obj = self.serializer.save(initiator=user)
        action.send(user, verb='created', action_object=obj)
        return obj

    def get_data_for_api(self):
        data = super(CreatePaymentRequestForm, self).get_data_for_api()
        # If staff has selected 'Use default email' then add a flag in extra
        if data.get('default_email', None):
            if data.get('extra', None):
                data['extra'].update({'has_default_email': 'true'})
            else:
                data['extra'] = {'has_default_email': 'true'}

        return data


class CreateHelpRequestForm(AjaxBaseForm, forms.Form):
    SUBTITLE_CHOICES = (
        ("support_request", _("Support Request")),
        ("suggestion", _("Suggestion")),
        ("feedback", _("Feedback")),
    )
    email = forms.EmailField(label=_("Email"))
    subject = forms.ChoiceField(label=_("Subject"), choices=SUBTITLE_CHOICES)
    message = forms.CharField(
                    label='Message',
                    widget=forms.Textarea(attrs={'data-override': 'content'})
                )
    def __init__(self, *args, **kwargs):
        super(CreateHelpRequestForm, self).__init__(*args, **kwargs)
        

class PaymentRequestDetailForm(BasePaymentRequestForm,
                               forms.ModelForm):

    class Meta:
        model = PaymentTransaction
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(PaymentRequestDetailForm, self).__init__(*args, **kwargs)
        extra = self.instance.extra
        self.fields['currency_code'] = forms.CharField(
            label=_("Currency"), required=False)
        for key, value in extra.items():
            field = Field.objects.get_for_name(key, self.config)
            label = field.get_label() if field is not None else key
            self.add_defined_field(
                key, label=label, initial=value, field=field)

        for field in PaymentTransaction.CUSTOMER_FIELDS:
            val = getattr(self.instance, field)
            if val:
                self.add_defined_field(field, label=PaymentTransaction._meta.get_field(field).verbose_name,
                                       initial=val)
        if self.fields.get('attachment'):
            self.fields['attachment'].widget = forms.HiddenInput()

        # Hide SMS Checkbox if sms_notification not selected
        if not getattr(self.instance, 'sms_notification'):
            self.fields['sms_notification'].widget = forms.HiddenInput()

        for field in self.fields:
            self.fields[field].disabled = True
