# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django import forms
from django.contrib.auth import password_validation
from django.contrib.auth.forms import AuthenticationForm as AbstractAuthenticationForm
from django.contrib.auth.forms import UserCreationForm as BaseUserCreationForm
from django.utils.translation import ugettext_lazy as _

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

from utils.forms import AjaxBaseForm


class AuthenticationForm(AjaxBaseForm, AbstractAuthenticationForm):
    @property
    def helper(self):
        helper = FormHelper()
        helper.form_id = 'login-form'
        helper.add_input(Submit('submit', _("Login")))
        return helper


class UserCreationForm(BaseUserCreationForm):
    password1 = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput,
        help_text=password_validation.password_validators_help_text_html(),
    )

#
# form = UserChangeForm
# add_form = UserCreationForm