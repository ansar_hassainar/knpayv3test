# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from braces.views import AnonymousRequiredMixin, LoginRequiredMixin
from django.contrib.auth import REDIRECT_FIELD_NAME, login as auth_login, \
    logout as auth_logout
from django.core.urlresolvers import reverse_lazy
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.utils.http import is_safe_url
from django.utils.text import force_text
from django.utils.translation import ugettext_lazy as _
from django.views import generic
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from rest_framework import status, serializers, permissions
from rest_framework.authtoken.models import Token
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from utils.views.mixins import PageTitleMixin
from .forms import AuthenticationForm
from .permissions import IsAnonymous
from .serializers import AuthSerializer


class LoginView(PageTitleMixin, AnonymousRequiredMixin, generic.FormView):
    """
    Provides the ability to login as a user with a username and password
    """
    success_url = reverse_lazy('dashboard:index')
    form_class = AuthenticationForm
    redirect_field_name = REDIRECT_FIELD_NAME
    template_name = 'account/login.html'
    page_title = _("Login")
    hide_navbar = True

    @method_decorator(sensitive_post_parameters('password'))
    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        # Sets a test cookie to make sure the user has cookies enabled
        request.session.set_test_cookie()
        return super(LoginView, self).dispatch(request, *args, **kwargs)

    def get_authenticated_redirect_url(self):
        return '/%s/' % self.request.LANGUAGE_CODE
        
    def get_form_kwargs(self):
        kw = super(LoginView, self).get_form_kwargs()
        kw.update({'request': self.request})
        return kw

    def form_valid(self, form):
        auth_login(self.request, form.get_user())

        # If the test cookie worked, go ahead and
        # delete it since its no longer needed
        if self.request.session.test_cookie_worked():
            self.request.session.delete_test_cookie()

        return JsonResponse(
            {'redirect_url': force_text(self.get_success_url())}
        )

    def form_invalid(self, form):
        data = {'errors': form.errors_as_dict()}
        return JsonResponse(data, status=status.HTTP_400_BAD_REQUEST)

    def get_success_url(self):
        redirect_to = self.request.GET.get(self.redirect_field_name)
        if not is_safe_url(url=redirect_to, host=self.request.get_host()):
            redirect_to = self.success_url
        return redirect_to


class LogoutView(LoginRequiredMixin, generic.RedirectView):
    """
    Provides users the ability to logout
    """
    url = reverse_lazy('two_factor:login')

    def get(self, request, *args, **kwargs):
        auth_logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)


class LoginApiView(GenericAPIView):
    """
    #Authentication endpoint.#
    *Returns token if credentials provided are valid and merge open basket if any.*

    **POST request example:**

    * /api/v1/login/

        {
            "username": "testing",
            "password": "1234567a"
        }

    **Response example:**

        {
            "token":"25c5c4bcc7a667d910548f3d601f4da5696b2801",
            "user_id":1
        }

    ##Fields legend:##

        * username - string (required)
        * password - string (required)
    """
    permission_classes = [IsAnonymous]
    serializer_class = AuthSerializer

    def post(self, request):
        serializer = self.serializer_class(request=request, data=request.data)
        if serializer.is_valid():
            return Response(serializer.validated_data)
        raise serializers.ValidationError(serializer.errors)


class LogoutApiView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        """
        # Logout ENDPOINT #
        ** NOTE ** Send GET request with auth token.
        **NOTE** Wih this request, old token will be deleted.
        So, You'll have to login again to get a new token.
        :param request:
        :return: 200 ok
        """

        Token.objects.filter(user=request.user).delete()

        auth_logout(request)
        return Response(status=status.HTTP_200_OK)

    def post(self, request):
        return self.get(request)
