# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import re

from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _


class SpecialCharValidator:
    """
    Validate whether the password has the minimum special chars required.
    """
    def __init__(self, min_special_chars=1):
        self.min_special_chars = min_special_chars

    def validate(self, password, user=None):
        pattern = r'\W'
        chars = re.findall(pattern, password)
        if len(chars) < self.min_special_chars:
            raise ValidationError(_("This password doesn't contain "
                                    "at least %d special character.")
                                  % self.min_special_chars)

    def get_help_text(self):
        return _("Your password must contain at least %d "
                 "special characters.") % self.min_special_chars


class DigitCountValidator:
    """
    Validate whether the password has the minimum digits required.
    """
    def __init__(self, min_digits=1):
        self.min_digits = min_digits

    def validate(self, password, user=None):
        pattern = r'\d'
        digits = re.findall(pattern, password)
        if len(digits) < self.min_digits:
            raise ValidationError(_("This password doesn't contain "
                                    "at least %d digits.")
                                  % self.min_digits)

    def get_help_text(self):
        return _("Your password must contain at least %d "
                 "digits.") % self.min_digits


class UpperCaseValidator:
    """
    Validate whether the password has the minimum uppercase letters
    required.
    """
    def __init__(self, min_letters=1):
        self.min_letters = min_letters

    def validate(self, password, user=None):
        pattern = r'[A-Z]'
        letters = re.findall(pattern, password)
        if len(letters) < self.min_letters:
            raise ValidationError(_("This password doesn't contain "
                                    "at least %d upper case letters.")
                                  % self.min_letters)

    def get_help_text(self):
        return _("Your password must contain at least %d "
                 "upper case letter.") % self.min_letters
