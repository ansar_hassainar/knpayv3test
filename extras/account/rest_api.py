# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from .views import LoginApiView, LogoutApiView

urlpatterns = [
    url(r'^login', LoginApiView.as_view(), name='login'),
    url(r'^logout', LogoutApiView.as_view(), name='logout'),
]
