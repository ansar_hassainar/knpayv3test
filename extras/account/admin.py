# -*- coding: utf-8 -*-

from django.contrib import admin

from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from .forms import UserCreationForm


admin.site.unregister(User)


@admin.register(User)
class UserAdmin(BaseUserAdmin):
    add_form = UserCreationForm
