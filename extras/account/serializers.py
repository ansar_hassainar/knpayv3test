# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model

from rest_framework import serializers
from rest_framework.authtoken.models import Token
from rest_framework.fields import empty

class AuthSerializer(serializers.Serializer):
    username = serializers.CharField(label=_("Username"))
    password = serializers.CharField(label=_("Password"))

    def __init__(self, instance=None, request=None, data=empty, **kwargs):
        if request:
            self.request = request
        super(AuthSerializer, self).__init__(instance, data,**kwargs)

    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')

        user = authenticate(request=self.request, username=username, password=password)

        if user:
            if not user.is_active:
                raise serializers.ValidationError(_('Account is not active.'))
        else:
            raise serializers.ValidationError(
                _('Please enter a correct username and password.'))

        token, _created = Token.objects.get_or_create(user=user)

        return {'token': token.key, 'user_id': user.id}


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()
        fields = ('id', 'username')
