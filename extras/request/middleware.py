# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.core.urlresolvers import reverse, resolve
from django.conf import settings
from django.shortcuts import redirect
from django.utils.deprecation import MiddlewareMixin
from django.utils.functional import SimpleLazyObject


from .models import RequestLog

from two_factor.utils import default_device


class RequestLogMiddleware(object):
    """
    Store all requests from gateway views
    """
    def process_response(self, request, response):
        pk = getattr(request, 'request_log_pk', None)
        if pk is not None:
            r = RequestLog.objects.get(pk=pk)
            r.store_response(response)
        return response

    def process_request(self, request):
        resolver = resolve(request.path)
        if resolver.namespace in settings.LOG_NAMESPACES or resolver.view_name in settings.LOG_URL_NAMES:
            r = RequestLog()
            r = r.from_request(request)
            request.request_log_pk = r.pk


class Enforce2FA(MiddlewareMixin):
    """
    Middleware that enforce user to use 2FA.
    """

    def process_request(self, request):
        user = request.user

        from config.models import Config
        c = Config.get_solo()
        enable_2fa_on = c.enable_2fa_on

        IGNORE_URLS = ('setup', 'qr', 'logout')

        IGNORE_NAMESPACES = ('admin', 'jet') if enable_2fa_on == c.DASHBOARD else ()

        if enable_2fa_on == c.DISABLED and resolve(request.path_info).namespace == 'two_factor':
            return redirect(reverse('accounts:login'))

        if user.is_authenticated() and enable_2fa_on in [c.DASHBOARD, c.ALL]:
            if not default_device(user):
                if resolve(request.path_info).url_name not in IGNORE_URLS and \
                        resolve(request.path_info).namespace not in IGNORE_NAMESPACES:
                    return redirect(reverse('two_factor:setup'))
