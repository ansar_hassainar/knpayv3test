# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import json
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from jsonfield import JSONField


class RequestLog(models.Model):
    """
    Store requests from PSP and API
    """
    requested_at = models.DateTimeField(_("Requested at"), auto_now_add=True)
    replied_at = models.DateTimeField(_("Replied at"), auto_now=True)
    inbound_payload = JSONField(_("Inbound payload"), default={})
    method = models.CharField(_("Method"), default='GET', max_length=7)
    path = models.CharField(_("Path"), max_length=255)
    outbound_payload = JSONField(_("Outbound payload"), default={})
    status_code = models.SmallIntegerField(_("Status code"), null=True, blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True,
                             verbose_name=_("User"), on_delete=models.SET_NULL)
    ip = models.GenericIPAddressField(_("IP address"))

    def __unicode__(self):
        return self.path

    class Meta:
        verbose_name = _("Request log")
        verbose_name_plural = _("Request logs")

    def from_request(self, request):
        # content info
        payload = getattr(getattr(request, request.method.upper()), 'dict')()
        self.inbound_payload = payload

        # Request info
        self.method = request.method
        self.path = request.path[:255]

        # User info
        self.ip = request.META.get('REMOTE_ADDR', '')
        if hasattr(request, 'user') and hasattr(request.user, 'is_authenticated'):
            if request.user.is_authenticated():
                self.user = request.user

        self.save()
        return self

    def store_response(self, response):
        self.status_code = response.status_code
        try:
            self.outbound_payload = json.loads(response.content)
        except ValueError:
            pass
        self.save()
