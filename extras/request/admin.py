# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib import admin

from .models import RequestLog


@admin.register(RequestLog)
class RequestLogAdmin(admin.ModelAdmin):
    list_filter = ('method', 'user', 'requested_at', 'replied_at', 'status_code')
    list_display = ('path', 'method', 'user', 'requested_at', 'replied_at', 'status_code')

    def get_form(self, request, obj=None, **kwargs):
        form = super(RequestLogAdmin, self).get_form(request, obj=obj, **kwargs)
        for field_name in form.base_fields:
            form.base_fields[field_name].disabled = True
        return form

    def has_add_permission(self, request):
        return False

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_add_another'] = False
        extra_context['show_save_and_continue'] = False
        extra_context['show_save'] = False
        return super(RequestLogAdmin, self).change_view(request, object_id,
                                                        form_url, extra_context=extra_context)
