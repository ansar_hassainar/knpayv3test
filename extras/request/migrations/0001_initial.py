# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2017-03-31 09:51
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import jsonfield.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='RequestLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('requested_at', models.DateTimeField(auto_now=True, verbose_name='Requested at')),
                ('replied_at', models.DateTimeField(auto_now_add=True, verbose_name='Replied at')),
                ('inbound_payload', jsonfield.fields.JSONField(default={}, verbose_name='Inbound payload')),
                ('method', models.CharField(default='GET', max_length=7, verbose_name='Method')),
                ('path', models.CharField(max_length=255, verbose_name='Path')),
                ('outbound_payload', jsonfield.fields.JSONField(default={}, verbose_name='Outbound payload')),
                ('status_code', models.SmallIntegerField(blank=True, null=True, verbose_name='Status code')),
                ('ip', models.GenericIPAddressField(verbose_name='IP address')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, verbose_name='User')),
            ],
            options={
                'verbose_name': 'Request log',
                'verbose_name_plural': 'Request logs',
            },
        ),
    ]
