# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging
from collections import OrderedDict

from django.db import transaction
from django.utils.html import strip_spaces_between_tags, strip_tags
from django.utils.decorators import method_decorator
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from rest_framework.exceptions import NotFound
from django.utils.translation import activate
from django.utils.translation import ugettext_lazy as _, force_text as _ft

from rest_framework import mixins, viewsets, status
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from api.func import get_knpay_url
from .filters import PaymentTransactionFilter
from utils.views.mixins import EncryptedPKMixin, APISecurityMixin
from utils.throttling.api_throttling import APISecurityThrottle
from extras.dashboard.models import PaymentRequestFormConfig, Field
from extras.currency.models import Currency
from gateway.models import PaymentTransaction
from gateway.models.gateway import GatewaySettings
from gateway.serializers import ECommerceTransactionSerializer, \
    DashboardTransactionSerializer, PrivateTransactionSerializer, \
    AppDashboardSerializer, DiscloseToMerchantSerializer
from extras.account.serializers import UserSerializer
from config.models import Config, MerchantDetails

logger = logging.getLogger('api')


class ECommerceAPIView(APISecurityMixin, mixins.CreateModelMixin,
                       viewsets.GenericViewSet):
    serializer_class = ECommerceTransactionSerializer
    queryset = PaymentTransaction.objects.e_commerce()
    throttle_classes = [APISecurityThrottle]
    security_config_field = APISecurityMixin.ECOMMERCE

    @method_decorator(transaction.atomic)
    def create(self, request, *args, **kwargs):
        logger.info("Inbound merchant payload {}".format(request.data))
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        payment_transaction = self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response({'url': payment_transaction.get_payment_url()},
                        status=status.HTTP_200_OK, headers=headers)

    def perform_create(self, serializer):
        return serializer.save()


class DashboardAPIView(ECommerceAPIView):
    """
    API endpoint for Payment Requests (Dashboard/bulk type)

    **POST (create) /pos/d/crt**
    """
    serializer_class = DashboardTransactionSerializer
    security_config_field = APISecurityMixin.DASHBOARD
    throttle_classes = [APISecurityThrottle]

    def create(self, request, *args, **kwargs):
        many = True if isinstance(request.data, list) else False
        serializer = self.get_serializer(data=request.data, many=many)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class MerchantTransactionAPIView(viewsets.ModelViewSet):
    """
    # API endpoint for Transactions

    **POST (Create) /api/v1/transaction **

        {
          "amount": "10",
          "gateway_code": "testKNET",
          "currency_code": "KWD",
          "language": "en",
          "customer_email": "abc@abc.com",
          "sms_notification": true,
          "customer_first_name": "Abc",
          "customer_last_name": "Def",
          "customer_phone": "+911234567890",
          "generate_qr_code": true
        }

    **GET (Retrieve) /api/v1/transaction **

        {
          "count": 2,
          "next": "?limit=2&offset=2",
          "previous": null,
          "results": [
            {
              "gateway_code": "testKNET",
              "currency_code": "KWD",
              "language": "en",
              "amount": 10,
              "order_no": "",
              "disclosure_url": "",
              "redirect_url": "",
              "customer_first_name": "Dipen",
              "customer_last_name": "Patel",
              "customer_email": "dipen@kuwaitnet.com",
              "customer_phone": "+919913770988",
              "customer_address_line1": "",
              "customer_address_line2": "",
              "customer_address_city": "",
              "customer_address_state": "",
              "customer_address_country": "",
              "customer_address_postal_code": "",
              "extra": {},
              "email_payment_details": true,
              "sms_payment_details": false,
              "sms_notification": true,
              "email_notification": true,
              "type": "payment_request",
              "initiator": 16,
              "state": "pending",
              "id": 1617,
              "initiator_name": "-",
              "amount_string": "10.000",
              "details_fields": {
                    "Amount": "11.000 KWD",
                    "Reference No": "AQ69F",
                    "Status": "Success",
                    "Gateway": "Knet",
                    "Result": "CAPTURED",
                    "Track ID": "AQ69F",
                    "Post Date": "1209",
                    "Transaction ID": "6868153391373420",
                    "Payment ID": "4740614391373420",
                    "Auth ID": "530050",
                    "Reference ID": "734213964233",
                    "Customer first name": "AKif",
                    "Customer email": "akif@kuwiatnet.com",
                    "Customer phone": "+919898400132",
                    "Customer ID": "Akif007",
                    "Date": "2017-12-08T10:39:31Z"
              },
              "notifications": {
                  "push": true,
                  "sms": false,
                  "email": true
              },
              "qr_code": "http://localhost:8000/media/qr_code/qrcode-1730.png",
              "attachment_short_url": "http://example.com/"
            },
            {
              "gateway_code": "knet",
              "currency_code": "KWD",
              "language": "en",
              "amount": 19.998,
              "order_no": "9z505wuSoaDEGAYEG:101705",
              "disclosure_url": "https://degayeg.mykuwaitnet.net/api/v1/payment-details/",
              "redirect_url": "https://degayeg.mykuwaitnet.net/api/v1/payment-details/?format=json",
              "customer_first_name": "",
              "customer_last_name": "",
              "customer_email": "bogdan@kuwaitnet.com",
              "customer_phone": "",
              "customer_address_line1": "",
              "customer_address_line2": "",
              "customer_address_city": "",
              "customer_address_state": "",
              "customer_address_country": "",
              "customer_address_postal_code": "",
              "extra": {
                "registration_id": "2314DA816F340E655803B10A35A8334B1B72AAC92D00A30D6CF15FDC3DF6385B"
              },
              "email_payment_details": false,
              "sms_payment_details": false,
              "sms_notification": false,
              "email_notification": false,
              "type": "e_commerce",
              "initiator": null,
              "state": "pending",
              "id": 1613,
              "initiator_name": "-",
              "amount_string": "19.998"
              "details_fields": {
                    "Amount": "11.000 KWD",
                    "Reference No": "AQ69F",
                    "Status": "Success",
                    "Gateway": "Knet",
                    "Result": "CAPTURED",
                    "Track ID": "AQ69F",
                    "Post Date": "1209",
                    "Transaction ID": "6868153391373420",
                    "Payment ID": "4740614391373420",
                    "Auth ID": "530050",
                    "Reference ID": "734213964233",
                    "Customer first name": "AKif",
                    "Customer email": "akif@kuwiatnet.com",
                    "Customer phone": "+919898400132",
                    "Customer ID": "Akif007",
                    "Date": "2017-12-08T10:39:31Z"
              },
              "notifications": {
                  "push": true,
                  "sms": false,
                  "email": true
              },
              "qr_code": "http://localhost:8000/media/qr_code/qrcode-1730.png",
              "attachment_short_url": "http://example.com/"
            }
          ]
        }

    **PATCH (Partial update) /api/v1/transaction/1 **

        {
          "state": "canceled"
        }

    ** Filters **

        - created_0     :   Created From(Date)
        - created_1     :   Created To(Date)
        - modified_0    :   Modified From(Date)
        - modified_1    :   Modified To(Date)
        - state         :   Status(String)
        - gateway_code  :   Gateway(String)
        - initiator     :   Created By(ID)
        - q             :   Keyword(String)
    """
    serializer_class = AppDashboardSerializer
    queryset = PaymentTransaction.objects.all()
    permission_classes = [IsAuthenticated]
    filter_class = PaymentTransactionFilter

    @method_decorator(transaction.atomic)
    def create(self, request, *args, **kwargs):
        data = request.data
        data['initiator'] = request.user.pk

        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        instance = serializer.save()
        serializer_data = serializer.data

        # Generate QR Code
        serializer_data['qr_code'] = ""
        generate_qr = data.pop('generate_qr_code', None)
        if generate_qr:
            instance.generate_qr_code()
            serializer_data['qr_code'] = '{}{}'.format(
                get_knpay_url(), instance.qr_code.url)

        headers = self.get_success_headers(serializer.data)
        return Response(serializer_data, status=status.HTTP_201_CREATED,
                        headers=headers)


class CancelTransactionViewSet(APIView):
    """
    # Cancel Transaction

    ** GET /api/v1/transaction/cancel/transaction_id/ **

    ** Note ** transaction id is required

    ** response **

        HTTP 200 OK

    ** legends **

        transaction_id: transaction object's id (integer)

    """
    permission_classes = [IsAuthenticated]
    permission_required = "gateway.cancel_paymenttransaction"

    def get(self, request, *args, **kwargs):
        tran_pk = kwargs.get('pk')
        tran_obj = get_object_or_404(PaymentTransaction, pk=tran_pk)
        if tran_obj.can_be_cancelled() and \
                request.user.has_perm(self.permission_required):
            tran_obj.cancel()
            tran_obj.save()
            return Response(status=status.HTTP_200_OK)
        raise NotFound(code=404)


class GatewayCodeViewSet(APIView):
    """
    * API endpoint for Gateway Codes

    **GET /api/v1/gateways**


        [
          {
            "code": "testKNET",
            "name": "KUWAITNET Test"
          },
          {
            "code": "migs",
            "name": "Test MiGS"
          },
          {
            "code": "paypal",
            "name": "PayPal"
          }
        ]
    """
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        accounts = GatewaySettings.objects.get_for_user(request.user)
        choices = accounts.choices(blank=False)

        gateway_codes = []
        for gateway in choices:
            gateway_codes.append({
                'code': gateway[0],
                'name': gateway[1]
            })

        return Response(gateway_codes)


class CurrencyCodeViewSet(APIView):
    """
    * API endpoint for Currency Codes

    **GET /api/v1/currencies **

        [
          {
            "code": "KWD",
            "name": "KWD"
          },
          {
            "code": "USD",
            "name": "USD"
          },
          {
            "code": "EUR",
            "name": "EUR"
          }
        ]
    """

    def get(self, request, format=None):
        currencies = Currency.objects.choices()

        currency_codes = []
        for currency in currencies:
            currency_codes.append({
                'code': currency[0],
                'name': currency[1]
            })

        return Response(currency_codes)


class InitiatorsViewSet(viewsets.ViewSet):
    """
    # API endpoint for Initiators

    ** GET /api/v1/initiators **

        [
            {
                "id": 1,
                "username": "admin"
            },
            {
                "id": 2,
                "username": "akif"
            }
        ]

    """

    def list(self, request):
        queryset = get_user_model().objects.all()
        serializer = UserSerializer(queryset, many=True)
        return Response(serializer.data)

    permission_classes = [IsAuthenticated]


class PaymentReqFieldsViewSet(APIView):
    """
    # Payment Request Forms Active Fields

    ** GET /api/v1/payment-request-form-fields **

        [
            {
                "field_required": true,
                "field_name": "customer_first_name",
                "field_label_ar": "",
                "field_label_en": ""
            },
            {
                "field_required": true,
                "field_name": "customer_id",
                "field_label_ar": "AR Customer ID",
                "field_label_en": "Customer ID"
            },
            {
                "field_required": true,
                "field_name": "customer_phone",
                "field_label_ar": "",
                "field_label_en": ""
            }
        ]
    """
    permission_classes = [IsAuthenticated]

    def get(self, request):
        data = []
        form_obj = PaymentRequestFormConfig.get_solo()
        for field in form_obj.fields.active():
            field_dict = {
                "field_name": field.get_name(),
                "field_label_ar": field.label_ar,
                "field_label_en": field.label_en,
                "field_required": field.required
            }
            data.append(field_dict)
        return Response(data, status=200)


class TermsAndConditionAPI(APIView):
    """
    # Terms and Condition API for users

    ** GET: /api/v1/terms-and-conditions **

        [
            {
                "text": "This is some dummy terms and conditions for the making payment on the KNPAY.",
                "is_required": true
            }
        ]

    """

    def get(self, request):
        config = Config.get_solo()
        terms_text = strip_tags(
            strip_spaces_between_tags(config.terms_text)).strip()
        data = [{
            'is_required': config.terms,
            'text': terms_text.replace('&nbsp;', ' ')
        }]
        return Response(data, status=200)


class CreatePaymentPrerequisites(APIView):
    """
    # Necessary data like currency, gateway, form fields during payment request#

    ** GET: /api/v1/create-payment-prerequisites **

    {
        "gateway_codes": [
            {
                "currency": {
                    "code": "KWD",
                    "name": "KWD"
                },
                "code": "testKNET",
                "name": "KUWAITNET Test"
            },
            {
                "currency": {
                    "code": "EUR",
                    "name": "EUR"
                },
                "code": "migs",
                "name": "Test MiGS"
            },
            {
                "currency": {
                    "code": "EUR",
                    "name": "EUR"
                },
                "code": "knet",
                "name": "KNET"
            },
            {
                "currency": {
                    "code": "KWD",
                    "name": "KWD"
                },
                "code": "cybersource-kwd",
                "name": "CyberSource KWD"
            },
            {
                "currency": {
                    "code": "KWD",
                    "name": "KWD"
                },
                "code": "degayeg-knet",
                "name": "Degayeg KNET"
            },
            {
                "currency": {
                    "code": "KWD",
                    "name": "KWD"
                },
                "code": "sahara-kuwait",
                "name": "Sahara Kuwait"
            },
            {
                "currency": {
                    "code": "KWD",
                    "name": "KWD"
                },
                "code": "agknet",
                "name": "AGKNET"
            }
        ],
        "currency_codes": [
            {
                "code": "KWD",
                "name": "KWD"
            },
            {
                "code": "USD",
                "name": "USD"
            },
            {
                "code": "EUR",
                "name": "EUR"
            }
        ],
        "payment_form_fields": [
            {
                "field_required": true,
                "field_name": "customer_first_name",
                "field_label_ar": "الاسم الاول",
                "field_label_en": "First Name"
            },
            {
                "field_required": true,
                "field_name": "customer_id",
                "field_label_ar": "AR Customer ID",
                "field_label_en": "Customer ID"
            },
            {
                "field_required": true,
                "field_name": "customer_phone",
                "field_label_ar": "الهاتف",
                "field_label_en": "Phone"
            }
        ]
    }

    """
    permission_classes = [IsAuthenticated]

    def get(self, request):
        data = {
            'payment_form_fields': [],
            'currency_codes': [],
            'gateway_codes': []
        }

        form_obj = PaymentRequestFormConfig.get_solo()
        currencies = Currency.objects.choices()
        accounts = GatewaySettings.objects.get_for_user(request.user).active()

        for field in form_obj.fields.active():
            activate('ar')
            label_ar = field.get_label()
            activate('en')
            field_dict = {
                "field_name": field.get_name(),
                "field_label_ar": field.label_ar if field.type == 'custom' else label_ar,
                "field_label_en": field.label_en if field.type == 'custom' else field.get_label(),
                "field_required": field.required
            }
            data['payment_form_fields'].append(field_dict)

        for currency in currencies:
            data['currency_codes'].append({
                'code': currency[0],
                'name': currency[1]
            })

        for gateway in accounts:
            data['gateway_codes'].append({
                'code': gateway.code,
                'name': gateway.name,
                'currency': {
                    'code': gateway.currency_config.default_currency.code,
                    'name': gateway.currency_config.default_currency.code
                }
            })

        return Response(data, status=200)


class PaymentRequestAPIView(EncryptedPKMixin, APIView):
    """
    # Payment Request Details API #

    ** GET: /api/v1/pay/{encrypted_key} **

    ** If gateway is not selected **

        {
            "has_gateway": false,
            "terms": "This is to test the terms and condition feature.  heloo",
            "gateway_choices": [
                {
                    "code": "testKNET",
                    "value": "KUWAITNET Test",
                    "total_label": "Total in KWD",
                    "url": "http://localhost:8000/media/gateway/logos/knet.jpg",
                    "fee": "0.125",
                    "total": "5.13",
                    "fee_label": "Fee in KWD"
                },
                {
                    "code": "migs",
                    "value": "Test MiGS",
                    "total_label": "Total in KWD",
                    "url": "http://localhost:8000/media/gateway/logos/visa.jpg",
                    "fee": "0.125",
                    "total": "5.13",
                    "fee_label": "Fee in KWD"
                },
                {
                    "code": "knet",
                    "value": "KNET",
                    "total_label": "Total in KWD",
                    "url": "http://localhost:8000/media/gateway/logos/paypal.jpg",
                    "fee": "0.125",
                    "total": "5.13",
                    "fee_label": "Fee in KWD"
                }
            ],
            "fields": {
                "Amount": "5.000",
                "Developer Email": "akif@kuwaitnet.com",
                "Business Email": "akif@kuwaitnet.com",
                "Personal Email": "akif@kuwaitnet.com",
                "Customer ID": "NBV345",
                "Customer email": "akif@kuwaitnet.com"
            },
            "terms_required": true
        }

    ** If gateway is selected **

        {
            "has_gateway": true,
            "terms_required": true,
            "terms": "This is to test the terms and condition feature.  heloo",
            "fields": {
                "Amount": "20.000",
                "Personal Email": "akif@kuwaitnet.com",
                "Customer ID": "XYZ1235",
                "Customer first name": "Abc",
                "Customer last name": "Def",
                "Customer email": "akif@kuwaitnet.com",
                "Customer phone": "+919898400132",
                "Fee": "0.500",
                "Total": "20.5",
                "Currency Code": "KWD"
            }
        }

    ** POST: /api/v1/pay/{encrypted_key} **

        {
            "gateway_code": "migs"
        }

    *Response:*

        {
            "message": "You are being redirected to payment page...",
            "redirect_url": "url to redirect here"
        }

    """

    permission_classes = [AllowAny]
    queryset = PaymentTransaction.objects.payment_requests()

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.config = PaymentRequestFormConfig.get_solo()

        transaction_amount = self.object.get_amount()
        transaction_currency_code = self.object.currency_code
        response_data = {}
        data = OrderedDict()
        data['Amount'] = self.object.amount

        extra = self.object.extra
        for key, value in extra.items():
            field = Field.objects.get_for_name(key, self.config)
            label = field.get_label() if field is not None else key
            data[_ft(label)] = value

        for field in PaymentTransaction.CUSTOMER_FIELDS:
            val = getattr(self.object, field)
            if val:
                data[_ft(PaymentTransaction._meta.get_field(
                    field).verbose_name)] = val

        if self.object.gateway_code:
            response_data['has_gateway'] = True
            try:
                gateway = GatewaySettings.objects.get_for_code(
                    self.object.gateway_code)
                gateway_fee = gateway.currency_config.compute_fee(amount=transaction_amount,
                                                                  from_code=transaction_currency_code)
                total = gateway.currency_config.compute_total(amount=transaction_amount,
                                                              from_code=transaction_currency_code)
                data['Fee'] = str(gateway_fee)
                data['Total'] = total
                data['Currency Code'] = gateway.currency_config.default_currency.code
            except:
                pass
        else:
            response_data['has_gateway'] = False
            gateway_choices = GatewaySettings.objects.get_for_user(
                self.object.initiator)
            choices = []
            for gateway in gateway_choices:
                if gateway.is_active:
                    if self.object._currency_exchange_is_available(
                            gateway_code=gateway.code):
                        if gateway.has_fee():
                            gateway_fee = gateway.currency_config.compute_fee(
                                amount=transaction_amount,
                                from_code=transaction_currency_code)

                            total = gateway.currency_config.compute_total(
                                amount=transaction_amount,
                                from_code=transaction_currency_code)
                            currency_code = gateway.currency_config.default_currency.code
                            total_label = _ft(_("Total in %s")) % currency_code
                            fee_label = _ft(_("Fee in %s")) % currency_code
                            choice = dict(fee=str(gateway_fee), total=str(total),
                                          total_label=total_label,
                                          fee_label=fee_label,
                                          code=gateway.code,
                                          value=gateway.name,
                                          url='{}{}'.format(get_knpay_url(),
                                                            gateway.gateway.logo.url))
                            choices.append(choice)

            response_data['gateway_choices'] = choices

        config = Config.get_solo()
        if config.terms:
            terms_text = strip_tags(
                strip_spaces_between_tags(config.terms_text)).strip()
            response_data['terms'] = terms_text.replace('&nbsp;', ' ')
            response_data['terms_required'] = True
        else:
            response_data['terms_required'] = False
        response_data['fields'] = data
        return Response(response_data, status=200)

    def post(self, request, *args, **kwargs):
        gateway_code = request.data.get('gateway_code', None)
        self.object = self.get_object()
        url = self.object.get_attempt_url(gateway_code=gateway_code)

        if url is not None:
            _status = status.HTTP_200_OK
            data = {
                'message': _ft(_("You are being redirected to payment page...")),
                'redirect_url': url
            }
        else:
            _status = status.HTTP_400_BAD_REQUEST
            data = {
                'errors': {
                    '__all__': _ft(_("There was an error while connecting to payment server. "
                                     "Please try again later."))
                }
            }
        return Response(data, status=_status)


class MerchantLogoApiView(APIView):

    def get(self, request, *args, **kwargs):
        merchant = MerchantDetails.get_solo()
        try:
            logo_url = get_knpay_url() + merchant.logo.url
        except:
            logo_url = ""
        data = {'logo_url': logo_url}
        return Response(data, status=200)


class DiscloseData(APISecurityMixin, APIView):
    """
    1. order_id is required.
    2. disclosure_url is optional which will override the initial disclosure_url saved in transaction.
    3. disclosure_url will be used once during this API and
        will not update the transactions's disclosure_url
    4. the disclosing response status will update the attempt record.
    """
    permission_classes = [IsAuthenticated]
    serializer_class = DiscloseToMerchantSerializer
    throttle_classes = [APISecurityThrottle]
    security_config_field = APISecurityMixin.ECOMMERCE

    def get(self, request, *args, **kwargs):
        
        serializer = self.serializer_class(data=request.query_params)
        if serializer.is_valid():
            try:
                disclosure_url = serializer.validated_data['disclosure_url']
            except KeyError:
                disclosure_url = None
            disclosed = serializer.transaction.disclose_to_merchant(disclosure_url=disclosure_url)
            if disclosed:
                return Response({"message": _('Transaction details disclosed successfully to %s') % disclosure_url})
            else:
                return Response({"message": _('Transaction details were not disclosed to %s. '
                                              'Please check disclose url error.') % disclosure_url})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GetDisclosedData(APISecurityMixin, APIView):
    
    permission_classes = [IsAuthenticated]
    serializer_class = DiscloseToMerchantSerializer
    throttle_classes = [APISecurityThrottle]
    security_config_field = APISecurityMixin.ECOMMERCE

    def get(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.query_params)
        if serializer.is_valid():
            transaction = serializer.transaction
            disclosed_data = transaction.attempts.latest().disclosed_data
            return Response(disclosed_data) 
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)