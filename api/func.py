# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

import hashlib
import json

from django.db import close_old_connections
from django.contrib.sites.models import Site
from django.conf import settings
from django.utils.http import urlquote
from django.utils.translation import ugettext_lazy as _

import requests

from config.models import Config


def get_knpay_url():
    site = Site.objects.get_current()
    return '{protocol}://{domain}'.format(protocol=settings.PROTOCOL, domain=site.domain)


def create_payment(data=None):
    """
    :param data: dict of kwargs for creating PaymentTransaction
    :return: PaymentTransaction instance
    """
    from gateway.models import PaymentTransaction
    assert isinstance(data, dict)
    _type = data.pop('type', PaymentTransaction.PAYMENT_REQUEST)
    return PaymentTransaction.objects.create(type=_type, **data)


def initialize_transaction(data, instance=None, serializer_class=None, queue=False):

    if queue:
        # avoid OperationalError: (2006, 'MySQL server has gone away')
        # when calling the func from the queue
        close_old_connections()

    logger = logging.getLogger('gateway')

    if serializer_class is None:
        from gateway.serializers import PrivateTransactionSerializer
        serializer_class = PrivateTransactionSerializer
    try:
        data = json.loads(data)
    except TypeError:
        pass
    serializer = serializer_class(instance=instance, data=data)
    if serializer.is_valid(raise_exception=False):
        return serializer.save(), True

    logger.info('Serializer data %s '
                'instance %s '
                'serializer_class %s'
                'errors %s ' % (data, instance,
                                serializer_class.__class__.__name__, serializer.errors))
    return serializer.errors, False


def calculate_checksum(checksum_payload):
    """

    :param payload: dict containing the following keys
                amount, result, secret_key,  reference_number
    :return: checksum
    """
    assert 'amount' in checksum_payload.keys()
    assert 'result' in checksum_payload.keys()
    assert 'secret_key' in checksum_payload.keys()
    assert 'reference_number' in checksum_payload.keys()
    h = hashlib.md5()
    h.update(''.join([urlquote(checksum_payload[param]) for param in checksum_payload]))
    return h.hexdigest().upper()