# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url, include
from rest_framework import routers

from .views import MerchantTransactionAPIView, GatewayCodeViewSet, \
    CurrencyCodeViewSet, InitiatorsViewSet, CancelTransactionViewSet, \
    PaymentReqFieldsViewSet, TermsAndConditionAPI, CreatePaymentPrerequisites, \
    PaymentRequestAPIView, MerchantLogoApiView

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'transaction', MerchantTransactionAPIView,
                base_name='transaction')

urlpatterns = [
    url(r'^gateways', GatewayCodeViewSet.as_view(), name='gateways'),
    url(r'^merchant_logo', MerchantLogoApiView.as_view(), name='merchant_logo'),
    url(r'^transaction/cancel/(?P<pk>\d+)$', CancelTransactionViewSet.as_view(),
        name='cancel_transaction'),
    url(r'^initiators', InitiatorsViewSet.as_view({'get': 'list'}),
        name='initiators'),
    url(r'^currencies', CurrencyCodeViewSet.as_view(), name='currencies'),
    url(r'^payment-request-prerequisites', CreatePaymentPrerequisites.as_view(),
        name='payment_request_prerequisites'),
    url(r'^payment-request-form-fields', PaymentReqFieldsViewSet.as_view(),
        name='payment_request_form_fields'),
    url(r'^terms-and-conditions', TermsAndConditionAPI.as_view(),
        name='terms_and_conditions'),
    url(r'^pay/(?P<encrypted_pk>.+)$', PaymentRequestAPIView.as_view(),
        name='payment_request_detail'),
    url(r'^', include(router.urls)),
]
