# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import django_filters
from django.db.models import Q

from gateway.models import PaymentTransaction


class PaymentTransactionFilter(django_filters.FilterSet):
    q = django_filters.CharFilter(method='filter_q')
    created = django_filters.DateFromToRangeFilter()
    modified = django_filters.DateFromToRangeFilter()

    start_date = None
    end_date = None
    period = 'day'

    class Meta:
        model = PaymentTransaction
        fields = ('created', 'modified', 'type',
                  'currency_code', 'order_no', 'q',
                  'gateway_code', 'state', 'initiator')

    def filter_q(self, queryset, name, value):
        # ToDO: remove hardcoded filter when paypal goes live
        queryset = queryset.exclude(gateway_code='akif-paypal').exclude(gateway_code='paypal')
        if not value:
            return queryset
        result = queryset.filter(
            Q(order_no__icontains=value) |
            Q(bulk_id__icontains=value) |
            Q(customer_first_name__icontains=value) |
            Q(customer_last_name__icontains=value) |
            Q(customer_email__icontains=value) |
            Q(customer_phone__icontains=value) |
            Q(id__icontains=value)
        )
        return result
