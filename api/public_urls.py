# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from .views import DiscloseData, GetDisclosedData

urlpatterns = [
    url(r'^disclose-data$', DiscloseData.as_view(), name='disclose_data'),
    url(r'^get-disclosed-data$', GetDisclosedData.as_view(), name='get_disclosed_data'),
]
