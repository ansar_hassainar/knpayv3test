# -*- coding: utf-8 -*-
from decimal import Decimal
from django.core.urlresolvers import reverse, resolve
from django.test import TestCase

import pytest
from mixer.backend.django import mixer
from mock import patch

from rest_framework import status
from rest_framework.authtoken.models import Token
from utils.tests import knet_settings_factory
from gateway.models import PaymentTransaction, PaymentAttempt
from config.models import APISecurityConfig


class TestViews(TestCase):
    """
    Test API Views for api app
    """

    def setUp(self):
        self.settings = knet_settings_factory()
        self.access_token = "3cde12539208a6bcf0e564606624b388c574d0da"
        mixer.blend(Token, key=self.access_token)
        APISecurityConfig.objects.all().delete()
        self.api_security_config = APISecurityConfig.objects.create(
            whitelisted_ips="*", ecommerce_on=True, dashboard_on=True, legacy_api_on=True)
        
    def test_disclose_data_view_response_401_with_no_authorization(self):
        transaction = mixer.blend(PaymentTransaction,amount=Decimal(12), currency_code='KWD', order_no=1000,
                                  gateway_code=self.settings.code)
        attempt = transaction.create_attempt()
        attempt.status = PaymentAttempt.SUCCESS
        response = self.client.get(reverse('public_api:disclose_data'), follow=True)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED, 'View should response with 401'

    def test_disclose_data_view_response_401_with_wrong_authorization(self):
        transaction = mixer.blend(PaymentTransaction,amount=Decimal(12), currency_code='KWD', order_no=1000,
                                  gateway_code=self.settings.code)
        attempt = transaction.create_attempt()
        attempt.status = PaymentAttempt.SUCCESS
        response = self.client.get(reverse('public_api:disclose_data'), {}, HTTP_AUTHORIZATION='Token ' + "WRONG_TOKEN", follow=True)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED, 'View should response with 401'

    def test_disclose_data_view_response_400_with_authorization_with_no_order_no(self):
        transaction = mixer.blend(PaymentTransaction,amount=Decimal(12), currency_code='KWD', order_no=1000,
                                  gateway_code=self.settings.code)
        attempt = transaction.create_attempt()
        attempt.status = PaymentAttempt.SUCCESS
        response = self.client.get(reverse('public_api:disclose_data'), {}, HTTP_AUTHORIZATION='Token ' + self.access_token, follow=True)
        assert response.status_code == status.HTTP_400_BAD_REQUEST, 'View should response with 400'

    def test_disclose_data_view_response_400_with_authorization_with_not_exist_order_no(self):
        transaction = mixer.blend(PaymentTransaction,amount=Decimal(12), currency_code='KWD', order_no=1000,
                                  gateway_code=self.settings.code)
        attempt = transaction.create_attempt()
        attempt.status = PaymentAttempt.SUCCESS
        url = '{0}{1}'.format(reverse('public_api:disclose_data'),'?order_no=8888')
        response = self.client.get(url, {}, HTTP_AUTHORIZATION='Token ' + self.access_token,follow=True)
        assert response.status_code == status.HTTP_400_BAD_REQUEST, 'View should response with 400'

    def test_disclose_data_view_response_200_with_authorization_with_exist_order_no_and_no_disclosure_url(self):
        transaction = mixer.blend(PaymentTransaction,amount=Decimal(12), currency_code='KWD', order_no=1000,
                                  gateway_code=self.settings.code)
        attempt = transaction.create_attempt()
        attempt.status = PaymentAttempt.SUCCESS
        url = '{0}{1}'.format(reverse('public_api:disclose_data'),'?order_no=1000')
        response = self.client.get(url, {}, HTTP_AUTHORIZATION='Token ' + self.access_token, follow=True)
        assert response.status_code == status.HTTP_200_OK, 'View should response with 200'

    def test_disclose_data_view_response_200_with_authorization_with_exist_order_no_and_with_disclosure_url(self):
        transaction = mixer.blend(PaymentTransaction,amount=Decimal(12), currency_code='KWD', order_no=1000,
                                  gateway_code=self.settings.code)
        attempt = transaction.create_attempt()
        attempt.status = PaymentAttempt.SUCCESS
        url = '{0}{1}'.format(reverse('public_api:disclose_data'),'?order_no=1000&disclosure_url=http://example.com/test')
        response = self.client.get(url, {}, HTTP_AUTHORIZATION='Token ' + self.access_token, follow=True)
        assert response.status_code == status.HTTP_200_OK, 'View should response with 200'

    def test_disclose_data_view_response_200_with_authorization_with_exist_order_no_and_with_no_disclosure_url_with_not_SUCCESS_attempt(self):
        transaction = mixer.blend(PaymentTransaction,amount=Decimal(12), currency_code='KWD', order_no=1000,
                                  gateway_code=self.settings.code)
        attempt = transaction.create_attempt()
        attempt.status = PaymentAttempt.FAILED        
        url = '{0}{1}'.format(reverse('public_api:disclose_data'),'?order_no=1000&disclosure_url=http://example.com/test')
        response = self.client.get(url, {}, HTTP_AUTHORIZATION='Token ' + self.access_token, follow=True)
        assert response.status_code == status.HTTP_200_OK, 'View should response with 200'

    def test_disclose_data_view_response_200_with_authorization_with_exist_order_no_and_with_disclosure_url_with_not_SUCCESS_attempt(self):
        transaction = mixer.blend(PaymentTransaction,amount=Decimal(12), currency_code='KWD', order_no=1000,
                                  gateway_code=self.settings.code)
        attempt = transaction.create_attempt()
        attempt.status = PaymentAttempt.FAILED
        url = '{0}{1}'.format(reverse('public_api:disclose_data'),'?order_no=1000&disclosure_url=http://example.com/test')
        response = self.client.get(url, {}, HTTP_AUTHORIZATION='Token ' + self.access_token, follow=True)
        assert response.status_code == status.HTTP_200_OK, 'View should response with 200'

    def test_get_disclosed_data_view_response_401_with_no_authorization(self):
        transaction = mixer.blend(PaymentTransaction,amount=Decimal(12), currency_code='KWD', order_no=1000,
                                  gateway_code=self.settings.code)
        attempt = transaction.create_attempt()
        attempt.status = PaymentAttempt.SUCCESS
        response = self.client.get(reverse('public_api:get_disclosed_data'), follow=True)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED, 'View should response with 401'

    def test_get_disclosed_data_view_response_401_with_wrong_authorization(self):
        transaction = mixer.blend(PaymentTransaction,amount=Decimal(12), currency_code='KWD', order_no=1000,
                                  gateway_code=self.settings.code)
        attempt = transaction.create_attempt()
        attempt.status = PaymentAttempt.SUCCESS
        response = self.client.get(reverse('public_api:get_disclosed_data'), {}, HTTP_AUTHORIZATION='Token ' + "WRONG_TOKEN", follow=True)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED, 'View should response with 401'

    def test_get_disclosed_data_view_response_400_with_authorization_with_no_order_no(self):
        transaction = mixer.blend(PaymentTransaction,amount=Decimal(12), currency_code='KWD', order_no=1000,
                                  gateway_code=self.settings.code)
        attempt = transaction.create_attempt()
        attempt.status = PaymentAttempt.SUCCESS
        response = self.client.get(reverse('public_api:get_disclosed_data'), {}, HTTP_AUTHORIZATION='Token ' + self.access_token, follow=True)
        assert response.status_code == status.HTTP_400_BAD_REQUEST, 'View should response with 400'

    def test_get_disclosed_data_view_response_400_with_authorization_with_not_exist_order_no(self):
        transaction = mixer.blend(PaymentTransaction,amount=Decimal(12), currency_code='KWD', order_no=1000,
                                  gateway_code=self.settings.code)
        attempt = transaction.create_attempt()
        attempt.status = PaymentAttempt.SUCCESS
        url = '{0}{1}'.format(reverse('public_api:disclose_data'),'?order_no=8888')
        response = self.client.get(url, {}, HTTP_AUTHORIZATION='Token ' + self.access_token,follow=True)
        assert response.status_code == status.HTTP_400_BAD_REQUEST, 'View should response with 400'

    def test_get_disclosed_data_view_response_200_with_authorization_with_exist_order_no(self):
        transaction = mixer.blend(PaymentTransaction,amount=Decimal(12), currency_code='KWD', order_no=1000,
                                  gateway_code=self.settings.code)
        attempt = transaction.create_attempt()
        attempt.status = PaymentAttempt.SUCCESS
        url = '{0}{1}'.format(reverse('public_api:disclose_data'),'?order_no=1000')
        response = self.client.get(url, {}, HTTP_AUTHORIZATION='Token ' + self.access_token, follow=True)
        assert response.status_code == status.HTTP_200_OK, 'View should response with 200'

    def test_get_disclosed_data_view_response_200_with_authorization_with_exist_order_no_and_with_not_SUCCESS_attempt(self):
        transaction = mixer.blend(PaymentTransaction,amount=Decimal(12), currency_code='KWD', order_no=1000,
                                  gateway_code=self.settings.code)
        attempt = transaction.create_attempt()
        attempt.status = PaymentAttempt.FAILED
        url = '{0}{1}'.format(reverse('public_api:disclose_data'),'?order_no=1000')
        response = self.client.get(url, {}, HTTP_AUTHORIZATION='Token ' + self.access_token, follow=True)
        assert response.status_code == status.HTTP_200_OK, 'View should response with 200'


class TestAPISecurity(TestCase):
    """
    Test API Security
    """
    @classmethod
    def setUp(self):
        self.settings = knet_settings_factory()
        self.access_token = "3cde12539208a6bcf0e564606624b388c574d0da"
        mixer.blend(Token, key=self.access_token)
        self.url = '{0}{1}'.format(reverse('public_api:disclose_data'),'?order_no=1000')
        self.transaction = mixer.blend(PaymentTransaction, amount=Decimal(12), currency_code='KWD', order_no=1000,
                                       gateway_code=self.settings.code)
        self.attempt = self.transaction.create_attempt()
        self.attempt.status = PaymentAttempt.SUCCESS

        APISecurityConfig.objects.all().delete()
        self.api_security_config = APISecurityConfig.objects.create(
            whitelisted_ips="*", ecommerce_on=True, dashboard_on=True, legacy_api_on=True)

    def test_ecommerce_view_response_200_when_all_ips_allowed_and_ecommerce_is_True(self):
        self.api_security_config.ecommerce_on = True
        self.api_security_config.save()
        response = self.client.get(self.url, {},
                                   HTTP_AUTHORIZATION='Token ' + self.access_token, follow=True)
        assert response.status_code == status.HTTP_200_OK, 'View should response with 200'

    def test_ecommerce_view_response_403_when_all_ips_allowed_and_ecommerce_is_False(self):
        self.api_security_config.ecommerce_on = False
        self.api_security_config.save()
        response = self.client.get(self.url, {},
                                   HTTP_AUTHORIZATION='Token ' + self.access_token, follow=True)
        assert response.status_code == status.HTTP_403_FORBIDDEN, 'View should response with 403'

    def test_ecommerce_view_response_403_when_empty_ips_whitelisted_ips_and_ecommerce_is_True(self):
        self.api_security_config.ecommerce_on = True
        self.api_security_config.whitelisted_ips = ""
        self.api_security_config.save()
        response = self.client.get(self.url, {}, REMOTE_ADDR="35.35.35.35",
                                   HTTP_AUTHORIZATION='Token ' + self.access_token, follow=True)
        assert response.status_code == status.HTTP_403_FORBIDDEN, 'View should response with 403'

    def test_ecommerce_view_response_403_when_request_ip_is_not_whitelisted_ips_and_ecommerce_is_True(self):
        self.api_security_config.ecommerce_on = True
        self.api_security_config.whitelisted_ips = "36.36.36.36,37.37.37.37"
        self.api_security_config.save()
        response = self.client.get(self.url, {}, REMOTE_ADDR="35.35.35.35",
                                   HTTP_AUTHORIZATION='Token ' + self.access_token, follow=True)
        assert response.status_code == status.HTTP_403_FORBIDDEN, 'View should response with 403'

    def test_ecommerce_view_response_200_when_request_ip_is_whitelisted_ips_and_ecommerce_is_True(self):
        self.api_security_config.ecommerce_on = True
        self.api_security_config.whitelisted_ips = "35.35.35.35,36.36.36.36,37.37.37.37"
        self.api_security_config.save()
        response = self.client.get(self.url, {}, REMOTE_ADDR="35.35.35.35",
                                   HTTP_AUTHORIZATION='Token ' + self.access_token, follow=True)
        assert response.status_code == status.HTTP_200_OK, 'View should response with 200'
