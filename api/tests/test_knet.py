# -*- coding: utf-8 -*-

import json
from random import choice
from urlparse import urlparse
from string import lowercase

from django.core.urlresolvers import reverse, resolve
from django.test import TestCase

import pytest
from mock import patch
from mixer.backend.django import mixer
from rest_framework import status
from rest_framework.test import APIClient, APIRequestFactory, APITestCase
from config.models import APISecurityConfig

from utils.tests import knet_settings_factory
from gateway.models import PaymentAttempt


pytestmark = pytest.mark.django_db
factory = APIRequestFactory()


def long_string(n=130):
    return "".join(choice(lowercase) for i in range(n))


class TestBasicKNETInboundAPI(TestCase):
    """
    Test KNET API for e-commerce with minimum of required fields
    """
    client_class = APIClient
    DISCLOSURE_URL = 'http://example.com'
    REDIRECT_URL = 'http://example.com'
    ORDER_NO = '234'
    BASE_PAYLOAD = {
        'amount': '230.000',
        'gateway_code': 'knet-test',
        'order_no': ORDER_NO,
        'redirect_url': REDIRECT_URL,
        'disclosure_url': DISCLOSURE_URL,
        'currency_code': 'KWD',
    }

    def setUp(self):
        self.base_payload = self.BASE_PAYLOAD
        knet_settings_factory()
        self.create_url = reverse('pos_create')
        APISecurityConfig.objects.all().delete()
        self.api_security_config = APISecurityConfig.objects.create(
            whitelisted_ips="*", ecommerce_on=True, dashboard_on=True, legacy_api_on=True)

    def test_create_transaction(self):
        response = self.client.post(self.create_url, self.base_payload)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "url")

    def test_without_required_kwargs(self):
        response = self.client.post(self.create_url, {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertContains(response, "amount", status_code=status.HTTP_400_BAD_REQUEST)
        self.assertContains(response, "gateway_code", status_code=status.HTTP_400_BAD_REQUEST)
        self.assertContains(response, "order_no", status_code=status.HTTP_400_BAD_REQUEST)
        self.assertContains(response, "currency_code", status_code=status.HTTP_400_BAD_REQUEST)

    def test_uniqueness_of_order_no(self):
        self.client.post(self.create_url, self.base_payload)
        response = self.client.post(self.create_url, self.base_payload)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertContains(response, "order_no", status_code=status.HTTP_400_BAD_REQUEST)

    def test_invalid_currency_code(self):
        data = {'currency_code': "gin"}
        response = self.client.post(self.create_url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertContains(response, "currency_code", status_code=status.HTTP_400_BAD_REQUEST)

    def test_valid_amount(self):
        data = {'amount': "2"}
        response = self.client.post(self.create_url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertNotContains(response, "amount", status_code=status.HTTP_400_BAD_REQUEST)

    def test_redirect_url_is_required(self):
        response = self.client.post(self.create_url, {})
        assert response.status_code == status.HTTP_400_BAD_REQUEST, 'should be error'
        assert 'redirect_url' in response.content, 'redirect url should be in errors'
        mixer.blend('config.ECommerceConfig', redirect_url='http://example.com/')
        response = self.client.post(self.create_url, {})
        assert 'redirect_url' not in response.content, 'redirect url should\'t be in errors'

    def test_disclosure_url_is_required(self):
        # pytest.set_trace()
        response = self.client.post(self.create_url, {})
        assert response.status_code == status.HTTP_400_BAD_REQUEST, 'should be error'
        assert 'disclosure_url' in response.content, 'error_url should be in errors'
        mixer.blend('config.ECommerceConfig', disclosure_url='http://example.com/', secret_key='wipeIT')
        response = self.client.post(self.create_url, {})
        assert 'disclosure_url' not in response.content, 'disclosure_urlshould\'t be in errors'

    def test_negative_amount(self):
        payload = self.base_payload.copy()
        payload['amount'] = "-2"
        response = self.client.post(self.create_url, payload)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertContains(response, "amount", status_code=status.HTTP_400_BAD_REQUEST)

    def test_invalid_postal_code(self):
        data = {'customer_address_postal_code': 'CA#78S&%'}
        response = self.client.post(self.create_url, data)
        self.assertContains(response, "customer_address_postal_code", status_code=status.HTTP_400_BAD_REQUEST)

    def test_invalid_language(self):
        data = {'language': 'lorem'}
        response = self.client.post(self.create_url, data)
        self.assertContains(response, "language", status_code=status.HTTP_400_BAD_REQUEST)

    def test_long_order_no(self):
        data = {'order_no': long_string()}
        response = self.client.post(self.create_url, data)
        self.assertContains(response, "order_no", status_code=status.HTTP_400_BAD_REQUEST)

    def test_invalid_extra(self):
        data = {'extra': 'str'}
        response = self.client.post(self.create_url, json.dumps(data), content_type="application/json")
        self.assertContains(response, "extra", status_code=status.HTTP_400_BAD_REQUEST)

    def test_dispatch_page(self):
        """
        Test when KNET is setup and running
        """
        response = self.client.post(self.create_url, self.base_payload)
        url = response.data['url']
        path = urlparse(url).path
        view_name = resolve(path).url_name
        expected_url_name = 'dispatch'
        assert view_name == expected_url_name, 'upon create, the link should redirect to dispatch'

    # skip parsing the resource file
    @patch('gateway.knet.client.KnetClient.__init__', return_value=None)
    @patch('gateway.knet.client.KnetClient.connect', return_value='6536968592062610:http://example.com/')
    def test_redirect_to_psp_page(self, mock1, mock2):
        """
        Test when KNET is setup and running
        """
        response = self.client.post(self.create_url, self.base_payload)
        url = response.data['url']

        # this is dispatch url, accessing it should return the http redirect to psp url
        response = self.client.get(url)

        self.assertRedirects(self.client.get(response.url),
                             'http://example.com/?PaymentID=6536968592062610', fetch_redirect_response=False)

    @patch('gateway.models.PaymentAttempt.disclose_to_merchant', return_value=False)
    @patch('gateway.knet.client.KnetClient.__init__', return_value=None)
    @patch('gateway.knet.client.KnetClient.connect', return_value=None)
    def test_redirect_to_failure_page(self, mock1, mock2, mock3):
        """
        Test when error occurs between knpay and PSP and fails to get a redirect url
        :param mock1: side effect to reject the payment attempt
        :param mock2: return value False, mocking bad response from merchant server
        :return:
        """
        response = self.client.post(self.create_url, self.base_payload)
        url = response.data['url']

        response = self.client.get(url)
        response = self.client.get(response.url)

        attempt = PaymentAttempt.objects.latest()
        # assert redirection to internal page
        self.assertRedirects(response,
                             attempt.get_details_url(),
                             fetch_redirect_response=False)
        # assert attempt state changed
        self.assertEqual(attempt.state, PaymentAttempt.ERROR)
        # assert payment transaction state changed
        self.assertEqual(attempt.transaction.state, attempt.transaction.FAILED)

    @patch('gateway.knet.client.KnetClient.__init__', return_value=None)
    @patch('gateway.knet.client.KnetClient.connect', return_value='6536968592062610:http://example.com/')
    def test_knet_response(self, mock1, mock2):
        # create the payment request
        url = self.client.post(self.create_url, self.base_payload).data['url']
        response = self.client.get(url)
        # create the attempt
        self.client.get(response.url)
        attempt = PaymentAttempt.objects.latest()
        knet_payload = {'result': 'CAPTURED'}
        response = self.client.post(reverse('knet:response', args=(attempt.reference_number,)), data=knet_payload)
        self.assertIn('REDIRECT=', response.content)

    @patch('gateway.models.PaymentTransaction.can_be_redirected_to_merchant', return_value=True)
    @patch('gateway.models.PaymentTransaction.was_disclosed_to_merchant', return_value=True)
    @patch('gateway.knet.client.KnetClient.__init__', return_value=None)
    @patch('gateway.knet.client.KnetClient.connect', return_value='6536968592062610:http://example.com/')
    def test_redirect_to_merchant(self, mock1, mock2, mock3, mock4):
        # create payment request
        r = self.client.post(self.create_url, self.base_payload)
        url = r.data['url']
        # create attempt
        response = self.client.get(url)
        self.client.get(response.url)
        # simulate knet call
        knet_payload = {'result': 'CAPTURED'}
        attempt = PaymentAttempt.objects.latest()
        response = self.client.post(reverse('knet:response', args=(attempt.reference_number,)), data=knet_payload)
        url = response.content.split('=')[1]
        response = self.client.get(url)
        redirect_url = "%s?reference_number=%s&order_no=%s" % (self.REDIRECT_URL, attempt.reference_number, self.ORDER_NO)
        self.assertRedirects(response, redirect_url,
                             fetch_redirect_response=False)

    @patch('gateway.models.PaymentAttempt.disclose_to_merchant', return_value=False)
    @patch('gateway.knet.client.KnetClient.__init__', return_value=None)
    @patch('gateway.knet.client.KnetClient.connect', return_value='6536968592062610:http://example.com/')
    def test_redirect_to_internal_on_merchant_bad_response(self, mock1, mock2, mock3):
        # create payment request
        url = self.client.post(self.create_url, self.base_payload).data['url']
        # create attempt
        response = self.client.get(url)
        self.client.get(response.url)
        # simulate knet call
        knet_payload = {'result': 'CAPTURED'}
        attempt = PaymentAttempt.objects.latest()
        response = self.client.post(reverse('knet:response', args=(attempt.reference_number,)), data=knet_payload)
        url = response.content.split('=')[1]
        response = self.client.get(url)
        redirect_url = reverse('ui:attempt_detail', args=(attempt.reference_number,))
        self.assertRedirects(response, redirect_url,
                             fetch_redirect_response=False)


class TestFullKNETInboundAPI(APITestCase):
    client_class = APIClient
    DISCLOSURE_URL = 'http://example.com'
    REDIRECT_URL = 'http://example.com'
    ORDER_NO = '234'
    BASE_PAYLOAD = {
        'amount': '230.000',
        'gateway_code': 'knet-test',
        'order_no': ORDER_NO,
        'redirect_url': REDIRECT_URL,
        'disclosure_url': DISCLOSURE_URL,
        'currency_code': 'KWD',
        'language': "en",
        'customer_first_name': "Grigore",
        'customer_last_name': "Alexandrescu",
        'customer_email': "grigore.alexandrescu@example.com",
        'customer_phone': '+40741123321',
        'customer_address_line1': "satul prahova",
        'customer_address_line2': "nr 342",
        'customer_address_city': "Moldova de Sus",
        'customer_address_country': "KW",
        'customer_address_postal_code': "123617",
    }
    extra = {"cart_id": 1}

    def setUp(self):
        extra = {"cart_id": 1}
        self.base_payload = self.BASE_PAYLOAD
        knet_settings_factory()
        self.create_url = reverse('pos_create')
        self.base_payload.update({'extra': extra})
        APISecurityConfig.objects.all().delete()
        self.api_security_config = APISecurityConfig.objects.create(
            whitelisted_ips="*", ecommerce_on=True, dashboard_on=True, legacy_api_on=True)

    @patch('gateway.models.PaymentTransaction.can_be_redirected_to_merchant', return_value=True)
    @patch('gateway.models.PaymentTransaction.was_disclosed_to_merchant', return_value=True)
    @patch('gateway.knet.client.KnetClient.__init__', return_value=None)
    @patch('gateway.knet.client.KnetClient.connect', return_value='6536968592062610:http://example.com/')
    def test_redirect_to_merchant(self, mock1, mock2, mock3, mock4):
        # create payment request
        r = self.client.post(self.create_url, json.dumps(self.base_payload), content_type="application/json")
        # pytest.set_trace()
        url = r.data['url']
        # create attempt
        response = self.client.get(url)
        self.client.get(response.url)
        # simulate knet call
        knet_payload = {'result': 'CAPTURED'}
        attempt = PaymentAttempt.objects.latest()
        response = self.client.post(reverse('knet:response', args=(attempt.reference_number,)), data=knet_payload)
        url = response.content.split('=')[1]
        response = self.client.get(url)

        # assert it contains the params from extra field
        redirect_url = "%s?reference_number=%s&cart_id=%s&order_no=%s" % (self.REDIRECT_URL, attempt.reference_number, self.extra['cart_id'], self.ORDER_NO, )
        self.assertRedirects(response, redirect_url,
                             fetch_redirect_response=False)


