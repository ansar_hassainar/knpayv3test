# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.shortcuts import redirect


class ExpireMiddleware(object):
    """
    Redirect to expired page, if the knpay is expired
    """
    # def __init__(self, get_response):
    #     self.get_response = get_response
    #
    # def __call__(self, request):
    #     response = self.get_response(request)
    #     return response
    # #
    # # def process_view(self, request, *view_args, **view_kwargs):
    #

    def process_response(self, request, response):
        from django.conf import settings
        if settings.EXPIRED:
            ignored_paths = [reverse(view_name) for view_name in settings.EXPIRED_IGNORED_URLS]
            for ignored_path in ignored_paths:
                if ignored_path in request.path:
                    return response
            return redirect('expired')
        return response

