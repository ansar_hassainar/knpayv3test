# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.admin import AdminSite
from django.views.generic import RedirectView
from django.views.i18n import javascript_catalog
from django.utils.translation import ugettext_lazy as _

from two_factor.urls import urlpatterns as tf_urls

from api.views import ECommerceAPIView, DashboardAPIView
from .views import ReportView, ExpiredView
from extras.dashboard.views import send_communication_event
from config.models import Config


def bad(request):
    """ Simulates a server error """
    1 / 0


js_info_dict = {
    'domain': 'djangojs',
    'packages': ('knpay',)
}

try:
    c = Config.get_solo()
    if hasattr(c, 'enable_2fa_on') and c.enable_2fa_on == c.ALL:
        from two_factor.admin import AdminSiteOTPRequired
        admin.site.__class__ = AdminSiteOTPRequired
except Exception as e:
    import logging
    logger = logging.getLogger('config')
    logger.exception('Error accessing config model %s' % e)

AdminSite.site_header = _('Administration')
AdminSite.site_title = _('Site admin')
AdminSite.index_title = _('Administration')

urlpatterns = [
    url(r'', include(tf_urls)),
    url(r'^api/', include('legacy.urls', namespace='legacy')),

    url(r'^api/v1/', include('extras.account.rest_api', namespace='account')),
    url(r'^api/v1/', include('api.rest_api', namespace='apis')),
    url(r'^pbl/', include('api.public_urls', namespace='public_api')),

    url(r'^bad/$', bad),
    url(r'^expired/$', ExpiredView.as_view(), name='expired'),
    url(r'^pos/crt/$',
        ECommerceAPIView.as_view({'post': 'create'}), name='pos_create'),
    url(r'^rp/$', ReportView.as_view()),
    url(r'^pos/d/crt/$', DashboardAPIView.as_view({'post': 'create'}), name='pos_d_create'),
    url(r'^jsi18n/$', javascript_catalog, js_info_dict, name='javascript-catalog'),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^kp/', include('gateway.kpay.urls', namespace='kpay')),
    url(r'^kt/', include('gateway.knet.urls', namespace='knet')),
    url(r'^ms/', include('gateway.migs.urls', namespace='migs')),
    url(r'^bn/', include('gateway.burgan.urls', namespace='burgan')),
    url(r'^cs/', include('gateway.cybersource.urls', namespace='cs')),
    url(r'^pp/', include('gateway.pp.urls', namespace='paypal')),
    url(r'^mpgs/', include('gateway.mpgs.urls', namespace='mpgs')),
    url(r'^gw/', include('gateway.urls', namespace='gateway')),
    url(r'^django-rq/', include('django_rq.urls')),
    url(r'^activity/', include('actstream.urls')),
    url(r'^spf/', include('plugins.shopify.urls', namespace='shopify')),
    url(r'^messenger/', include('messenger.urls',namespace='messenger')),
    url(r'^dbmail/', include('dbmail.urls')),
]

urlpatterns += i18n_patterns(
    url(r'^pos/', include('extras.ui.urls', namespace='ui')),
    url(r'^jet/', include('jet.urls', 'jet')),
    url(r'^%s/' % settings.ADMIN_URL, admin.site.urls, name='admin'),
    url(r'^calendar/', include('schedule.urls')),
    url(r'^', include('extras.dashboard.urls', namespace='dashboard')),
    url(r'^', include('extras.account.urls', namespace='accounts')),
    url(r'^', include('plugins.urls', namespace='plugins')),
)

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)

if 'rest_framework_swagger' in settings.INSTALLED_APPS:
    from rest_framework_swagger.views import get_swagger_view

    schema_view = get_swagger_view(title='Knpay API',
                                   patterns=[
                                       url(r'^api/v1/',
                                           include('extras.account.rest_api',
                                                   namespace='account')),
                                       url(r'^api/v1/', include('api.rest_api',
                                                                namespace='apis')),
    url(r'^send-communication-event/(?P<transaction_pk>\d+)/(?P<event_type>\w+)$',
        send_communication_event, name='send_communication_event'),
                                   ])

    urlpatterns += [url(r'^api-docs/$', schema_view)]

handler404 = 'knpay.views.handler404'
handler500 = 'knpay.views.handler500'
