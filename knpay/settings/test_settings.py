# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from .base import *

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": ":memory:",
    }
}
ALLOWED_HOSTS = ['*']
ROOT_URLCONF = 'knpay.urls'

EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'

SECRET_KEY = 'hush' * 4

SECURE_SSL_REDIRECT = False
PROTOCOL = 'http'
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
        'LOCATION': ''
    },
    'axes_cache': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

ADMIN_URL = 'admin'

RAVEN_CONFIG = {}
