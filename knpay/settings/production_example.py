# -*- coding: utf-8 -*-
"""
Production Configurations

"""
from __future__ import absolute_import, unicode_literals

from .base import *


# add IP + website link
ALLOWED_HOSTS = []

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': '',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
        'OPTIONS': {
            'init_command': 'SET storage_engine=InnoDB; SET sql_mode="STRICT_TRANS_TABLES"',
            'charset': 'utf8',
            'use_unicode': True,
        },
    },
}

ADMINS = (
    ('dacian', 'dacian@kuwaitnet.com'),
    ('saif', 'saif@kuwaitnet.com')
)

# SECURITY WARNING: keep the secret key used in production secret!
# GENERATE A NEW KEY 50 random chars key for every installation
SECRET_KEY = None

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = ''
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = False
EMAIL_PORT = 25
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
SERVER_EMAIL = EMAIL_HOST_USER

# define custom url for each installation
# must be provided by who requested to deploy the installation
ADMIN_URL = 'admin'


if not DEBUG:
    REST_FRAMEWORK['DEFAULT_RENDERER_CLASSES'] = ('rest_framework.renderers.JSONRenderer',)


# auth token for centralised dashboard
# must be provided by who requested to deploy the installation
CENTRAL_AUTH_TOKEN = ''


# absolute path of old knpay root folder (where manage_server.py is located)
# To be added only when old knpay is migrated to new
LEGACY_CURRENCY = 'KWD'
FIXTURE_DIRS += ['/home/kwknpay/knpay/legacy_data']

# Fallback Default email for transaction creation
MERCHANT_DEFAULT_EMAIL = ''
