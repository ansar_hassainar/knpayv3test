"""
Django settings for ctc project.
"""
from os.path import abspath, basename, dirname, join, normpath
from sys import path

from datetime import timedelta

from django.utils.translation import ugettext_lazy as _

BASE_DIR = dirname(dirname(__file__)+"../../../")

########## PATH CONFIGURATION
# Absolute filesystem path to the config directory:
CONFIG_ROOT = dirname(dirname(abspath(__file__)))

# Absolute filesystem path to the project directory:
PROJECT_ROOT = dirname(CONFIG_ROOT)

# Absolute filesystem path to the django repo directory:
DJANGO_ROOT = dirname(PROJECT_ROOT)

# Project name:
PROJECT_NAME = basename(PROJECT_ROOT).capitalize()

# Project folder:
PROJECT_FOLDER = basename(PROJECT_ROOT)

# Project domain:
PROJECT_DOMAIN = '%s.com.kw' % PROJECT_NAME.lower()

# Add our project to our pythonpath, this way we don't need to type our project
# name in our dotted import paths:
path.append(CONFIG_ROOT)
########## END PATH CONFIGURATION


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

########## DEBUG CONFIGURATION
# https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = STAGING = False
########## END DEBUG CONFIGURATION

ADMINS = (
    ('dacian', 'dacian@kuwaitnet.com'),
    ('akif', 'akif@kuwaitnet.com')
)
SUPPORT_SUBJECTS_EMAILS = {
    "support_request": ["support@knpay.com"],
    "suggestion": ["support@knpay.com"],
    "feedback": ["support@knpay.com"]
}
# Application definition

DJANGO_APPS = [
    'modeltranslation',
    'django.contrib.sites',
    'jet',
    'django.contrib.admin',
    'django.contrib.auth',
    'polymorphic',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'axes',
]

THIRD_PARTY_APPS = [
    'rules.apps.AutodiscoverRulesConfig',
    'django_extensions',
    'rest_framework',
    'rest_framework.authtoken',
    'solo',
    'django_rq',
    'import_export',
    'raven.contrib.django.raven_compat',
    'django_fsm',
    'fsm_admin',
    'crispy_forms',
    'ckeditor',
    'unfriendly',
    'el_pagination',
    'actstream',
    'jsonfield',
    'django_bootstrap_breadcrumbs',
    'django_filters',
    'dbmail',
    'schedule',
    # 'djangobower',
    'compressor',
    'logentry_admin',
    # 'paypal.standard.ipn',
    'easy_thumbnails',
    'phonenumber_field',
    # form widget for international telephone numbers
    'intl_tel_input',

    # Two factor Authorization
    'django_otp',
    'django_otp.plugins.otp_static',
    'django_otp.plugins.otp_totp',
    'two_factor',
]

PROJECT_APPS = [
    'api',
    'config',
    'extras.account',
    'extras.currency',
    'extras.dashboard',
    'extras.dashboard.bulk',
    'extras.ui',
    'gateway',
    'gateway.knet',
    'gateway.migs',
    'gateway.burgan',
    'gateway.cybersource',
    'extras.request',
    'gateway.pp',
    'gateway.mpgs',
    'gateway.kpay',
    'sms',
    "plugins",
    'plugins.shopify',
    "plugins.code_pairing",
    "plugins.catalogue",
    'legacy',
]

EXTENSION_APPS = [
    # 'moderation',
]

# https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + PROJECT_APPS + EXTENSION_APPS
########## END APP CONFIGURATION

MIDDLEWARE_CLASSES = [
    'knpay.middleware.ExpireMiddleware',
    'django.middleware.gzip.GZipMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django_otp.middleware.OTPMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'extras.request.middleware.RequestLogMiddleware',
    'extras.request.middleware.Enforce2FA',
]

ROOT_URLCONF = 'knpay.urls'


WSGI_APPLICATION = 'knpay.wsgi.application'


# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {'min_length': 8}
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
    {
        'NAME': 'extras.account.password_validation.SpecialCharValidator',
    },
    {
        'NAME': 'extras.account.password_validation.DigitCountValidator',
    },
    {
        'NAME': 'extras.account.password_validation.UpperCaseValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'en'

LANGUAGES = (
    ('en', _('English')),
    ('ar', _('Arabic')),
)

TIME_ZONE = 'Asia/Kuwait'


USE_I18N = True

USE_L10N = False

USE_TZ = True

LOCALE_PATHS = (normpath(join(PROJECT_ROOT, 'locale')),)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

STATIC_URL = '/static/'

# Media files (Uploaded images, videos, etc)
# https://docs.djangoproject.com/en/1.9/ref/settings/#std:setting-MEDIA_URL

MEDIA_URL = '/media/'

STATIC_ROOT = normpath(join(PROJECT_ROOT, 'assets'))
MEDIA_ROOT = normpath(join(PROJECT_ROOT, 'media'))
BOWER_COMPONENTS_ROOT = normpath(join(PROJECT_ROOT, 'components'))

BOWER_INSTALLED_APPS = (
    'jquery',
    'bootstrap'
)

STATICFILES_DIRS = [
    normpath(join(PROJECT_ROOT, 'static')),
]

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'djangobower.finders.BowerFinder',
    'compressor.finders.CompressorFinder',
)


########## LOGGING CONFIGURATION
# https://docs.djangoproject.com/en/dev/ref/settings/#logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'production_only': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
        'development_only': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    'formatters': {
        'verbose': {
            'format': '[%(asctime)s] %(levelname)-8s [%(name)s:%(lineno)s] %(message)s',
            'datefmt': '%m/%d/%Y %H:%M:%S',
        },
        'simple': {
            'format': '%(levelname)-8s [%(name)s:%(lineno)s] %(message)s',
        },
        "rq_console": {
            "format": "%(asctime)s %(message)s",
            "datefmt": "%H:%M:%S",
        },
    },
    'handlers': {
        "rq_console": {
            "level": "DEBUG",
            "class": "rq.utils.ColorizingStreamHandler",
            "formatter": "rq_console",
            "exclude": ["%(asctime)s"],
        },

        'sentry': {
            'level': 'ERROR',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
        },
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'default': {
            'level': 'DEBUG',
            'class': 'knpay.lib.colorstreamhandler.ColorStreamHandler',
        },
        'console_dev': {
            'level': 'DEBUG',
            'filters': ['development_only'],
            'class': 'knpay.lib.colorstreamhandler.ColorStreamHandler',
            'formatter': 'simple',
        },
        'console_prod': {
            'level': 'INFO',
            'filters': ['production_only'],
            'class': 'knpay.lib.colorstreamhandler.ColorStreamHandler',
            'formatter': 'simple',
        },
        'file_log': {
            'level': 'DEBUG',
            'filters': ['development_only'],
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': join(PROJECT_ROOT, 'logs/log.log'),
            'maxBytes': 1024 * 1024 * 100,
            'backupCount': 3,
            'formatter': 'verbose',
        },
        'gateway_file': {
            'level': 'INFO',
            'filters': ['production_only'],
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': join(PROJECT_ROOT, 'logs/gateway.log'),
            'maxBytes': 1024 * 1024 * 100,
            'backupCount': 3,
            'formatter': 'verbose',
        },
        'legacy_file': {
            'level': 'INFO',
            'filters': ['production_only'],
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': join(PROJECT_ROOT, 'logs/legacy.log'),
            'maxBytes': 1024 * 1024 * 100,
            'backupCount': 3,
            'formatter': 'verbose',
        },
        'bulk_file': {
            'level': 'INFO',
            'filters': ['production_only'],
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': join(PROJECT_ROOT, 'logs/bulk.log'),
            'maxBytes': 1024 * 1024 * 100,
            'backupCount': 3,
            'formatter': 'verbose',
        },
        'ui_file': {
            'level': 'INFO',
            'filters': ['production_only'],
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': join(PROJECT_ROOT, 'logs/ui.log'),
            'maxBytes': 1024 * 1024 * 100,
            'backupCount': 3,
            'formatter': 'verbose',
        },
        'catalogue_file': {
            'level': 'INFO',
            'filters': ['production_only'],
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': join(PROJECT_ROOT, 'logs/catalogue.log'),
            'maxBytes': 1024 * 1024 * 100,
            'backupCount': 3,
            'formatter': 'verbose',
        },
        'dashboard_file': {
            'level': 'INFO',
            'filters': ['production_only'],
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': join(PROJECT_ROOT, 'logs/dashboard.log'),
            'maxBytes': 1024 * 1024 * 100,
            'backupCount': 3,
            'formatter': 'verbose',
        },
        'currency_file': {
            'level': 'INFO',
            'filters': ['production_only'],
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': join(PROJECT_ROOT, 'logs/currency.log'),
            'maxBytes': 1024 * 1024 * 100,
            'backupCount': 3,
            'formatter': 'verbose',
        },

        'config_file': {
            'level': 'INFO',
            'filters': ['production_only'],
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': join(PROJECT_ROOT, 'logs/config.log'),
            'maxBytes': 1024 * 1024 * 100,
            'backupCount': 3,
            'formatter': 'verbose',
        },
        'api_file': {
            'level': 'INFO',
            'filters': ['production_only'],
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': join(PROJECT_ROOT, 'logs/api.log'),
            'maxBytes': 1024 * 1024 * 100,
            'backupCount': 3,
            'formatter': 'verbose',
        },
        'shopify_file': {
            'level': 'INFO',
            'filters': ['production_only'],
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': join(PROJECT_ROOT, 'logs/shopify.log'),
            'maxBytes': 1024 * 1024 * 100,
            'backupCount': 3,
            'formatter': 'verbose',
        },
        'communication_file': {
            'level': 'INFO',
            'filters': ['production_only'],
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': join(PROJECT_ROOT, 'logs/communication.log'),
            'maxBytes': 1024 * 1024 * 100,
            'backupCount': 3,
            'formatter': 'verbose',
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['production_only', ],
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
        },
    },
    # Catch-all modules that use logging
    # Writes to console and file on development, only to console on production
    'root': {
        'handlers': ['console_dev', 'console_prod', 'file_log'],
        'level': 'DEBUG',
    },
    'loggers': {
        # Email admins when 500 error occurs
        'django.request': {
            # 'handlers': ['mail_admins', 'console_dev', 'sentry'],
            'handlers': ['console_dev', 'sentry'],
            'level': 'ERROR',
            'propagate': False,
        },
        'gateway': {
            'handlers': ['gateway_file', 'sentry'],
            'level': 'INFO',
            'propagate': True,
        },
        'utils.views.mixins': {
            'handlers': ['gateway_file', 'sentry'],
            'level': 'INFO',
            'propagate': False,
        },
        'legacy': {
            'handlers': ['legacy_file', 'sentry'],
            'level': 'INFO',
            'propagate': False,
        },   
        'dashboard.bulk': {
            'handlers': ['bulk_file', 'sentry'],
            'level': 'INFO',
            'propagate': False,
        },
        'ui': {
            'handlers': ['ui_file', 'sentry'],
            'level': 'INFO',
            'propagate': False,
        },
        'catalogue': {
            'handlers': ['catalogue_file', 'sentry'],
            'level': 'INFO',
            'propagate': False,
        },
        'dashboard': {
            'handlers': ['dashboard_file', 'sentry'],
            'level': 'INFO',
            'propagate': False,
        },
        'currency': {
            'handlers': ['currency_file', 'sentry'],
            'level': 'INFO',
            'propagate': False,
        },
        'config': {
            'handlers': ['config_file', 'sentry'],
            'level': 'INFO',
            'propagate': False,
        },     
        'api': {
            'handlers': ['api_file', 'sentry'],
            'level': 'INFO',
            'propagate': False,
        },   
        'shopify': {
            'handlers': ['shopify_file', 'sentry'],
            'level': 'INFO',
            'propagate': False,
        },
        'communications': {
            'handlers': ['communication_file', 'mail_admins', 'sentry'],
            'level': 'INFO',
            'propagate': False,
        },
        "rq.worker": {
            "handlers": ["rq_console", "sentry"],
            # "handlers": ["rq_console",],
            "level": "DEBUG"
        },
        'urllib3': {
            "handlers": ["sentry"],
            "level": "INFO",
            "propagate": True
        }

    },
}
########## END LOGGING CONFIGURATION


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': (normpath(join(PROJECT_ROOT, 'templates')),),
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'extras.dashboard.context_processors.menu'
            ],
        },
    },
]

SITE_ID = 1

########## TESTING CONFIGURATION
# TEST_RUNNER = 'django.test.runner.DiscoverRunner'
TEST_RUNNER = 'knpay.runner.PyTestRunner'
########## END TESTING CONFIGURATION


CORS_ORIGIN_ALLOW_ALL = True

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.AllowAny'
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ),

    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        # 'rest_framework.renderers.TemplateHTMLRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),

    'DEFAULT_PARSER_CLASSES': (
        'rest_framework.parsers.JSONParser',
        'rest_framework.parsers.FormParser',
        'rest_framework.parsers.MultiPartParser'
    ),
    'DEFAULT_THROTTLE_CLASSES': (
        'rest_framework.throttling.AnonRateThrottle',
        'rest_framework.throttling.UserRateThrottle',
        'extras.dashboard.throttles.EventRateThrottle'
    ),
    'DEFAULT_THROTTLE_RATES': {
        'anon': '90000/day',
        'user': '90000/day',
        'event': '1000/min',
    },
    'NON_FIELD_ERRORS_KEY': 'non_field_errors',
    # 'EXCEPTION_HANDLER': 'knpay.error_handler.custom_exception_handler',
    'DEFAULT_PAGINATION_CLASS': 'knpay.custom_pagination.CustomPagination',
    'DEFAULT_FILTER_BACKENDS': ('django_filters.rest_framework.DjangoFilterBackend',),
    # 'VIEW_DESCRIPTION_FUNCTION': 'rest_framework_swagger.views.get_restructuredtext',
    'PAGE_SIZE': 50
    # 'DEFAULT_CONTENT_NEGOTIATION_CLASS': 'base.negotiation.BrowserOrAPIContentNegotiation'
}


SWAGGER_SETTINGS = {
    'exclude_namespaces': ['common', 'contact/create/', 'builder', 'contact'],
    'api_version': '1',
    'api_path': '/',
    'enabled_methods': [
        'get',
        'post',
        'put',
        'patch',
        'delete'
    ],
    'api_key': '',
    'is_authenticated': False,
    'is_superuser': False,
    'permission_denied_handler': None,
    'info': {},
    'unauthenticated_user': 'django.contrib.auth.models.AnonymousUser',
    'doc_expansion': 'none',
}

THUMBNAIL_HIGH_RESOLUTION = True

# THUMBNAIL_PROCESSORS = (
#     'easy_thumbnails.processors.colorspace',
#     'easy_thumbnails.processors.autocrop',
#     #'easy_thumbnails.processors.scale_and_crop',
#     'filer.thumbnail_processors.scale_and_crop_with_subject_location',
#     'easy_thumbnails.processors.filters',
# )

SECURE_SSL_REDIRECT = True
PROTOCOL = 'https'

MODELTRANSLATION_DEFAULT_LANGUAGE = 'en'
MODELTRANSLATION_LANGUAGES = ('en', 'ar')
# MODELTRANSLATION_TRANSLATION_FILES = (
#     'dbmail.translation',
# )


CRISPY_TEMPLATE_PACK = 'bootstrap3'

PAYKN_URL = 'https://pay.kn/yourls-api.php'
# KMSGER_URL = 'http://kmsger.knpay2.mykuwaitnet.net/mail/api/'
# KMSGER_URL = 'http://127.0.0.1:8002/mail/api/'


RQ_QUEUES = {
    'default': {
        'HOST': 'localhost',
        'PORT': 6379,
        'DB': 0,
        'PASSWORD': '',
        'DEFAULT_TIMEOUT': 360,
    },
    # 'high': {
    #     'URL': os.getenv('REDISTOGO_URL', 'redis://localhost:6379/0'), # If you're on Heroku
    #     'DEFAULT_TIMEOUT': 500,
    # },
    # 'low': {
    #     'HOST': 'localhost',
    #     'PORT': 6379,
    #     'DB': 0,
    # }
}

# ACTSTREAM_SETTINGS = {
#     # 'MANAGER': 'myapp.managers.MyActionManager',
#     'FETCH_RELATIONS': True,
#     'USE_PREFETCH': True,
#     'USE_JSONFIELD': True,
#     'GFK_FETCH_DEPTH': 1,
# }

BREADCRUMBS_TEMPLATE = 'django_bootstrap_breadcrumbs/bootstrap3.html'

RQ_SHOW_ADMIN_LINK = False

DB_MAILER_TRACK_ENABLE = True
DB_MAILER_ENABLE_LOGGING = True
DB_MAILER_ENABLE_CELERY = False

DB_MAILER_ALLOWED_MODELS_ON_ADMIN = [
    'MailFromEmailCredential',
    'MailFromEmail',
    'MailCategory',
    'MailTemplate',
    'MailLog',
    'MailBcc',
    'MailLogTrack',
    'MailBaseTemplate',
]

CKEDITOR_CONFIGS = {
    'default': {
        'extraPlugins': 'wordcount,notification',
    },
    'fillEmptyBlocks': False,
    'catalogue_ckeditor': {
        'height': 100,
        'width': 600
    },
    'merchant_ckeditor': {
        'height': 150,
        'width': 480
    }
}

# Dashboard urls for getting transactions and sending push notifications
DASHBOARD_BASE = 'http://dashboard.knpay.net'
DASHBOARD_URL = DASHBOARD_BASE + '/api/transactions'
SEND_NOTIFICATION_URL = DASHBOARD_BASE + '/api/v1/send_notifications'


# template
VAR_MAPPING = [
    # knet
    ('result', _("Result")),
    ('trackid', _("Track ID")),
    ('postdate', _("Post Date")),
    ('tranid', _("Transaction ID")),
    ('paymentid', _("Payment ID")),
    ('auth', _("Auth ID")),
    ('ref', _("Reference ID")),

    # CS
    ('decision', _("Decision")),
    ('transaction_id', _("Transaction ID")),

    # MiGS
    ('vpc_Message', _("Message")),
    ('vpc_ReceiptNo', _("Receipt No")),
    ('vpc_TransactionNo', _("Transaction No")),
]


LOG_NAMESPACES = ['knet', 'migs', 'burgan', 'cs', 'paypal', 'mpgs', 'kpay']
LOG_URL_NAMES = ['pos_create', 'pos_d_create']


JET_SIDE_MENU_CUSTOM_APPS = [
    ('auth', ['__all__']),
    ('admin', ['__all__']),
    ('sites', ['__all__']),
    ('catalogue', ['__all__']),
    ('config', ['__all__']),
    ('dbmail', ['__all__']),
    ('sms', ['__all__']),
    ('axes', ['__all__']),
    ('gateway', ['__all__']),
    ('currency', ['__all__']),
    ('dashboard', ['__all__']),
    ('bulk', ['__all__']),
    ('schedule', ['__all__']),
    ('plugins', ['__all__']),
    ('code_pairing', ['__all__']),
    ('shopify', ['__all__']),
    ('request', ['__all__']),
]

# AUTH_USER_MODEL = 'account.User'

AUTHENTICATION_BACKENDS = (
    'axes.backends.AxesModelBackend',
    'rules.permissions.ObjectPermissionBackend',
    'django.contrib.auth.backends.ModelBackend',
)

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    },
    'axes_cache': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}
AXES_CACHE = 'axes_cache'

EL_PAGINATION_PREVIOUS_LABEL = _('Previous')
EL_PAGINATION_NEXT_LABEL = _('Next')

FIXTURE_DIRS = [normpath(join(PROJECT_ROOT, 'fixtures'))]

VERSION_FILE = join(PROJECT_ROOT, 'knpay', '__init__.py')


with open(VERSION_FILE) as vf:
    release = vf.readline().split("'")[1]

RAVEN_CONFIG = {
    'dsn': 'https://6c3027a7a1ba43599df7d8da7e9ffc5d:c97fe232ab714976a87383535c06ceeb@sentry.mykuwaitnet.net/62',
    # If you are using git, you can also automatically configure the
    # release based on the git info.
    'release': release,
}

DB_MAILER_BACKEND = {
    'mail': 'messenger.backends.mail',
    'tts': 'dbmail.backends.tts',
    'sms': 'dbmail.backends.sms',
    'push': 'dbmail.backends.push',
    'bot': 'dbmail.backends.bot',
}

# Two factor auth settings
from django.core.urlresolvers import reverse_lazy
LOGIN_URL = reverse_lazy('two_factor:login')
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'
TWO_FACTOR_PATCH_ADMIN = False
# LOGIN_REDIRECT_URL = reverse_lazy('two_factor:profile')
TWO_FACTOR_QR_FACTORY = 'qrcode.image.pil.PilImage'
TWO_FACTOR_SMS_GATEWAY = False
TWO_FACTOR_CALL_GATEWAY = False

# Enable catalogue auto restore reserved quantity
# It requires rqworker rqscheduler to be ran on instance
CATALOGUE_STOCK_RESTORE = False


EXPIRED = False
EXPIRATION_DATE = '10-06-2019'
EXPIRED_IGNORED_URLS = ('expired', 'pos_create', 'pos_d_create')


CSRF_COOKIE_SECURE = True


THROTTLING_RATE = '30/min'


AXES_FAILURE_LIMIT = 5
AXES_COOLOFF_TIME = timedelta(seconds=120)
AXES_COOLOFF_MESSAGE = _('Account locked: too many login attempts. '
                         'Please try again after 2 minutes with correct username and password.')
