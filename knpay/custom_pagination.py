from collections import OrderedDict

from rest_framework.pagination import LimitOffsetPagination
from rest_framework.response import Response


class CustomPagination(LimitOffsetPagination):
    def get_paginated_response(self, data):
        next_link = self.get_next_link()
        previous_link = self.get_previous_link()

        response = Response(OrderedDict([
            ('count', self.count),
            ('next', '?'+next_link.rsplit('?', 1)[-1] if next_link else next_link),
            ('previous', '?'+previous_link.rsplit('?', 1)[-1] if previous_link else previous_link),
            ('results', data)
        ]))

        return response
