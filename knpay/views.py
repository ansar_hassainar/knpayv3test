from __future__ import unicode_literals

from datetime import date

from django.conf import settings
from django.views.generic import TemplateView
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import render, redirect

from rest_framework.mixins import ListModelMixin
from rest_framework.generics import GenericAPIView
from rest_framework.pagination import LimitOffsetPagination

from gateway.models import PaymentTransaction
from gateway.serializers import ReportSerializer
from utils.views.mixins import PageTitleMixin


def handler404(request):
    response = render(request, '404.html', {})
    response.status_code = 404
    return response


def handler500(request):
    response = render(request, '500.html', {})
    response.status_code = 500
    return response


class ReportView(ListModelMixin, GenericAPIView):
    """
    View for exposing transaction information to central dashboard
    """
    queryset = PaymentTransaction.objects.filter(is_sandbox=False)
    serializer_class = ReportSerializer
    pagination_class = LimitOffsetPagination

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ExpiredView(PageTitleMixin, TemplateView):
    """
    Expired view
    """
    template_name = 'expired.html'
    page_title = _("Expired")

    def get(self, request, *args, **kwargs):
        if settings.EXPIRED:
            return super(ExpiredView, self).get(request, *args, **kwargs)
        else:
            return redirect('/')

    def get_context_data(self, **kwargs):
        context = super(ExpiredView, self).get_context_data(**kwargs)
        try:
            day, month, year = map(int, settings.EXPIRATION_DATE.split('-'))
            termination_date = date(year=year, month=month, day=day)
            days_left = termination_date - now().date()
            days_left = days_left.days if days_left.days > 0 else 0
        except Exception as e:
            print e
            # TODO: log the exception or move the date to db maybe?
            termination_date = None
            days_left = 0
            pass

        context.update({
            'termination_date': termination_date,
            'days_left': days_left
        })
        return context

