# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.utils.translation import activate, ugettext as _
from rest_framework.views import exception_handler


def render_error_message(field_name, error_message, language):
    activate(language)

    if field_name == 'non_field_errors':
        return " ".join(error_message)
    else:
        return '%(key)s: %(value)s' % {'key': _(field_name),
                                       'value': _(" ".join(error_message))}


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)
    # from pprint import pprint
    # print pprint(context['request']._request.__dict__)

    if response is not None:
        # check if exception has dict items
        if hasattr(exc, 'detail') and hasattr(exc.detail, 'items'):
            errors = {
                'en': [],
                'ar': []
            }

            for key, value in exc.detail.items():
                errors['en'].append(render_error_message(key, value, 'en'))
                errors['ar'].append(render_error_message(key, value, 'ar'))
                activate('en')

            # add property errors to the response
            response.data = errors
    return response
