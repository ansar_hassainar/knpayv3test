Basic installation
------------------

1. Checkout to the correct branch:

    - $ git checkout master
    - $ git pull origin master

2. Create production.py from example_production.py and fill it accordingly:

    - ``ALLOWED_HOSTS`` - [] Contains the domain name and the ip of the server
    - ``DATABASES`` - Database details
    - ``SECRET_KEY`` - Has to be a random generated string of 50 chars. ATTENTION! For each new installation a new key has to be generated. Do not use the one present in example_production.py!
    - ``ADMIN_URL`` - The admin link. Must be UNIQUE for each installation, ie: kwtadmin
    - ``EMAIL SETTINGS`` - Mandatory for each installation
    - ``CENTRAL_AUTH_TOKEN`` - Mandatory for each installation. Has to be created from Central dashboard admin panel

3. Run the following commands:

    - $ pip install -r requirements.txt
    - $ ./manage.py migrate
    - $ ./manage.py collectstatic
    - $ ./manage.py sync_translation_fields --noinput
    - $ ./manage.py loaddata currencies
    - $ ./manage.py loaddata mail_data
    - $ ./manage.py loaddata plugins

4. Media & Static

    - Media folder: /knpayv3/media/
    - Static folder: /knpayv3/assets/

5. Cron

    - Daily @ midnight
        - $ python manage.py purge_requests 1 month

6. Admin

    - open ``<KNPAY_LINK>/<ADMIN_LINK>/sites/site/1/change/`` and update the values with <KNPAY_LINK>


BULK Setup
----------

    - setup redis
    - setup supervisor for running following commands, example file can be found in supervisor dir:
        - $ python manage.py rqworker default
        - $ python manage.py rqscheduler


CATALOGUE Setup
---------------

    - follow all the steps from bulk setup
    - set CATALOGUE_STOCK_RESTORE=True from production settings


How to add a new version
------------------------

    - Make sure all the features/bugfixes are tested and finalised
    - Make sure requirements are added to requirements/prod.txt
    - DO NOT RELEASE A NEW VERSION WITH UNFINISHED BUSINESS
    - New versions must be ALWAYS created from master branch
    - Merge the updates to master branch
    - This is the versioning mechanism (no need to do it manually, it's automatised):
        - 2.0.X - patch/bugfix
        - 2.X.0 - feature (minor release)
        - X.0.0 - major release
    - Execute the following commands for creating a new version:
        - $ ./manage.py create_version <patch> or <feature>
        - Go and update CHANGELOG.rst with what will be released in this version
        - Push the changes to the repository. Commit can be named `release version <version_number>`
        - Add git tags using `$ ./manage.py add_version_tag`


Developer notes
---------------

- permission name starting with `use_` is reserved and can be used only for gateway settings.
- as a convention, the owner field for any object is `initiator`