from fabric.api import run, cd, local, env, prefix
from contextlib import contextmanager as _contextmanager


def test():
    local("echo test")


params = {
    "HOME_DIR": "/var/www/html",
    "PROJECT_DIR": "/var/www/html/knpayv3"
}


# dev testing
# params = {
#     "PROJECT_DIR": "/home/knpay3mykuwaitne/knpayv3",
#     "HOME_DIR": "/home/knpay3mykuwaitne"
# }
# env.user = "knpay3mykuwaitne"

env.user = "knpay"
env.port = 222
env.directory = params['HOME_DIR']
env.activate = "source %s/virtual/bin/activate" % params['HOME_DIR']


@_contextmanager
def virtualenv():
    with cd(env.directory):
        with prefix(env.activate):
            yield


def checkout(branch_to_checkout="master"):
    """Checkout the repository"""
    with cd(params["PROJECT_DIR"]):
        run('git pull origin {0}'.format(branch_to_checkout))


def status():
    """
    gets the current active branch and status
    """
    with cd(params["PROJECT_DIR"]):
        run('git status')
        run('git branch')


def install_dependencies():
    with virtualenv():
        with cd(params["PROJECT_DIR"]):
            run('pip install -r requirements.txt')


def run_migrations():
    with virtualenv():
        with cd(params["PROJECT_DIR"]):
            run('python manage.py migrate')


def collect_static():
    """
    collect static files
    """
    with virtualenv():
        with cd(params["PROJECT_DIR"]):
            run('python manage.py collectstatic --no-input')


def pull_version_info():
    """
    updates version info to show at admin
    """
    with virtualenv():
        with cd(params["PROJECT_DIR"]):
            run('python manage.py pull_version_info')


def update_email_context():
    """
    updates email context for the db mailer templates
    """
    with virtualenv():
        with cd(params["PROJECT_DIR"]):
            run('python manage.py update_email_context')


def restart_server():
    """
    Restart python executables
    """

    with cd(params["PROJECT_DIR"]):
        run('touch wsgi.py')


def deploy(branch_to_checkout="master"):
    local('echo %s' % branch_to_checkout)
    status()
    checkout(branch_to_checkout)
    install_dependencies()
    run_migrations()
    collect_static()
    pull_version_info()
    update_email_context()
    restart_server()
