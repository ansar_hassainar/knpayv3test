# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.conf import settings

from git import Repo
from utils.funcs import get_knpay_version


class Command(BaseCommand):
    help = """
        Version current commit with knpay version
    """

    def handle(self, **options):
        repo = Repo(settings.PROJECT_ROOT)
        tag_name = 'v%s' % get_knpay_version()
        new_tag = repo.create_tag(tag_name, message='Auto versioning tag.', lightweight=False)
        repo.remotes.origin.push(new_tag)
        self.stdout.write(self.style.SUCCESS('Tag %s pushed to repo successfully' % tag_name))
