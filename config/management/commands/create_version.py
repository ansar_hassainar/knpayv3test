# -*- coding: utf-8 -*-

from os.path import join

from datetime import datetime

from django.core.management.base import BaseCommand
from django.conf import settings


from utils.funcs import get_knpay_version

FEATURE = 'feature'
PATCH = 'patch'
CHANGELOG_FILE = join(settings.PROJECT_ROOT, 'CHANGELOG.rst')


def write_to_changelog(new_version):
    content = '''%(version)s\n%(dashes)s\n%(date)s\n\n''' % {
        'version': new_version,
        'dashes': '-' * len(new_version),
        'date': datetime.today().strftime("%B %d, %Y")
    }

    with open(CHANGELOG_FILE, "r+") as changelog:
        s = changelog.read()
        changelog.seek(0)
        changelog.write(content + s)


def calculate_new_version(type_, current_version):
    """
    Calculate what digit to increase based on the
    version type and return the new version string
    """
    split_version = current_version.split('.')
    if type_ == FEATURE:
        position = 1
        # if the update is feature
        # we need to reset the last digit to be 0
        split_version[-1] = '0'
    else:
        position = 2

    new_value = str(int(split_version[position]) + 1)

    for i, v in enumerate(split_version):
        if i == position:
            split_version[i] = new_value

    return '.'.join(split_version)


def write_new_version_to_file(new_version):
    file_content = "__version__ = '%s'\n" % new_version
    with open(settings.VERSION_FILE, 'w') as vf:
        vf.write(file_content)


class Command(BaseCommand):
    help = """
        Create new version of the KNPay.\n
        2.0.X - patch\n
        2.X.0 - feature (minor release)\n
        X.0.0 - major release\n
    """

    def add_arguments(self, parser):
        parser.add_argument('type', nargs=1, type=str,
                            choices=[FEATURE, PATCH],
                            help='Version type: feature or patch')

    def handle(self, *args, **options):
        type_ = options['type'][0]

        current_version = get_knpay_version()
        new_version = calculate_new_version(type_, current_version)

        print('Current version is %s' % current_version)
        print('New version will be %s' % new_version)

        write_new_version_to_file(new_version)
        write_to_changelog(new_version)

        self.stdout.write(
            self.style.SUCCESS('Version was updated. Header was added in CHANGELOG.RST\n'
                               'Go and update the file with the features/bugfixes you have done'))

