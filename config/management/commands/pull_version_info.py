# -*- coding: utf-8 -*-

import datetime

from django.core.management.base import BaseCommand
from django.conf import settings

from git import Repo

from utils.funcs import get_knpay_version
from config.models import About


class Command(BaseCommand):
    help = """
        Update current version release information on the current instance
    """

    def handle(self, *args, **options):
        repo = Repo(settings.PROJECT_ROOT)
        version = get_knpay_version()
        # release_commit = repo.commit('v'+version)
        master = repo.head.reference

        about_obj = About.get_solo()
        about_obj.version = version

        # about_obj.last_update = time.strftime("%Y-%m-%d", time.gmtime(release_commit.committed_date))
        about_obj.last_update = datetime.datetime.fromtimestamp(master.commit.committed_date)

        about_obj.save()
        self.stdout.write(self.style.SUCCESS('Version data updated '
                                             'successfully'))
