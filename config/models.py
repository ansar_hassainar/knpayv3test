# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging
from datetime import timedelta

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now
from django.db.models.signals import post_migrate

from model_utils import FieldTracker
from solo.models import SingletonModel
from ckeditor.fields import RichTextField as HTMLField

from utils.db.models import (EmailMailTemplate, SMSMailTemplate,
                             NotificationMailTemplate, MerchantUrls)
from utils.validators import validate_hh_mm, FileValidator


logger = logging.getLogger('config')


class MerchantDetails(SingletonModel):
    name = models.CharField(
        _("Name"), max_length=64,
        default='', blank=True
    )
    website = models.URLField(
        _("Merchant Website URL"), default='',
        blank=True)
    email = models.EmailField(
        _("Email",), max_length=64,
        default='', blank=True
    )
    phone = models.CharField(
        _("Customer Phone"), default='',
        max_length=16, blank=True
    )
    logo = models.ImageField(
        _("Logo"), blank=True, null=True,
        upload_to='config/merchant'
    )
    facebook_url = models.URLField(
        _("Merchant Facebook account URL"), default='',
        blank=True)
    twitter_url = models.URLField(
        _("Merchant Twitter account URL"), default='',
        blank=True)
    instagram_url = models.URLField(
        _("Merchant Instagram account URL"), default='',
        blank=True)
    favicon = models.ImageField(
        _("Favicon"),
        help_text=_("Upload only .png image of 32X32 pixels for "
                    "the best result."),
        validators=[FileValidator(allowed_mimetypes=['image/png'])],
        blank=True, null=True
    )
    merchant_address = HTMLField(
        _("Merchant Address"),
        help_text=_("Merchant address in multi-line format."),
        blank=True,
        default='',
        config_name='merchant_ckeditor'
    )

    def clean(self):
        self.logo.name = (self.logo.name).encode('unicode-escape')

    def __unicode__(self):
        return 'Merchant details'

    class Meta:
        verbose_name = _("Merchant details")
        # permissions = (
        #         ('view_merchantdetails', _('View Merchant Details')),
        #     )

    def get_name(self):
        return self.name or 'KNPay'

    def has_logo(self):
        try:
            return bool(self.logo.url)
        except (AttributeError, ValueError):
            return False


class Config(SingletonModel):
    ROUTE, MPP, NEXMO, TWILIO = 'route', 'mpp', 'nexmo', 'twilio'
    SMS_PROVIDER_CHOICES = (
        (ROUTE, _("Route")),
        (MPP, _("MPP")),
        (NEXMO, _("Nexmo")),
        (TWILIO, _("Twilio")),
    )
    DISABLED, DASHBOARD, ALL = 'disabled', 'dashboard', 'all'
    # 2fa choices
    TFA_CHOICES = (
        (DISABLED, _("Disabled")),
        (DASHBOARD, _("Only dashboard")),
        (ALL, _("Both dashboard and admin"))
    )
    title = models.CharField(
        _("Title"), max_length=64, default='KNPay',
        help_text=_("Name to be displayed in status bar, templates, etc.")
    )
    # redirect_url = models.URLField(_("Redirect URL"), default='', blank=True)
    # disclosure_url = models.URLField(_("Disclosure URL"), default='', blank=True)
    # error_url = models.URLField(_("Error URL"), default='', blank=True)
    secret_key = models.CharField(verbose_name=_("Secret Key"), default='VPLguAAl',
                                  max_length=8,)

    pay_kn_username = models.CharField(
        _("pay.kn username"), default='', max_length=64, blank=True,
        help_text=_("Credentials for login to short urls provider - pay.kn")
    )
    pay_kn_password = models.CharField(_("pay.kn password"), default='', max_length=64,
                                       blank=True)
    # kmsger_url = models.URLField(_("KMSGER url"), default='')
    # kmsger_api_key = models.CharField(_("KMSGER API key"), default='', max_length=128)
    is_paused = models.BooleanField(
        _("Is paused?"), default=False,
        help_text=_("Pause payment requests links and pay invoice section for indefinite "
                    "period of time. Unpause for reloading activity."))
    tracker = FieldTracker()
    transaction_expiration_time = models.CharField(
        _("Transaction expiration time"),
        max_length=10,
        default='',
        blank=True,
        validators=[validate_hh_mm],
        help_text=_("Define how many hours & minutes a PaymentTransaction "
                    "should be valid for payment. Use HH:MM format. "
                    "Leave it empty for infinite time.")
    )
    sms_provider = models.CharField(_("SMS Provider"), choices=SMS_PROVIDER_CHOICES,
                                    default='', blank=True, max_length=24)

    terms = models.BooleanField(
        _("Ask for terms & conditions"), default=False,
        help_text=_("Ask customer to accept terms and conditions before "
                    "proceeding with the payment")
    )
    terms_text = HTMLField(_("Terms and conditions"), blank=True,
                           default='')

    enable_2fa_on = models.CharField(_("Enable 2fa on"), choices=TFA_CHOICES,
                                     default=DISABLED, max_length=50)

    attach_invoice_pdf = models.BooleanField(
        _("Attach invoice pdf"),
        default=False,
        help_text=_("Attach invoice pdf for successful payments?")
    )

    invoice_pdf_template = HTMLField(
        _("Invoice pdf html"),
        blank=True,
        default=''
    )

    attach_catalogue_invoice_pdf = models.BooleanField(
        _("Catalogue payment attach invoice pdf"),
        default=False,
        help_text=_("Attach invoice pdf for successful catalogue payments?")
    )

    catalogue_invoice_pdf_template = HTMLField(
        _("Catalogue Invoice pdf html"),
        blank=True,
        default=''
    )

    context_note = models.TextField(
        _('Context note'), null=True, blank=True,
        help_text=_(
            'This is simple note field for context variables with description.'
        )
    )

    def has_expiration_time(self):
        return bool(self.transaction_expiration_time)

    def has_pauses(self, start):
        return self.pauses.filter(models.Q(started_at__gte=start) |
                                  models.Q(ended_at__gte=start)).exists()

    def get_expiration_time(self, start):
        """
        :param start: payment transaction creation time
        :return: timedelta object
        """
        if self.has_expiration_time():
            hours, minutes = self.transaction_expiration_time.split(':')
            expiration_time = timedelta(hours=int(hours), minutes=int(minutes))
            if self.has_pauses(start):
                return expiration_time + self.get_total_paused_period(start)
            return expiration_time
        return timedelta(hours=10000)

    def get_total_paused_period(self, start):
        pauses = self.pauses.filter(started_at__gte=start)
        total = timedelta(minutes=0)
        for pause in pauses:
            total += pause.duration

        # check if there was any pause when payment transaction
        # was created.
        before_pauses = self.pauses.filter(ended_at__gte=start).exclude(
            id__in=list(pauses.values_list('id', flat=True)))
        for pause in before_pauses:
            duration = pause.ended_at - start
            total += duration
        return total

    def __unicode__(self):
        return 'Configuration'

    class Meta:
        verbose_name = _("Configuration")


class About(SingletonModel):
    version = models.CharField(_("Version"), default='0.01', max_length=32)
    ssl_expiry = models.DateTimeField(_("SSL expiry date"), blank=True, null=True)
    live_date = models.DateField(_("Live date"), blank=True, null=True)
    last_update = models.DateField(_("Last update"), blank=True, null=True)
    renewal_date = models.DateField(_("Renewal date"), blank=True, null=True)

    def __unicode__(self):
        return 'About'

    class Meta:
        verbose_name = _("About")


class ECommerceConfig(EmailMailTemplate,
                      SMSMailTemplate,
                      NotificationMailTemplate,
                      MerchantUrls,
                      SingletonModel):

    class Meta:
        verbose_name = _('E-Commerce Config')
        verbose_name_plural = _('E-Commerce Config')

    def __unicode__(self):
        return 'E-Commerce Config'

    def extra_fields(self):
        return {}


class KNPaySection(models.Model):
    """
    So far knpay is split in 4 sections:
        - payment requests
        - customer payments
        - e-commerce
        - third party (shopify)

    We need to store these sections in database
    for being able how to define which gateways are avaible where.
    """
    from gateway.models import PaymentTransaction, GatewaySettings

    name = models.CharField(_("Name"), choices=PaymentTransaction.TYPE_CHOICES,
                            max_length=64)

    def __unicode__(self):
        return self.get_name_display()

    class Meta:
        verbose_name = _("KNPay section")
        verbose_name_plural = _("KNPay sections")


class Pause(models.Model):
    """
    Record start/end of installation pause
    """
    config = models.ForeignKey(Config, verbose_name=_("Config"),
                               related_name='pauses', null=True)
    started_at = models.DateTimeField(_("Started at"), default=now, db_index=True)
    ended_at = models.DateTimeField(_("Ended at"), db_index=True, null=True)

    @property
    def duration(self):
        if self.started_at and self.ended_at:
            return self.ended_at - self.started_at
        return now() - self.started_at

    def __unicode__(self):
        return '%s - %s' % (self.started_at, self.ended_at)

    class Meta:
        verbose_name = _("Pause")
        verbose_name_plural = _("Pauses")
        get_latest_by = 'started_at'


class APISecurityConfig(SingletonModel):
    
    ecommerce_on = models.BooleanField(
        _("Ecommerce API"), default=False, help_text=_("Enable the ECommerce API."))
    dashboard_on = models.BooleanField(
        _("Dashboard API"), default=False, help_text=_("Enable the Dashboard API."))
    legacy_api_on = models.BooleanField(
        _("Legacy API"), default=False, help_text=_("Enable the Legacy API."))
    whitelisted_ips = models.TextField(
        _("Whitelisted IP Addresses"), default='',
        help_text=_(
            "Define the IPs which can access the APIs as comma separated values or use * to whitelist all IPs.")
    )

    class Meta:
        verbose_name = _("API Security Config")


@receiver(post_save, sender=Config)
def record_paused_time(sender, instance, **kwargs):
    if instance.tracker.has_changed('is_paused'):
        if instance.is_paused:
            Pause.objects.create(config=instance)
        else:
            pause = Pause.objects.latest()
            pause.ended_at = now()
            pause.save()
