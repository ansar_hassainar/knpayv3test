# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from solo.admin import SingletonModelAdmin
from modeltranslation.admin import TabbedDjangoJqueryTranslationAdmin

from gateway.models import GatewaySettings
from .forms import ConfigForm
from .models import MerchantDetails, About, Config, ECommerceConfig, Pause, KNPaySection, APISecurityConfig


@admin.register(MerchantDetails)
class MerchantDetailsAdmin(SingletonModelAdmin):
    pass


class PauseInline(admin.TabularInline):
    model = Pause
    extra = 0
    readonly_fields = ('started_at', 'ended_at')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Config)
class ConfigAdmin(SingletonModelAdmin, TabbedDjangoJqueryTranslationAdmin):
    form = ConfigForm
    inlines = [PauseInline]
    readonly_fields = ('context_note',)
    fieldsets = (
        (None, {
            'fields': ('title',)
        }),
        (_("Merchant"), {
            'fields': ('secret_key', )
        }),
        (_("Payment"), {
            'fields': ('transaction_expiration_time',),
        }),
        (_("Two-factor"), {
            'fields': ('enable_2fa_on',),
        }),
        (_("Invoice attachment"), {
            'fields': ('attach_invoice_pdf', 'invoice_pdf_template',
                       'attach_catalogue_invoice_pdf',
                       'catalogue_invoice_pdf_template',
                       'context_note'),
        }),
        (_("Short urls"), {
            'fields': ('pay_kn_username', 'pay_kn_password')
        }),
        (_("Pause"), {
            'fields': ('is_paused',)
        }),
        (_("Terms"), {
            'fields': ('terms', 'terms_text')
        }),
    )

    class Media:
        js = ("js/admin/invoice_field_show_hide.js",)


@admin.register(About)
class AboutAdmin(SingletonModelAdmin):
    readonly_fields = ('version', 'last_update')


@admin.register(KNPaySection)
class KNPaySectionAdmin(admin.ModelAdmin):
    readonly_fields = ('name',)
    actions = None
    fields = ('name',)

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False


@admin.register(APISecurityConfig)
class APISecurityConfigAdmin(SingletonModelAdmin):
    readonly_fields = ()
        
@admin.register(ECommerceConfig)
class ECommerceConfigAdmin(SingletonModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('redirect_url', 'disclosure_url')
        }),
        (_("Communication"), {
            'fields': ('email_payment_details_success_template',
                       'email_payment_details_failure_template',
                       'sms_payment_details_success_template',
                       'sms_payment_details_failure_template',
                       'customer_payment_email_notification_template',
                       'customer_payment_sms_notification_template',

                       )
        }),
    )