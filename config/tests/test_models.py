# -*- coding: utf-8 -*-

from django.test import TestCase
from django.utils.timezone import now, timedelta

import pytest
from mixer.backend.django import mixer

from ..models import Config


pytestmark = pytest.mark.django_db


class TestConfig(TestCase):
    def test_has_expiration_time(self):
        config = Config.get_solo()
        assert config.has_expiration_time() is False, 'if no expiration time set' \
                                                           'it should return False'
        config.transaction_expiration_time = '0:2'
        config.save()
        config.refresh_from_db()
        assert config.has_expiration_time(), 'once expiration time set,' \
                                                  'it should return True'

    def test_has_pauses(self):
        config = Config.get_solo()
        # start pause
        config.is_paused = True
        config.save()
        start = now() - timedelta(minutes=1)
        assert config.has_pauses(start), 'a pause model shall be created'

        # stop pause
        config.is_paused = False
        config.save()
        start = now() + timedelta(minutes=1)
        assert config.has_pauses(start) is False, 'pause period shall be ended'

    def test_create_pauses(self):
        config = Config.get_solo()
        config.is_paused = True
        config.save()
        config.is_paused = False
        config.save()
        pause = config.pauses.latest()
        assert bool(pause.started_at), 'started at should be added'
        assert bool(pause.ended_at), 'ended at should be added'

    def test_get_expiration_time(self):
        config = Config.get_solo()
        config.transaction_expiration_time = ''
        config.save()
        start = now()
        # check with pauses off
        assert config.get_expiration_time(start) == timedelta(hours=10000), 'should return infinite'

        config.transaction_expiration_time = '0:2'
        config.save()
        assert config.get_expiration_time(start) == timedelta(minutes=2), 'should return 2 minutes'
        config.transaction_expiration_time = '2:2'
        assert config.get_expiration_time(start) == timedelta(hours=2, minutes=2), \
            'should return 2 hours & 2 minutes'

        # check with pause on
        # pause period should be ignored
        config.is_paused = True
        config.save()
        config.is_paused = False
        config.save()
        pause = config.pauses.latest()
        pause.ended_at += timedelta(hours=100)
        pause.save()
        assert int(config.get_expiration_time(start).total_seconds()) == \
               int(timedelta(hours=102, minutes=2).total_seconds()), \
            'should return 102 hours & 2 minutes'

    def test_get_total_paused_period(self):
        config = Config.get_solo()
        # create `payment link` when pause is off
        start = now()

        # start pause
        config.is_paused = True
        config.save()
        config.is_paused = False
        config.save()
        # adjust a bit the time of ending
        pause = config.pauses.first()
        pause.ended_at += timedelta(seconds=60)
        pause.save()
        pause_time = config.get_total_paused_period(start)
        assert int(pause_time.total_seconds()) == 60, 'should be only 1 minute pause'

        # create `payment link` when pause is on
        config.is_paused = True
        config.save()
        start = now() + timedelta(seconds=60)

        config.is_paused = False
        config.save()
        pause = config.pauses.latest()
        # modify the ended_at time in order to have pause
        # on when `payment link` was created
        pause.ended_at += timedelta(seconds=120)
        pause.save()
        pause_time = config.get_total_paused_period(start)
        assert int(pause_time.total_seconds()) == 60, 'should be only 1 minute pause'


class TestPause(TestCase):
    def test_duration(self):
        # check for ongoing pause
        pause = mixer.blend('config.Pause', config=None, started_at=now() - timedelta(minutes=1), ended_at=None)
        assert int(pause.duration.total_seconds()) == int(timedelta(minutes=1).total_seconds()), \
            'should return 1 minute'

        # check for ended pause
        pause.ended_at = pause.started_at + timedelta(minutes=1)
        pause.save()
        assert pause.duration == timedelta(minutes=1), 'pause should have been only ' \
                                                       '1 min'



