# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django import forms


class ConfigForm(forms.ModelForm):
    class Meta:
        fields = "__all__"
        widgets = {
            'secret_key': forms.PasswordInput(render_value=True),
            'pay_kn_password': forms.PasswordInput(render_value=True),
            'kmsger_api_key': forms.PasswordInput(render_value=True),
        }
