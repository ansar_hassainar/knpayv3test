# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from modeltranslation.translator import translator, TranslationOptions

from .models import Config


class ConfigOptions(TranslationOptions):
    fields = ('invoice_pdf_template',
              'catalogue_invoice_pdf_template')


translator.register(Config, ConfigOptions)
