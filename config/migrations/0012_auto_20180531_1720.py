# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-05-31 11:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('config', '0011_knpaysection'),
    ]

    operations = [
        migrations.AddField(
            model_name='config',
            name='enable_2fa_on',
            field=models.CharField(choices=[('disabled', 'Disabled'), ('dashboard', 'Only dashboard'), ('all', 'Both dashboard and admin')], default='disabled', max_length=50, verbose_name='Enable 2fa on'),
        ),
        migrations.AlterField(
            model_name='knpaysection',
            name='name',
            field=models.CharField(choices=[('e_commerce', 'E-Commerce'), ('payment_request', 'Payment request'), ('catalogue', 'Catalogue Purchase'), ('customer_payment', 'Customer payment'), ('third_party', 'Third party')], max_length=64, verbose_name='Name'),
        ),
    ]
