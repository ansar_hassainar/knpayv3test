# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-11-26 12:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('config', '0015_merchantdetails_merchant_address'),
    ]

    operations = [
        migrations.CreateModel(
            name='APISecurityConfig',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ecommerce_on', models.BooleanField(default=False, help_text='Enable the ECommerce API.', verbose_name='Ecommerce API')),
                ('dashboard_on', models.BooleanField(default=False, help_text='Enable the Dashboard API.', verbose_name='Dashboard API')),
                ('legacy_api_on', models.BooleanField(default=False, help_text='Enable the Legacy API.', verbose_name='Legacy API')),
                ('whitelisted_ips', models.TextField(default='', help_text='Define the IPs which can access the APIs as comma separated values or use * to whitelist all IPs.', verbose_name='Whitelisted IP Addresses')),
            ],
            options={
                'verbose_name': 'API Security Config',
            },
        ),
    ]
