# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-06-07 03:49
from __future__ import unicode_literals

import ckeditor.fields
from django.db import migrations, models
import utils.validators


class Migration(migrations.Migration):

    dependencies = [
        ('config', '0012_auto_20180531_1720'),
    ]

    operations = [
        migrations.AddField(
            model_name='config',
            name='attach_invoice_pdf',
            field=models.BooleanField(default=False, help_text='Attach invoice pdf for successful payments?', verbose_name='Attach invoice pdf'),
        ),
        migrations.AddField(
            model_name='config',
            name='invoice_pdf_template',
            field=ckeditor.fields.RichTextField(blank=True, default='', verbose_name='Invoice pdf html'),
        ),
        migrations.AddField(
            model_name='config',
            name='context_note',
            field=models.TextField(blank=True,
                                   help_text='This is simple note field for context variables with description.',
                                   null=True, verbose_name='Context note'),
        ),

        migrations.AddField(
            model_name='config',
            name='invoice_pdf_template_ar',
            field=ckeditor.fields.RichTextField(blank=True, default='',
                                                null=True,
                                                verbose_name='Invoice pdf html'),
        ),
        migrations.AddField(
            model_name='config',
            name='invoice_pdf_template_en',
            field=ckeditor.fields.RichTextField(blank=True, default='',
                                                null=True,
                                                verbose_name='Invoice pdf html'),
        ),
        # migrations.AddField(
        #     model_name='config',
        #     name='terms_text_ar',
        #     field=ckeditor.fields.RichTextField(blank=True, default='', null=True, verbose_name='Terms and conditions'),
        # ),
        # migrations.AddField(
        #     model_name='config',
        #     name='terms_text_en',
        #     field=ckeditor.fields.RichTextField(blank=True, default='', null=True, verbose_name='Terms and conditions'),
        # ),
        migrations.AddField(
            model_name='merchantdetails',
            name='favicon',
            field=models.ImageField(blank=True,
                                    help_text='Upload only .png image of 32X32 pixels for the best result.',
                                    null=True, upload_to=b'', validators=[
                    utils.validators.FileValidator(
                        allowed_mimetypes=['image/png'])],
                                    verbose_name='Favicon'),
        ),
    ]
