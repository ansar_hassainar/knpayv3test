2.6.8
-----
January 08, 2019

- Fix migration files

2.6.7
-----
January 07, 2019

- Convert catalogue app to plugin
- Catalogue updates & fixes
- Fix PDF invoice for arabic chars
- Encode logo name to handle arabic filename
- KPay live preparation (callback included)
- Change account lockout configuration to 2 minutes auto-unlock
- Various small bug fixes

2.6.6
-----
December 25, 2018

- Fix import Arabic and cell files

2.6.5
-----
December 20, 2018

- Fix empty attachments on bulk error files

2.6.4
-----
December 18, 2018

- Fix Arabic and English cells mix.

2.6.3
-----
December 04, 2018

- Fix bulk error rendering when notify on email is enabled (Sentry ID 7567)

2.6.2
-----
November 28, 2018

- Resize gateway logo

2.6.1
-----
November 27, 2018

- Restrict access to public APIs
- Public APIs throttling
- Fix KPay jks import
- Update shopify page response template


2.6.0
-----
November 19, 2018

- 2 factor authentication
- Selectable export fields for transactions
- Send PDF invoices
- Add Deleted transactions section and restore ability + related permissions
- Dynamic favicon
- Fabric script
- Merchant can add address in HTML style
- Payment attempt page details updates
- Gateway logo position fixed
- Itinerary display field added
- Mail tracker
- Support ticketing system
- Add email seen at
- Add beta KPay
- APIs for manual data disclosure
- minor improvements
- minor fixes
- missing migration files


2.5.5
-----
November 06, 2018

- Add `dummy` column to bulk.

2.5.4
-----
October 30, 2018

- Hide javascript catalog url from login page
- Send CSRF cookie only over https

2.5.3
-----
October 08, 2018

- Fix unittests for Code Pairing plugin
- Add dynamic fields in listing display of Catalogue transactions
- Fix 500 error on invalidation e-commerce transactions when PSP is down

2.5.2
-----
October 04, 2018

- Fix catalogue bug when allowed bought qty can be more than available stock

2.5.1
-----
September 18, 2018

- Separate Terms & Conditions for catalogue payments requests

2.5.0
-----
September 18, 2018

- Add Code Pairing plugin
- Fix "Try again" button on catalogue payments when payment is failed

2.4.5
-----
September 01, 2018

- Restore JQuery phone number

2.4.4
-----
August 29, 2018

- Fix multipart form submission for iOS 11.3+ bug.

2.4.3
-----
August 16, 2018

- move WhatsApp url type detection to separated function
- display "Try again" link at all non-succeed payment states

2.4.2
-----
August 14, 2018

- set whatsapp link for sending notification depending on device type
- fix issue with terms & conditions modal text displaying
- add knet old resource style parsing

2.4.1
-----
August 07, 2018

- fix issue with empty column at bulk transaction details if user hasn't permission to dispatch transaction


2.4.0
-----
August 05, 2018

- 30680
- add can view statistics and can cancel owned transactions
- move commands for creating transactions download link to separate function
- add options to change bulk upload process behavior
- add email sending at bulk file fetching error
- add additional details into bulk listing template
- tests for Bulk model
- remove initiator details from payment result page
- set permission-based transaction statistics showing in more strict way add tests for transaction statistics permissions
- fix issue with None value fetching in bulk rows counters
- display customer data fields at payment request and payment attempt page in the same way as in create payment request

2.3.2
-----
July 25, 2018

- show attachment only in payment request mails
- show attachment only in payment fail page

2.3.1
-----
July 04, 2018

- Ignore API paths for expired

2.3.0
-----
July 04, 2018

- Add EXPIRED feature

2.2.4
-----
July 02, 2018

- delete empty fields from shopify payload, before instantiate the serializer class

2.2.3
-----
May 31, 2018
- increase timeout when requesting payment link from KNET to 25 seconds
- enable logging to sentry for urllib3 library

2.2.2
-----
May 29, 2018

- Sentry production dsn added
- Sentry added in each logger
- logger.error replaced with logger.exception

2.2.1
-----
May 24, 2018

- Fix management command for fetching knpay version from git

2.2.0
-----
May 24, 2018

- Paypal Integrated
- Library upgrades (Django 1.9.9 to Django 1.11)
- Other dependency third-party packages updated
- is_sandbox field added to PaymentTransaction model
- QR code functionality added
- Section added for Gateways
- UI upgrades for the Payment-Request pay page, Attempt details page
- Test-cases updated related to PaymentTransaction and PaymentAttempt
- email_recipients as builtin field added
- EmailListValidator added for email_recipients
- GatewaySettings is_active filter at admin added
- amount field added in keyword search at transaction filters
- add gateway code sort possibility in admin
- transaction is sandbox or not predefined while creating transaction
- add gateway code in admin filter & list
- send_notifications set to False on error init
- Social Links added on payment request pages
- Copy the payment-req link added on payment request details page
- Default-Email functionality added on bulk, payment-request, dashboard API, pay-req API
- payment request attachment builtin field added
- currency code added when gateway has fee
- Custom fields shown at payment request pay and attempt page
- Remarks shown if added custom field Remarks at PayReFormConfig
- MIGS currency decimal issue
- correct_decimals method added
- Round the last decimal at currency exchange config
- Attachment Download enabled | attachment shorten url method added
- DBMail-Templates context updated with new fields with also product details
- Catalogue Feature added
    - Add product from admin
    - Share shorten url, copy from admin
    - update quantity and PAY with selected gateway
    - On click Pay check quantity
    - Reserve Quantity feature
    - Admin product logo click zoom
    - Restore Quantity | Django-FSM signal used
    - set CATALOGUE_STOCK_RESTORE=True from production settings
- Builtin Attachments added in email notifications
- update how to release a version steps.
- Hide SMS checkbox if no sms provider is available
- Gateway logo shown when gateway is pre-selected
- PaymentRequest API update, Bulk API enabled
- Dashboard API update, Extra data saved within
- ID field shown at PaymentTransaction model at admin
- Customer Details fields on attempt page reorder
- KNPAY favicon added
- A whole Custom Export reports on transaction listing pages
    - Each gateway_response key as a separate column
    - Label for each builtin and custom fields
    - All extra data included
    - A nice select fields option on transaction listing page
    - Export selected or Export Default options
- Home page chart click redirect to particular DAY's transactions
- Transaction listing Filters updated and boxes of analytics added
- Catalogue transaction listing has custom columns
- Description ckeditor HTMLField size reduced
- Latest KNPAY Release message shown at admin panel
- Country select dropdown at create payment-request
- PhoneNumber with country code/flag dropdown at above page
- Counter fix to sender added in legacy API
- sms_payment_details with phone validations corrected
- Cybersource MDD fields added in forms
- Disclose to Merchant from admin fixed
- Disclose url error changed to JsonField
- Whatsapp share message added in API as share_message
- Whatsapp share message at payment-request details page
- Whatsapp share mobile url
- Pay page, Terms & conditions with admin configured
- Arabic CSS added for new template designs
- Shopify shipping_address sent in cybersource mdd8
- Log files size increased
- Amount 10 fills enabled
- Notification template context update | gateway_name added
    - gateway_code as fallback
- admin responsive html fixes
- Gateway Setting delete set_null
- Postal Code related Field length updated, validations added
- Readme updated
- fix e-commerce payments allowance of multiple payment attempts
    - fix disclosing multiple times same payload to merchant

2.1.0
-----
April 03, 2018

- Cybersource mdd fields added for decision manager
- Passing mdd fields from shopify
- udf9 added in legacy
- Export updates | gateway_payload added
- Fix sender in legacy API
- Log files size increased

2.0.7
-----
February 05, 2018

- add status field in legacy API

2.0.6
-----
February 01, 2018

- ID field added in all export type
- Bulk ID removed from all export type
- Both PaymentFormConfig & BulkFormConfig unique active fields are included for PaymentRequest transactions export


2.0.5
-----
January 31, 2018

- Fix bulk where all bulks were considered as recurring
- Removed the extra columns from the exported file and included only the fields which are active and configured at PaymentRequestForm Config for PaymentRequest transactions export
- E-commerce and other transactions will not have an initiator, builk-id column in exported file
- New Builtin field(mail_recipients) added in export resource


2.0.4
-----
January 04, 2018

- Calculate the multiplier for MIGS amount based on currency decimale


2.0.3
-----
December 28, 2017

- Add default value for KNET gateway in legacy API


2.0.2
-----
December 18, 2017

- Fix `gateway_code` in email templates for payment transactions which didn't have a predefined gateway_code chosen


2.0.1
-----
December 02, 2017

- Add version script
- Remove BULK_ENABLED settings and make bulk auto, if redis enabled will work by default.
- Fix transactions generated for ANY payment gateway is not going back to the same payment request page, it takes to the previously selected gateway by default
- Fix some cases where can be an error while parsing the PSP resource file (There is no item named 'kuw.xml' in the archive) and customer payment form remained stuck
- Fix 404 when logo is missing for payment request
- Fix error in payment page when fees can't be calculated merge
- Fix bulk sms_payment_details to work as supposed to work, when setting is checking in admin config.
