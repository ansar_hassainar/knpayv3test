jQuery(document).ready(function($) {
	$('.endless_page_template').on('click', '.send-communication-event', function(event) {
		event.preventDefault();

        $.get($(this).attr('href'), function(payload) {
			toastr.success(payload['message']);
		}).fail(function (response) {
			toastr.error(response.responseJSON['message'])
		});
	});

	$('.cancel_transaction').on('click', function(e){
		e.preventDefault();
		var $this = $(this);
		$.ajax({
			url: $this.attr('action'),
			dataType:"JSON"
		}).done(function (response) {
			toastr.success(response.message);
			$this.parent().siblings('td.state').text(response.status);
			$this.attr('disabled', 'disabled')
		}).fail(function (response) {
			var data = response.responseJSON;
			toastr.error(data.message)
		})
	})

	$('.export').on('click', function(e){
        var query_string = location.search;
        if(query_string == ""){
            new_location = window.location.href + "?file_format="+$(this).data('type');
            window.open(new_location)
        }
        else{
            new_location = window.location.href + "&file_format="+$(this).data('type');
            window.open(new_location)
        }
	});


	function get_export_transactions_url(format, export_type, fields_string){
	    var existing_query_string = window.location.search;
	    var new_location =
	        window.location.href +
	        ((existing_query_string == "") ? '?' : '&') +
	        "export_type="+export_type +
	        "&" +
	        "fields="+fields_string +
	        "&" +
	        "file_format="+format
        return new_location
	}

	$('.export-transactions').on('click', function(e){
        var query_string = location.search;
        var fieldsArray = []
        var field_regEx = '[a-zA-Z_]'
        var file_format = $("input[name='file_format']:checked").val();

        $("input:checkbox[name=fields]:checked").each(function(){
            var field = $(this).val();
            if(field.match(field_regEx)){
                fieldsArray.push(field);
            }
        });

        $(".popover-header .close").trigger("click");

        var export_type = $(this).data('export-type');
        var fields_string = fieldsArray.join();
        var new_location = get_export_transactions_url(file_format, export_type, fields_string)

        window.open(new_location)
	});

	$('.export-transactions-default').on('click', function(e){
	    var new_location = get_export_transactions_url('csv', 'default', '');
	    window.open(new_location)
	})

});

$('.popoverfrm form').click(function(e){
/* To stop closing popup while clicked on any checkbox*/
  e.stopPropagation();
});