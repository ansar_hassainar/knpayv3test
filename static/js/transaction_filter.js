jQuery(document).ready(function ($) {

    var created_0 = $('#id_created_0').datepicker({
        onRender: function (date) {
            // return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        if (ev.date.valueOf() > created_1.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            created_1.setValue(newDate);
        }
        created_0.hide();
        $('#id_created_1')[0].focus();

    }).data('datepicker');
    var created_1 = $('#id_created_1').datepicker({
        onRender: function (date) {
            // return date.valueOf() <= created_0.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        created_1.hide();

    }).data('datepicker');


    var modified_0 = $('#id_modified_0').datepicker({
        onRender: function (date) {
            // return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        if (ev.date.valueOf() > modified_1.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            created_1.setValue(newDate);
        }
        modified_0.hide();
        $('#id_modified_1')[0].focus();

    }).data('datepicker');
    var modified_1 = $('#id_modified_1').datepicker({
        onRender: function (date) {
            // return date.valueOf() <= modified_0.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        modified_1.hide();

    }).data('datepicker');

    $('.transaction_filter_form').on('submit', function (e) {
        e.preventDefault();
        var $this = $(this)
        $.ajax({
            data: $this.serialize(),
            dataType: 'html'
        }).always(function (response) {
            $('.endless_page_template').html(response)
            history.pushState(null, null, window.location.pathname + '?' + $this.serialize());
        })
    });

    $('input[type="reset"]').on('click', function (e) {
        $('.selectpicker').selectpicker('val', '');
    });

    $('.export').on('click', function (e) {
        e.preventDefault();
        var $this = $(this);
        var href = $this.attr('href');
        window.open(location.search + href, '_blank')
    })
});