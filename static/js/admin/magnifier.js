/*
Author : Akif Vohra
Added on: 22 Feb, 2018
*/
if (!$) {
    $ = django.jQuery;
}
jQuery(document).ready(function ($) {

    var images = '.image_magnifier'
    var $images = $(images)

    $.each($images, function(index, val) {
        var $this = $(this);
        $this.imageTooltip();
    });

});