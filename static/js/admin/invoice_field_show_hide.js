jQuery(document).ready(function ($) {
    var invoice_check_field = '#id_attach_invoice_pdf';
    var invoice_html_field = '#tab_id_invoice_pdf_template_en';

    var catalogue_invoice_check_field = '#id_attach_catalogue_invoice_pdf';
    var catalogue_invoice_html_field = '#tab_id_catalogue_invoice_pdf_template_en';

    var context_note_field = '.field-context_note';

    function context_hide_show(){
        if ($(invoice_check_field).is(':checked') ||
        $(catalogue_invoice_check_field).is(':checked')) {
            $(context_note_field).show();
        }
        else{
            $(context_note_field).hide();
        }
    }

    function div_hide_show(){
        if ( $(invoice_check_field).is(':checked') ) {
            $(context_note_field).show();
            $(invoice_html_field).parent().show();
        }
        else {
            $(context_note_field).hide();
            $(invoice_html_field).parent().hide();
        }
        context_hide_show();
    }

    function catalogue_hide_show(){
        if ( $(catalogue_invoice_check_field).is(':checked') ) {
            $(context_note_field).show();
            $(catalogue_invoice_html_field).parent().show();
        }
        else {
            $(context_note_field).hide();
            $(catalogue_invoice_html_field).parent().hide();
        }
        context_hide_show();
    }

    div_hide_show();
    catalogue_hide_show();

    $(invoice_check_field).click(function() {
        div_hide_show();
    });

    $(catalogue_invoice_check_field).click(function() {
        catalogue_hide_show();
    });

});