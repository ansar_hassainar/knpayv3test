jQuery(document).ready(function ($) {
    var selects = "select[id*='-type']:visible";
    var $selects = $(selects);
    var builtinRow = '.field-field';
    var customRow = '.field-label_en';
    var builtin = 'builtin';
    var custom = 'custom';
    var inline_class = '.inline-related:visible';

    function resetRow(obj, rowClass){
        var holder = obj.parents('.form-row').siblings(rowClass);
        holder.find('input').val('');
        holder.find(selects).val('');
    }

    // when hiding row,
    // for avoiding submitting them and generate
    // form errors
    function hideRow ($this, rowClass) {
        $this.parents('.form-row').siblings(rowClass).hide();
    }

    function showRow($this, rowClass) {
        $this.parents('.form-row').siblings(rowClass).show();
    }

    // hide all unnecessary fields for initial data
    $.each($selects, function(index, val) {
        var $this = $(this);
	    if (val.value == builtin) {
            hideRow($this, customRow)
        } else {
            hideRow($this, builtinRow)
        }

    });

    // hide field rows based on what value
    // user has changed
    var onChange = function (e) {
        var $this = $(e.target);
        if ($this.val() == builtin) {
            hideRow($this, customRow);
            resetRow($this, customRow);
            showRow($this, builtinRow);
        } else {
            hideRow($this, builtinRow);
            resetRow($this, builtinRow);
            showRow($this, customRow);
        }
    }

    // bind all future selectors to our function
    $(document).on('change', selects, onChange);

    // listen when new rows are added and bind from django-jet js event
    $(document).on('inline-group-row:added', function(e, formset){
        var $this = $(formset[0]);
        $this.find(selects).last().trigger('change');
    });
});