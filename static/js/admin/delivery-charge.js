jQuery(document).ready(function ($) {

    function delivery_chargesShowOrHide() {
        if ($("#id_add_delivery_charges").is(':checked')) {
            $(".field-delivery_charges").show();
        }
        else {
            $(".field-delivery_charges").hide();
            $("#id_delivery_charges").val(null)
        }
    }

    delivery_chargesShowOrHide();

    $("#id_add_delivery_charges").on('click', function () {
        delivery_chargesShowOrHide();
    });


    function display_stockShowOrHide() {
        if ($("#id_stock").val() == '') {
            $("#id_display_stock").attr("checked", false);
            $(".field-display_stock").hide();
        }
        else {
            $(".field-display_stock").show();
        }
    }

    display_stockShowOrHide();

    $("#id_stock").on('input', function () {
        display_stockShowOrHide();
    });
});
