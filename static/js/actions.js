jQuery(document).ready(function($) {

	$( "body" ).delegate('.mark_all', 'click', function(e){
		var $this = $(this)
		if($this.prop('checked')){
			$('.action_checkbox').prop('checked', true)
		} else {
			
			$('.action_checkbox').prop('checked', false)
			// $('.action_checkbox').attr('checked', false)
		}	
	})
	$( "body" ).delegate('.action_checkbox', 'click', function(e){
		if($('.action_checkbox').length == $('.action_checkbox:checked').length){
			$('.mark_all').prop('checked', true)
		} else {
			$('.mark_all').prop('checked', false)
		}
	})
	$( "body" ).delegate('.action', 'click', function(e){
		e.preventDefault()
		var action = $(this)
		var ids = []
		$('.action_checkbox:checked').each(function(){
			ids.push($(this).val())
		})
		ids = ids.join()
		$.ajax({
			url: action.attr('href'),
			dataType:'JSON',
			data:{
				action: action.attr('action'),
				ids: ids
			}
		}).always(function(response){
			if(response.success){

				toastr.success(response.message)
				if(action.attr('action') == 'delete'){
					$('.action_checkbox:checked').each(function(){
						$(this).parent().parent().remove()
					})
				}
				if(action.attr('action') == 'restore'){
					$('.action_checkbox:checked').each(function(){
						$(this).parent().parent().remove()
					})
				}
				if(action.attr('action') == 'cancel'){
					$('.action_checkbox:checked').each(function(){
						$(this).parent().siblings('td.state').text(response.status)
						$(this).parent().siblings('td.cancel').children().attr('disabled', 'disabled')
					})
				}
			} else {
				toastr.error(response.message)
			}
		})
	})
});