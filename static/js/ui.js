jQuery(document).ready(function ($) {
	var $submitBtn = $("#pay-form");
    $submitBtn.attr('disabled', true);

    $('input[name="gateway_code"]').on('change', function () {
        var $feeSection = $('#fee-section');
        var val = $('input[name="gateway_code"]:checked').val();

        $.ajax({
            url: '/' + lang_code + '/pos/calculate-payment-fee/' + target + '/' + val,
            dataType: 'json'
        })
            .done( function (payload) {
                $submitBtn.attr('disabled', false);
                if (payload.fee) {
                    $('#id_fee').val(payload.fee);
                    $('#id_total').val(payload.total);
                    $('#label_id_total').text(payload.total_label);
                    $('#label_id_fee').text(payload.fee_label);
                    $feeSection.show()
                } else {
                    $feeSection.hide()
                }
            })
            .fail(function (response) {
                $submitBtn.attr('disabled', true);
                var data = response.responseJSON;
                toastr.error(data.message)
            })
    })
});

