function refresh() {
    setTimeout(function () {location.reload()}, 700);
}

jQuery(document).ready(function($) {
    // import
    var $importBtn = $('#import');
    var $fileInput = $('#id_file');
    var $bulkForm = $('#bulk-file-form');

    $('.datetimepicker').datetimepicker({
                    format: 'MM/DD/YYYY HH:mm',
                    sideBySide: true,
                    calendarWeeks: true,
                });

    $importBtn.on('click', function(e) {
        e.preventDefault();
        $fileInput.trigger('click');
    });

    $fileInput.on('change', function () {
        // if no file selected, cancel
        // if (!$('this').val()) return;

        // create form for submit
    	var formData = new FormData();
    	formData.append('file', $fileInput.prop('files')[0]);
        formData.append('bulk', $('#id_bulk option:selected').val());

        // set loading state
        $importBtn.button('loading');

        // do ajax
        $.ajax({
        	url: $bulkForm.attr('action'),
        	type: 'post',
        	data: formData,
            processData: false,
            contentType: false
        })
        .done(function(payload) {
            toastr.options.tapToDismiss = false;
            toastr.options.timeOut = 0;
            toastr.options.extendedTimeOut = 0;
        	toastr.success(payload['message']);
            $importBtn.text(gettext('Processing...'));

            // trigger the process url
            $.ajax({
                url: payload['process_url']
            })
            .done(function(payload) {
                $importBtn.button('reset');
                toastr.clear();
                $importBtn.button('reset');
                refresh();

            })
            .fail(function(response) {
                $importBtn.button('reset');
                toastr.clear();
                if (response.status != 500) {
                    response = response.responseJSON;

                    if (response['partial']) {
                        toastr.warning(gettext("Import finished with some errors. Check the report for more details."));
                        refresh();
                    } else {
                        var msg;
                        if (response['message']) msg = response['message'];
                        else msg = gettext("Import failed. Check the report for more details.");
                        toastr.options.timeOut = 2000;
                        toastr.error(msg)
                    }
                } else {
                    toastr.error(gettext('Err! Refresh and try again.'))
                }
            })
        })
        .fail(function(response) {
            var errors = response.responseJSON['errors'];

            $.each(errors, function(index, error) {
                toastr.error(error)
            });

            // disable loading state
            toastr.options.timeOut = 5000;
            $importBtn.button('reset')

        })
    });

    // dispatch
    $('body').on('click', '.dispatch' ,function () {
        var $this = $(this);
        $this.attr('disabled', true);
        $.ajax({
            'url': $(this).attr('data-href'),
            'dataType': 'json'
        }).done(function(payload){
            $this.attr('disabled', false);
            toastr.success(payload.message)
        }).fail(function (response) {
            if (response.status==400) {
                $this.attr('disabled', false);
                var data = response.responseJSON;
                toastr.error(data.message);
                $.each(data.errors, function(index, val) {
                    toastr.error(index + ': ' + val)
                });
            } else {
                toastr.error(gettext("Server error. Refresh the page and try again. If the problem persists, contact the administrator."))
            }

        });
    });



    // error report
    $('.error-report').on('click', function (e) {
        e.preventDefault();
        var $modal = $('#modal')
        $.ajax({
            url: $(this).attr('href'),
            dataType: 'html'
        })
            .done( function(response) {
                $modal.html(response);
                $modal.modal('show')
            })
    });

        var created_0 = $('#id_created_0').datepicker({
          onRender: function(date) {
            // return date.valueOf() < now.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          if (ev.date.valueOf() > created_1.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            created_1.setValue(newDate);
          }
          created_0.hide();
          $('#id_created_1')[0].focus();

        }).data('datepicker');
        var created_1 = $('#id_created_1').datepicker({
          onRender: function(date) {
            // return date.valueOf() <= modified_0.date.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          created_1.hide();

        }).data('datepicker');

     $('.bulk_filter_form').on('submit', function(e){
        e.preventDefault()
        var $this = $(this)
        $.ajax({
            data: $this.serialize(),
            dataType:'html'
        }).always(function(response){
            $('.endless_page_template').html(response)
            history.pushState(null, null, window.location.pathname +'?'+$this.serialize());
        })
     })
     $('input[type="reset"]').on('click', function(e){
        $('.selectpicker').selectpicker('val', '');
     })
});