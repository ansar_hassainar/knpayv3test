jQuery(document).ready(function($) {    
    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            vars[key] = value;
        });
        return vars;
    }
    var currentGatewayCode = undefined;
    var page = getUrlVars()['page'] || 1;

    // disabling dates
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#date_filter_start').datepicker({
          onRender: function(date) {
            // return date.valueOf() < now.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
          }
          checkin.hide();
          $('#date_filter_end')[0].focus();
          if($('#date_filter_start').val() !== '' && $('#date_filter_end').val() !== ''){
            refreshGraph()
          }
        }).data('datepicker');
        var checkout = $('#date_filter_end').datepicker({
          onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          checkout.hide();
          if($('#date_filter_start').val() !== '' && $('#date_filter_end').val() !== ''){
           refreshGraph()
          }
        }).data('datepicker');


    $('.gateway_tab_switch').on('click', function(e){

        $this = $(this)
        if($this.attr('gateway') !== currentGatewayCode){
            page = 1
            $('#date_filter_start').val('')
            $('#date_filter_end').val('')
        }
        currentGatewayCode = $this.attr('gateway')

        $.ajax({
            url: window.location.href.split('?')[0],
            dataType: 'html',
            type:'GET',
            data: {
                gateway: currentGatewayCode,
                page: page
            }
        }).always(function(response){
            $('.gateway_table').html(response)
        })
    })


    $('.gateway_tab_switch').on('click', function(e){
        $this = $(this)

        $.ajax({
            url: $this.attr('graph'),
            dataType: 'JSON',
            type:'GET',
            data:{
                graph: 'true',
                gateway_code: $this.attr('gateway')
            }

        }).always(function(response){
            $('#morris-bar-chart').empty();
            Morris.Bar({
                element: 'morris-bar-chart',
                data: response['data'],
                xkey: 'y',
                ykeys: response['ykeys'],
                labels: response['labels'],
                hideHover: 'auto',
                hoverCallback: function (index, options, content, row) {
                    content += "<div class='morris-hover-point' style='color: #0b62a4'>"+options['labels'][2]+":"
                    $.each(row['b'], function (n, v) {
                       content += "</br>"+v
                    });
                    content += "</div>"
                    return content;
                },
                resize: true
            }).on('click', function(i, row){
                var url = "/transactions/";
                window.location.href= url + row.y;
            });
        })

    })
    $('li.active .gateway_tab_switch').trigger('click')

    var all = undefined

    function refreshGraph(){
        payload = {graph: 'true'}
        if (all){
            payload['gateway_code'] = ''

        } else {
            payload['gateway_code'] = $('li.active .gateway_tab_switch').attr('gateway')
           
        }
        if($('#date_filter_start').val() !== ''){
            payload['created_0'] = $('#date_filter_start').val()
        }

        if($('#date_filter_end').val() !== ''){
            payload['created_1'] = $('#date_filter_end').val()
        }

        $.ajax({
            url: $('li.active .gateway_tab_switch').attr('graph'),
            dataType: 'JSON',
            type:'GET',   
            data: payload
        }).always(function(response){
            $('#morris-bar-chart').empty();
            Morris.Bar({
                element: 'morris-bar-chart',
                data: response['data'],
                xkey: 'y',
                ykeys: response['ykeys'],
                labels: response['labels'],
                hideHover: 'auto',
                hoverCallback: function (index, options, content, row) {
                    content += "<div class='morris-hover-point' style='color: #0b62a4'>"+options['labels'][2]+":"
                    $.each(row['b'], function (n, v) {
                       content += "</br>"+v
                    });
                    content += "</div>"
                    return content;
                },
                resize: true
            }).on('click', function(i, row){
                var url = "/transactions/";
                window.location.href= url + row.y;
            });
        })
    }

    $('.display_all').on('click', function(e){
        e.preventDefault()
        all = true
        refreshGraph()

    })

    $('.display_current').on('click', function(e){
        e.preventDefault()
        all = false
        refreshGraph()

    })

});