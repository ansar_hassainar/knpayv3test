jQuery(document).ready(function ($) {
	$("#id_gateway_code").change(function () {
		var gateway_code = $(this).val();  // get the selected gateway_code from the HTML input
		$.ajax({
			url: '/load-currencies/',
			data: {
				'gateway_code': gateway_code       // add the gateway_code to the GET parameters
			},
			// `data` is the return of the `load-currencies` view function
			success: function (data) {
				var currencies = $('#id_currency_code');
				currencies.children().remove();
				$.each(data.currencies, function (index, value) {
					currencies.append(
						$('<option></option>').val(value.code).html(value.code)
					);
				});
				currencies.val(data.default);
				if (data.currencies.length <= 1)
					$('#div_id_currency_code').hide()
				else
					$('#div_id_currency_code').show()
			}
		});
	});

	var default_email = $('.form-wrapper').attr('data-default')
	$('#id_default_email').on('click', function () {
		var $this = $(this);
		var $customerEmail = $('#id_customer_email');
		if ($this.is(':checked')) {
			$this.prop('checked', true);
			$customerEmail.val(default_email);
			$customerEmail.prop('readonly', true);
		}
		else {
			$customerEmail.prop('readonly', false);
			$this.prop('checked', false).removeAttr('checked');
			$customerEmail.val('')
		}
	})

});