// Catalogue Scripts

// Product quantity Update Script
jQuery(document).ready(function ($) {
    var $submitBtn = $('#quantity_update');
    $submitBtn.on('click', function () {
        $submitBtn.attr('disabled', true);
        $submitBtn.attr('value', 'updating');

        var quantity = $('#product_quantity').val();

        $.ajax({
            url: '/' + lang_code + '/catalogue/product-quantity-update/' + target + '/' + quantity,
            dataType: 'json'
        })
        .done( function (payload) {
            $('.product_quantity').val(quantity);
            $submitBtn.attr('disabled', false);
            $submitBtn.attr('value', 'Update');
            if (payload.success_message) {
                toastr.success(payload.success_message);
                $('.product_prices').html(payload.product_prices_html)
            }
        })
        .fail(function (response) {
            $submitBtn.attr('disabled', false);
            $submitBtn.attr('value', 'update');
            var data = response.responseJSON;
            toastr.error(data.message)
        })
    });
});


// Pay script
$(document).on('click', '.submit-form', function(){
    var _this = $(this)
    $('input[name="gateway_code"]').val(_this.data('gateway-code'));
    $('input[name="amount"]').val(_this.data('total-amount'));

    var remove_errors = function(){
        $('#pay_form').find('.has-error').each(function (item) {
           $('div.help-block', item).remove();
        }).removeClass('has-error');
    };

    remove_errors();
    $.ajax({
        url: $('#pay_form').attr('action'),
        type: 'POST',
        data: $('#pay_form').serialize(),
        dataType: 'json',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            _this.button('loading');
        },
        success: function (data) {
            toastr.success(data['message']);
            if (data['redirect_url']) {
                setTimeout(function() {window.location.href = data['redirect_url']}, 1500)
            }
        },
        error: function(jqXHR, status, error){
            var data = jqXHR.responseJSON;

            if (jqXHR.status==400 && data['errors']) {
                    var errors = $.map( data['errors'], function( v, k ) {
            if(k=='error_message'){
            }else{
                if(k=='__all__')
                    toastr.error(v);
                else
                {
                    var field = $('#pay_form').find('[name='+k+']');
                    if (field.length > 1){
                        field = field.last()
                        field.parentsUntil('.form-group').parent().addClass('has-error');
                        if(field.parent().next() && field.parent().next().hasClass('help-block')){
                            field.parent().next().text(v);
                        }else{
                            field.parent().after('<div class="help-block">'+v+'</div>');
                        }
                    } else {
                        field.parentsUntil('.form-group').parent().addClass('has-error');
                        if(field.parent().next() && field.parent().next().hasClass('help-block')){
                            field.parent().next().text(v);
                        }else{
                            field.parent().after('<div class="help-block">'+v+'</div>');
                        }
                    }
                }
            }
            }).join('<br>');
            }else{
                if (data.message) {
                    toastr.error(data.message)
                } else {
                    notifyServerError()
                }
            }

        },
        complete: function () {
            _this.button('reset');
        }
    });
});