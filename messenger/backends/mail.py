from dbmail.backends.mail import Sender as BaseSender
from dbmail.defaults import ENABLE_LOGGING
from dbmail.models import MailLog,MailLogException
from dbmail.defaults import (_BACKEND)
from dbmail import defaults
from django.contrib.sites.models import Site
from django.core import signing
from django.core.urlresolvers import reverse, NoReverseMatch


class Sender(BaseSender):

    def _store_log(self, is_sent):
        if ENABLE_LOGGING is True:
            if self._template.enable_log or not is_sent:
                log = self.my_store(
                    self._recipient_list, self._cc, self._bcc,
                    is_sent, self._template, self._user,
                    self._num, self._err_msg, self._err_exc,
                    self._log_id, self._backend, self._provider
                )
                tran = self._context.get("transaction",None)
                if tran:
                    tran.maillog = log
                    tran.save()
    
    def _get_msg_with_track(self):
        message = self._message
        if defaults.TRACK_ENABLE is False:
            return message
        if ENABLE_LOGGING and self._template.enable_log:
            try:
                domain = Site.objects.get_current().domain
                encrypted = signing.dumps(self._log_id, compress=True)
                path = reverse('messenger:messenger_tracker', args=[encrypted])
                message += defaults.TRACK_HTML % {
                    'url': 'http://%s%s' % (domain, path)}
            except (Site.DoesNotExist, NoReverseMatch):
                pass
        return message

    def my_store(self, to, cc, bcc, is_sent, template,
              user, num, msg='', ex=None, log_id=None,
              backend=None, provider=None):
        if ex is not None:
            ex = MailLogException.get_or_create(ex)

        log = MailLog.objects.create(
            template=template, is_sent=is_sent, user=user,
            log_id=log_id, num_of_retries=num, error_message=msg,
            error_exception=ex, backend=_BACKEND.get(backend, backend),
            provider=provider
        )
        MailLog.store_email_log(log, to, 'to')
        MailLog.store_email_log(log, cc, 'cc')
        MailLog.store_email_log(log, bcc, 'bcc')
        return log
    
class SenderDebug(Sender):
    def _send(self):
        self.debug('Provider', self._provider or 'default')
        self.debug('Message', self._message)
        self.debug('From', self._from_email)
        self.debug('Recipients', self._recipient_list)
        self.debug('CC', self._cc)
        self.debug('BCC', self._bcc)
        self.debug('Additional kwargs', self._kwargs)