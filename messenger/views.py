from django.http import HttpResponse

from dbmail import defaults

def mail_read_tracker(request, encrypted):
    if defaults.TRACK_ENABLE and defaults.ENABLE_LOGGING:
        from messenger.tasks import mail_track

        req = {k: v for k, v in request.META.items()
               if k.startswith('HTTP_') or k.startswith('REMOTE')}
        if defaults.ENABLE_CELERY is True:
            mail_track.apply_async(
                args=[req, encrypted], queue=defaults.TRACKING_QUEUE,
                retry=1, retry_policy={'max_retries': 3})
        else:
            mail_track(req, encrypted)

    return HttpResponse(
        content=defaults.TRACK_PIXEL[1],
        content_type=defaults.TRACK_PIXEL[0],
    )