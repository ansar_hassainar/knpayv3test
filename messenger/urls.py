from django.conf.urls import url

from messenger.views import mail_read_tracker

urlpatterns = [
    url(r'^mail-read-tracker/(.*?)/$',mail_read_tracker, name='messenger_tracker'),
]
