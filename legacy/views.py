# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import json
import logging

from django.views import generic
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from django.views.generic.detail import SingleObjectMixin

import requests
from braces.views import CsrfExemptMixin
from rest_framework.renderers import JSONRenderer
from rest_framework.views import APIView

from gateway.models import PaymentTransaction
from .serializers import LegacySerializer
from utils.views.mixins import APISecurityMixin
from utils.throttling.api_throttling import APISecurityThrottle

logger = logging.getLogger('legacy')


class ApiDispatchView(APISecurityMixin, APIView):
    renderer_classes = (JSONRenderer,)
    http_method_names = ['post']
    throttle_classes = [APISecurityThrottle]
    security_config_field = APISecurityMixin.LEGACY

    def post(self, request, *args, **kwargs):
        try:
            legacy_payload = json.loads(request.body)
        except Exception as e:
            legacy_payload = request.POST.copy()

        logger.info("Inbound merchant payload {}".format(legacy_payload))
        amount = legacy_payload.get('amount', legacy_payload.get('amt', ''))
        currencycode = legacy_payload.get('currencycode', legacy_payload.get('currency', '414'))
        legacy_payload.update({
            'amount': amount,
            'currencycode': currencycode
        })
        serializer = LegacySerializer(data=legacy_payload, context={'request': request})
        serializer.is_valid(raise_exception=True)
        transaction = serializer.save()
        return HttpResponse(transaction.get_payment_url())


class LegacyResponseView(CsrfExemptMixin,
                         SingleObjectMixin,
                         generic.View):
    queryset = PaymentTransaction.objects.all()

    def post(self, request, *args, **kwargs):
        return HttpResponse()

    def get(self, request, *args, **kwargs):
        self.payment_transaction = self.get_object()
        if self.payment_transaction.is_paid():
            self.payment_attempt = self.payment_transaction.get_paid_attempt()
        else:
            self.payment_attempt = self.payment_transaction.get_latest_attempt()

        transaction_data = self.payment_transaction.extra

        if self.payment_transaction.extra.get('consumed', False):
            return HttpResponseForbidden()
        else:
            self.payment_transaction.extra['consumed'] = True
            self.payment_transaction.save()

        if self.payment_transaction.is_paid():
            post_url = transaction_data['responseURL']
        else:
            post_url = transaction_data['errorURL']

        method = transaction_data['responseTYPE']
        key = 'data' if method == 'post' else 'json'
        payload = self.prepare_legacy_payload()
        logger.info("Response merchant payload {}".format(payload))
        kwargs = {'verify': False, key: payload, 'timeout': 35}
        try:
            response = requests.post(post_url, **kwargs)
        except Exception as e:
            logger.exception("Exception during disclosing payload to merchant %s" % e)
            return HttpResponseRedirect(self.payment_attempt.get_details_url())

        logger.info("Response %s" % response.status_code)

        if response.status_code == 200:
            try:
                url = response.text.split("REDIRECT=")[1]
                return HttpResponseRedirect(url)
            except IndexError:
                post_url += '?trackid=%s' % self.payment_transaction.order_no
                logger.info('redirect URL: %s' % post_url)
                return HttpResponseRedirect(post_url)
        return HttpResponseRedirect(self.payment_attempt.get_details_url())

    def prepare_legacy_payload(self):
        result = 'CAPTURED' if self.payment_transaction.is_paid() else 'NOT CAPTURED'
        status = 'success' if result == 'CAPTURED' else 'cancelled'
        data = {
            'postdate': "%02d%02d" % (self.payment_attempt.modified.month,
                                      self.payment_attempt.modified.day),
            'result': result,
            'status': status,
            'trackid': self.payment_transaction.order_no,
            'udf1': self.payment_transaction.extra['udf1'],
            'udf2': self.payment_transaction.customer_email,
            'udf3': '%s %s' % (self.payment_transaction.customer_first_name,
                               self.payment_transaction.customer_last_name),
            'udf4': self.payment_transaction.customer_phone,
            'udf5': self.payment_transaction.customer_address_line1,
            'udf6': self.payment_transaction.customer_address_city,
            'udf7': self.payment_transaction.customer_address_state,
            'udf8': self.payment_transaction.customer_address_country,
            'udf9': self.payment_transaction.extra.get('udf9', ''),
        }
        data.update(self._prepare_gateway_payload())
        return data

    def _prepare_gateway_payload(self):
        attempt_data = self.payment_attempt.gateway_response
        if self.payment_attempt.is_knet() or self.payment_attempt.is_burgan():
            return {
                'tranid': attempt_data.get('tranid', ''),
                'paymentid': attempt_data.get('paymentid', ''),
                'auth': attempt_data.get('auth', ''),
                'ref': attempt_data.get('ref', ''),
            }
        elif self.payment_attempt.is_migs():
            return {
                'tranid': attempt_data.get('vpc_TransactionNo', ''),
                'auth': attempt_data.get('vpc_AuthorizeId', ''),
                'paymentid': '',
                'ref': '',
            }
        elif self.payment_attempt.is_cs():
            return {
                'tranid': attempt_data.get('transaction_id', ''),
                'auth': attempt_data.get('auth_trans_ref_no', ''),
                'ref': attempt_data.get('req_reference_number', ''),
                'paymentid': '',
            }
        else:
            return {
                'tranid': attempt_data.get('txn_id'),
                'paymentid': self.payment_attempt.reference_number,
                'ref': '',
                'auth': attempt_data.get('auth')
            }
