# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import json

from django.core.management.base import BaseCommand
from django.conf import settings
from django.contrib.auth.models import User, Group, Permission


perm_mapping = {
    # view
    # 'view_burganapitransaction': ['use_burgansettings', 'change_paymenttransaction'],
    'view_cybertransaction': ['use_cybersourcesettings', 'change_paymenttransaction'],
    'view_knetapitransaction': ['use_knetsettings', 'change_paymenttransaction'],
    'view_migstransaction': ['use_migssettings', 'change_paymenttransaction'],

    # change
    # 'change_burganapitransaction': ['use_burgansettings', 'change_paymenttransaction'],
    'change_cybertransaction': ['use_cybersourcesettings', 'change_paymenttransaction'],
    'change_knetapitransaction': ['use_knetsettings', 'change_paymenttransaction'],
    'change_migstransaction': ['use_migssettings', 'change_paymenttransaction'],

    # delete
    # 'delete_burganapitransaction': ['delete_paymenttransaction'],
    'delete_cybertransaction': ['delete_paymenttransaction'],
    'delete_knetapitransaction': ['delete_paymenttransaction'],
    'delete_migstransaction': ['delete_paymenttransaction'],

    # cancel
    'disable_transaction': ['cancel_paymenttransaction'],

    # dashboard
    'view_transaction': ['view_statistics'],
}


def get_mapped_permissions(old_perm_pk, old_user_perms):
    old_perm_data = (old_perm_data for old_perm_data in old_user_perms if old_perm_data['pk'] == old_perm_pk).next()
    try:
        perms = perm_mapping[old_perm_data['fields']['codename']]
    except KeyError:
        return
    return list(Permission.objects.filter(codename__in=perms))


class Command(BaseCommand):
    help = "migrate old users/groups and perms to new one"
    outfile = '%s/auth.json' % settings.FIXTURE_DIRS[1]

    def handle(self, **options):
        with open(self.outfile) as data_file:
            data = json.load(data_file)

        perms_data = []
        users_data = []
        groups_data = []
        for item in data:
            if item['model'] == 'auth.permission':
                perms_data.append(item)
            elif item['model'] == 'auth.user':
                if item['fields']['password']:
                    users_data.append(item)
            else:
                groups_data.append(item)
        required_perms = set()

        for user_data in users_data + groups_data:
            try:
                required_perms.update(set(user_data['fields']['user_permissions']))
            except KeyError:
                required_perms.update(set(user_data['fields']['permissions']))

        # import groups
        group_count = 0
        group_perms_count = 0
        Group.objects.all().delete()
        for group_data in groups_data:
            old_group_perms = group_data['fields'].pop('permissions')
            group_data['fields']['pk'] = group_data['pk']
            group = Group.objects.create(**group_data['fields'])
            group_count += 1
            for old_perm_pk in old_group_perms:
                perms = get_mapped_permissions(old_perm_pk, perms_data)
                if perms:
                    group_perms_count += 1
                    group.permissions.add(*perms)
                    group.save()
        print('%s groups imported with %s permissions' % (group_count, group_perms_count))

        # import users
        user_count = 0
        user_perms_count = 0
        User.objects.all().delete()
        for user_data in users_data:
            old_user_perms = user_data['fields'].pop('user_permissions')
            user_groups = user_data['fields'].pop('groups')
            user_data['fields']['pk'] = user_data['pk']
            user = User.objects.create(**user_data['fields'])
            user_count += 1
            user.groups.add(*list(Group.objects.filter(id__in=user_groups)))
            if not user.is_superuser:
                for old_perm_pk in old_user_perms:
                    perms = get_mapped_permissions(old_perm_pk, perms_data)
                    if perms:
                        user.user_permissions.add(*perms)
                        user.save()
                        user_perms_count += 1
        print('%s users imported with %s permissions' % (user_count, user_perms_count))
