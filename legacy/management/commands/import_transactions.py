# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import copy
import json
import operator

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from django.conf import settings

from gateway.models import PaymentTransaction


class Command(BaseCommand):
    help = "Import legacy transactions"
    outfile = '%s/transactions.json' % settings.FIXTURE_DIRS[1]
    failed_outfile = 'failed_import.json'

    def handle(self, **options):
        with open(self.outfile) as data_file:
            data = json.load(data_file)
        imported = 0
        failed = 0
        failed_transactions = []
        PaymentTransaction.objects.all().delete()
        self.stdout.write("Importing...")
        for i, tran_data in enumerate(sorted(data, key=operator.itemgetter('state'), reverse=True), 1):
            prepared_data = copy.deepcopy(tran_data)
            prepared_data.pop('tran_pk', None)
            prepared_data['currency_code'] = settings.LEGACY_CURRENCY

            try:
                prepared_data['initiator'] = User.objects.get(pk=tran_data['initiator'])
            except User.DoesNotExist:
                pass

            try:
                tran = PaymentTransaction.objects.create(**prepared_data)
                tran.created = prepared_data['created']
                tran.state_changed_at = prepared_data['state_changed_at']
                tran.save()
                imported += 1
            except Exception as e:
                failed += 1
                tran_data['error'] = str(e)
                failed_transactions.append(tran_data)

        with open(self.failed_outfile, 'w') as outfile:
            json.dump(failed_transactions, outfile, indent=4)

        self.stdout.write(self.style.SUCCESS("Imported %s transactions" % imported))
        if failed:
            self.stdout.write(self.style.ERROR("Failed to import %s transactions" % failed))
            self.stdout.write(self.style.ERROR("Check failed_import.json for more details"))
