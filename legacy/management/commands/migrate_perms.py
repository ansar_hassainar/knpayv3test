# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib.auth.models import User, Permission
from django.core.management.base import BaseCommand
from django.db.models import Q


class Command(BaseCommand):
    help = "migrate old dashboard permission to new one"

    def handle(self, **options):
        # update permissions from legacy
        try:
            old_perm = Permission.objects.get(codename='view_merchantdetails')
            new_perm = Permission.objects.get(codename='can_view_statistics')
        except Permission.DoesNotExist:
            print('No permissions to migrate')
            return

        users = User.objects.filter(
            Q(groups__permissions=old_perm) |
            Q(user_permissions=old_perm)
        )
        print('Found %s users with old perms' % users.count())
        for user in users:
            user.user_permissions.add(new_perm)
        if users.exists():
            print ('Permissions updated')
        old_perm.delete()
