# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import json

from django.conf import settings
from django.core.files import File
from django.core.management.base import BaseCommand
from django.db.utils import IntegrityError

from dbmail.models import MailTemplate, MailFromEmailCredential, MailFromEmail, MailBcc

from config.models import About, MerchantDetails, Config
from gateway.models import Gateway, GatewaySettings
from gateway.knet.models import KnetSettings
from gateway.migs.models import MIGSSettings
from gateway.cybersource.models import CyberSourceSettings
from gateway.burgan.models import BurganSettings
from extras.currency.models import Currency, ExchangeConfig, KWD
from extras.dashboard.models import Field, PaymentRequestFormConfig
from sms.models import NexmoSMSAuthConfig


def create_currency_config():
    default_currency, created = Currency.objects.get_or_create(code=KWD)
    return ExchangeConfig.objects.get_or_create(
        name='default',
        default_currency=default_currency,
    )[0]


class Command(BaseCommand):
    help = "Import metadata"
    outfile = '%s/metadata.json' % settings.FIXTURE_DIRS[1]

    def handle(self, **options):
        with open(self.outfile) as data_file:
            data = json.load(data_file)

        try:
            About.objects.create(**data['about_data'])
            print('About imported')
        except IntegrityError:
            pass

        try:
            MerchantDetails.objects.all().delete()
            merchant_details = MerchantDetails.objects.create(**data['merchant_details'])
            print('Merchant details imported')
            logo_path = data['merchant_details'].pop('logo', None)
            if logo_path:
                with open(logo_path) as logo:
                    name = logo_path.split('/')[-1]
                    merchant_details.logo.save(name, File(logo))
        except Exception as e:
            print e

        # mail
        try:
            MailFromEmailCredential.objects.all().delete()
            MailFromEmail.objects.all().delete()
            data['email_auth']['credential'] = MailFromEmailCredential.objects.create(**data['email_credential'])
            MailFromEmail.objects.create(**data['email_auth'])
        except Exception as e:
            print('Email credentials couldn\'t be created due to %s' % e)

        # sms
        try:
            NexmoSMSAuthConfig.objects.all().delete()
            NexmoSMSAuthConfig.objects.create(**data['sms'])
            print('Nexmo imported')
        except Exception as e:
            print('No sms settings or they couldn\t be created due to %s' % e)

        # fields
        field_config = PaymentRequestFormConfig.get_solo()
        Field.objects.all().delete()
        for field_data in data['fields_data']:
            if field_data['label_en']:
                field_data.update({'config': field_config})
                field = Field.objects.create(**field_data)
                field.name = field_data['name']
                field.save()
                print('Field %s with label %s created' % (field_data['name'], field_data['label_en']))
        Field.objects.create(field='customer_first_name', config=field_config)
        Field.objects.create(field='customer_last_name', config=field_config)

        currency_config = create_currency_config()

        # email bcc
        if data['bcc_addresses']:
            MailBcc.objects.all().delete()
            emails = data['bcc_addresses']
            mail_bcc = [MailBcc(**email) for email in emails]
            MailBcc.objects.bulk_create(mail_bcc)

        # terms
        try:
            config = Config.get_solo()
            config.terms_text = data['terms_text']
            config.terms = True
            config.save()
        except KeyError:
            pass

        for gateway_name, gateway_data in data['gateways'].items():
            if gateway_name == 'knet':
                KnetSettings.objects.all().delete()
                knet, created = Gateway.objects.get_or_create(name=Gateway.KNET)
                for index, knet_data in enumerate(gateway_data):
                    file_ = knet_data.pop('file', None)
                    acc = KnetSettings.objects.create(
                        gateway=knet,
                        currency_config=currency_config,
                        **knet_data)
                    print('Knet imported')
                    if file_:
                        with open(file_) as resource:
                            name = file_.split('/')[-1]
                            acc.file.save(name, File(resource))
            elif gateway_name == 'creditcard':
                MIGSSettings.objects.all().delete()
                migs, created = Gateway.objects.get_or_create(name=Gateway.MIGS)
                MIGSSettings.objects.create(
                    gateway=migs,
                    name=gateway_name.title(),
                    currency_config=currency_config,
                    type=GatewaySettings.PRODUCTION,
                    **gateway_data
                )
                print('MiGS imported')

            elif gateway_name == 'cybersource':
                CyberSourceSettings.objects.all().delete()
                cs, created = Gateway.objects.get_or_create(name=Gateway.CYBERSOURCE)
                CyberSourceSettings.objects.create(
                    gateway=cs,
                    name=gateway_name.title(),
                    currency_config=currency_config,
                    **gateway_data
                )
                print('CyberSource imported')
            # else:
            #     burgan
                # BurganSettings.objects.all().delete()
                # burgan, created = Gateway.objects.get_or_create(name=Gateway.BURGAN)
                # BurganSettings.objects.create(
                #     gateway=burgan,
                #     name=gateway_name.title(),
                #     currency_config=currency_config,
                #     **gateway_data
                # )
                # print('Burgan imported')