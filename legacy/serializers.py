# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.core.urlresolvers import reverse

import pycountry
from rest_framework import serializers

from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _

from gateway.models import PaymentTransaction, GatewaySettings
from gateway.serializers import BaseTransactionSerializer
from .validators import LegacyUniqueValidator


User = get_user_model()


def get_field_mapping():
    return {
        'initiator': ['sender'],
        'gateway_code': ['gateway'],
        'amount': ['amount', 'amt'],
        'language': ['langid'],
        'currency_code': ['currencycode', 'currency'],
        'order_no': ['trackid', 'track_id'],
        'customer_email': ['udf2'],
        'customer_first_name': ['udf3'],
        'customer_phone': ['udf4'],
        'customer_address_line1': ['udf5'],
        'customer_address_city': ['udf6'],
        'customer_address_state': ['udf7'],
        'customer_address_country': ['udf8'],
        'extra': ['udf9']
    }


def convert_legacy_payload(legacy_payload):
    """
    :param legacy_payload: old knpay style payload
    :return: new style payload
    """
    mapping = get_field_mapping()
    new_payload = {}
    new_payload['extra'] = {}
    for field, field_names in mapping.items():
        field_value = ' '.join([legacy_payload.get(field_name, '')
                                for field_name in field_names if legacy_payload.get(field_name)])
        if field == 'extra':
            # for now it's meant only for udf9
            new_payload['extra'].update({field_names[0]: field_value})
        else:
            new_payload[field] = field_value
    return new_payload


class LegacySerializer(serializers.Serializer):
    LANGID_CHOICES = (
        ('USA', 'English'),
        ('ARA', 'Arabic'),
    )
    RESPONSETYPE_CHOICES = (
        ('post', 'POST'),
        ('json', 'JSON')
    )
    CURRENCYCODE_CHOICES = (
        ('414', '414 (KWD)'),
        ('840', '840 (USD)'),
        ('KWD', 'KWD'),
        ('USD', 'USD')
    )
    amount = serializers.DecimalField(max_digits=12, decimal_places=3)
    responseURL = serializers.URLField()
    errorURL = serializers.URLField()
    currencycode = serializers.ChoiceField(choices=CURRENCYCODE_CHOICES, default='414')
    langid = serializers.ChoiceField(choices=LANGID_CHOICES, required=False, default='USA')
    responseTYPE = serializers.ChoiceField(
        choices=RESPONSETYPE_CHOICES, required=False, default='post')
    udf1 = serializers.CharField(required=False, default='')
    udf2 = serializers.CharField(required=False)
    udf3 = serializers.CharField(required=False)
    udf4 = serializers.CharField(required=False)
    udf5 = serializers.CharField(required=False)
    udf6 = serializers.CharField(required=False)
    udf7 = serializers.CharField(required=False)
    udf8 = serializers.CharField(required=False)
    udf9 = serializers.CharField(required=False)
    sender = serializers.IntegerField(required=False)

    def __init__(self, *args, **kwargs):
        self.fields['gateway'] = serializers.ChoiceField(choices=GatewaySettings.objects.choices(),
                                                         default='knet')
        self.fields['trackid'] = serializers.CharField(validators=[
            LegacyUniqueValidator(PaymentTransaction.objects.all(), field_name='order_no')])
        super(LegacySerializer, self).__init__(*args, **kwargs)

    def validate_udf8(self, value):
        try:
            return pycountry.countries.get(name=value).alpha_2
        except KeyError:
            return 'KW'

    def validate_amount(self, value):
        return str(value)

    def validate_langid(self, value):
        return 'ar' if value == 'ARA' else 'en'

    def validate_currencycode(self, value):
        if value == '414':
            return 'KWD'
        elif value == '840':
            return 'USD'
        else:
            return value

    def validate_sender(self, value):
        try:
            User.objects.get(pk=value)
        except User.DoesNotExist:
            raise serializers.ValidationError(_("Sender does not exist"))
        return unicode(value)

    def validate(self, attrs):
        payload = convert_legacy_payload(attrs)
        payload['extra'].update({
                'responseURL': attrs['responseURL'],
                'responseTYPE': attrs['responseTYPE'],
                'errorURL': attrs['errorURL'],
                'udf1': attrs['udf1']
            })
        self.serializer = BaseTransactionSerializer(data=payload)
        self.serializer.is_valid(raise_exception=True)
        return attrs

    def create(self, validated_data):
        request = self.context['request']
        payment_transaction = self.serializer.save()
        return_url = request.build_absolute_uri(reverse('legacy:response',
                                                        args=(payment_transaction.pk,)))
        payment_transaction.disclosure_url = return_url
        payment_transaction.redirect_url = return_url
        payment_transaction.save()
        return payment_transaction

    def update(self, instance, validated_data):
        pass
