from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^user/$', views.ApiDispatchView.as_view(), name='api_dispatch'),
    url(r'^response/(?P<pk>\d+)/$',
        views.LegacyResponseView.as_view(), name='response'),
]