# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from rest_framework.validators import UniqueValidator


class LegacyUniqueValidator(UniqueValidator):
    def __init__(self, queryset, field_name, message=None, lookup='exact'):
        super(LegacyUniqueValidator, self).__init__(queryset, message=None, lookup=lookup)
        self.queryset = queryset
        self.field_name = field_name
        self.serializer_field = None
        self.message = message or self.message

    def set_context(self, serializer_field):
        self.instance = None

