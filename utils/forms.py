# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django import forms
from django.conf import settings
from django.template.defaultfilters import striptags
from django.utils.translation import ugettext_lazy as _, force_text as _ft

from phonenumber_field.validators import validate_international_phonenumber
from pycountry import countries

from extras.currency.models import Currency
from extras.dashboard.models import Field, CUSTOMER_PHONE
from gateway.models import GatewaySettings
from gateway.serializers import PrivateTransactionSerializer


class AjaxBaseForm(forms.BaseForm):
    """
    Returns the form errors as json
    """
    def errors_as_dict(self, strip_tags=True, error_message=True):
        errors = {}
        for error in self.errors.iteritems():
            errors.update({error[0]: striptags(error[1] if strip_tags else error[1])})
        if error_message:
            errors['error_message'] = _ft(_("Errors has occurred during form validation."))
        return errors


class BasePaymentForm(AjaxBaseForm, forms.Form):
    """
    Base fields for initializing any payment form (including bulk)
    """
    amount = forms.DecimalField(label=_("Amount"), decimal_places=3, max_digits=19)
    gateway_code = forms.CharField(label=_("Gateway"), required=False)
    currency_code = forms.ChoiceField(label=_("Currency"), choices=())
    language = forms.ChoiceField(label=_("Language"), choices=settings.LANGUAGES, initial='en')
    customer_email = forms.EmailField(label=_("Email"), required=True)
    sms_payment_details = forms.BooleanField(
        widget=forms.HiddenInput(), required=False,
        help_text=_("Admin defines if sms confirmation will be sent.")
    )

    def __init__(self, config, *args, **kwargs):
        self.config = config
        super(BasePaymentForm, self).__init__(*args, **kwargs)
        self.phone_field = self.config.fields.all().filter(field=CUSTOMER_PHONE)
        self.has_phone_field = self.phone_field.exists()

    def add_defined_field(self, name, label=None, initial=None, required=False, field=None):
        if name == 'attachment':
            field_klass = forms.FileField
            self.fields[name] = field_klass(label=label or name,
                                            initial=initial, required=required)
        elif name == 'customer_address_country':
            field_klass = forms.ChoiceField
            COUNTRY_CHOICES = [('', '----------')] + [(c.alpha_2, c.name) for c in list(countries)]
            self.fields[name] = field_klass(label=label or name,
                                            initial=initial,
                                            required=required,
                                            choices=COUNTRY_CHOICES)
        else:
            field_klass = forms.CharField
            self.fields[name] = field_klass(label=label or name, initial=initial,
                                            required=required)

    def clean(self):
        """
        2 step validation:
        1. validate the form normally against the static rules defined
        2. validate the validated data against TransactionSerializer rules -
            This is required to keep all business logic of creating a transaction
            in one place
        """
        if self.has_phone_field and self.cleaned_data.get(CUSTOMER_PHONE, False):
            self.cleaned_data['sms_payment_details'] = self.config.sms_payment_details

        if self.is_valid():
            self.validate_api()
        return self.cleaned_data

    def clean_customer_phone(self):
        customer_phone = self.cleaned_data.get('customer_phone')
        if customer_phone and not customer_phone.startswith('+'):
            customer_phone = "+%s" % customer_phone
        validate_international_phonenumber(customer_phone)
        return customer_phone


class InstancePaymentTransactionForm(forms.ModelForm):
    """
    Render fields from `extra` field
    """
    def __init__(self, config, *args, **kwargs):
        self.config = config
        super(InstancePaymentTransactionForm, self).__init__(*args, **kwargs)
        extra = self.instance.extra
        for key, value in extra.items():
            field = Field.objects.get_for_name(key, self.config)
            label = key if field is None else field.get_label()
            self.fields[key] = forms.CharField(label=label, initial=value)
        for field in self.fields:
            self.fields[field].disabled = True
            # hide the * from fields
            self.fields[field].required = False


class CurrencyForm(forms.Form):
    """
    Handles currency field
    """
    def __init__(self, *args, **kwargs):
        super(CurrencyForm, self).__init__(*args, **kwargs)
        self.manage_currency()

    def manage_currency(self):
        """
        Rules for currencies:
        -------------------
        1. currency will be changable upon the gateway selection by front-end code
        2. if gateway has more than one currency then we will display the list of the currencies
        3. if the gateway has one currency also it should be the default cur then it will
            be selected with hidden dropdown.
        4. if no selected gatway then currency will be selected with the form config currency
        5. if only one currency is available, select it by default and hide it from the form
        6. if multiple currencies are available, list all of them
        7. should be mandatory
        """

        choices = Currency.objects.choices()
        self.fields['currency_code'].choices = choices
        try:
            self.fields['currency_code'].initial = self.config.currency.code
        except:
           self.fields['currency_code'].initial = None 

class GatewayForm(forms.Form):
    """
    Handles gateway field
    """
    # define if gateway is required or not
    blank_gateway = True
    check_permission = True
    knpay_section = None

    def __init__(self, *args, **kwargs):
        super(GatewayForm, self).__init__(*args, **kwargs)
        self.manage_gateway(blank=self.blank_gateway)

    def manage_gateway(self, blank=blank_gateway):
        """
        Rules for gateway:
        ------------------
        1. If only one gateway is available, select it by default and hide it from the form
        2. If multiple gateway available, list all of them
        3. Gateway should have currencies in its currency config
        """
        # if permission is not required, most sure is shopify
        # need to update this when will Separate Gateway Accounts will be done.

        assert self.knpay_section is not None, 'Gateway section not configured!'

        accounts = GatewaySettings.objects.active().with_extra_currencies().get_for_section(self.knpay_section)

        if self.check_permission:
            accounts = accounts.get_for_user(self.request.user)

        choices = accounts.choices(blank=blank)
        self.fields['gateway_code'] = forms.ChoiceField(
            label=_("Gateway"),
            choices=choices)
        self.fields['gateway_code'].required = not(blank and accounts.exists())
        # check if there is only one gateway and select it by default
        if (blank and len(choices) == 2) or (not blank and len(choices) == 1):
            index = len(choices) - 1
            self.fields['gateway_code'].initial = choices[index][0]
            self.fields['gateway_code'].widget = forms.HiddenInput()


class AttemptURLMixin(object):
    def get_url(self):
        return self.instance.get_attempt_url(gateway_code=self.cleaned_data.get('gateway_code'))


class APIForm(forms.Form):
    """
    Validates form cleaned data against the private API.
    """
    transaction_type = None

    def __init__(self, request=None, *args, **kwargs):
        self.request = request
        super(APIForm, self).__init__(*args, **kwargs)

    def get_data_for_api(self):
        """
        Rules of building the API data
        1. all fields from form that match the API shall be returned
        2. Based on FormConfiguration model certain values has to be sent
            - disclose_to_merchant_url
                - disclosure_url (has to be validated that is configured)
        :return dict
        """
        data = self.cleaned_data.copy()
        data.update({'type': self.transaction_type})

        extra_fields = self.config.fields.active().values_list('name', flat=True)
        extra = {}
        for extra_field in extra_fields:
            if data.get(extra_field) is not None:
                extra[extra_field] = self.cleaned_data[extra_field]
                del data[extra_field]
        if extra:
            data['extra'] = extra
        if hasattr(self.config, 'disclose_to_merchant_url'):
            if self.config.disclose_to_merchant_url is True:
                if not self.config.disclosure_url:
                    raise forms.ValidationError(_("Merchant notification settings are ON, "
                                                  "but no URL is being set in Config section. "
                                                  "Please contact an administrator."))
                else:
                    data.update({'disclosure_url': self.config.disclosure_url})
                    data.update({'redirect_url': self.config.redirect_url})
        self.data_for_api = data
        return data

    def validate_api(self):
        field_errors = self.errors_as_dict().keys()
        # extract dynamic fields and pass them as Json to the serializer to be saved
        # in extra field
        # instantiate and validate the serializer
        data = self.get_data_for_api()
        serializer = PrivateTransactionSerializer(data=data)
        if serializer.is_valid():
            self.serializer = serializer
        else:
            # add errors from serializer to the form
            # for being rendered in template
            for field, error in serializer.errors.items():
                if field not in field_errors:
                    try:
                        self.add_error(field, error)
                    except ValueError:
                        # means the field which is having an error
                        # does not exist on the form
                        raise forms.ValidationError(_("Following error has occurred: '%s' regarding %s field. "
                                                      "Please contact an administrator.") % (error[0], field))


class AbstractBasePaymentRequestConfigForm(APIForm,
                                           GatewayForm,
                                           CurrencyForm):
    pass


class AbstractBasePaymentRequestForm(AbstractBasePaymentRequestConfigForm,
                                     BasePaymentForm):
    pass
