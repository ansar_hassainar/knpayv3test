# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import json

from django.utils.translation import ugettext_lazy as _
from django.core.validators import validate_email

from rest_framework.exceptions import ValidationError


class DigitValidator(object):
    """
    Validator that corresponds to `unique=True` on a model field.

    Should be applied to an individual field on the serializer.
    """
    message = _('A valid number is required.')

    def set_context(self, serializer_field):
        self.field_name = serializer_field.source_attrs[-1]

    def __call__(self, value):
        if not value.isdigit():
            raise ValidationError(self.message)
        return value


class JsonValidator(object):
    message = _("A json object is required.")

    def __call__(self, value):
        try:
            json.loads(value)
        except ValueError:
            raise ValidationError(self.message)
        return value


class EmailListValidator(object):
    """
    Calls validate_email on each Email from the email list
    """

    message = _('Please enter valid email recipients')

    def set_context(self, seializer_fields):
        self.field_name = seializer_fields.source_attrs[-1]

    def __call__(self, value):
        emails = [x.strip().strip(',') for x in value.split(',')]
        validate_email.message = self.message
        [validate_email(x) for x in emails]
        return value


class AlphaNumericValidator(object):
    """
    Checks given value is alphanumeric or not
    """
    message = _('A valid code is required.')

    def set_context(self, serializer_field):
        self.field_name = serializer_field.source_attrs[-1]

    def __call__(self, value):
        value = value.replace(' ', '')
        if not value.isalnum():
            raise ValidationError(self.message)
        return value
