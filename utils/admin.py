# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from pygments import highlight
from pygments.lexers.data import JsonLexer
from pygments.formatters.html import HtmlFormatter

from django.utils.safestring import mark_safe


class PrettyJsonAdminMixin(object):
    json_field = None
    json_short_description = None
    exclude = (json_field,)

    def get_readonly_fields(self, request, obj=None):
        fields = super(PrettyJsonAdminMixin, self).get_readonly_fields(request, obj=obj)
        return list(fields) + [self.json_field]

    def json_prettified(self, instance):
        formatter = HtmlFormatter(style='colorful')
        value = getattr(instance, self.json_field)
        response = ''
        try:
            if value:
                response = highlight(value, JsonLexer(), formatter)
        except:
            pass
        style = "<style>" + formatter.get_style_defs() + "</style><br>"
        return mark_safe(style + response)
    json_prettified.short_description = json_short_description
