# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _


class TimeStampedModel(models.Model):

    """ TimeStampedModel
    An abstract base class model that provides self-managed "created" and
    "modified" fields.
    """
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    def save(self, **kwargs):
        self.update_modified = kwargs.pop(
            'update_modified', getattr(self, 'update_modified', True))
        super(TimeStampedModel, self).save(**kwargs)

    class Meta:
        get_latest_by = 'modified'
        ordering = ('-modified', '-created',)
        abstract = True


class StatusModel(models.Model):
    is_active = models.BooleanField(_("Is active?"), default=True, db_index=True,
                                    help_text=_("Designates if this object is "
                                                "active, can be used and displayed on site."))

    class Meta:
        abstract = True


class EmailMailTemplate(models.Model):
    email_payment_details_success_template = models.ForeignKey(
        'dbmail.MailTemplate', blank=True, null=True, related_name='+')
    email_payment_details_failure_template = models.ForeignKey(
        'dbmail.MailTemplate', blank=True, null=True, related_name='+')

    class Meta:
        abstract = True


class SMSMailTemplate(models.Model):
    sms_payment_details_success_template = models.ForeignKey(
        'dbmail.MailTemplate', blank=True, null=True, related_name='+')
    sms_payment_details_failure_template = models.ForeignKey(
        'dbmail.MailTemplate', blank=True, null=True, related_name='+')

    class Meta:
        abstract = True


class NotificationMailTemplate(models.Model):
    customer_payment_email_notification_template = models.ForeignKey(
        'dbmail.MailTemplate', blank=True, null=True, related_name='+')
    customer_payment_sms_notification_template = models.ForeignKey(
        'dbmail.MailTemplate', blank=True, null=True, related_name='+')

    class Meta:
        abstract = True


class MerchantUrls(models.Model):
    disclosure_url = models.URLField(
        _("Disclosure URL"),
        default='',
        blank=True,
        help_text=_("URL where to send the payment result details. "
                    "Must return a http status 200, in order to "
                    "proceed with redirection to redirect_url")
    )
    redirect_url = models.URLField(
        _("Redirect URL"),
        default='',
        blank=True,
        help_text=_("URL where the user to be redirected "
                    "after payment process has completed")
    )

    class Meta:
        abstract = True


class MerchantURLConfig(MerchantUrls):
    disclose_to_merchant_url = models.BooleanField(
        _("Disclose to merchant URL"), default=False,
        help_text=_("If checked, the disclosure url from Configuration section "
                    "will be notified upon success payment.")
    )

    class Meta:
        abstract = True
