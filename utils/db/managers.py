# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
from utils.perms import perm_check, perm_codes


class PermissionQuerySet(models.QuerySet):
    def _has_superuser_perm(self, user):
        return perm_check(user, self.model._meta, perm_codes['view']['all'])

    def _has_initiator_perm(self, user):
        return perm_check(user, self.model._meta, perm_codes['view']['one'])

    def get_for_user(self, user, field_name='initiator'):
        if user.is_superuser or self._has_superuser_perm(user):
            return self.all()
        elif self._has_initiator_perm(user):
            return self.filter(**{field_name: user})
        else:
            return self.none()


class PermissionManager(models.Manager):
    def get_queryset(self):
        return PermissionQuerySet(self.model, using=self._db)

    def get_for_user(self, user, field_name='initiator'):
        return self.get_queryset().get_for_user(user, field_name=field_name)


class StatusQuerySet(models.QuerySet):
    def active(self):
        return self.filter(is_active=True)