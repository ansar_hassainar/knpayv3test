# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from functools import wraps

from django.http import JsonResponse
from django.utils.translation import ugettext_lazy as _

from config.models import Config


def pause_restriction():
    def decorator(view_func):
        def _view(request, *args, **kwargs):
            config = Config.get_solo()
            if request.is_ajax() and config.is_paused:
                error_msg = _("Can't perform this operation while the platform is paused.")
                return JsonResponse({'message': error_msg}, status=400)
            return view_func(request, *args, **kwargs)
        return wraps(view_func)(_view)
    return decorator
