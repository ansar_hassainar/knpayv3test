# -*- coding: utf-8 -*-

from __future__ import unicode_literals


perm_codes = {
    'view': {
        'all': ('view', 'change'),
        'one': ('view_owned', 'change_owned')
    },
    'change': {
        'all': ('change',),
        'one': ('change_owned',)
    }
}


def perm_check(user, opts, perm_codes):
    return any([user.has_perm('%s.%s_%s' % (opts.app_label, perm, opts.model_name))
                for perm in perm_codes])
