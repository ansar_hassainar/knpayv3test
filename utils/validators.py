# -*- coding: utf-8 -*-

import mimetypes
from os.path import splitext

from django.core.exceptions import ValidationError
from django.template.defaultfilters import filesizeformat
from django.utils.deconstruct import deconstructible
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext_lazy


@deconstructible
class FileValidator(object):

    """
    @author dokterbob
    @author jrosebr1

    Validator for files, checking the size, extension and mimetype.

    Initialization parameters:
        allowed_extensions: iterable with allowed file extensions
            ie. ('txt', 'doc')
        allowed_mimetypes: iterable with allowed mimetypes
            ie. ('image/png', )
        min_size: minimum number of bytes allowed
            ie. 100
        max_size: maximum number of bytes allowed
            ie. 24*1024*1024 for 24 MB

    Usage example::

        MyModel(models.Model):
            myfile = FileField(validators=FileValidator(max_size=24*1024*1024), ...)

    """
    extension_message = _(
        "Extension '%(extension)s' not allowed. Allowed extensions are: '%(allowed_extensions)s.'")
    mime_message = _(
        "MIME type '%(mimetype)s' is not valid. Allowed types are: %(allowed_mimetypes)s.")
    min_size_message = _(
        'The current file %(size)s, which is too small. The minumum file size is %(allowed_size)s.')
    max_size_message = _(
        'The current file %(size)s, which is too large. The maximum file size is %(allowed_size)s.')

    def __init__(self, *args, **kwargs):
        self.allowed_extensions = kwargs.pop('allowed_extensions', [])
        self.allowed_mimetypes = kwargs.pop('allowed_mimetypes', [])
        self.min_size = kwargs.pop('min_size', 0)
        self.max_size = kwargs.pop('max_size', None)

    def __call__(self, value):
        """
        Check the extension, content type and file size.
        """

        # Check the extension
        ext = splitext(value.name)[1][1:].lower()
        if not (not self.allowed_extensions or ext in self.allowed_extensions):
            message = self.extension_message % {
                'extension': ext,
                'allowed_extensions': ', '.join(self.allowed_extensions)
            }

            raise ValidationError(message)

        # Check the content type
        mimetype = mimetypes.guess_type(value.name)[0]
        if self.allowed_mimetypes and not mimetype in self.allowed_mimetypes:
            message = self.mime_message % {
                'mimetype': mimetype,
                'allowed_mimetypes': ', '.join(self.allowed_mimetypes)
            }

            raise ValidationError(message)

        # Check the file size
        filesize = len(value)
        if self.max_size and filesize > self.max_size:
            message = self.max_size_message % {
                'size': filesizeformat(filesize),
                'allowed_size': filesizeformat(self.max_size)
            }

            raise ValidationError(message)

        elif filesize < self.min_size:
            message = self.min_size_message % {
                'size': filesizeformat(filesize),
                'allowed_size': filesizeformat(self.min_size)
            }

            raise ValidationError(message)

    def __eq__(self, other):
        return (
            isinstance(other, FileValidator) and
            self.allowed_extensions == other.allowed_extensions and
            self.allowed_mimetypes == other.allowed_mimetypes and
            self.min_size == other.min_size and
            self.max_size == other.max_size and
            (self.extension_message == other.extension_message) and
            (self.mime_message == other.mime_message) and
            (self.min_size_message == other.min_size_message) and
            (self.max_size_message == other.max_size_message))

    def __ne__(self, other):
        return not (self == other)


def _validate_positive_int(val):
    try:
        val = int(val)
    except ValueError:
        raise ValidationError(ugettext_lazy("%(val)s is not an integer" % {'val': val}))
    if val < 0:
        raise ValidationError(ugettext_lazy("%(val)s has to be positive" % {'val': val}))


def _validate_min(minute):
    _validate_positive_int(minute)
    minute = int(minute)
    if minute > 59:
        raise ValidationError(ugettext_lazy("Minute has to be max 59"))


def validate_hh_mm(value):
    value_list = value.split(':')
    if len(value_list) == 2:
        _validate_positive_int(value_list[0])
        _validate_min(value_list[1])
    else:
        # we have another format
        raise ValidationError(ugettext_lazy("Follow HH:MM format, not %(value)s" % {'value': value}))

