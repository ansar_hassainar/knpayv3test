# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from mixer.backend.django import mixer

from gateway.knet.models import KnetSettings
from gateway.burgan.models import BurganSettings
from gateway.cybersource.models import CyberSourceSettings
from gateway.pp.models import PayPalSettings
from gateway.migs.models import MIGSSettings
from gateway.mpgs.models import MPGSSettings
from config.models import KNPaySection
from gateway.models import PaymentTransaction


def knet_settings_factory(currency=None):
    if not currency:
        currency = mixer.blend('currency.Currency', currency='KWD')
    gateway = mixer.blend('gateway.Gateway', name='knet')
    exchange_config = mixer.blend('currency.ExchangeConfig', default_currency=currency, fee_type='', name='default')
    # use concrete create because of polymorphic app (content type creation is not done correctly using fixtures)
    gateway_setting = KnetSettings.objects.create(name='knet-test', code='knet-test',
                                gateway=gateway,
                                currency_config=exchange_config)

    knpay_section = mixer.blend('config.KNPaySection', name=PaymentTransaction.PAYMENT_REQUEST)
    gateway_setting.sections.add(knpay_section)
    return gateway_setting
    # return mixer.blend('knet.KnetSettings', name='knet-test', code='knet-test', gateway=gateway, currency_config=exchange_config)


def burgan_settings_factory():
    currency = mixer.blend('currency.Currency', currency='KWD')
    gateway = mixer.blend('gateway.Gateway', name='burgan')
    exchange_config = mixer.blend('currency.ExchangeConfig', default_currency=currency, fee_type='', name='default')
    # use concrete create because of polymorphic app (content type creation is not done correctly using fixtures)
    gateway_setting = BurganSettings.objects.create(name='burgan-test', code='burgan-test', gateway=gateway, currency_config=exchange_config)
    gateway_setting.sections = KNPaySection.objects.all()
    gateway_setting.save()
    return gateway_setting
    # return mixer.blend('burgan.BurganSettings', name='burgan-test', code='burgan-test', gateway=gateway, currency_config=exchange_config)


def cybersource_settings_factory(currency=None):
    if not currency:
        currency = mixer.blend('currency.Currency', currency='KWD')
    gateway = mixer.blend('gateway.Gateway', name='cybersource')
    exchange_config = mixer.blend('currency.ExchangeConfig', default_currency=currency, fee_type='', name='default')
    # use concrete create because of polymorphic app (content type creation is not done correctly using fixtures)
    gateway_setting = CyberSourceSettings.objects.create(name='cyber-test', code='cyber-test', gateway=gateway, currency_config=exchange_config)
    gateway_setting.sections = KNPaySection.objects.all()
    gateway_setting.save()
    return gateway_setting


def paypal_settings_factory(currency=None):
    if not currency:
        currency = mixer.blend('currency.Currency', currency='KWD')
    gateway = mixer.blend('gateway.Gateway', name='paypal')
    exchange_config = mixer.blend('currency.ExchangeConfig', default_currency=currency, fee_type='', name='default')
    # use concrete create because of polymorphic app (content type creation is not done correctly using fixtures)
    gateway_setting = PayPalSettings.objects.create(name='paypal-test', code='paypal-test', gateway=gateway, currency_config=exchange_config)
    gateway_setting.sections = KNPaySection.objects.all()
    gateway_setting.save()
    return gateway_setting


def migs_settings_factory(currency=None):
    if not currency:
        currency = mixer.blend('currency.Currency', currency='KWD')
    gateway = mixer.blend('gateway.Gateway', name='migs')
    exchange_config = mixer.blend('currency.ExchangeConfig', default_currency=currency, fee_type='', name='default')
    # use concrete create because of polymorphic app (content type creation is not done correctly using fixtures)
    gateway_setting = MIGSSettings.objects.create(name='migs-test', code='migs-test', gateway=gateway, currency_config=exchange_config)
    gateway_setting.sections = KNPaySection.objects.all()
    gateway_setting.save()
    return gateway_setting


def mpgs_settings_factory(currency=None):
    if not currency:
        currency = mixer.blend('currency.Currency', currency='KWD')
    gateway = mixer.blend('gateway.Gateway', name='mpgs')
    exchange_config = mixer.blend('currency.ExchangeConfig', default_currency=currency, fee_type='', name='default')
    # use concrete create because of polymorphic app (content type creation is not done correctly using fixtures)
    gateway_setting = MPGSSettings.objects.create(name='mpgs-test', code='mpgs-test', gateway=gateway, currency_config=exchange_config)
    gateway_setting.sections = KNPaySection.objects.all()
    gateway_setting.save()
    return gateway_setting
