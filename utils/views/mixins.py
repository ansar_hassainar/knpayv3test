# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

from django.utils.decorators import method_decorator
from django.http import JsonResponse
from django.http import Http404, HttpResponseRedirect, HttpResponseForbidden
from django.utils.translation import ugettext_lazy as _, force_text as _ft
from django.contrib.auth.views import redirect_to_login
from django.contrib.auth import get_permission_codename

from braces.views import LoginRequiredMixin
from unfriendly import settings as unfriendly_settings
from unfriendly.utils import CheckSumError, InvalidKeyError, decrypt
from gateway.models import PaymentTransaction, DeletedPaymentTransaction
from config.models import APISecurityConfig

from ipware.ip import get_ip

logger = logging.getLogger(__name__)


class APISecurityMixin(object):
    LEGACY, DASHBOARD, ECOMMERCE = 'legacy_api_on', 'dashboard_on', 'ecommerce_on'
    
    security_config_field = None

    def dispatch(self, request, *args, **kwargs):
        api_config = APISecurityConfig.get_solo()

        if not getattr(api_config, self.security_config_field, False):
            return HttpResponseForbidden()
        else:
            whitelisted_ips = api_config.whitelisted_ips
            ip = get_ip(request)
            if "*" in whitelisted_ips or ip in whitelisted_ips:
                return super(APISecurityMixin, self).dispatch(request, *args, **kwargs)
        return HttpResponseForbidden()


class DashboardMixin(LoginRequiredMixin):
    """
    Adds a context variable to be used on template
    for showing dashboard containers, like side menu, etc
    """

    def no_permissions_fail(self, request=None):
        """
        Called when the user has no permissions and no exception was raised.
        This should only return a valid HTTP response.
        By default we redirect to login.
        """
        if not request.user.is_authenticated():
            return redirect_to_login(request.get_full_path(),
                                     self.get_login_url(),
                                     self.get_redirect_field_name())
        raise Http404()

    def get_context_data(self, **kwargs):
        ctx = super(DashboardMixin, self).get_context_data(**kwargs)
        ctx['dashboard'] = True
        return ctx


class PageTitleMixin(object):
    """
    Passes page_title and active_tab into context, which makes it quite useful
    for the accounts views.
    Dynamic page titles are possible by overriding get_page_title.
    """
    page_title = None
    active_tab = None
    hide_navbar = False

    def get_page_title(self):
        return self.page_title

    def get_context_data(self, **kwargs):
        ctx = super(PageTitleMixin, self).get_context_data(**kwargs)
        ctx.setdefault('page_title', self.get_page_title())
        ctx.setdefault('active_tab', self.active_tab)
        ctx.setdefault('hide_navbar', self.hide_navbar)
        return ctx


class EncryptedPKMixin(object):
    def get_object(self, queryset=None):
        # if queryset is None:
        queryset = self.get_queryset()

        try:
            pk = decrypt(str(self.kwargs.get('encrypted_pk')),
                         str(unfriendly_settings.UNFRIENDLY_SECRET),
                         str(unfriendly_settings.UNFRIENDLY_IV),
                         checksum=unfriendly_settings.UNFRIENDLY_ENFORCE_CHECKSUM)
        except (CheckSumError, InvalidKeyError, UnicodeEncodeError):
            raise Http404(_("Invalid encrypted key"))
        except TypeError as e:
            raise e

        queryset = queryset.filter(pk=pk)

        try:
            # Get the single item from the filtered queryset
            obj = queryset.get()
        except queryset.model.DoesNotExist:
            raise Http404(_("No %(verbose_name)s found matching the query") %
                          {'verbose_name': queryset.model._meta.verbose_name})
        return obj

    def get_queryset(self):
        return self.queryset


class PSPRedirectMixin(object):
    def redirect(self):
        # KNPAY URL
        internal_redirect = self.object.get_details_url()
        payment_transaction = self.object.transaction
        payment_attempt = self.object

        # disclose payment attempt maximum once to merchant
        if payment_transaction.has_to_be_disclosed_to_merchant() and not payment_attempt.disclosed_to_merchant:
            payment_attempt.disclose_to_merchant()

        if any([payment_transaction.is_e_commerce(), payment_transaction.is_payment_request()]) \
                and payment_transaction.can_be_redirected_to_merchant():
            return HttpResponseRedirect(payment_transaction.get_merchant_redirect_url())

        elif any((payment_transaction.is_payment_request(),
                  payment_transaction.is_customer_payment(),
                  payment_transaction.is_catalogue(),
                  payment_transaction.is_e_commerce())):
            return HttpResponseRedirect(internal_redirect)
        else:
            # payment transaction is third party (shopify only as of now)
            return HttpResponseRedirect(payment_transaction.get_local_shopify_url())


class PSPResponseMixin(PSPRedirectMixin):
    def post(self, request, *args, **kwargs):
        data = request.POST.copy()
        return self.process(request, data, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        data = request.GET.copy()
        return self.process(request, data, *args, **kwargs)

    def process(self, request, data, *args, **kwargs):
        logger.info('Gateway payload %s' % data)
        self.object = self.get_object()  # PaymentAttempt
        self.transaction = self.object.transaction  # PaymentTransaction
        return self.handle_psp_response(data)

    def handle_psp_response(self, data):
        """
        Change transaction state based on PSP response
        :param data: request.GET.copy() or request.POST.copy()
        """
        raise NotImplementedError


class LoggerMixin(object):
    def post(self, request, *args, **kwargs):
        import logging
        logger = logging.getLogger('post.request')
        logger.info('POSTed data to %s: %s' %
                    (self.__class__.__name__, dict(request.POST.items())))
        return super(LoggerMixin, self).post(request, *args, **kwargs)


class ActionManagerMixin(object):
    '''
    A mixin used to perform certain actions based on a queryset
    Should be used along with generic.ListView
    '''

    actions = ['delete', 'cancel', 'restore']

    def has_delete_paid_permission(self, request, obj=None):
        codename = get_permission_codename(
            'delete_paid', PaymentTransaction._meta)
        return request.user.has_perm('%s.%s' % (PaymentTransaction._meta.app_label, codename), obj)

    def has_restore_deletedpaymenttransaction_permission(self, request, obj=None):
        codename = get_permission_codename(
            'restore', DeletedPaymentTransaction._meta)
        return request.user.has_perm('%s.%s' % (DeletedPaymentTransaction._meta.app_label, codename), obj)

    def get(self, request, *args, **kwargs):
        action = request.GET.get('action', None)
        if action not in self.actions or not request.is_ajax():
            return Http404()

        if action == 'restore':
            self.queryset = DeletedPaymentTransaction.objects.all()
        else:
            self.queryset = PaymentTransaction.objects.all()

        queryset = self.generate_queryset()

        if queryset:
            payload = dict()
            if action == "delete" and queryset.paid().exists():
                if not self.has_delete_paid_permission(request):
                    payload.update(dict(success=False,
                                        message=_ft(_('You are not having permission to delete paid transactions.'))))
                    return JsonResponse(payload)
            if action == "restore":
                if not self.has_restore_deletedpaymenttransaction_permission(request):
                    payload.update(dict(success=False,
                                        message=_ft(_('You are not having permission to restore deleted transactions.'))))
                    return JsonResponse(payload)

            payload = getattr(self, action)(queryset)
            payload.update(dict(success=True,
                                message=_ft(_('Action successfully performed'))))
            return JsonResponse(payload)
        return JsonResponse(dict(message=_ft(_('Please select at least 1 row'))))

    def generate_queryset(self):
        ids = self.request.GET.get('ids', None)
        queryset = None
        if ids:
            ids = [int(id) for id in ids.split(',')]
            queryset = self.queryset.filter(id__in=ids)
        return queryset

    def cancel(self, queryset):
        for q in queryset:
            if hasattr(q, 'cancel'):
                if q.can_be_cancelled():
                    q.cancel()
                    q.save()
            return dict(status=_ft(_('CANCELED')))

    def delete(self, queryset):
        for obj in queryset:
            if obj.state == obj.PAID:
                obj.is_deleted = True
                obj.save()
            else:
                obj.delete()
        return dict()

    def restore(self, queryset):
        for obj in queryset:
            if obj.is_deleted:
                obj.is_deleted = False
                obj.save()
        return dict()
