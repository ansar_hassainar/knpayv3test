# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import requests
import logging
import unicodedata as ud
from collections import OrderedDict

from django.core.urlresolvers import resolve
from django.db.models import Q
from django.conf import settings
from django.utils.translation import ugettext as _, force_text as _ft
from django.utils.module_loading import import_string

from extras.dashboard.models import Field
from config.models import Config

from pycountry import countries

import re


def get_class(class_path):
    """
    dynamically get a class from a string
    """
    class_data = class_path.split(".")
    module_path = ".".join(class_data[:-1])
    class_str = class_data[-1]

    module = import_string(module_path)
    # Finally, we retrieve the Class
    return getattr(module, class_str)


def flatten_list(l):
    """
    :param l: list containing lists
    :return: merged list
    """
    return [item for sublist in l for item in sublist]


def is_ascii(string):
    try:
        string.decode('ascii')
        return True
    except (UnicodeDecodeError, UnicodeEncodeError):
        return False


def check_is_not_failure_url(url):
    try:
        return resolve(url).url_name != 'attempt_failure'
    except Exception as e:
        return True


def str_to_hex(message):
    import sys
    reload(sys)
    sys.setdefaultencoding("utf-8")
    return ''.join([str(hex(ord(char))).replace('0x', '0').zfill(4).upper() for char in unicode(message)])


def is_latin(latin_letters, uchr):
    try:
        return latin_letters[uchr]
    except KeyError:
        return latin_letters.setdefault(uchr, 'LATIN' in ud.name(uchr))


def only_roman_chars(unistr):
    latin_letters = {}
    return all(is_latin(latin_letters, uchr)
               for uchr in unistr
               if uchr.isalpha())


def format_country_code(code, definition):
    if definition not in ['alpha_2', 'alpha_3', 'numeric']:
        return code
    country = countries.get(alpha_2=code)
    return getattr(country, definition)


def get_context_note():
    """
    :return: the updated mail context
    """
    from extras.dashboard.models import Field
    new_line = "\r\n"
    pattern = "{{ FIELD_NAME }}"
    paragraph = "\r\n\r\nAll the custom fields which is added in dashboard payment " \
                "request form and in bulk configuration (FIELDS) section " \
                "will be auto added in the above list."
    context_note = "{{ amount }}  - the amount to be paid \r\n" \
                   "{{ gateway_code }} - gateway code\r\n" \
                   "{{ gateway_name }} - gateway name\r\n" \
                   "{{ currency_code }} - currency code\r\n" \
                   "{{ order_no }} - merchant unique identifier\r\n" \
                   "{{ reference_number }} - reference number of the attempt\r\n" \
                   "{{ id }} - id of the transaction (do not confuse it with order no or bulk id)\r\n" \
                   "{{ date }} - date when the state of the transaction is changed\r\n" \
                   "{{ initiator.name }} - name of the person which created the transaction\r\n" \
                   "{{ merchant.email }}  - email of the merchant\r\n" \
                   "{{ merchant.name }} - name of the merchant\r\n" \
                   "{{ merchant.phone }} - phone of the merchant\r\n" \
                   "{{ payment_url }} - the payment url of the transaction\r\n" \
                   "{{ attachment_url }} - the payment-request attachment url of the transaction\r\n" \
                   "{{ logo_url }} - url of the logo\r\n" \
                   "{{ customer_first_name }} \r\n" \
                   "{{ customer_last_name }} \r\n" \
                   "{{ customer_email }} \r\n" \
                   "{{ customer_phone }} \r\n" \
                   "{{ customer_address_line1 }} \r\n" \
                   "{{ customer_address_line2 }} \r\n" \
                   "{{ customer_address_line2 }} \r\n" \
                   "{{ customer_address_city }} \r\n" \
                   "{{ customer_address_state }} \r\n" \
                   "{{ customer_address_country }} \r\n" \
                   "{{ customer_address_postal_code }} \r\n" \
                   "{{ gateway_response.FIELD_NAME }} - the response received back from gateway " \
                   "(replace FIELD_NAME with any variable received from gateway, ie: 'trackid')\r\n" \
                   "{{ product_id }} - only for catalogue transaction type\r\n" \
                   "{{ product_name }} \r\n" \
                   "{{ product_type }} \r\n" \
                   "{{ product_quantity }} \r\n" \
                   "{{ product_image_url }} \r\n" \
                   "{{ invoice_url }} - short url for the invoice of paid transaction \r\n"

    fields = Field.objects.custom()
    field_names = []
    for field in fields:
        field_name = field.get_name()
        if field_name not in field_names:
            context_note += "%s - %s%s" % (
                pattern.replace('FIELD_NAME', field_name), field.get_label(), new_line)
        field_names.append(field_name)

    context_note += paragraph
    return context_note


def update_email_context():
    from dbmail.models import MailTemplate
    Config.objects.all().update(context_note=get_context_note())
    MailTemplate.objects.all().update(context_note=get_context_note())


def base_payment_payload(payment_attempt):
    payload = OrderedDict()
    gateway_payload = payment_attempt.gateway_response

    payload[_("Amount")] = '%s %s' % (payment_attempt.transaction.get_amount(
    ), payment_attempt.transaction.currency_code)
    if payment_attempt.transaction.order_no:
        payload[_("Order No")] = payment_attempt.transaction.order_no
    payload[_("Reference No")] = payment_attempt.reference_number
    payload[_("Status")] = payment_attempt.get_state_display()
    if payment_attempt.settings:
        payload[_("Gateway")] = payment_attempt.settings.gateway.get_name_display()
    else:
        payload[_("Gateway Code")] = payment_attempt.transaction.gateway_code

    for item in settings.VAR_MAPPING:
        val = gateway_payload.get(item[0])
        if val:
            payload[_ft(item[1])] = val

    return payload


def prepare_payment_payload(payment_attempt):
    payment_transaction = payment_attempt.transaction
    excluded_keys = ['consumed', 'errorURL', 'responseTYPE', 'responseURL']

    payload = base_payment_payload(payment_attempt)

    for field in payment_transaction._meta.get_fields():
        if field.name.startswith('customer'):
            val = getattr(payment_transaction, field.name)
            if val:
                payload[_ft(field.verbose_name)] = val

    for key, val in payment_transaction.extra.iteritems():

        if key in excluded_keys:
            continue

        try:
            label = Field.objects.filter(
                Q(field=key) | Q(name=key)).get().get_label()
        except Field.MultipleObjectsReturned:
            label = Field.objects.filter(
                Q(field=key) | Q(name=key)).first().get_label()
        except Field.DoesNotExist:
            label = key.title()
        if val:
            payload[_ft(label)] = val

    payload[_("Date")] = payment_attempt.state_changed_at

    return payload


def correct_decimals(number):
    num, decimal_string = str(number).split(".")
    if len(decimal_string) == 3:
        if not decimal_string[2] == 0:
            number = round(number, 2)
    return "%.3f" % number


def get_knpay_version():
    with open(settings.VERSION_FILE) as vf:
        return vf.readline().split("'")[1]


def shortify_url(url):
    """
    Takes a local url and return an external short url
    :param url: str
    :return: str
    """
    config = Config.get_solo()
    data = {
        'username': config.pay_kn_username,
        'password': config.pay_kn_password,
        'action': "shorturl",
        'format': "json",
        'url': url,
    }
    try:
        response = requests.post(settings.PAYKN_URL, data=data, verify=False,
                                 timeout=35)
    except Exception as e:
        dashboard_logger = logging.getLogger('dashboard')
        dashboard_logger.info("Cannot create short url due to %s" % e)
        return url
    if response.status_code == requests.codes.ok:
        return response.json()['shorturl']
    return url


def is_mobile_browser(request):
    try:
        return bool(re.search('/Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile/', request.META['HTTP_USER_AGENT']))
    except KeyError:
        return False


def get_invoice_pdf_url(encrypted_pk):
    file_url = "%(media_url)sinvoices/invoice_%(encrypted_pk)s.pdf" % {
        'media_url': settings.MEDIA_URL,
        'encrypted_pk': encrypted_pk
    }
    return file_url


def get_invoice_pdf_path(encrypted_pk):
    file_path = "%(media_path)s/invoices/invoice_%(encrypted_pk)s.pdf" % {
        'media_path': settings.MEDIA_ROOT,
        'encrypted_pk': encrypted_pk
    }
    file_name = "invoice_%(encrypted_pk)s.pdf" % {'encrypted_pk': encrypted_pk}
    return file_name, file_path


def get_whatsapp_send_origin(request):
    return 'whatsapp://send' if is_mobile_browser(request) else 'https://web.whatsapp.com/send'
