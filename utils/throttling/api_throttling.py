# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf import settings

from rest_framework.throttling import SimpleRateThrottle


class APISecurityThrottle(SimpleRateThrottle):
    def get_rate(self):
        return settings.THROTTLING_RATE

    def get_cache_key(self, request, view):
        return type(view).__name__
