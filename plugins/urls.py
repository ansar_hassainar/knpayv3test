# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url, include

urlpatterns = [
    url(r'^catalogue/', include('plugins.catalogue.urls', namespace='catalogue')),
    url(r'^cpr/', include('plugins.code_pairing.urls', namespace='code_pairing')),
]
