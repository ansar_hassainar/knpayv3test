# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.http import HttpResponseNotFound
from plugins.models import Plugin, InstalledPlugins

class PluginInstallMixin(object):

    plugin_name = None

    def dispatch(self, request, *args, **kwargs):
        try:
            plugin = Plugin.objects.get(name=self.plugin_name)
            if not plugin in InstalledPlugins.get_solo().plugins.all():
                return HttpResponseNotFound()
            return super(PluginInstallMixin, self).dispatch(request, *args, **kwargs)
        except Plugin.DoesNotExist:
            return HttpResponseNotFound()
