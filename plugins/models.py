# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _

from solo.models import SingletonModel


class Plugin(models.Model):
    name = models.CharField(_("Name"), max_length=32)
    slug = models.SlugField(_("Slug"), editable=False)
    about = models.TextField(_("About plugin"))

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _("Plugin")
        verbose_name_plural = _("Plugins")


class InstalledPlugins(SingletonModel):
    plugins = models.ManyToManyField(
        Plugin, verbose_name=_("Plugins"), blank=True)

    def __unicode__(self):
        return 'installed plugins'

    class Meta:
        verbose_name = _("Installed plugins")
