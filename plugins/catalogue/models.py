# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging
import decimal
from datetime import datetime
import pytz
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django_extensions.db.fields import AutoSlugField
from django.core.exceptions import ValidationError
from django.utils.functional import cached_property

from django.utils import timezone
from embed_video.fields import EmbedVideoField
from easy_thumbnails.fields import ThumbnailerImageField

from ckeditor.fields import RichTextField as HTMLField

from api.func import get_knpay_url
from extras.dashboard.models import BaseFormConfig
from gateway.signals import payment_acknowledged
from gateway.friendly_id import encode
from utils.db.models import (EmailMailTemplate, SMSMailTemplate,
                             TimeStampedModel, StatusModel
                             )
from utils.db.managers import StatusQuerySet
from utils.funcs import shortify_url
from utils.db.fields import NullCharField

logger = logging.getLogger('catalogue')


class CatalogueConfig(EmailMailTemplate, SMSMailTemplate,
                      BaseFormConfig):
    fields = GenericRelation('dashboard.Field',
                             related_query_name='catalogue_config')

    default_email = models.EmailField(
        _("default email"), max_length=120, blank=True)
    catalogue_url = models.URLField(_("Catalouge URL"), blank=True, default='',
                                    help_text=_("Product list url, to be shared with the social interfaces."))

    def __unicode__(self):
        return 'catalogue config'

    @cached_property
    def url(self):
        if not self.catalogue_url:
            path = reverse('plugins:catalogue:product_list')
            url = '{knpay_url}{path}'.format(
                knpay_url=get_knpay_url(),
                path=path)
            self.catalogue_url = shortify_url(url)
            self.save()
        return self.catalogue_url

    class Meta:
        verbose_name = _("Catalogue config")


class ProductImage(models.Model):
    image = ThumbnailerImageField(_("Image"), upload_to='products',
                                  resize_source=dict(size=(250, 180)))
    product = models.ForeignKey('catalogue.Product', on_delete=models.CASCADE,
                                related_name="images")

    def __unicode__(self):
        return '{product_name} images'.format(product_name=self.product.name)

    class Meta:
        verbose_name = _('Product Image')


class Product(TimeStampedModel, StatusModel):
    """
    Concrete model for Product
    Product can be a product or a service
    """
    PRODUCT, SERVICE = 'product', 'service'
    TYPES = (
        (PRODUCT, _("Product")),
        (SERVICE, _("Service"))
    )
    type = models.CharField(_("Type"), max_length=20,
                            choices=TYPES)
    name = models.CharField(_("Name"), max_length=128)
    slug = AutoSlugField(
        _("Slug"), populate_from='name', editable=True,
        separator='-', default='', allow_duplicates=False,
        help_text=_("Slug to be used in the url"))
    description = HTMLField(_("Description"), blank=True, default='',
                            help_text=_("A brief description of the product."),
                            config_name='catalogue_ckeditor')
    original_price = models.DecimalField(_("Original price"), max_digits=8,
                                         decimal_places=3, blank=True, null=True, help_text=_(
        'Original price of the product before the discount, and should be greater than price'))
    base_price = models.DecimalField(
        _("Price"), max_digits=8, decimal_places=3,
        help_text=_('Product price after the discount and the price of the sale'))
    currency = models.ForeignKey('currency.Currency',
                                 verbose_name=_("Currency"))
    stock = models.PositiveSmallIntegerField(
        _("Stock"), null=True, blank=True,
        help_text=_("Define stock for this product. Leave it empty for infinite."))
    display_stock = models.BooleanField(_("Display stock"),
                                        default=False, help_text=_('Display the available stock on the website'))
    max_quantity = models.PositiveSmallIntegerField(
        _("Max Quantity"), null=True, blank=True,
        help_text=_("Define max quantity purchase per order."))
    sold = models.PositiveIntegerField(
        _("Total Sold"), default=0, editable=False,
        help_text=_("Count based on the success payments"))
    start_date = models.DateTimeField(_('Start date'),
                                  null=True, blank=True,
                                  help_text=_('Start of the sale date'))
    end_date = models.DateTimeField(_('End date'),
                                null=True, blank=True,
                                help_text=_('End of the sale date'))
    add_delivery_charges = models.BooleanField(
        _('Add delivery charges'), default=False)
    delivery_charges = models.DecimalField(_("Delivery charges"), max_digits=8,
                                           decimal_places=3, null=True, blank=True)
    contact_number = models.CharField(_("Contact number"),
                                      max_length=30, default='', blank=True,
                                      help_text=_("Helpline Number/ Inquiry number about the product."))
    model_number = NullCharField(_("Model number"),
                                 null=True, max_length=30, blank=True,
                                 help_text=_("Model number of a product."), unique=True)
    youtube_link = EmbedVideoField(_("Youtube link"), default='', blank=True,
                                   help_text=_("Youtube link for the product video."))
    terms = models.BooleanField(
        _("Ask for terms & conditions"), default=False,
        help_text=_("Ask customer to accept terms and conditions before "
                    "proceeding with the payment")
    )
    terms_text = HTMLField(_("Terms and conditions"), blank=True,
                           default='', help_text=_("Checking this terms & conditions will override the global terms & conditions")
                           )
    gateway_settings = models.ManyToManyField(
        'gateway.GatewaySettings',
        verbose_name=_("Gateway settings"),
        help_text=_("Select the available gateway accounts for this product."))
    url = models.URLField(_("Short URL"), editable=False,
                          help_text=_("Short URL of the product page. Useful to "
                                      "spread the word among the social networks."))
    objects = StatusQuerySet.as_manager()

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _("Product")
        verbose_name_plural = _("Products")

    def clean(self, *args, **kwargs):
        super(Product, self).clean(*args, **kwargs)
        if self.start_date and self.end_date and \
                self.start_date > self.end_date:
            raise ValidationError(
                _('Start date must come before end date.'))
        #check the original_price is bigger than base_price
        if self.original_price and self.original_price < self.base_price:
            raise ValidationError(
                _('Price can not be more than original price'))
        #delivery_charges required if add_delivery_charges is checked
        if self.add_delivery_charges and not self.delivery_charges:
            raise ValidationError(
                {'delivery_charges': _('This field is required.')})
        #check the third decimal place should be 0
        if self.delivery_charges is not None:
            delivery_value = decimal.Decimal(self.delivery_charges)
            digit = str(self.delivery_charges)[-1]
            if delivery_value.as_tuple().exponent == -3 and digit != '0':
                raise ValidationError({'delivery_charges': _(
                    'Ensure that the third decimal place must be 0.')})

    def payment_link(self):
        if self.url:
            return self.url
        path = reverse('plugins:catalogue:product_detail', args=(self.slug,))
        url = '{knpay_url}{path}'.format(
            knpay_url=get_knpay_url(),
            path=path)
        self.url = shortify_url(url)
        self.save()
        return self.url

    def get_price(self):
        """
        :return: the price formatted as per given currency. 1-2-3-4 Decimal places.
        """
        pass

    def get_product_data(self):
        """
        Email context data
        """
        data = {'product_id': self.pk,
                'product_name': self.name,
                'product_type': self.type
                }
        if self.images.exists():
            data['product_image_url'] = "%s%s" % (get_knpay_url(),
                                                  self.images.all().first().image.url)
        return data

    @property
    def is_available(self):
        """
        Product must have some quantity or None. If it is 0 then unavailable
        Sell is available during Start and End date.
        """
        quantity_available = False if self.stock == 0 else True
        today = timezone.datetime.now().date()
        if self.start_date and self.end_date:
            date_available = self.start_date <= today <= self.end_date
        else:
            date_available = True

        return quantity_available and date_available

    @property
    def total_saving(self):
        """
        Product total saving percentage based on the original price and promotion price.
        """
        if self.original_price == 0:
            return 0
        return 100 - (self.base_price * 100 / self.original_price)

    @property
    def days_left(self):
        """
        Product remaing days it is calculated from the end date.
        """
        delta =  self.end_date.replace(tzinfo=pytz.UTC) - datetime.now().replace(tzinfo=pytz.UTC)
        if delta.days < 0:
            return 0
        return delta.days

    @property
    def hours_left(self):
        """
        Product remaing days it is calculated from the end date.
        """
        delta =  self.end_date.replace(tzinfo=pytz.UTC) - datetime.now().replace(tzinfo=pytz.UTC)
        if delta.seconds < 0:
            return 0
        return delta.seconds / 60 / 60

    def get_prices_for_gateways(self, quantity=1):
        """
        Each product will have specific gateways which can be used by the customer
        to pay for them. They are defined in gateway_settings field.
        This method will return all the possible gateway prices and fees
        for being rendered in the template.
        """
        gateway_choices = self.gateway_settings.all()
        choices = []
        for gateway in gateway_choices:
            if gateway.is_active:
                total, gateway_fee = gateway.currency_config.compute_total_with_quantity(
                    amount=self.base_price, from_code=self.currency.code,
                    quantity=quantity)
                currency_code = gateway.currency_config.default_currency.code
                choice = dict(total=str(total),
                              code=gateway.code,
                              value=gateway.name,
                              currency_code=currency_code,
                              fee=gateway_fee)

                if gateway.gateway.logo:
                    choice['url'] = '{}{}'.format(get_knpay_url(),
                                                  gateway.gateway.logo.url)

                choices.append(choice)

        data = {'gateway_choices': choices}
        return data

    def admin_thumbnail(self):
        if self.images.exists():
            return u'<a class"example-image-link" href="{0}" ' \
                u'data-lightbox="example-{1}" ><img src="{0}" ' \
                u'width="100" height="75" class="example-image"/>' \
                u'</a>'.format(self.images.all().first().image.url, self.pk)
        return u'<a class"example-image-link" href="{0}" ' \
            u'data-lightbox="example-{1}" ><img src="{0}" ' \
            u'width="100" ' \
            u'height="75" class="example-image"/>' \
            u'</a>'.format(self.name, self.pk)

    admin_thumbnail.short_description = 'Thumbnail'
    admin_thumbnail.allow_tags = True


class ReservedStock(models.Model):
    """
    Will reserve stock when user clicks on PAY button.
    If user doesn't comeback in 10 minutes with payment
    confirmation then rollback the quantity and delete this entry.
    """
    product = models.ForeignKey(Product, verbose_name=_('Product'),
                                related_name='reserved_stock')
    quantity = models.PositiveSmallIntegerField(
        _("Stock"), help_text=_("Reserved stock for the product."))
    reserved_at = models.DateTimeField(_('Product Reserved time'))

    def __unicode__(self):
        return '%s reserved with %s quantity at %s' % (self.product,
                                                       self.quantity,
                                                       self.reserved_at)


class OrderManager(models.Manager):

    def create_after_payment(self, payment_transaction, payment_attempt):
        extra = payment_transaction.extra
        product = Product.objects.get(pk=extra.get('product'))
        quantity = extra.get('quantity')

        # delete reserved stock and update sold value
        reserved_id = extra.get('reserved_id')
        ReservedStock.objects.filter(pk=reserved_id).delete()

        if product.sold:
            product.sold += int(quantity)
        else:
            product.sold = int(quantity)
        product.save()
        try:
            kwargs = {
                'product': product,
                'product_name': product.name,
                'quantity': int(quantity),
                'total': payment_attempt.total,
                'payment_transaction': payment_transaction
            }
            return self.create(**kwargs)
        except Exception as e:
            logger.exception('Order creation error %s' % e)
        pass


class Order(TimeStampedModel):
    """
    As of now users will be able to order one productz
    at a time, that means the order don't have to
    """
    number = models.CharField(_("Order number"), max_length=128, db_index=True,
                              unique=True)
    product = models.ForeignKey(Product, verbose_name=_("Product"), null=True,
                                blank=True)
    product_name = models.CharField(_("Product name"), max_length=128)
    quantity = models.PositiveSmallIntegerField(_("Quantity"))
    total = models.CharField(_("Total"), max_length=16)
    payment_transaction = models.OneToOneField('gateway.PaymentTransaction',
                                               verbose_name=_("Payment transaction"))  # Create a new type of PaymentTransaction called Catalogue. It resembles a lot with Customer Payment Model
    objects = OrderManager()

    def __unicode__(self):
        return '%s - %s' % (self.number, self.product_name)

    def save(self, *args, **kwargs):
        super(Order, self).save(*args, **kwargs)
        if not self.number:
            self.number = encode(self.pk)
            kwargs['force_insert'] = False
            super(Order, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _("Order")
        verbose_name_plural = _("Orders")


@receiver(payment_acknowledged)
def place_order(sender, **kwargs):
    """
    :param sender: PaymentTransaction class
    :param kwargs: request & payment transaction instance
    """
    payment_transaction = kwargs['instance']
    payment_attempt = kwargs['payment_attempt']

    if not payment_transaction.is_catalogue():
        return

    Order.objects.create_after_payment(payment_transaction, payment_attempt)
