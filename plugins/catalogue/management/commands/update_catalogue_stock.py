# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.core.management.base import BaseCommand
from django.utils import timezone

from plugins.catalogue.models import ReservedStock


class Command(BaseCommand):
    help = "update stocks of products if product was reserved more " \
           "than 10 minutes ago"

    def handle(self, **options):
        """
        Checks if reserved stocks are older than 10 minutes to current time
        or not.
        If they are then restore the stock and delete the ReservedStock Obj
        """
        reserved_stocks = ReservedStock.objects.all()
        for rstock in reserved_stocks:
            reserved_at = rstock.reserved_at
            time_delta = timezone.now() - reserved_at
            if time_delta.total_seconds() > 600:
                rstock.product.stock += rstock.quantity
                rstock.product.save()
                rstock.delete()
