# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging

from django.http import HttpResponseNotFound
from django.views import generic
from django.utils.translation import ugettext_lazy as _, force_text as _ft
from django.http import JsonResponse
from django.template.loader import render_to_string

from rest_framework import status

from api.func import get_knpay_url
from config.models import Config
from utils.views.mixins import PageTitleMixin
from plugins.mixins import PluginInstallMixin
from plugins.defaults import CATALOGUE

from .models import Product, CatalogueConfig
from .forms import CatalogueForm

logger = logging.getLogger('catalogue')


class ProductDetailView(PluginInstallMixin, PageTitleMixin, generic.DetailView, generic.FormView):
    """
    Product details + checkout page
    """
    slug_field = 'slug'
    slug_url_kwarg = 'slug'
    template_name = 'catalogue/product_detail.html'
    queryset = Product.objects.active()
    page_title = _("Pay")
    form_class = CatalogueForm
    plugin_name = CATALOGUE

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(ProductDetailView, self).dispatch(request, *args, **kwargs)

    def get_template_names(self):
        config = Config.get_solo()
        if config.is_paused:
            return ['ui/paused_new.html']
        return [self.template_name]

    def get_form_kwargs(self):
        kwargs = super(ProductDetailView, self).get_form_kwargs()
        kwargs.update({
            'request': self.request,
            'config': CatalogueConfig.get_solo(),
            'product': self.object
        })
        return kwargs

    def form_invalid(self, form):
        data = {'errors': form.errors_as_dict()}
        return JsonResponse(data, status=status.HTTP_400_BAD_REQUEST)

    def form_valid(self, form):
        _status = status.HTTP_400_BAD_REQUEST
        try:
            # url can be None also here and in Exception
            payment_obj = form.save()
            # reserve product quantity
            payment_obj.reserve_product()
            url = payment_obj.get_payment_url()
        except Exception as e:
            logger.exception("Following error occurred while trying "
                             "to generate a payment link %s" % e)
            url = None

        if url is not None:
            _status = status.HTTP_200_OK
            data = {
                'message': _ft(_("You are being redirected to payment page...")),
                'redirect_url': url
            }
        else:
            data = {
                'errors': {
                    '__all__': _ft(_("There was an error while connecting to payment server. "
                                     "Please try again later."))
                }
            }
        return JsonResponse(data, status=_status)

    def get_context_data(self, **kwargs):
        context = super(ProductDetailView, self).get_context_data(**kwargs)
        context.update(self.object.get_prices_for_gateways())
        context.update({'qty_available': self.object.is_available})
        return context


class QuantityUpdateView(PluginInstallMixin, generic.DetailView):
    """
    View to update the product quantity
    and return updated prices as html snippet
    to be replaced in the DOM
    """
    slug_field = 'slug'
    slug_url_kwarg = 'slug'
    template_name = 'catalogue/partials/product_prices.html'
    queryset = Product.objects.active()
    http_method_names = ['get']
    plugin_name = CATALOGUE

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        quantity = kwargs.get('quantity')
        error_payload = dict(message=_ft(_(
            'There was an error updating quantity. Please Try again later.')))

        if not request.is_ajax() or quantity is None or quantity == '0':
            logger.exception(
                'Quantity was not provided for product %s' % self.object.name)
            return JsonResponse(error_payload, status=400)

        if not self.object.is_available:
            error_payload = dict(message=_ft(
                _('Product is not available now')))
            return JsonResponse(error_payload, status=400)

        if int(quantity) > self.object.stock:
            if self.object.max_quantity is not None and self.object.max_quantity < self.object.stock:
                error_payload = dict(message=_ft(_(
                    'You cannot order more than %s QTY per order' % self.object.max_quantity or self.object.stock)))
                return JsonResponse(error_payload, status=400)
            else:
                error_payload = dict(message=_ft(_(
                    'You cannot order more than %s QTY for now' % self.object.stock)))
                return JsonResponse(error_payload, status=400)
        if self.object.max_quantity is not None and int(quantity) > self.object.max_quantity:
            error_payload = dict(message=_ft(_(
                'You cannot order more than %s QTY per order' % self.object.max_quantity or self.object.stock)))
            return JsonResponse(error_payload, status=400)

        try:
            data = self.object.get_prices_for_gateways(quantity=int(quantity))
        except Exception as e:
            logger.exception('Error while updating product quantity %s' % e)
            return JsonResponse(error_payload, status=400)
        product_prices_html = render_to_string(self.template_name, data)

        data['success_message'] = _('Quantity Updated successfully.')
        data['product_prices_html'] = product_prices_html
        return JsonResponse(data, status=200)


class ProductListView(PageTitleMixin, generic.ListView):
    """
    Listing all catalogue products.
    """
    template_name = 'catalogue/product_list.html'
    page_title = _("Product")
    model = Product
    context_object_name = 'products'
    queryset = Product.objects.all()

