# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django import template

from ..models import Product

register = template.Library()


@register.simple_tag
def get_product_url(product_id):
    try:
        product_obj = Product.objects.get(pk=product_id)
        return product_obj.payment_link()
    except Product.DoesNotExist:
        return ''
