# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^product-detail/(?P<slug>[\w-]+)/$',
        views.ProductDetailView.as_view(), name='product_detail'),
    url(r'^product-quantity-update/(?P<slug>[\w-]+)/(?P<quantity>\d+)/$',
        views.QuantityUpdateView.as_view(), name='product_quantity_update'),
    url(r'^product-list/$',views.ProductListView.as_view(),name='product_list'),
]
