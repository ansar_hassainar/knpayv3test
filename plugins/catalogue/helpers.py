# -*- coding: utf-8 -*-

from __future__ import unicode_literals


from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Div


def catalogue_payment_form_helper(form, **kwargs):
    helper = FormHelper()
    helper.layout = Layout()
    for key, value in kwargs.items():
        setattr(helper, key, value)
    for index, field in enumerate(form.fields):
        helper.layout.insert(index, field)
    return helper
