# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.test import TestCase, RequestFactory
import pytest
from mixer.backend.django import mixer

from utils.tests import knet_settings_factory

pytestmark = pytest.mark.django_db

# ToDO
# create product
# quantity can be Empty

# Create catalogueForm
# check quantity is set to 1 by default
# Try to update quantity
# check product's quantity has a limit or not
# If has some limit then can not update, or else all good Infinite

# place order
# transaction object is created
# quantity decreases


class TestCreatePaymentRequestForm(TestCase):
    def setUp(self):
        from .forms import CatalogueForm
        self.form_class = CatalogueForm
        self.form_config = mixer.blend('catalogue.CatalogueConfig')
        self.kwd_currency = mixer.blend('currency.Currency', code='KWD')
        self.exchange_config = mixer.blend('currency.ExchangeConfig',
                                           default_currency=self.kwd_currency,
                                           fee_type='', name='default')

    def test_product(self):
        product1 = mixer.blend('catalogue.Product', type='product',
                               name='test product', base_price='10.99',
                               currency=self.kwd_currency)
        assert product1.sold == 0, 'New product will have default 0 sold'
        assert product1.slug == 'test-product', 'Check slug is auto created'

    def test_place_order(self):
        pass

    def test_catalogue_form(self):
        product1 = mixer.blend('catalogue.Product', type='product',
                               name='test product', base_price='10',
                               currency=self.kwd_currency)
        gateway_settings = knet_settings_factory(self.kwd_currency)
        customer_email = 'foo@example.com'
        data = {'amount': 20,
                'currency_code': gateway_settings.currency_config.default_currency.code,
                'language': settings.LANGUAGE_CODE,
                'customer_email': customer_email,
                'product': product1,
                'quantity': 2,
                'gateway_code': gateway_settings.code,
                }
        request = RequestFactory().get('/')
        user = mixer.blend('auth.User', is_superuser=True)
        request.user = user
        form = self.form_class(config=self.form_config, data=data, product=product1,
                               request=request)
        # pytest.set_trace()
        # assert form.is_valid()
        # assert form.extra.get('quantity') == 2
        pass
