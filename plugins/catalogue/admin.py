# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from solo.admin import SingletonModelAdmin
from modeltranslation.admin import TabbedDjangoJqueryTranslationAdmin

from gateway.models import GatewaySettings
from extras.dashboard.admin import FieldInline as CoreFieldInline
from .models import CatalogueConfig, Product, Order, ReservedStock, ProductImage


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'number', 'product', 'quantity',
                    'payment_transaction')
    search_fields = ('id', 'number', 'product_name')


class ProductImageInline(admin.TabularInline):
    model = ProductImage
    extra = 0
    max_num = 5


@admin.register(Product)
class ProductAdmin(TabbedDjangoJqueryTranslationAdmin):
    inlines = [ProductImageInline]
    list_display = ('id', 'type', 'name', 'sold', 'stock', 'admin_thumbnail',
                    'is_active', 'payment_link', 'created')
    list_filter = ('type', 'is_active')
    search_fields = ('name',)
    fieldsets = (
        (None, {
            'fields': ('type', 'name', 'slug', 'description', 'original_price',
                       'base_price', 'currency', 'gateway_settings', 'start_date', 'end_date',
                       'model_number', 'terms', 'terms_text')
        }),
        (_("Stock"), {
            'fields': ('stock', 'display_stock', 'max_quantity',)
        }),
        (_("Delivery"), {
            'fields': ('add_delivery_charges', 'delivery_charges',)
        }),
        (_("Communication"), {
            'fields': ('contact_number', 'youtube_link',)
        }),
    )

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == "gateway_settings":
            kwargs["queryset"] = GatewaySettings.objects.active()
        return super(ProductAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    class Media:
        js = ("js/admin/lightbox-plus-jquery.min.js",
              "js/admin/delivery-charge.js",)
        css = {'': ("css/admin/lightbox.min.css",)}


class FieldInline(CoreFieldInline):
    extra = 2


@admin.register(CatalogueConfig)
class CatalogueFormConfigAdmin(SingletonModelAdmin):
    readonly_fields = ['url']
    inlines = [FieldInline]
    fieldsets = (
        (None, {
            'fields': ('default_email', 'currency',
                       'sms_payment_details', 'url')
        }),
        (_("Communication"), {
            'fields': ('email_payment_details_success_template',
                       'email_payment_details_failure_template',
                       'sms_payment_details_success_template',
                       'sms_payment_details_failure_template',
                       )
        }),
    )


@admin.register(ReservedStock)
class ReservedStockAdmin(admin.ModelAdmin):
    list_display = ('id', 'product', 'quantity', 'reserved_at')
    list_filter = ('reserved_at',)
