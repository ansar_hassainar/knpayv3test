# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from modeltranslation.translator import translator, TranslationOptions

from .models import Product


class ProductOptions(TranslationOptions):
    fields = ('name', 'description')


translator.register(Product, ProductOptions)
