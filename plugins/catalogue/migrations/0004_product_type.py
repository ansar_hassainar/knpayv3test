# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-01-30 05:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0003_auto_20180118_1026'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='base_price',
            field=models.DecimalField(decimal_places=3, max_digits=8,
                                      verbose_name='Price'),
        ),
        migrations.AddField(
            model_name='product',
            name='type',
            field=models.CharField(choices=[('product', 'Product'), ('service', 'Service')], default='product', max_length=20, verbose_name='Product Type'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='sold',
            field=models.PositiveSmallIntegerField(blank=True,
                                                   help_text='Count based on the success payments',
                                                   null=True,
                                                   verbose_name='Total Sold'),
        ),

    ]
