# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-01-30 06:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0004_product_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='sold',
            field=models.PositiveIntegerField(blank=True, help_text='Count based on the success payments', null=True, verbose_name='Total Sold'),
        ),
    ]
