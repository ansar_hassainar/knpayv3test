# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

from django import forms
from django.utils.translation import ugettext_lazy as _

from intl_tel_input.widgets import IntlTelInputWidget

from .models import CatalogueConfig, Product
from .helpers import catalogue_payment_form_helper
from utils.forms import AbstractBasePaymentRequestForm
from gateway.models import PaymentTransaction

logger = logging.getLogger('catalogue')


class CatalogueForm(AbstractBasePaymentRequestForm):
    """
    Allow user to pay for the product/service
    """
    transaction_type = PaymentTransaction.CATALOGUE
    knpay_section = PaymentTransaction.CUSTOMER_PAYMENT
    currency_code = forms.CharField(disabled=True)
    check_permission = False
    blank_gateway = False

    field_order = ('amount', 'customer_email',)

    def __init__(self, product, *args, **kwargs):
        super(CatalogueForm, self).__init__(*args, **kwargs)
        self.product = product
        del self.fields['language']
        del self.fields['amount']
        del self.fields['gateway_code']

        self.fields['currency_code'].initial = product.currency.code
        for field in self.config.fields.active():
            self.add_defined_field(field.get_name(), label=field.get_label(),
                                   required=field.required,
                                   field=field)
        if 'customer_phone' in self.fields:
            self.fields['customer_phone'].widget = IntlTelInputWidget(default_code='kw',
                                                                      preferred_countries=['kw', 'ro', 'in'])

        # customer can pay without email
        catalogue_config = CatalogueConfig.get_solo()
        # if the default email is empty then the customer should insert the email
        if catalogue_config.default_email is not None:
            self.fields['customer_email'].required = False
        else:
            self.fields['customer_email'].required = True
        self.helper = catalogue_payment_form_helper(self)
        self.helper.include_media = False

    def save(self):
        return self.serializer.save()

    def get_data_for_api(self):
        data = super(CatalogueForm, self).get_data_for_api()
        if not data.get('extra', None):
            data['extra'] = {}
        data['extra'].update({'product': data['product'],
                              'quantity': data['quantity'],
                              'product_name': data['product_name'],
                              'product_type': data['product_type']
                              })
        # if customer_email is populated from the default_email then set flag
        # in extra data
        catalogue_config = CatalogueConfig.get_solo()
        if data['customer_email'] == catalogue_config.default_email:
            data['extra'].update({'has_default_email': 'true'})

        return data

    def clean(self):
        # check product is available or not when user clicks on PAY button
        if not self.product.is_available:
            if self.product.type == Product.PRODUCT:
                message = _('OOPS! Out of Stock, Check back soon.')
            else:
                message = _("OOPS! Service unavailable.")

            raise forms.ValidationError(message)

        # non-field data, required for extra field
        self.cleaned_data['product'] = self.product.pk
        self.cleaned_data['product_name'] = self.product.name
        self.cleaned_data['product_type'] = self.product.type
        self.cleaned_data['quantity'] = self.request.POST.get('quantity')
        self.cleaned_data['amount'] = self.request.POST.get('amount')
        self.cleaned_data['gateway_code'] = self.request.POST.get('gateway_code')

        # If there is no default_email and customer_email we will require the email
        # else we will take the default_email if there is no customer_email
        catalogue_config = CatalogueConfig.get_solo()
        if catalogue_config.default_email is None:
            if self.cleaned_data.get('customer_email',"") == "":
                raise forms.ValidationError("Email field is required.")
        elif self.cleaned_data.get('customer_email',"") == "":
            self.cleaned_data['customer_email'] = catalogue_config.default_email
        return super(CatalogueForm, self).clean()
