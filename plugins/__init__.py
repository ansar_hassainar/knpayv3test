# -*- coding: utf-8 -*-

from __future__ import unicode_literals

def plugin_is_installed(plugin_name):
    from plugins.models import Plugin, InstalledPlugins
    try:
        plugin = Plugin.objects.get(name=plugin_name)
    except Plugin.DoesNotExist:
        return False
    return plugin in InstalledPlugins.get_solo().plugins.all()