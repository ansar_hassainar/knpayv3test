# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from solo.admin import SingletonModelAdmin

from .models import ShopifyConfig, ShopifyTransaction


class ConfigForm(forms.ModelForm):
    class Meta:
        fields = "__all__"
        widgets = {
            'key': forms.PasswordInput(render_value=True),
        }


@admin.register(ShopifyConfig)
class ShopifyConfigAdmin(SingletonModelAdmin):
    form = ConfigForm
    fieldsets = (
        (None, {
            'fields': ('key',)
        }),
        (_("Communication"), {
            'fields': ('email_payment_details_success_template',
                       'email_payment_details_failure_template',
                       )
        }),
    )


@admin.register(ShopifyTransaction)
class ShopifyTransactionAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'modified')
    fields = ('reference', 'created', 'modified', 'shopify_payload')
    list_display = ('reference', 'created', 'modified')
    list_filter = ('created', 'modified')

    def has_add_permission(self, request):
        return False

    def get_form(self, request, obj=None, **kwargs):
        form = super(ShopifyTransactionAdmin, self).get_form(request, obj=obj, **kwargs)
        form.base_fields['reference'].disabled = True
        return form

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_add_another'] = False
        extra_context['show_save_and_continue'] = False
        extra_context['show_save'] = False
        return super(ShopifyTransactionAdmin, self).change_view(request, object_id,
                                                                form_url, extra_context=extra_context)

