# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django import forms

from utils.forms import GatewayForm, AjaxBaseForm, AttemptURLMixin
from extras.ui.helpers import gateway_helper_factory
from gateway.models import PaymentTransaction


class SelectGatewayForm(AjaxBaseForm,
                        GatewayForm,
                        AttemptURLMixin,
                        forms.ModelForm):
    """
    Form where user will select the gateway to proceed with
    """
    blank_gateway = False
    check_permission = False
    knpay_section = PaymentTransaction.THIRD_PARTY

    class Meta:
        fields = 'gateway_code',
        model = PaymentTransaction

    def __init__(self, request, *args, **kwargs):
        self.request = request
        super(SelectGatewayForm, self).__init__(*args, **kwargs)
        self.helper = gateway_helper_factory(self, form_id='select-gateway')
