# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging
from random import randint

from django.db import transaction
from django.core.urlresolvers import reverse
from django.http import JsonResponse, HttpResponseRedirect
from django.views import generic
from django.views.generic.detail import SingleObjectMixin, SingleObjectTemplateResponseMixin
from django.utils.translation import ugettext as _
from django.utils.decorators import method_decorator

from braces.views import CsrfExemptMixin
from rest_framework import status

from api.func import initialize_transaction
from gateway.models import GatewaySettings
from gateway.serializers import ShopifyTransactionSerializer, PaymentTransaction
from utils.views.mixins import PageTitleMixin, EncryptedPKMixin
from .forms import SelectGatewayForm
from .models import ShopifyTransaction, sign
from config.models import MerchantDetails

logger = logging.getLogger('shopify')


@method_decorator(transaction.atomic, name='post')
class DispatchView(CsrfExemptMixin, generic.View):
    """
    Check if there is one or multiple gateways configured.
    If only one, redirect to payment page,
    else redirect to select gateway view
    """
    http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        shopify_payload = dict(request.POST.items())
        reference = shopify_payload['x_reference']
        signed = sign(shopify_payload).lower() == shopify_payload.get('x_signature')
        cancel_url = shopify_payload.get('x_url_cancel')

        if not signed:
            logger.info("Transaction %s not signed at incoming." % reference)
            return JsonResponse({'redirect_url': cancel_url})

        shopify_transaction, created = ShopifyTransaction.objects.get_or_create(reference=reference)
        shopify_transaction.shopify_payload = shopify_payload
        if created:
            # for Decision manager Cybersource
            shopify_transaction.items_sold = randint(1, 10)
        shopify_transaction.save()

        # delete PaymentTransaction if exist, it would be an unpaid one, so it doesn't matter
        # it's mandatory to be deleted, because Shopify doesn't change the x_reference variable
        # even if the cart value has been changed.

        PaymentTransaction.objects.filter(order_no=reference).delete()

        try:
            gateway_code = GatewaySettings.objects.active().get().code
        except GatewaySettings.MultipleObjectsReturned:
            gateway_code = ''

        serializer_data = self.prepare_serializer_data(shopify_payload, shopify_transaction.items_sold, gateway_code)

        payment_transaction, status = initialize_transaction(
            data=serializer_data, serializer_class=ShopifyTransactionSerializer)

        if not status:
            redirect_url = cancel_url
        else:
            # check if payment transaction is paid (maybe some error occurred earlier)
            # and redirect the customer to payment details.
            if payment_transaction.is_paid():
                path = payment_transaction.get_local_shopify_url()
            elif GatewaySettings.objects.active().count() > 1:
                path = reverse('shopify:select_gateway', args=(payment_transaction.encrypted_pk,))
            else:
                path = payment_transaction.get_payment_url()
            redirect_url = request.build_absolute_uri(path)
            logger.info('Shopify transaction redirect_url: %s' % redirect_url)
        return JsonResponse({'redirect_url': redirect_url})

    @staticmethod
    def prepare_serializer_data(shopify_payload, items_sold, gateway_code=''):
        data = {
            'gateway_code': gateway_code,
            'amount': shopify_payload.get('x_amount'),
            'order_no': shopify_payload.get('x_reference'),
            'currency_code': shopify_payload.get('x_currency'),
            'customer_address_line1': shopify_payload.get('x_customer_billing_address1', ''),
            'customer_address_line2': shopify_payload.get('x_customer_billing_address2', ''),
            'customer_address_city': shopify_payload.get('x_customer_billing_city', ''),
            'customer_address_state': shopify_payload.get('x_customer_billing_state', ''),
            'customer_address_postal_code': shopify_payload.get('x_customer_billing_zip', ''),
            'customer_email': shopify_payload.get('x_customer_email', ''),
            'customer_first_name': shopify_payload.get('x_customer_first_name', ''),
            'customer_last_name': shopify_payload.get('x_customer_last_name', ''),
            'customer_phone': shopify_payload.get('x_customer_phone	', ''),
            'extra': {
                'shipping_country': shopify_payload.get('x_customer_shipping_country', 'KW'),
                'product_description': shopify_payload.get('x_description', ''),
                'items_sold': items_sold,
            },
        }
        # remove empty fields, to not be sent to the serializer and
        # because they are empty, the serializer will throw an error.
        empty_ignored_fields = ['gateway_code']
        for key, value in data.items():
            if key not in empty_ignored_fields:
                if not value:
                    del data[key]
        return data


class SelectGatewayView(PageTitleMixin,
                        EncryptedPKMixin,
                        generic.UpdateView):
    """
    Select the
    """
    form_class = SelectGatewayForm
    queryset = PaymentTransaction.objects.third_parties().valid_for_payment()
    template_name = 'shopify/select_gateway.html'
    page_title = _("Select gateway you wish to use")
    model = PaymentTransaction

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.shopify_transaction = ShopifyTransaction.objects.get(reference=self.object.order_no)
        return super(SelectGatewayView, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(SelectGatewayView, self).get_form_kwargs()
        kwargs.update({
            'request': self.request,
        })
        return kwargs

    def form_valid(self, form):
        redirect_url = form.get_url()
        if redirect_url is not None:
            message = _("You are being redirected to payment page..."),
        else:
            message = _("There was an error while connecting to the gateway. "
                        "Please try again and if the problem persists contact "
                        "an administrator"),
            redirect_url = self.shopify_transaction.get_cancel_url()
        data = {'message': message, 'redirect_url': redirect_url}
        return JsonResponse(data)

    def form_invalid(self, form):
        data = {'errors': form.errors_as_dict()}
        return JsonResponse(data, status=status.HTTP_400_BAD_REQUEST)


@method_decorator(transaction.atomic, name='get')
class RedirectToShopify(PageTitleMixin,
                        EncryptedPKMixin,
                        SingleObjectMixin,
                        SingleObjectTemplateResponseMixin,
                        generic.View):
    """
    View for either showing payment details
    or for redirecting back to shopify.
    """
    queryset = PaymentTransaction.objects.third_parties()
    template_name = 'shopify/transaction_details.html'

    def get_page_title(self):
        return _("Transaction details for %s Order %s" % (MerchantDetails.get_solo().name,
                                                          self.shopify_transaction.get_reference()))

    def get(self, request, *args, **kwargs):
        payment_transaction = self.get_object()
        self.object = payment_transaction
        self.shopify_transaction = ShopifyTransaction.objects.get(reference=payment_transaction.order_no)
        self.shopify_transaction.disclose_to_shopify(payment_transaction)
        if payment_transaction.was_disclosed_to_merchant:
            shopify_url = self.shopify_transaction.get_complete_url()
        else:
            shopify_url = self.shopify_transaction.get_cancel_url()

        attempt = payment_transaction.get_latest_attempt()

        if attempt.is_knet():
            # if knet we must show the transaction details before redirect
            context = self.get_context_data(
                shopify_url=shopify_url,
                attempt=attempt
            )
            return self.render_to_response(context)
        else:
            return HttpResponseRedirect(shopify_url)
