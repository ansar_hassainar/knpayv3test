from __future__ import unicode_literals

import hmac
import hashlib
import logging
import requests
import time

from django.db import models
from django.utils.translation import ugettext_lazy as _

from django_extensions.db.models import TimeStampedModel
from jsonfield import JSONField
from solo.models import SingletonModel

from utils.db.models import EmailMailTemplate

logger = logging.getLogger('shopify')


class ShopifyConfig(EmailMailTemplate, SingletonModel):
    """
    Shopify configuration model
    """
    key = models.CharField(_('key'), max_length=64, default='')

    def __unicode__(self):
        return "secret auth"

    def extra_fields(self):
        return {}

    class Meta:
        verbose_name = _('Shopify config')
        verbose_name_plural = _('Shopify config')


def sign(payload):
    """
    Sign shopify payload (incoming & outgoing)
    """
    config = ShopifyConfig.get_solo()
    key = config.key
    message = [(k, payload[k]) for k in sorted(payload) if k.startswith('x_') and k != 'x_signature']
    message = ''.join(['%s%s' % (k, v) for (k, v) in message])
    digest = hmac.new(str(key), message.encode('utf-8'), digestmod=hashlib.sha256).hexdigest()
    return digest


class ShopifyTransaction(TimeStampedModel):
    """
    Shopify transaction log
    """
    COMPLETED, FAILED = 'completed', 'failed'

    reference = models.CharField(_('x_reference'), blank=True, default='', max_length=128)
    shopify_payload = JSONField(_("Shopify payload"), default={},
                                help_text=_("Payload sent by shopify for checkout"))
    items_sold = models.PositiveIntegerField(_('Number of Items Sold'),
                                             null=True)

    def __unicode__(self):
        return self.reference

    def get_complete_url(self):
        return self.shopify_payload['x_url_complete']

    def get_cancel_url(self):
        return self.shopify_payload['x_url_cancel']

    def get_reference(self):
        return self.shopify_payload['x_reference']

    def disclose_to_shopify(self, payment_transaction):
        """
        To be called only in case of complete flow payment
        # https://help.shopify.com/api/sdks/hosted-payment-sdk/api-reference/response-values
        """
        attempt = payment_transaction.get_latest_attempt()

        if attempt.disclosed_to_merchant:
            return

        if payment_transaction.is_paid():
            result = self.COMPLETED
        else:
            result = self.FAILED

        payload = {
            'x_account_id': self.shopify_payload['x_account_id'],
            'x_reference': self.reference,
            'x_currency': self.shopify_payload['x_currency'],
            'x_timestamp': time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime()),
            'x_test': self.shopify_payload['x_test'],
            'x_amount': self.shopify_payload['x_amount'],
            'x_gateway_reference': attempt.reference_number,
            'x_result': result
        }
        payload['x_signature'] = sign(payload)
        logger.info("Shopify payload for transaction %s %s" % (self.reference, payload))

        response = requests.post(self.shopify_payload['x_url_callback'], data=payload)
        logger.info("Shopify response %s" % response.content)

        if response.status_code == requests.codes.ok:
            payment_transaction.disclosed_to_merchant = True
            payment_transaction.save()
            attempt.disclosed_to_merchant = True
            attempt.save()
