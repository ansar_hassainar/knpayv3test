# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.DispatchView.as_view(), name='dispatch'),
    url(r'^gwc/(?P<encrypted_pk>.+)/$',
        views.SelectGatewayView.as_view(), name='select_gateway'),
    url(r'^rbts/(?P<encrypted_pk>.+)/$',
        views.RedirectToShopify.as_view(), name='redirect_to_shopify'),
]
