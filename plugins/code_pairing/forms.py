# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.utils.translation import ugettext as _

from utils.forms import AjaxBaseForm
from .models import PluginConfig, Code


class CodeForm(AjaxBaseForm, forms.Form):
    code = forms.CharField(label=_("Code"))

    def __init__(self, *args, **kwargs):
        super(CodeForm, self).__init__(*args, **kwargs)
        config = PluginConfig.get_solo()
        field = config.mandatory_field

        if field is not None:
            self.fields[field.get_name()] = forms.CharField(label=field.get_label())
            self.field = field

            setattr(self, 'clean_%s' %
                    field.get_name(), self.clean_mandatory_field)

    def clean_code(self):
        try:
            return Code.objects.get(code=self.cleaned_data['code'].strip())
        except Code.DoesNotExist:
            self.add_error('code', _("Invalid code"))

    def clean_mandatory_field(self):
        field = self.field
        cleaned_data = self.cleaned_data
        try:
            code = self.cleaned_data['code']
        except KeyError:
            return

        if code is not None:
            if field.is_builtin():
                default_val = getattr(code.transaction, field.get_name(), None)
            else:
                default_val = code.transaction.extra.get(field.get_name())

            if default_val is not None:
                field_val = cleaned_data[field.get_name()].strip()
                if field_val != default_val:
                    self.add_error(field.get_name(),
                                   _("%s's value doesn't match with the associated code.") % field.get_label())
