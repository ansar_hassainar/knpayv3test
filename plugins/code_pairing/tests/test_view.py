# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.contenttypes.models import ContentType
from django.test import TestCase
from plugins import plugin_is_installed
from plugins.mixins import PluginInstallMixin
from plugins.models import InstalledPlugins, Plugin
from plugins.code_pairing.models import Code, PluginConfig
from django.test import Client
from rest_framework import status
from plugins.defaults import CODE_PAIRING


class TestView(TestCase):

    @classmethod
    def setUp(self):
        try:
            self.plugin = Plugin.objects.get(name=CODE_PAIRING)
        except Plugin.DoesNotExist:
            self.plugin = Plugin.objects.create(name=CODE_PAIRING)

        if not plugin_is_installed(CODE_PAIRING):
            self.installed_plugins = InstalledPlugins()
            self.installed_plugins.save()
            self.installed_plugins.plugins.add(self.plugin)

    def test_index_view_response_404_with_not_added_nor_installed_plugin(self):
        Plugin.objects.all().delete()
        InstalledPlugins.objects.all().delete()
        response = self.client.get('/cpr/', follow=True)
        assert response.status_code == status.HTTP_404_NOT_FOUND, 'View should response with 400'

    def test_index_view_response_200_wiht_added_and_installed_plugin_with_no_forced_referrers(self):
        response = self.client.get('/cpr/', follow=True)
        assert response.status_code == status.HTTP_200_OK, 'View should response with 200'

    def test_index_view_response_200_wiht_added_and_installed_plugin_with_no_forced_referrers_but_only_space(self):
        plugin_config = PluginConfig.objects.create(forced_referers=" ")
        response = self.client.get('/cpr/', follow=True)
        assert response.status_code == status.HTTP_200_OK, 'View should response with 200'

    def test_index_view_response_200_wiht_added_and_installed_plugin_with_one_forced_referrers_and_correct_referrer(self):
        plugin_config = PluginConfig.objects.create(
            forced_referers="http://foo/bar")
        response = self.client.get(
            '/cpr/', {}, HTTP_REFERER='http://foo/bar', follow=True)
        assert response.status_code == status.HTTP_200_OK, 'View should response with 200'

    def test_index_view_response_403_wiht_added_and_installed_plugin_with_one_forced_referrers_and_wrong_referrer(self):
        plugin_config = PluginConfig.objects.create(
            forced_referers="http://foo/bar")
        response = self.client.get(
            '/cpr/', {}, HTTP_REFERER='http://fake/fake', follow=True)
        assert response.status_code == status.HTTP_403_FORBIDDEN, 'View should response with 403'

    def test_index_view_response_200_wiht_added_and_installed_plugin_with_one_forced_referrers_with_spaces_and_correct_referrer(self):
        plugin_config = PluginConfig.objects.create(
            forced_referers=" http://foo/bar ")
        response = self.client.get(
            '/cpr/', {}, HTTP_REFERER='http://foo/bar', follow=True)
        assert response.status_code == status.HTTP_200_OK, 'View should response with 200'

    def test_index_view_response_200_wiht_added_and_installed_plugin_with_multiple_forced_referrers_and_correct_referrer(self):
        plugin_config = PluginConfig.objects.create(
            forced_referers="http://foo/bar,http://foo1/bar1")
        response = self.client.get(
            '/cpr/', {}, HTTP_REFERER='http://foo/bar', follow=True)
        assert response.status_code == status.HTTP_200_OK, 'View should response with 200'

    def test_index_view_response_403_wiht_added_and_installed_plugin_with_multiple_forced_referrers_and_wrong_referrer(self):
        plugin_config = PluginConfig.objects.create(
            forced_referers="http://foo/bar,http://foo1/bar1")
        response = self.client.get(
            '/cpr/', {}, HTTP_REFERER='http://fake/fake', follow=True)
        assert response.status_code == status.HTTP_403_FORBIDDEN, 'View should response with 403'

    def test_index_view_response_200_wiht_added_and_installed_plugin_with_multiple_forced_referrers_with_spaces_and_correct_referrer(self):
        plugin_config = PluginConfig.objects.create(
            forced_referers=" http://foo/bar , http://foo1/bar1 ")
        response = self.client.get(
            '/cpr/', {}, HTTP_REFERER='http://foo/bar', follow=True)
        assert response.status_code == status.HTTP_200_OK, 'View should response with 200'