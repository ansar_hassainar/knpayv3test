# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.contenttypes.models import ContentType
from django.test import TestCase
from gateway.models import PaymentTransaction
from plugins import plugin_is_installed
from plugins.defaults import CODE_PAIRING
from plugins.models import InstalledPlugins, Plugin
from plugins.code_pairing.models import Code, PluginConfig
from plugins.code_pairing.forms import CodeForm
from django.test import Client
from django.db.models.signals import post_save
from extras.dashboard.models import Field
import random


class TestForms(TestCase):

    fixtures = ['fixtures/plugins.json']

    @classmethod
    def setUpTestData(cls):
        cls.builtin_field_type = 'builtin'
        # name must be member at the paymenttransaction
        cls.builtin_field_name = 'customer_phone'
        cls.builtin_field_value = '0100'
        cls.custom_field_type = 'custom'
        cls.custom_field_value = '0200'
        cls.custom_field_name = 'mandatory_field'
        cls.plugin = None
        cls.installed_plugins = None
        # Select ContentType so we can build relation between the feild model and the dashboard app
        cls.content_type = ContentType.objects.get(
            app_label='dashboard', model='field')
        #this feild will be used for the custom_field test cases
        cls.custom_field = Field.objects.create(
            type=cls.custom_field_type,
            content_type=cls.content_type,
            object_id=1,
            field="mandatory_field",
            label="mandatory_field",
            name="mandatory_field",
        )
        #this feild will be used for the builtin_field test cases
        cls.builtin_field = Field.objects.create(
            type=cls.builtin_field_type,
            content_type=cls.content_type,
            object_id=1,
            field=cls.builtin_field_name,
            label=cls.builtin_field_name,
            name=cls.builtin_field_name,
        )
        #Install the plugin so we can test the senario
        try:
            cls.plugin = Plugin.objects.get(name=CODE_PAIRING)
        except Plugin.DoesNotExist:
            cls.plugin = Plugin.objects.create(name=CODE_PAIRING)

        if not plugin_is_installed(CODE_PAIRING):
            cls.installed_plugins = InstalledPlugins()
            cls.installed_plugins.save()
            cls.installed_plugins.plugins.add(cls.plugin)

        #Set the plugin configurations feild, we will update that at the first of each test case
        cls.plugin_config = PluginConfig.objects.create(
            mandatory_field=cls.custom_field)

        #transaction releated with builtin feild
        cls.trans_with_builtin_field = PaymentTransaction.objects.create(
            state=PaymentTransaction.CREATED,
            type=PaymentTransaction.PAYMENT_REQUEST,
            amount=100,
            language="1",
            currency_code="1",
            customer_phone=cls.builtin_field_value
        )
        #transaction releated with custom feild
        cls.trans_with_custom_mandatory_field = PaymentTransaction.objects.create(
            state=PaymentTransaction.CREATED,
            type=PaymentTransaction.PAYMENT_REQUEST,
            amount=100,
            language="1",
            currency_code="1"
        )
        cls.trans_with_custom_mandatory_field.extra = {
            cls.custom_field_name: cls.custom_field_value}
        cls.trans_with_custom_mandatory_field.save()

    def test_form_is_valid_with_correct_code_and_with_correct_custom_mandatory_field(self):
        self.plugin_config.mandatory_field = self.custom_field
        self.plugin_config.save()
        self.code = Code.objects.get(
            transaction=self.trans_with_custom_mandatory_field)
        form_data = {'code': self.code,
                     self.custom_field_name: self.custom_field_value}
        form = CodeForm(data=form_data)
        assert form.is_valid() is True, 'Form should be valid'

    def test_form_is_not_valid_with_correct_code_and_without_custom_mandatory_field(self):
        self.plugin_config.mandatory_field = self.custom_field
        self.plugin_config.save()
        self.code = Code.objects.get(
            transaction=self.trans_with_custom_mandatory_field)
        form_data = {'code': self.code, self.custom_field_name: ""}
        form = CodeForm(data=form_data)
        assert form.is_valid() is False, 'Form should not be valid'

    def test_form_is_not_valid_without_code_with_correct_custom_mandatory_field(self):
        self.plugin_config.mandatory_field = self.custom_field
        self.plugin_config.save()
        self.code = Code.objects.get(
            transaction=self.trans_with_custom_mandatory_field)
        form_data = {'code': "",
                     self.custom_field_name: self.custom_field_value}
        form = CodeForm(data=form_data)
        assert form.is_valid() is False, 'Form should not be valid'

    def test_form_is_not_valid_without_code_without_custom_mandatory_field(self):
        self.plugin_config.mandatory_field = self.custom_field
        self.plugin_config.save()
        self.code = Code.objects.get(
            transaction=self.trans_with_custom_mandatory_field)
        form_data = {'code': "", self.custom_field_name: ""}
        form = CodeForm(data=form_data)
        assert form.is_valid() is False, 'Form should not be valid'

    def test_form_is_not_valid_with_wrong_code_with_correct_custom_mandatory_field(self):
        self.plugin_config.mandatory_field = self.custom_field
        self.plugin_config.save()
        self.code = Code.objects.get(
            transaction=self.trans_with_custom_mandatory_field)
        form_data = {'code': "Some thing Wrong",
                     self.custom_field_name: self.custom_field_value}
        form = CodeForm(data=form_data)
        assert form.is_valid() is False, 'Form should not be valid'

    def test_form_is_not_valid_with_correct_code_and_with_wrong_custom_mandatory_field(self):
        self.plugin_config.mandatory_field = self.custom_field
        self.plugin_config.save()
        self.code = Code.objects.get(
            transaction=self.trans_with_custom_mandatory_field)
        form_data = {'code': self.code,
                     self.custom_field_name: "Something Wrong"}
        form = CodeForm(data=form_data)
        print(form.errors)
        assert form.is_valid() is False, 'Form should not be valid'

    def test_form_is_valid_with_correct_code_and_with_correct_builtin_field(self):
        self.plugin_config.mandatory_field = self.builtin_field
        self.plugin_config.save()
        self.code = Code.objects.get(
            transaction=self.trans_with_builtin_field)
        form_data = {'code': self.code,
                     self.builtin_field_name: self.builtin_field_value}
        form = CodeForm(data=form_data)
        print(form.errors)
        assert form.is_valid() is True, 'Form should be valid'

    def test_form_is_not_valid_with_correct_code_and_with_wrong_builtin_field(self):
        self.plugin_config.mandatory_field = self.builtin_field
        self.plugin_config.save()
        self.code = Code.objects.get(
            transaction=self.trans_with_builtin_field)
        form_data = {'code': self.code,
                     self.builtin_field_name: "Something Wrong"}
        form = CodeForm(data=form_data)
        print(form.errors)
        assert form.is_valid() is False, 'Form should not be valid'

    def test_form_is_valid_with_correct_code_and_with_correct_custom_mandatory_field_but_with_spaces(self):
        self.plugin_config.mandatory_field = self.custom_field
        self.plugin_config.save()
        self.code = Code.objects.get(
            transaction=self.trans_with_custom_mandatory_field)
        form_data = {'code':  ' %s ' % (
            self.code.code), self.custom_field_name: ' %s ' % (self.custom_field_value)}
        form = CodeForm(data=form_data)
        assert form.is_valid() is True, 'Form should be valid'

    def test_form_is_valid_with_correct_code_and_with_correct_builtin_field_but_with_spaces(self):
        self.plugin_config.mandatory_field = self.builtin_field
        self.plugin_config.save()
        self.code = Code.objects.get(
            transaction=self.trans_with_builtin_field)
        form_data = {'code':  ' %s ' % (
            self.code.code), self.builtin_field_name: ' %s ' % (self.builtin_field_value)}
        form = CodeForm(data=form_data)
        print(form.errors)
        assert form.is_valid() is True, 'Form should be valid'
