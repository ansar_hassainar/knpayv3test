# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _


class CodePairingConfig(AppConfig):
    name = 'plugins.code_pairing'
    verbose_name = _("Code pairing plugin")

    def ready(self):
        pass
