# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import random
import types
import string

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from django_extensions.db.models import TimeStampedModel
from solo.models import SingletonModel

from gateway.models import PaymentTransaction
from plugins import plugin_is_installed
from plugins.defaults import CODE_PAIRING


class PluginConfig(SingletonModel):
    mandatory_field = models.OneToOneField(
        'dashboard.Field', verbose_name=_("Mandatory field"), null=True, blank=True,
        help_text=_("Dashboard field. If defined, will ask user to fill the field value before accessing "
                    "the payment url. Just make sure the field value is added to payment transaction by the "
                    "creator of it, either using dashboard, bulk upload or api, otherwise validation will fail.")
    )
    forced_referers = models.TextField(
        _("Forced referer"),
        blank=True, default='',
        help_text=_(
            "If defined, only users coming from one of these URLs will be able to browse the verification page, multiple urls can be added as comma separated.")
    )

    def __unicode__(self):
        return 'plugin config'

    class Meta:
        verbose_name = _("Plugin config")


class Code(TimeStampedModel):
    code = models.CharField(_("Code"), max_length=6, db_index=True)
    transaction = models.OneToOneField(PaymentTransaction,
                                       verbose_name=_("Transaction"), related_name='+')

    def __unicode__(self):
        return self.code

    class Meta:
        verbose_name = _("Code")
        verbose_name_plural = _("Codes")


@receiver(post_save, sender=PaymentTransaction)
def generate_code(sender, instance, **kwargs):
    if not plugin_is_installed(CODE_PAIRING):
        return

    if not kwargs.get('created'):
        return

    if not instance.is_payment_request():
        return

    default_ctx = instance._get_notification_context()
    code = ''.join(random.choice(string.ascii_uppercase + string.digits)
                   for _ in range(6))
    Code.objects.create(code=code, transaction=instance)

    def plugin_notification(self, ctx=None):
        if ctx is not None:
            default_ctx.update(ctx)
        default_ctx.update({'code': code})
        return default_ctx

    # monkey patch the method, to include the code in the sms mail context
    instance._get_notification_context = types.MethodType(
        plugin_notification, instance, PaymentTransaction)
