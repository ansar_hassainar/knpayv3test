# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import JsonResponse, HttpResponseForbidden, HttpResponseNotFound
from django.utils.translation import ugettext_lazy as _
from django.views import generic

from rest_framework import status

from utils.views.mixins import PageTitleMixin
from config.models import Config
from plugins.mixins import PluginInstallMixin
from plugins.defaults import CODE_PAIRING
from .forms import CodeForm
from .models import PluginConfig


class IndexView(PluginInstallMixin, PageTitleMixin, generic.FormView):
    template_name = 'plugins/code_pairing/index.html'
    form_class = CodeForm
    page_title = _("Code verification")
    plugin_name = CODE_PAIRING

    def dispatch(self, request, *args, **kwargs):
        plugin_conf = PluginConfig.get_solo()
        forced_referers = plugin_conf.forced_referers
        cleaned_forced_referers = forced_referers.replace(" ", "")
        if cleaned_forced_referers:
            forced_referers_list = cleaned_forced_referers.split(",")
            if len(forced_referers_list) > 0 and request.method == 'GET':
                referer = request.META.get('HTTP_REFERER')
                if referer not in forced_referers_list:
                    return HttpResponseForbidden()

        return super(IndexView, self).dispatch(request, *args, **kwargs)

    def get_template_names(self):
        # TODO: refactor all the views using the below code and move the logic to a middleware
        config = Config.get_solo()
        if config.is_paused:
            return ['ui/paused_new.html']
        return [self.template_name]

    def form_valid(self, form):
        data = {
            'redirect_url': form.cleaned_data['code'].transaction.get_payment_request_url()
        }
        return JsonResponse(data, status=status.HTTP_200_OK)

    def form_invalid(self, form):
        data = {'errors': form.errors_as_dict()}
        return JsonResponse(data, status=status.HTTP_400_BAD_REQUEST)
