# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from solo.admin import SingletonModelAdmin

from extras.dashboard.models import PaymentRequestFormConfig
from .models import PluginConfig


@admin.register(PluginConfig)
class PluginConfigAdmin(SingletonModelAdmin):

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'mandatory_field':
            config = PaymentRequestFormConfig.get_solo()
            kwargs['queryset'] = config.fields.all()
        return super(PluginConfigAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
