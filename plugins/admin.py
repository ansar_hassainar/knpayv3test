# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from solo.admin import SingletonModelAdmin

from .models import InstalledPlugins, Plugin


@admin.register(InstalledPlugins)
class InstalledPluginsAdmin(SingletonModelAdmin):
    pass


@admin.register(Plugin)
class PluginAdmin(admin.ModelAdmin):
    list_display = ('name',)
    readonly_fields = ('name', 'about',)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
