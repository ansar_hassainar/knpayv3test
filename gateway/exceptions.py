# -*- coding: utf-8 -*-

from __future__ import unicode_literals


class FailedCondition(Exception):
    def __init__(self, message=None, state_method=None):
        """
        :param message: error message to be logged in invalidation reason field
        :param state_method: state method to be called if the transaction isn't meant to be invalidated
        """
        self.message = message
        self.state_method = state_method
