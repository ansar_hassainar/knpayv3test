# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging

from django.views import generic
from django.views.generic.detail import SingleObjectMixin

import paypalrestsdk
from gateway.models import PaymentAttempt, GatewaySettings
from utils.views.mixins import PSPResponseMixin

logger = logging.getLogger('gateway')


class PayPalResponseView(PSPResponseMixin,
                         SingleObjectMixin,
                         generic.View):
    """
    Handle POST requests from PayPal
    """
    http_method_names = ['get']
    queryset = PaymentAttempt.objects.pending()
    slug_url_kwarg = 'reference_number'
    slug_field = 'reference_number'

    def assess_decision(self, data, decision):
        raise NotImplementedError

    def handle_psp_response(self, data):
        decision = data.get('payment_status')
        self.assess_decision(data, decision)

        self.object.save()

        self.object.transaction.send_post_attempt_notifications(self.object)

        return self.redirect()

    def post(self, request, *args, **kwargs):
        data = request.POST.dict()
        self.reference_number = data.get('invoice')
        self.object = self.get_object()
        logger.info("PayPal payload sent to response URL (%s) %s" % (self.__class__.__name__, data))
        return self.handle_psp_response(data)


class CompleteView(PayPalResponseView):
    """
    Execute payment and show payment details to user
    """
    http_method_names = ['get']

    def assess_decision(self, data, decision):
        result = 'approved' if self.object.settings.type == self.object.settings.SANDBOX else 'Paid'
        if decision == result:
            self.object.acknowledge_payment(self.request, data)
        else:
            self.object.fail(data)
        self.object.save()

    def handle_psp_response(self, data):
        decision = self.payment.state
        self.assess_decision(data, decision)
        return self.redirect()

    def configure_paypalrestsdk(self):
        transaction_obj = self.object.transaction
        # TODO: refactor this! not allowed here!
        gateway_settings = GatewaySettings.objects.get(code=transaction_obj.gateway_code)

        paypalrestsdk.configure({
            "mode": gateway_settings.type,  # sandbox or live
            "client_id": gateway_settings.client_id,
            "client_secret": gateway_settings.client_secret}
        )

    def get(self, request, *args, **kwargs):
        data = request.GET.copy()
        self.payment_id = data.get('paymentId', None)
        self.payer_id = data.get('PayerID', None)
        self.object = self.get_object()

        # configure paypal-rest-sdk
        self.configure_paypalrestsdk()

        # execute payment
        self.payment = paypalrestsdk.Payment.find(self.payment_id)

        if self.payment.execute({"payer_id": self.payer_id}):
            logger.info(
                "Payment [%s] executed successfully" % self.payment_id)
        else:
            logger.exception("Error in PayPal while executing payment %s " % self.payment.error)

        return self.handle_psp_response(data)


class CancelView(PayPalResponseView):
    """
    Custom Cancel Response Page
        Hosted by you
    """

    def get(self, request, *args, **kwargs):
        data = request.GET.dict()
        self.object = self.get_object()
        logger.info("PayPal payload sent to response URL (%s) %s" % (self.__class__.__name__, data))
        return self.handle_psp_response(data)

    def assess_decision(self, data, decision):
        self.object.reject(data)
