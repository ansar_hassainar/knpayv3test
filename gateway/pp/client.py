# -*- coding: utf-8 -*-

from __future__ import unicode_literals


import logging
import paypalrestsdk

from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib.sites.models import Site

from api.func import get_knpay_url
from gateway.client import BaseClient

logger = logging.getLogger('gateway')


class PayPalClient(BaseClient):

    def connect(self):
        """
        Performs a request to PayPal PSP
        :return: response text
        """
        # step 1 configure
        paypalrestsdk.configure({
            "mode": self.settings.type,  # sandbox or live
            "client_id": self.settings.client_id,
            "client_secret": self.settings.client_secret}
        )
        logger.info("paypal sdk configuration completed")
        try:
            # step 2 create payment obj
            payment = paypalrestsdk.Payment(self._build_payload())
            if payment.create():
                for link in payment.links:
                    if link.rel == "approval_url":
                        approval_url = str(link.href)
                        logger.info("PayPal response on url connect%s" % (approval_url))
                        return payment.id, approval_url
                return None, None
            else:
                logger.exception(
                    "Error in PayPal response after requesting payment URL {}".format(
                        payment.error))

        except Exception as e:
            logger.exception(
                "Error occurred while requesting payment url from PayPal. {}".format(
                    e))
            return None, None

    def _build_payload(self):
        data = {
            "intent": "sale",
            "payer": {
                "payment_method": "paypal"},
            "redirect_urls": {
                "return_url": '%s%s' % (get_knpay_url(),
                                        reverse('paypal:complete', args=(
                                            self.payment_attempt.reference_number,))),
                "cancel_url": '%s%s' % (get_knpay_url(),
                                        reverse('paypal:cancel', args=(
                                            self.payment_attempt.reference_number,))),
            },
            "transactions": [{
                "amount": {
                    "total": "%s" % self.payment_attempt.get_amount(),
                    "currency": self.payment_attempt.transaction.currency_code
                },
                "description": "KNPay payment",
                "invoice_number": self.payment_attempt.reference_number,
            }]
        }
        logger.info("Paypal prepared payload {}".format(data))
        return data
