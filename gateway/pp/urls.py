# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf.urls import url

# from paypal.standard.ipn.views import ipn

from . import views

urlpatterns = [
    url(r'^complete/(?P<reference_number>[0-9A-Za-z_\-]+)/$',
        views.CompleteView.as_view(), name='complete'),
    url(r'^cancel/(?P<reference_number>[0-9A-Za-z_\-]+)/$',
        views.CancelView.as_view(), name='cancel'),
]
