# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from decimal import Decimal

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from api.func import get_knpay_url
from gateway.models import GatewaySettings
from .client import PayPalClient


class PayPalSettings(GatewaySettings):
    receiver_email = models.EmailField(_('Receiver Email'), max_length=190,
                                       default='')
    client_secret = models.CharField(_("Secret key"), max_length=150)
    client_id = models.CharField(_("Client Id"), max_length=150)

    class Meta:
        verbose_name = _("PayPal")
        verbose_name_plural = _("PayPal")

    def get_field_mapping(self):
        return {
            'merchant_order_id': ['order_no'],
            'currency': ['currency'],
            'total': ['amount'],
            'name': ['customer_first_name'],
            'address': ['customer_address_line1'],
            'city': ['customer_address_city'],
            'state': ['customer_address_state'],
            'zip': ['customer_address_postal_code'],
            'country': ['customer_address_country'],
            'email': ['customer_email'],
            'phone_number': ['customer_phone'],
            'raw_payload': ['extra']
        }

    @property
    def payment_url(self):
        return getattr(self, "%s_url" % self.type)

    @staticmethod
    def create_payment_url(payment_attempt):
        client = PayPalClient(payment_attempt=payment_attempt)
        payment_id, url = client.connect()
        if payment_id and url:
            payment_attempt.data = {'payment_id': payment_id}
            payment_attempt.save()
            return url
        error_message = "Communication error between KNPay and gateway. " \
                        "Please report this issue to KNPay administrator."
        payment_attempt.error(error_message=error_message)
        payment_attempt.save()
