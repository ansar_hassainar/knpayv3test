from django.contrib import admin

# from paypal.standard.ipn.models import PayPalIPN
#
# admin.site.unregister(PayPalIPN)

from polymorphic.admin import PolymorphicChildModelAdmin
from .models import PayPalSettings

@admin.register(PayPalSettings)
class PayPalSettingsAdmin(PolymorphicChildModelAdmin):
    base_model = PayPalSettings
    prepopulated_fields = {'code': ('name',)}
    save_as = True
