from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class PPConfig(AppConfig):
    name = 'pp'
    verbose_name = _("PayPal")

