# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django import forms
from django.utils.translation import ugettext_lazy as _
from .models import PaymentTransaction, Gateway
from .helpers import PaymentTransactionFilterFormHelper, \
    AllPaymentTransactionFilterFormHelper


class GatewayAdminForm(forms.ModelForm):
    GATEWAY_CHOICES = (
        (Gateway.KNET, _("Knet")),
        (Gateway.BURGAN, _("Burgan")),
        (Gateway.MIGS, _("MIGS")),
        (Gateway.CYBERSOURCE, _("CyberSource")),
        (Gateway.MPGS, _("MPGS")),
        (Gateway.PAYPAL, _("PayPal")),
        (Gateway.KPAY, _("KPay")),
    )
    name = forms.ChoiceField(label=_("Name"), choices=GATEWAY_CHOICES,
                             help_text=_("Name of the gateway you want to register"))

    class Meta:
        model = Gateway
        fields = ('is_active', 'name', 'logo')


class PaymentTransactionFilterForm(forms.ModelForm):
    SEEN_CHOICES = (
        ('', _("Select Choice")),
        ('yes', _("Yes")),
        ('no', _("No"))
    )
    date_attrs= {'class': 'form-control', 'readonly': 'true'}
    date_widget = forms.DateInput(attrs=date_attrs)
    created_0 = forms.DateField(widget=date_widget)
    created_1 = forms.DateField(widget=date_widget)
    modified_0 = forms.DateField(widget=date_widget)
    modified_1 = forms.DateField(widget=date_widget)
    q = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),required=False)
    seen = forms.ChoiceField(required=False,choices=SEEN_CHOICES)
    email_seen = forms.ChoiceField(required=False,choices=SEEN_CHOICES)

    class Meta:
        model = PaymentTransaction
        fields = ('gateway_code', 'state', 'initiator')

    def __init__(self, transactions, *args, **kwargs):
        super(PaymentTransactionFilterForm, self).__init__(*args, **kwargs)
        search_attrs = dict(attrs={'class': 'selectpicker', 'data-live-search': "true"})

        empty_choice = [('', _('All'))] if transactions.exists() else []
        gateway_choices = empty_choice + list(transactions.order_by('gateway_code').values_list(
            'gateway_code', 'gateway_code').distinct())

        self.fields['gateway_code'] = forms.ChoiceField(choices=gateway_choices,
                                                        widget=forms.Select(**search_attrs),
                                                        required=False)

        initiator_choices = empty_choice + list(
            transactions.order_by('initiator').values_list(
                'initiator__id', 'initiator__username').distinct())
        self.fields['initiator'] = forms.ChoiceField(choices=initiator_choices,
                                                     widget=forms.Select(**search_attrs),
                                                     required=False)
        state_choices = list(transactions.order_by('state').values_list('state', flat=True).distinct())
        state_choices = empty_choice + [choice for choice in PaymentTransaction.STATE_CHOICES
                                        if choice[0] in state_choices]
        self.fields['state'] = forms.ChoiceField(choices=state_choices,
                                                 widget=forms.Select(**search_attrs),
                                                 required=False)
        self.helper = PaymentTransactionFilterFormHelper()


class AllPaymentTraFilterForm(PaymentTransactionFilterForm):

    def __init__(self, transactions, *args, **kwargs):
        super(AllPaymentTraFilterForm, self).__init__(transactions, *args, **kwargs)
        del self.fields['created_0']
        del self.fields['created_1']
        del self.fields['modified_0']
        del self.fields['modified_1']
        search_attrs = dict(
            attrs={'class': 'selectpicker', 'data-live-search': "true"})
        empty_choice = [('', _('All'))] if transactions.exists() else []
        type_choices = empty_choice + list(PaymentTransaction.TYPE_CHOICES)

        self.fields['type'] = forms.ChoiceField(choices=type_choices,
                                                        widget=forms.Select(
                                                            **search_attrs),
                                                        required=False)

        self.helper = AllPaymentTransactionFilterFormHelper()
