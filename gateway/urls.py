# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^dispatch/(?P<encrypted_pk>.+)/$',
        views.DispatchView.as_view(), name='dispatch'),
    url(r'^redirect-to-psp/(?P<encrypted_pk>.+)/$',
        views.RedirectToPSPView.as_view(), name='redirect_to_psp'),
]