# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.translation import ugettext_lazy as _

from django_extensions.db.fields import AutoSlugField
from easy_thumbnails.fields import ThumbnailerImageField
from polymorphic.manager import PolymorphicManager
from polymorphic.models import PolymorphicModel
from polymorphic.query import PolymorphicQuerySet

from utils.db.managers import StatusQuerySet
from utils.db.models import StatusModel, TimeStampedModel
from utils.funcs import flatten_list, is_ascii

logger = logging.getLogger('gateway')


# def all_sections():
#     from config.models import KNPaySection
#     return KNPaySection.objects.all()


class Gateway(StatusModel, TimeStampedModel):
    KNET = 'knet'
    CYBERSOURCE = 'cybersource'
    MIGS = 'migs'
    BURGAN = 'burgan'
    CASHU = 'cashu'
    PAYPAL = 'paypal'
    TWO_CHECKOUT = 'two_checkout'
    MPGS = 'mpgs'
    KPAY = 'kpay'

    CHOICES = (
        (KNET, _("Knet")),
        (BURGAN, _("Burgan")),
        (MIGS, _("MiGS")),
        (CYBERSOURCE, _("CyberSource")),
        (PAYPAL, _('PayPal')),
        (MPGS, _('MPGS')),
        (KPAY, _('KPay')),
    )
    name = models.CharField(
        _("Name"),
        choices=CHOICES,
        unique=True,
        max_length=32,
        help_text=_("Name of the gateway you want to register")
    )
    logo = ThumbnailerImageField(_("Logo"), null=True, blank=True,
                                 resize_source=dict(size=(75, 50), sharpen=True),
                                 upload_to='gateway/logos',
                                 help_text=_("Logo to be used in emails/confirmation pages/etc"))
    objects = StatusQuerySet.as_manager()

    def __unicode__(self):
        return self.get_name_display()

    def get_settings(self):
        return getattr(self, "%ssettings_set" % self.get_name_display().lower()).all()

    class Meta:
        app_label = 'gateway'
        verbose_name = _("Gateway")
        verbose_name_plural = _("Gateways")
        permissions = (
            ('view_statistics', _("Can view dashboard statistics (Index)")),
            ('add_support',_("Can add support tickets")),
        )


class GatewaySettingsQuerySet(PolymorphicQuerySet):
    def active(self):
        return self.filter(is_active=True)

    def choices(self, blank=False):
        choices = list(self.active().values_list('code', 'name'))
        if blank:
            choices.insert(0, ('', _("Select gateway")))
        return choices

    def with_extra_currencies(self):

        return self.filter(models.Q(currency_config__currencies__isnull=False) | 
            models.Q(currency_config__default_currency__isnull=False)).order_by('id').distinct() 

    def get_for_section(self, section_name):
        return self.filter(sections__name__in=[section_name])

    def get_for_user(self, user):
        """
        available only for filtering choices based on permission
        """
        if user.is_superuser:
            return self.all()
        user_perms = user.user_permissions.all() | Permission.objects.filter(group__user=user)
        perms = Permission.objects.filter(codename__startswith='use_',
                                          id__in=user_perms.values_list('id', flat=True))
        pks = []
        for perm in perms:
            pks += list(perm.content_type.model_class().objects.all().values_list('pk', flat=True))
        return self.filter(id__in=pks)


class GatewaySettingsManager(PolymorphicManager):
    queryset_class = GatewaySettingsQuerySet

    def active(self):
        return self.get_queryset().active()

    def with_extra_currencies(self):
        return self.get_queryset().with_extra_currencies()

    def get_for_section(self, section_name):
        return self.get_queryset().get_for_section(section_name)

    def get_for_user(self, user):
        return self.get_queryset().get_for_user(user)

    def get_for_code(self, code):
        try:
            return self.get(code=code)
        except self.model.DoesNotExist:
            return None

    def is_currency_available(self, currency_code, gateway_code=None):
        """
        Check if given currency code is widely available or per gateway account
        :param currency_code: Currency code
        :param gateway_code: Gateway code
        :return: bool
        """
        if gateway_code is None:
            currency_codes = flatten_list(self.values_list(
                'currency_config__default_currency__code',
                'currency_config__currencies__code'))
            return currency_code in currency_codes

        gateway_account = self.get_for_code(gateway_code)
        if gateway_account is not None:
            return gateway_account.currency_config.is_currency_code_accepted(currency_code)

        return False

    def choices(self, blank=False):
        return self.get_queryset().choices(blank=blank)

    def codes(self):
        return set(self.get_queryset().values_list('code', flat=True))


class GatewaySettings(PolymorphicModel, StatusModel, TimeStampedModel):
    SANDBOX, PRODUCTION = 'sandbox', 'production'
    TYPE_CHOICES = (
        (SANDBOX, _("Sandbox")),
        (PRODUCTION, _("Production")),
    )
    # do not add related name
    gateway = models.ForeignKey(
        Gateway, verbose_name=_("Gateway"),
        related_name='+'
    )
    currency_config = models.ForeignKey(
        'currency.ExchangeConfig',
        verbose_name=_("Currency configuration"),
        help_text=_("Select currency exchange configuration for this account"),
    )
    type = models.CharField(
        _("Type"), choices=TYPE_CHOICES,
        max_length=32, default=SANDBOX
    )
    name = models.CharField(
        _("Name"), max_length=16,
        help_text=_("Name to be displayed in dropdowns or "
                    "anywhere else the settings are being shown. "
                    "IE: 'Knet' or 'Knet demo'"))
    code = AutoSlugField(
        _("Code"), max_length=16, populate_from='name',
        editable=True, separator='-', blank=False,
        unique=True, db_index=True,
        help_text=_("Code to identify the settings in the API/URLs/etc")
    )
    sections = models.ManyToManyField(
        'config.KNPaySection', blank=True,
        help_text=_("Define in which sections this gateway account should be "
                    "available."))
    objects = GatewaySettingsManager()

    def __unicode__(self):
        return '[{}] - {}'.format(self.get_type_display(), self.name)

    class Meta:
        app_label = 'gateway'
        verbose_name = _("Settings")
        verbose_name_plural = _("Settings")

    def get_field_mapping(self):
        raise NotImplementedError

    def is_sandbox(self):
        return self.type == self.SANDBOX

    def has_fee(self):
        return self.currency_config.has_fee()

    @staticmethod
    def create_payment_url(payment_attempt):
        """
        Create a payment url
        :param payment_attempt: PaymentAttempt instance
        :return: str

        If the url can't be created due to various reasons,
        IE: communication error, the payment attempt has to be
        marked as failed/error/etc and when related method is called
        (error, failed, etc) make sure send_notifications positional
        argument is False. We don't want to send failer mail to the user
        when he press the Pay button on pay page.

        """
        raise NotImplementedError

    #################################################
    ####  METHODS AVAILABLE FOR PSP CONNECTIONS  ####
    #################################################

    def get_language(self, payment_attempt=None):
        """
        Return the language formatted for the PSP
        :param payment_attempt:
        :return: str representing the language. IE: ar or ARA, etc
        """
        raise NotImplementedError

    def get_currency(self, payment_attempt=None):
        """
        Return the currency formatted for the PSP
        :param payment_attempt: PaymentAttempt instance
        :return: str. IE: KWD, USD, etc
        """
        raise NotImplementedError

    def get_merchant_identifier(self, payment_attempt=None):
        """
        Get the merchant (unique) identifier
        :param payment_attempt: PaymentAttempt instance
        :return: str
        """
        order_no = payment_attempt.transaction.order_no
        return order_no if is_ascii(order_no) else ''

    def get_error_url(self, payment_attempt=None):
        """
        Returns the URL where PSP will send the response in case of error
        :param payment_attempt: PaymentAttempt instance
        :return: str
        """
        raise NotImplementedError

    def get_response_url(self, payment_attempt=None):
        """
        Returns the URL where PSP will send the response
        :param payment_attempt: PaymentAttempt instance
        :return: str
        """
        raise NotImplementedError

    @property
    def revenue(self):
        """
        Used to calculate revenue
        """
        return self.transactions.get_total_amount_by_gateway_code(self.code)

    @property
    def transactions(self):
        """
        Used to get all transactions of the gateway settings
        """
        from .transaction import PaymentTransaction
        return PaymentTransaction.reports.all().get_by_gateway_code(self.code)