# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import os
import logging
import operator
import urlparse

import StringIO
from decimal import Decimal
from urllib import urlencode, quote_plus
from mimetypes import MimeTypes
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.sites.models import Site
from django.conf import settings as django_settings
from django.core.urlresolvers import reverse
from django.db import models, connection
from django.db.models import Q
from django.dispatch import receiver
from django.template import Template, Context
from django.forms import model_to_dict
from django.utils.text import slugify
from django.utils.functional import cached_property
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _, activate, get_language
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.utils import timezone
from utils.funcs import get_invoice_pdf_path, get_invoice_pdf_url

import django_rq
import qrcode
import requests
from dbmail import send_db_sms, send_db_mail
from dbmail.models import MailLog, MailLogTrack
from django_fsm import FSMField, transition, signals as fsm_signals
from jsonfield import JSONField
from model_utils.models import MonitorField
from unfriendly import settings as unfriendly_settings
from unfriendly.utils import encrypt
from pycountry import countries

from api.func import get_knpay_url
from config.models import Config, MerchantDetails
from extras.currency.data import CURRENCY_CHOICES
from plugins.catalogue.models import Product, ReservedStock
from utils.validators import FileValidator
from utils.db.fields import NullCharField
from utils.db.models import TimeStampedModel
from utils.views.custom_pdf import CustomWKhtmlToPdf
from utils.funcs import shortify_url
from utils.db.managers import PermissionQuerySet, PermissionManager
from ..constants import PAYMENT_REQUEST, CUSTOMER_PAYMENT, THIRD_PARTY, E_COMMERCE, TYPE_CHOICES, CATALOGUE
from ..exceptions import FailedCondition
from ..friendly_id import encode
from . import GatewaySettings

logger = logging.getLogger('gateway')


def get_attachment_path(instance, filename):
    filename, dot, extension = filename.rpartition('.')
    return 'attachment/{}.{}'.format(slugify(filename), extension)


class PaymentTransactionQuerySet(PermissionQuerySet):
    def valid_for_payment(self):
        """
        :return: transactions which can be paid according to the state.
        Still an extra check of `can_be_paid` has to occur to check if the
        transaction is payable.
        """
        states = PaymentTransaction.CREATED, PaymentTransaction.PENDING, PaymentTransaction.ATTEMPTED
        query = reduce(operator.or_, (Q(state=state) for state in states))
        return self.filter(query)

    def invalid(self):
        return self.filter(state=PaymentTransaction.INVALID)

    def completed(self):
        return self.filter(models.Q(state=PaymentTransaction.PAID) |
                           models.Q(state=PaymentTransaction.CANCELED) |
                           models.Q(state=PaymentTransaction.FAILED)
                           )

    def paid(self):
        return self.filter(state=PaymentTransaction.PAID)

    def e_commerce(self):
        return self.filter(type=PaymentTransaction.E_COMMERCE)

    def payment_requests(self):
        return self.filter(type=PaymentTransaction.PAYMENT_REQUEST)

    def customer_requests(self):
        return self.filter(type=PaymentTransaction.CUSTOMER_PAYMENT)

    def third_parties(self):
        return self.filter(type=PaymentTransaction.THIRD_PARTY)

    def catalogue(self):
        return self.filter(type=PaymentTransaction.CATALOGUE)

    def unpaid(self):
        return self.exclude(state=PaymentTransaction.PAID)

    def get_by_currency_code(self, currency_code):
        return self.filter(currency_code=currency_code)

    def get_total_by_currency_code(self, currency_code):
        return self.get_by_currency_code(currency_code).aggregate(models.Sum('amount'))['amount__sum']

    def get_currencies(self):
        return self.values_list('currency_code', flat=True)

    def get_for_user(self, user, field_name='initiator'):
        qs = super(PaymentTransactionQuerySet, self).get_for_user(user)
        # if user is superuser or has superuser permission, he shall see all qs
        if user.is_superuser or self._has_superuser_perm(user):
            return qs
        elif user.groups.filter(permissions__codename='view_collaborators_paymenttransaction').exists():
            query = """
                SELECT
                    DISTINCT pt.id as id
                FROM
                    gateway_paymenttransaction pt
                WHERE
                    pt.initiator_id IN (SELECT
                            au2.id
                        FROM
                            auth_user au2
                                INNER JOIN
                            auth_user_groups aug2 ON aug2.user_id = au2.id
                        WHERE
                            aug2.group_id IN (SELECT
                                    ag.id
                                FROM
                                    auth_user au
                                        INNER JOIN
                                    auth_user_groups aug ON aug.user_id = au.id
                                        INNER JOIN
                                    auth_group ag ON ag.id = aug.group_id
                                        INNER JOIN
                                    auth_group_permissions agp ON agp.group_id = ag.id
                                        INNER JOIN
                                    auth_permission ap ON ap.id = agp.permission_id
                                WHERE
                                    ap.codename = 'view_collaborators_paymenttransaction'
                                        AND au.id = %s))
            """
            ids = [obj.id for obj in self.raw(query, [user.id])]
            return self.filter(id__in=ids)
        else:
            return qs


class PaymentTransactionManager(PermissionManager):
    def get_queryset(self):
        qs = PaymentTransactionQuerySet(self.model, using=self._db)
        return qs.filter(is_deleted=False)

    def create_from_data(self, data):
        return self.create(**data)

    def valid_for_payment(self):
        return self.get_queryset().valid_for_payment()

    def invalid(self):
        return self.get_queryset().invalid()

    def completed(self):
        return self.get_queryset().completed()

    def paid(self):
        return self.get_queryset().paid()

    def e_commerce(self):
        return self.get_queryset().e_commerce()

    def payment_requests(self):
        return self.get_queryset().payment_requests()

    def customer_requests(self):
        return self.get_queryset().customer_requests()

    def catalogue(self):
        return self.get_queryset().catalogue()

    def third_parties(self):
        return self.get_queryset().third_parties()

    def unpaid(self):
        return self.get_queryset().unpaid()


class DeletedTransactionManager(PermissionManager):

    def get_queryset(self):
        qs = super(DeletedTransactionManager, self).get_queryset()
        qs = qs.filter(Q(is_deleted=True))
        return qs


class PaymentTransactionReportQuerySet(PaymentTransactionQuerySet):
    def get_by_gateway_code(self, code):
        return self.filter(gateway_code=code)

    def get_total_amount_by_gateway_code(self, code):
        return self.get_by_gateway_code(code).aggregate(models.Sum('amount'))['amount__sum']

    def get_within_interval(self, start, end):
        return self.filter(created__range=(start, end))

    def get_by_currency_code(self, currency_code):
        return self.filter(currency_code=currency_code)

    def get_total_by_currency_code(self, currency_code):
        return self.get_by_currency_code(currency_code).aggregate(models.Sum('amount'))['amount__sum']

    def get_currencies(self):
        return self.values_list('currency_code', flat=True)

    def get_report(self, period, start=False, end=False):

        if all([start, end]):
            transactions = self.get_within_interval(start, end)
        else:
            transactions = self
        if period not in ['day', 'month', 'year']:
            return self.none()

        truncate_date = connection.ops.date_trunc_sql(period, 'created')
        transactions = transactions.extra({period: truncate_date})
        return transactions.values(period).annotate(models.Count('pk')).order_by(period)


class PaymentTransactionReportManager(PaymentTransactionManager):
    def get_queryset(self):
        return PaymentTransactionReportQuerySet(self.model, using=self._db)


class PaymentTransaction(TimeStampedModel):
    """
    The core model of any payment transaction
    """
    CREATED = 'created'
    PENDING = 'pending'
    ATTEMPTED = 'attempted'
    PAID = 'paid'
    FAILED = 'failed'
    CANCELED = 'canceled'
    EXPIRED = 'expired'
    INVALID = 'invalid'

    STATE_CHOICES = (
        # created in the API and returned to the merchant
        (CREATED, _("Created")),
        # customer has opened the payment link and the request to the PSP was sent
        # (only available for payment requests created though
        (PENDING, _("Pending")),
        # customer has attempted to pay, but failed due to various reasons
        (ATTEMPTED, _("Attempted")),
        # ovio, no? :)
        (PAID, _("Paid")),
        # transactions which are not PAYMENT_REQUEST cannot be paid multiple times
        # so, they have to be marked as failed from the first attempt
        (FAILED, _("Failed")),
        # only admin can mark as canceled
        (CANCELED, _("Canceled")),
        # expired transactions which were not paid till a particular date
        # or canceled on gateway page (except PAYMENT_REQUESTs, which are marked as attempted)
        (EXPIRED, _("Expired")),
        # transactions which were invalidated due to various reasons.
        # IE: something changed in the knpay configuration and transaction
        # cannot be initialized anymore (missing currency, etc)
        (INVALID, _("Invalid")),
    )
    UNPAID = [PENDING, ATTEMPTED, CANCELED, EXPIRED, INVALID, FAILED]

    E_COMMERCE = E_COMMERCE
    PAYMENT_REQUEST = PAYMENT_REQUEST
    CUSTOMER_PAYMENT = CUSTOMER_PAYMENT
    THIRD_PARTY = THIRD_PARTY
    CATALOGUE = CATALOGUE
    TYPE_CHOICES = TYPE_CHOICES

    PAYABLE_CONDITIONS = [
        'meet_payable_states',
        'gateway_is_available',
        'currency_is_available',
        'currency_exchange_is_available',
        'not_expired'
    ]
    CUSTOMER_FIELDS = [
        'order_no',
        'customer_first_name',
        'customer_last_name',
        'customer_email',
        'customer_phone',
        'customer_address_line1',
        'customer_address_line2',
        'customer_address_city',
        'customer_address_state',
        'customer_address_country',
        'customer_address_postal_code',
        'email_recipients',
        'attachment'
    ]
    PAID_TRANSACTION_FIELDS = ['amount'] + CUSTOMER_FIELDS

    state = FSMField(_("State"), default=CREATED, db_index=True,
                     # choices=STATE_CHOICES, protected=True)
                     choices=STATE_CHOICES, protected=False)
    type = models.CharField(
        _("Type"), choices=TYPE_CHOICES,
        default=E_COMMERCE, max_length=24)
    gateway_code = models.CharField(
        _("Gateway code"), max_length=16, db_index=True,
        blank=True, default='')
    is_sandbox = models.BooleanField(_("Is Sandbox?"),
                                     default=False,
                                     help_text=_('Designates if this payment done with sandbox or production gateway type?'))
    is_deleted = models.BooleanField(_('Is this transaction deleted?'),
                                     default=False,
                                     help_text=_('Mark true to transfer this '
                                                 'transaction to archived '
                                                 'list.'))
    amount = models.CharField(
        _("Amount"), max_length=24)
    language = models.CharField(
        _("Language"), max_length=2, choices=django_settings.LANGUAGES,
        default=django_settings.LANGUAGE_CODE)
    currency_code = models.CharField(
        _("Currency code"), max_length=3,
        choices=CURRENCY_CHOICES)
    order_no = NullCharField(
        _("Merchant order no"), null=True, db_index=True,
        max_length=128, blank=True, unique=True)
    bulk_id = models.CharField(
        _("Bulk ID"), null=True, db_index=True,
        max_length=128, blank=True)
    sms_payment_details = models.BooleanField(
        _("SMS payment details"), default=False,
        help_text=_("If checked, customer will be SMSed with payment details upon transaction "
                    "stage ends."))
    email_payment_details = models.BooleanField(
        _("Email payment details"), default=False,
        help_text=_("If checked, customer will be mailed with payment details upon transaction "
                    "stage ends."))
    sms_notification = models.BooleanField(
        _("SMS notification"), default=False,
        help_text=_("If checked, customer will get a SMS alert to pay the transaction."))
    email_notification = models.BooleanField(
        _("Email notification"), default=False,
        help_text=_("If checked, customer will get a mail alert to pay the transaction."))
    push_notification = models.BooleanField(
        _("Push notification"), default=False,
        help_text=_("If checked, customer will get a push-notification alert to pay the transaction."))
    last_date_checked_for_push = models.DateTimeField(
        auto_now_add=True, blank=True,
        help_text=_("When was the last time server checked with monitor dashboard "
                    "that user is registered with app or not."))
    disclosure_url = models.URLField(
        _("Disclosure URL"), blank=True, default='',
        help_text=_("URL where to send payment result after customer ends "
                    "the session on gateway page."))
    redirect_url = models.URLField(
        _("Redirect URL"), blank=True, default='',
        help_text=_("URL where to redirect the user after payment"))
    customer_first_name = models.CharField(
        _("Customer first name"), max_length=64,
        default='', blank=True)
    customer_last_name = models.CharField(
        _("Customer last name"), max_length=64,
        default='', blank=True)
    customer_email = models.CharField(
        _("Customer email"), max_length=128,
        default='', blank=True)
    customer_phone = models.CharField(
        _("Customer phone"), max_length=16,
        default='', blank=True)
    customer_address_line1 = models.TextField(
        _("Customer address line 1"),
        default='', blank=True)
    customer_address_line2 = models.TextField(
        _("Customer address line 2"),
        default='', blank=True)
    customer_address_city = models.CharField(
        _("Customer address city"), default='',
        max_length=40, blank=True)
    customer_address_state = models.CharField(
        _("Customer address state"), default='',
        max_length=40, blank=True)
    COUNTRY_CHOICES = [(c.alpha_2, c.name) for c in list(countries)]
    customer_address_country = models.CharField(
        _("Customer address country"), default='',
        choices=COUNTRY_CHOICES,
        max_length=2, blank=True)
    customer_address_postal_code = models.CharField(
        _("Customer address postal code"),
        default='', blank=True, max_length=12)
    email_recipients = models.TextField(
        _("Email Recipients"),
        help_text=_('Comma separated Email Recipients which will receive '
                    'all emails same as customer_email.'),
        default='', blank=True)
    attachment = models.FileField(
        verbose_name=_("Attachment"), upload_to=get_attachment_path,
        validators=[FileValidator(
            allowed_extensions=['pdf', 'jpeg', 'png', 'doc', 'docx'])],
        help_text=_('Attachment for Payment Requests'),
        blank=True, null=True)
    attachment_short_url = models.URLField(
        _("Attachment Short url"), blank=True,
        help_text=_("pay.kn URL")
    )
    invoice_short_url = models.URLField(
        _("Invoice Short url"), blank=True,
        help_text=_("pay.kn URL")
    )
    maillog = models.ForeignKey(
        MailLog, editable=False, null=True,
        blank=True, related_name='transaction'
    )
    email_seen_at = models.DateTimeField(
        _("Email Seen at"),
        null=True, blank=True,
        help_text=_("Defines if the customer or any "
                    "other guest user has opened the Email.")
    )
    extra = JSONField(_("Extra"), default={}, blank=True, null=False)
    qr_code = models.ImageField(upload_to='qr_code', blank=True,
                                null=True)

    # meta
    seen_at = models.DateTimeField(
        _("Seen at"),
        null=True, blank=True,
        help_text=_("Defines if the customer or any "
                    "other guest user has opened the payment link.")
    )
    state_changed_at = MonitorField(monitor='state', blank=True)
    invalidation_reason = models.CharField(
        _("Invalidation reason"),
        max_length=255, default='', blank=True,
        editable=False,
    )
    initiator = models.ForeignKey(
        django_settings.AUTH_USER_MODEL, verbose_name=_("initiator"),
        null=True, blank=True, related_name='transactions',
        help_text=_("The user who created the transaction")
    )
    short_url = models.URLField(
        _("Short url"), blank=True,
        help_text=_("pay.kn URL")
    )
    objects = PaymentTransactionManager()
    reports = PaymentTransactionReportManager()

    def __unicode__(self):
        return '[{}] {} - {}'.format(self.gateway_code or 'gateway not selected',
                                     self.order_no or self.bulk_id or self.pk,
                                     self.get_state_display())

    class Meta:
        verbose_name = _("Payment transaction")
        verbose_name_plural = _("Payment transactions")
        ordering = ('-created', '-modified')
        get_latest_by = 'created'
        permissions = (
            ('view_paymenttransaction', _('Can view payment transactions')),
            ('delete_paid_paymenttransaction', _(
                'Can delete paid payment transactions')),
            ('view_owned_paymenttransaction', _(
                'Can view owned payment transactions')),
            ('cancel_paymenttransaction', _('Can cancel payment transactions')),
            ('cancel_owned_paymenttransaction', _(
                'Can cancel owned payment transactions')),
            ('view_collaborators_paymenttransaction',
             _("Can view transactions created by collaborators")),
            ('view_catalogue_paymenttransaction', _(
                'Can view catalogue payment transactions')),
            ('view_paymenttransaction_statistics', _(
                'Can view transactions statistics')),
            ('view_paymenttransaction_search', _(
                'Can view transactions search options')),
            ('view_paymenttransaction_export_settings',
             _('Can use transactions export settings'))
        )

    @receiver(post_save, sender=MailLogTrack)
    def maillog_track_oncreated(sender, instance, created, **kwargs):
        if created:
            maillog = instance.mail_log
            try:
                transaction = PaymentTransaction.objects.get(maillog=maillog)
                transaction.email_seen_at = now()
                transaction.save()
            except PaymentTransaction.DoesNotExist:
                pass

    def get_amount(self):
        return Decimal(self.amount)

    def get_values_for_extra_fields(self, config):
        return [getattr(self, field.get_name(), self.extra.get(field.get_name(), ''))
                for field in config.extra_fields()]

    @cached_property
    def encrypted_pk(self):
        return encrypt(str(self.pk),
                       str(unfriendly_settings.UNFRIENDLY_SECRET),
                       str(unfriendly_settings.UNFRIENDLY_IV),
                       checksum=unfriendly_settings.UNFRIENDLY_ENFORCE_CHECKSUM)

    # Generate QR Code
    def generate_qr_code(self):
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=10,
            border=4,
        )
        site = Site.objects.get_current()
        enct_key = self.encrypted_pk
        data = str(self.encrypted_pk) + "_____" + site.domain
        qr.add_data(data)
        qr.make(fit=True)

        img = qr.make_image(fill_color="white", back_color="black")

        buffer = StringIO.StringIO()
        img.save(buffer)
        filename = 'qr_code%s.png' % (enct_key)
        filebuffer = InMemoryUploadedFile(
            buffer, None, filename, 'image/png', buffer.len, None)
        self.qr_code.save(filename, filebuffer)

    # urls

    def get_product_url(self):
        try:
            product = Product.objects.get(pk=self.extra['product'])
            return product.payment_link()
        except Product.DoesNotExist:
            return ''

    def get_absolute_url(self):
        return reverse('dashboard:payment_request_detail', args=(self.pk,))

    def get_payment_url(self):
        if self.short_url:
            return self.short_url

        path = reverse('gateway:dispatch', args=(self.encrypted_pk,))
        url = '{knpay_url}{path}'.format(
            knpay_url=get_knpay_url(),
            language=self.language,
            path=path)
        if self.is_payment_request():
            self.short_url = shortify_url(url)
            self.save()
            return self.short_url
        return url

    def get_attachment_url(self):
        if self.attachment_short_url:
            return self.attachment_short_url

        path = self.attachment.url
        url = '{knpay_url}{path}'.format(
            knpay_url=get_knpay_url(),
            language=self.language,
            path=path)
        if self.is_payment_request():
            self.attachment_short_url = shortify_url(url)
            self.save()
            return self.short_url
        return url

    def get_invoice_url(self):
        if not self.is_paid():
            return
        if self.invoice_short_url:
            return self.invoice_short_url
        file_url = get_invoice_pdf_url(self.encrypted_pk)
        url = '{knpay_url}{path}'.format(
            knpay_url=get_knpay_url(),
            language=self.language,
            path=file_url)
        self.invoice_short_url = shortify_url(url)
        self.save()
        return self.invoice_short_url

    def get_attempt_url(self, gateway_code=None):
        """
        try to create the PSP url
        if fails, fallback on error url which will redirect
        the customer back to the merchant error page
        :return: url
        """
        attempt = self.create_attempt(gateway_code=gateway_code)
        return attempt.get_payment_url()

    def get_payment_request_url(self):
        current_lang = get_language()
        activate(self.language)
        url = reverse('ui:payment_request', args=(self.encrypted_pk,))
        activate(current_lang)
        return url

    def _format_merchant_url(self, url):
        """
        Add querystring to the URL
        :param url: URL string
        :return: URL with updated querystring
        """

        fields = ('order_no', 'bulk_id')
        params = {field: getattr(self, field).encode('utf-8')
                  for field in fields if getattr(self, field)}

        if self.has_payment_attempts():
            # include the reference_number number also in the querystring
            attempt = self.get_latest_attempt()
            params.update({'reference_number': attempt.reference_number})
        if bool(self.extra):
            extra = self.extra
            # TODO: add a list of knpay values and exclude them from start. consumed is a good example for that.
            extra.pop('consumed', None)
            params.update({key: value.encode('utf-8')
                           for key, value in extra.items()})
        url_parts = list(urlparse.urlparse(url))
        query = dict(urlparse.parse_qsl(url_parts[4]))
        query.update(params)
        url_parts[4] = urlencode(query)
        return urlparse.urlunparse(url_parts)

    def get_merchant_redirect_url(self):
        return self._format_merchant_url(self.redirect_url)

    def get_local_shopify_url(self):
        """
        URL where customer has to be redirected after payment process ends
        """
        return reverse('shopify:redirect_to_shopify', args=(self.encrypted_pk,))

    def has_gateway(self):
        return bool(self.gateway_code)

    def has_payment_attempts(self):
        return self.attempts.exists()

    @cached_property
    def _config(self):
        return Config.get_solo()

    @cached_property
    def gateway_settings(self):
        from .gateway import GatewaySettings
        try:
            return GatewaySettings.objects.get(code=self.gateway_code)
        except GatewaySettings.DoesNotExist:
            return None

    def create_attempt(self, gateway_code=None):
        from .gateway import GatewaySettings

        settings = GatewaySettings.objects.get_for_code(
            gateway_code or self.gateway_code)
        attempt = self.attempts.create_with_amount(settings, self.amount)
        return attempt

    def get_paid_attempt(self):
        paid_attempt = self.attempts.filter(state=PaymentAttempt.SUCCESS)
        return paid_attempt.last() if paid_attempt.exists() else None

    @property
    def paid_attempt_fee(self):
        attempt = self.get_paid_attempt()
        return attempt.fee if attempt else '0'

    @property
    def paid_attempt_total(self):
        attempt = self.get_paid_attempt()
        return attempt.total if attempt else None

    @property
    def paid_attempt_amount(self):
        attempt = self.get_paid_attempt()
        return attempt.amount if attempt else '0'

    def fetch_data(self, installation):
        data = {
            'transaction_id': '%s' % self.id,
            'gateway_code': self.gateway_code,
            'currency_code': self.currency_code,
            'amount': self.paid_attempt_amount,
            'fee': self.paid_attempt_fee,
            'total': self.paid_attempt_total if self.paid_attempt_total else self.amount,
            'state': self.state,
            'transaction_created': self.created.strftime('%Y-%m-%d %H:%M'),
            'transaction_paid_at': self.state_changed_at.strftime('%Y-%m-%d %H:%M'),
            'type': self.type,
            'installation': installation
        }
        return data

    def is_paid(self):
        return self.state == self.PAID

    def is_e_commerce(self):
        return self.type == self.E_COMMERCE

    def is_payment_request(self):
        return self.type == self.PAYMENT_REQUEST

    def is_customer_payment(self):
        return self.type == self.CUSTOMER_PAYMENT

    def is_canceled(self):
        return self.state == self.CANCELED

    def is_pending(self):
        return self.state == self.PENDING

    def is_attempted(self):
        return self.state not in [self.CREATED, self.PENDING]

    def is_expired(self):
        return self.state == self.EXPIRED

    def is_invalid(self):
        return self.state == self.INVALID

    def is_third_party(self):
        return self.type == self.THIRD_PARTY

    def is_catalogue(self):
        return self.type == self.CATALOGUE

    def is_bulk(self):
        return bool(self.bulk_id)

    # catalogue
    def reserve_product(self):
        """
        Reserve product quantity with timestamp
        """
        product_obj = Product.objects.get(pk=self.extra.get('product'))
        quantity = self.extra.get('quantity')
        if product_obj.is_available:
            product_obj.stock -= int(quantity)
            product_obj.save()
            reserve_obj = ReservedStock.objects.create(product=product_obj,
                                                       quantity=quantity,
                                                       reserved_at=timezone.datetime.now())
            self.extra['reserved_id'] = reserve_obj.pk
            self.save()

    # conditions

    def _meet_payable_states(self, **kwargs):
        if self.state not in [self.CREATED, self.PENDING, self.ATTEMPTED]:
            raise FailedCondition("Invalid payment state.")

    def _gateway_is_available(self, **kwargs):
        from .gateway import GatewaySettings
        gateway_code = kwargs.get('gateway_code')
        if gateway_code and not GatewaySettings.objects.active().filter(code=gateway_code).exists():
            raise FailedCondition("Gateway code not available.")

    def _currency_exchange_is_available(self, **kwargs):
        gateway_code = kwargs.get('gateway_code')
        from .gateway import GatewaySettings
        from extras.currency.models import Currency, CurrencyExchange
        if gateway_code:
            gateway_settings = GatewaySettings.objects.get_for_code(
                gateway_code)
            currency_config = gateway_settings.currency_config
            if self.currency_code != currency_config.default_currency.code:
                if not CurrencyExchange.objects.filter(from_code=self.currency_code,
                                                       to_code=currency_config.default_currency.code).exists():
                    raise FailedCondition("Currency exchange rate does not exists between %s and %s" % (
                        currency_config.default_currency.code, self.currency_code))
        return True

    def _currency_is_available(self, **kwargs):
        gateway_code = kwargs.get('gateway_code')
        from .gateway import GatewaySettings
        from extras.currency.models import Currency
        if gateway_code:
            if not GatewaySettings.objects.is_currency_available(self.currency_code,
                                                                 gateway_code=gateway_code):
                raise FailedCondition(
                    "Currency code not available for %s gateway." % gateway_code)
        elif not Currency.objects.filter(code=self.currency_code).exists():
            raise FailedCondition("Currency does not exists.")

    def _not_expired(self, **kwargs):
        if self._config.has_expiration_time():
            if now() > (self.created + self._config.get_expiration_time(self.created)):
                raise FailedCondition(state_method='expire')

    def check_conditions(self, **kwargs):
        gateway_code = kwargs.get('gateway_code')
        for method_name in self.PAYABLE_CONDITIONS:
            getattr(self, '_%s' % method_name)(gateway_code=gateway_code)

    def is_valid_for_payment(self, gateway_code=None):
        """
        Check if transaction can be initialized for payment
        :return: bool
        """
        if self.state in [self.PAID, self.EXPIRED, self.CANCELED, self.INVALID, self.FAILED]:
            return False

        input_gateway_code = gateway_code or self.gateway_code
        try:
            self.check_conditions(gateway_code=input_gateway_code)
            return True
        except FailedCondition as e:
            if gateway_code is None:
                # meaning, we are NOT checking the possibility to pay with
                # ANY gateway or similar situation
                if e.state_method:
                    # if there is a state method defined, call it
                    # and let if follow it's course,
                    # otherwise invalidate the transaction
                    getattr(self, e.state_method)()
                else:
                    self.invalidate(e.message)
                self.save()
            return False

    def _get_notification_context(self, ctx=None):
        base_ctx = model_to_dict(self, exclude=(
            'created', 'modified', 'type', 'state_changed_at', 'initiator'))
        merchant_details = MerchantDetails.get_solo()
        base_ctx.update(
            {'merchant': model_to_dict(merchant_details, exclude=('logo',)),
             'payment_url': self.get_payment_url(),
             'initiator': getattr(self, 'initiator'),
             'transaction': self,
             'invoice_url': self.get_invoice_url(),
             })

        try:
            base_ctx['gateway_name'] = GatewaySettings.objects.get(
                code=self.gateway_code).name
        except GatewaySettings.DoesNotExist:
            base_ctx['gateway_name'] = self.gateway_code

        if self.attachment:
            base_ctx.update({'attachment_url': self.get_attachment_url()})

        if merchant_details.has_logo():
            base_ctx.update({'logo_url':  "%s%s" % (
                get_knpay_url(), merchant_details.logo.url)})

        if self.is_catalogue():
            try:
                product = Product.objects.get(pk=self.extra.get('product'))
                base_ctx.update(product.get_product_data())
                base_ctx.update(
                    {'product_quantity': self.extra.get('quantity')})
            except Product.DoesNotExist:
                pass

        config = self._get_transaction_config()

        if hasattr(config, 'fields'):
            for field in config.fields.all():
                key = field.field if field.field else field.name
                if not hasattr(self, key):
                    data = {
                        key: self.extra.get(key, '')
                    }
                    base_ctx.update(data)
        if ctx is not None:
            base_ctx.update(ctx)
        return {key: value for key, value in base_ctx.iteritems() if value}

    def _get_transaction_config(self, klass=None):
        if klass is not None:
            return klass()
        if self.is_e_commerce():
            from config.models import ECommerceConfig
            return ECommerceConfig.get_solo()
        if self.is_third_party():
            from plugins.shopify.models import ShopifyConfig
            return ShopifyConfig.get_solo()
        elif self.is_customer_payment() and not self.is_bulk():
            from extras.dashboard.models import CustomerPaymentFormConfig
            return CustomerPaymentFormConfig.get_solo()
        elif self.is_catalogue():
            from plugins.catalogue.models import CatalogueConfig
            return CatalogueConfig.get_solo()
        elif self.is_bulk():
            from extras.dashboard.bulk.models import BulkConfig
            return BulkConfig.get_solo()
        else:
            from extras.dashboard.models import PaymentRequestFormConfig
            return PaymentRequestFormConfig.get_solo()

    def send_post_attempt_notifications(self, payment_attempt):
        if not any([self.customer_phone, self.customer_email]):
            return

        ctx = {
            'reference_number': payment_attempt.reference_number,
            'state': payment_attempt.get_state_display(),
            'gateway_response': payment_attempt.gateway_response,
            'gateway_code': payment_attempt.settings.code,
            'date': payment_attempt.state_changed_at.strftime('%Y-%m-%d %H:%M'),
        }

        transaction_config = self._get_transaction_config()

        # mail
        if self.email_payment_details:
            template_field = 'email_payment_details_success_template' if self.is_paid() else \
                'email_payment_details_failure_template'
            try:
                template = getattr(transaction_config, template_field)
                self.send_mail(template.slug, ctx)
            except AttributeError:
                pass
        # sms
        if self.sms_payment_details:
            template_field = 'sms_payment_details_success_template' if self.is_paid() else \
                'sms_payment_details_failure_template'
            try:
                template = getattr(transaction_config, template_field)
                self.send_sms(template.slug, ctx)
            except AttributeError:
                pass

        payment_attempt.data['post_attempt_notifications_sent'] = True
        payment_attempt.save()

    def get_whatsapp_template(self):
        template_field = 'whatsapp_notification_template'
        transaction_config = self._get_transaction_config()
        try:
            template = getattr(transaction_config, template_field)
            ctx = self._get_notification_context()
            rendered_template = Template(template.message).render(Context(ctx))
            return rendered_template
        except AttributeError:
            pass

    def send_payment_request_notifications(self, email=False, sms=False, push=False):
        if not any([email, sms, push]):
            return

        transaction_config = self._get_transaction_config()

        # email
        if email:
            try:
                template = transaction_config.customer_payment_email_notification_template
                self.send_mail(template.slug, {}, include_attachment=True)
            except AttributeError:
                pass
        # sms
        if sms:
            try:
                template = transaction_config.customer_payment_sms_notification_template
                self.send_sms(template.slug, {})
            except AttributeError:
                pass

        # push-notification
        if push:
            self.send_push_notification()

    def check_user_registered(self):
        now_time = now()
        last_check_time = self.last_date_checked_for_push
        elapsed_time = now_time - last_check_time
        d = divmod(elapsed_time.days * 86400 + elapsed_time.seconds, 60)
        h = d[0]/60
        if h > 24:
            request_url = django_settings.CHECK_USER_REGISTERED_URL \
                + "?phone_number=" + quote_plus(self.customer_phone)
            headers = {'Content-Type': 'application/json'}
            try:
                resp = requests.get(request_url, headers=headers)
                if resp.json().get('is_registered'):
                    self.push_notification = True
                    self.save()
                    return True
            except requests.exceptions.RequestException as e:
                logger.exception(
                    "Error occurred while checking customer phone registered "
                    "or not on dashboard %s" % e)
                return False

    def _send_push_notification(self):
        path = reverse('apis:payment_request_detail',
                       args=(self.encrypted_pk,))
        url = '{knpay_url}{path}'.format(
            knpay_url=get_knpay_url(), path=path)
        data = {'link': self.short_url, 'phone_number': self.customer_phone,
                'pay_api': url}
        url = django_settings.SEND_NOTIFICATION_URL
        headers = {'Content-Type': 'application/json'}
        try:
            requests.post(url, json=data, headers=headers)
        except requests.exceptions.RequestException as e:
            logger.exception(
                "Error occurred while sending push notification %s" % e)

    def send_push_notification(self):
        """
        Sending payment-request push notification to user's phone
        Sends Post request to dashboard's push notification API

        UPDATE: First check if last check time is greater than 24 hr and
        get latest details of if user is registered on app or not
        if less then 24 hour the go with current push_notification setting
        """
        # ToDO: add scheduler task for checking user is registered on dashboard or not
        if self.customer_phone:
            flag = False
            if self.push_notification:
                self._send_push_notification()
            else:
                flag = self.check_user_registered()
            if flag:
                self._send_push_notification()

    def get_invoice_pdf(self):
        """
        - Rendering invoice pdf in the transaction language, 
            then switch back to the current language.  
        - This function called in the flow which created by the psp response,
            so the request running under the system default language.
        """
        file_name, file_path = get_invoice_pdf_path(self.encrypted_pk)
        current_lang = get_language()
        activate(self.language)
        current_site = Site.objects.get_current()
        domain = current_site.domain
        pdf_url = "{0}://{1}{2}".format(django_settings.PROTOCOL, domain, reverse(
            'dashboard:payment_invoice',
            kwargs={'encrypted_pk': self.encrypted_pk}))
        activate(current_lang)
        pdf = CustomWKhtmlToPdf(
            url=pdf_url,
            output_file=file_path
        )
        state, file_obj = pdf.render()
        if state:
            f = open(file_obj, 'r')
            resp_data = f.read()
            f.close()
        return file_path, file_name

    def send_mail(self, slug, ctx, include_attachment=False, **kwargs):
        if not self.customer_email:
            logger.exception("Send mail called without customer_email "
                             "present for PK %s" % self.pk)
            return

        transaction_config = self._get_transaction_config()
        ctx = self._get_notification_context(ctx=ctx)
        email_recipients = [self.customer_email]
        if self.email_recipients:
            email_recipients.extend(x.strip().strip(',')
                                    for x in self.email_recipients.split(','))

        if hasattr(transaction_config, 'bcc_initiator'):
            kwargs.update({
                'bcc': getattr(self.initiator, 'email', None) if transaction_config.bcc_initiator
                else None
            })

        attachments = []
        try:
            c = Config.get_solo()
            if c.attach_invoice_pdf and self.state == self.PAID:
                # attaching invoice pdf only for PAID payment
                invoice, filename = self.get_invoice_pdf()
                invoice_pdf = open(invoice, 'rb')
                attachments.append((filename, invoice_pdf.read(),
                                    'application/pdf'))
                logger.info('invoice pdf appended to attachments')
        except Exception as e:
            mail_logger = logging.getLogger('communications')
            mail_logger.exception("Error with invoice pdf attachment %s" % e)

        #dbmail activates template language and doesn't deactivate it again
        #so we save current language and reactivate it after dbmail call finished 
        current_lang = get_language()
        try:
            if self.attachment and include_attachment:
                attachment_content_type, _ = MimeTypes().guess_type(self.attachment.url)
                attachments.append(
                    (self.attachment.name, self.attachment.read(),
                     attachment_content_type)
                )
            send_db_mail(slug,
                         email_recipients,
                         ctx,
                         language=self.language,
                         attachments=attachments,
                         **kwargs)
        except Exception as e:
            mail_logger = logging.getLogger('communications')
            mail_logger.exception("Error while sending email %s" % e)
            # django_rq.enqueue(send_db_mail, slug, self.customer_email, ctx,
            # language=self.language, **kwargs)
        activate(current_lang)

    def send_sms(self, slug, ctx, **kwargs):
        if not self.customer_phone:
            logger.exception(
                "Send sms called without customer_phone present for PK %s" % self.pk)
            return
        from sms.models import SMSAuthConfig
        ctx = self._get_notification_context(ctx=ctx)

        provider = SMSAuthConfig.get_path_for_current()

        if provider is not None:
            try:
                send_db_sms(slug,
                            self.customer_phone,
                            ctx,
                            language=self.language,
                            provider=provider,
                            **kwargs)
            except Exception as e:
                mail_logger = logging.getLogger('communications')
                mail_logger.exception("Error while sending sms %s" % e)
                # django_rq.enqueue(send_db_sms, slug, self.customer_phone, ctx, language=self.language, provider=SMSAuthConfig.get_path_for_current(), **kwargs)
        else:
            logger.error("SMS not sent because there is no SMS configuration active.")

    def get_latest_attempt(self):
        return self.attempts.latest()

    @transition(field='state', source=[CREATED, PENDING], target=PENDING, custom=dict(admin=False))
    def pending(self):
        """
        mark as pending
        """

    @transition(field='state', source=[CREATED, PENDING], custom=dict(admin=False), target=ATTEMPTED)
    def attempt(self):
        """
        mark as attempted
        """

    @transition(field='state', source=[CREATED, PENDING, ATTEMPTED],
                custom=dict(button_name=_("Acknowledge payment")),
                target=PAID)
    def acknowledge_payment(self, request, gateway_code=None, payment_attempt=None):
        from .. import signals
        signal_kwargs = {
            'sender': self.__class__,
            'instance': self,
            'request': request,
            'payment_attempt': payment_attempt
        }
        signals.payment_acknowledged.send_robust(**signal_kwargs)

    @transition(field='state', target=CANCELED,
                custom=dict(button_name=_("Cancel")), source=[CREATED, PENDING, ATTEMPTED])
    def cancel(self):
        """
        Cancel the transaction
        """

    @transition(field='state', source=[CREATED, PENDING], custom=dict(admin=False), target=FAILED)
    def failed(self):
        """
        mark as failed
        """

    @transition(field='state', target=EXPIRED,
                custom=dict(button_name=_("Expire")), source=[CREATED, PENDING, ATTEMPTED])
    def expire(self):
        """
        Expire the transaction
        """

    @transition(field='state', target=INVALID, custom=dict(admin=False),
                source=[CREATED, PENDING, ATTEMPTED])
    def invalidate(self, invalidation_reason):
        """
        Invalidate the transaction
        """
        self.invalidation_reason = invalidation_reason

    def disclose_to_merchant(self, disclosure_url=None):
        '''
        1. Must check if there is any attempt as this method may be invoked before
            any created attempts
        2. disclosure_url is optional as it may override the
            the initial disclosure_url saved in transaction
        '''
        if not self.has_payment_attempts():
            return False
        attempt = self.attempts.latest()
        disclosed = attempt.disclose_to_merchant(disclosure_url=disclosure_url)
        return disclosed

    def has_to_be_disclosed_to_merchant(self):
        if self.is_e_commerce():
            return True
        elif self.is_payment_request():
            from extras.dashboard.models import PaymentRequestFormConfig
            config = PaymentRequestFormConfig.get_solo()
            if config.disclose_to_merchant_url:
                return bool(self.disclosure_url)
        else:
            return False

    def concur_customer_view(self, request):
        if request.user.is_anonymous():
            if self.seen_at is None:
                self.seen_at = now()
                self.save()

    def can_be_cancelled(self):
        return self.state in [self.CREATED, self.PENDING, self.ATTEMPTED]

    def was_disclosed_to_merchant(self):
        if self.is_paid():
            attempt = self.get_paid_attempt()
            return attempt.disclosed_to_merchant
        else:
            return self.attempts.all().filter(disclosed_to_merchant=True).exists()

    def can_be_redirected_to_merchant(self):
        """
        Returns bool if the customer can be redirected to merchant page
        """
        return self.attempts.all().filter(disclosed_to_merchant=True,
                                          transaction__redirect_url__regex='[0-9a-zA-Z]').exclude(
            disclosure_url_error__regex='[0-9a-zA-Z]').exists()


class DeletedPaymentTransaction(PaymentTransaction):

    objects = DeletedTransactionManager()

    class Meta:
        ordering = ['-created']
        proxy = True
        verbose_name = _('Deleted Paid payment transaction')
        verbose_name_plural = _('Deleted Paid payment transactions')
        permissions = (
            ('view_deletedpaymenttransaction', _(
                'Can view deleted payment transactions')),
            ('restore_deletedpaymenttransaction', _(
                'Can restore deleted payment transactions')),
        )


class PaymentAttemptQuerySet(models.QuerySet):
    def pending(self):
        return self.filter(state=PaymentAttempt.PENDING)

    def non_pending(self):
        return self.exclude(state=PaymentAttempt.PENDING)


class PaymentAttemptManager(models.Manager):
    def get_queryset(self):
        return PaymentAttemptQuerySet(self.model, using=self._db)

    def pending(self):
        return self.get_queryset().pending()

    def non_pending(self):
        return self.get_queryset().non_pending()

    def create_with_amount(self, settings, amount, transaction=None):
        config = settings.currency_config
        currency_code = config.default_currency.code

        calculated_amount = config.compute_amount(amount, currency_code)
        fee = config.compute_fee(amount, currency_code)
        total = config.compute_total(amount, currency_code)
        data = dict(settings=settings, fee=fee,
                    amount=calculated_amount, total=total)
        if transaction:
            data.update(transaction=transaction)
        return self.create(**data)


class PaymentAttempt(TimeStampedModel):
    PENDING, SUCCESS, CANCELED, ERROR, FAILED, = 'pending', 'success', 'canceled', 'error', 'failed'
    STATE_CHOICES = (
        (PENDING, _("Pending")),
        (SUCCESS, _("Success")),
        # payment failed due to various reasons, like insufficient
        (FAILED, _("Failed")),
        (CANCELED, _("Canceled")),
        (ERROR, _("Error")),  # error occurred while creating the psp link

    )
    state = FSMField(_("State"), default=PENDING, db_index=True,
                     choices=STATE_CHOICES, protected=True)
    transaction = models.ForeignKey(
        PaymentTransaction, related_name='attempts')
    reference_number = NullCharField(
        _("Reference number"), unique=True,
        max_length=64, db_index=True)
    gateway_response = JSONField(
        verbose_name=_("Gateway response"), default={}, blank=True)
    settings = models.ForeignKey('gateway.GatewaySettings',
                                 verbose_name=_("Gateway Settings"),
                                 null=True,
                                 on_delete=models.SET_NULL)
    state_changed_at = MonitorField(monitor='state')
    fee = models.CharField(_("Fee"), max_length=120, blank=True, default='0')
    amount = models.CharField(
        _("Amount"), max_length=120, blank=True, default='0')
    total = models.CharField(
        _("Total"), max_length=120, blank=True, default='0')
    disclosed_to_merchant = models.BooleanField(
        _("Disclosed to merchant?"), default=False)
    message = models.TextField(_("Message"), default='',
                               blank=True,
                               help_text=_("Error message in case of failed payment"))

    disclosure_url_error = JSONField(
        _("Disclosure URL error"), default={},
        help_text=_("Logs the error reason of "
                    "merchant disclosure url.")
    )
    disclosed_data = JSONField(
        _("Disclosed data"), default={},
        help_text=_("Disclosed data to merchant disclosure URL.")
    )
    data = JSONField(verbose_name=_("Data"), default={},
                     editable=False,
                     help_text=_("Used to store useful information for"
                                 "internal purposes"))
    objects = PaymentAttemptManager()

    def __unicode__(self):
        return 'Attempt for {} - {}'.format(self.transaction, self.get_state_display())

    def save(self, *args, **kwargs):
        super(PaymentAttempt, self).save(*args, **kwargs)
        if not self.reference_number:
            self.reference_number = encode(self.pk)
            kwargs['force_insert'] = False
            super(PaymentAttempt, self).save(*args, **kwargs)

    def get_amount(self):
        amount = self.transaction.get_amount()
        amount = self.settings.currency_config.compute_total(
            amount=amount, from_code=self.transaction.currency_code)
        return amount

    class Meta:
        verbose_name = _("Payment attempt")
        verbose_name_plural = _("Payment attempts")
        ordering = ('-created', '-modified')
        get_latest_by = 'created'

    def get_payment_url(self):
        """
        To be called only once the user access payment page
        to avoid creating the URL before the user really wants to pay
        :return: PSP URL or fake PSP URL
        """
        return self.settings.create_payment_url(self)

    def get_details_url(self):
        activate(self.transaction.language)
        return reverse('ui:attempt_detail', args=(self.reference_number,))

    def disclose_to_merchant(self, disclosure_url=None):
        disclosed = False
        disclosure_url = self.transaction.disclosure_url if disclosure_url is None else disclosure_url
        try:
            merchant_payload = self.get_merchant_payload()
            logger.info("POSTing payload to merchant")
            self.disclosed_data[str(
                timezone.datetime.now())] = merchant_payload
            response = requests.post(disclosure_url, json=merchant_payload,
                                     verify=False, timeout=60)
            self.disclosed_to_merchant = True
            if response.status_code != requests.codes.ok:
                self.disclosure_url_error[str(
                    timezone.datetime.now())] = 'status code: %s, disclosure_url: %s ' % (
                        response.status_code, disclosure_url)
            else:
                disclosed = True
        except Exception as e:
            self.disclosure_url_error[str(
                timezone.datetime.now())] = 'exception: %s, disclosure_url: %s ' % (e, disclosure_url)

        self.save()
        return disclosed

    def get_merchant_payload(self):
        field_names = (
            'amount',
            'order_no',
            'currency_code',
            'bulk_id',
            'customer_first_name',
            'customer_last_name',
            'customer_email',
            'customer_phone',
            'customer_address_line1',
            'customer_address_line2',
            'customer_address_city',
            'customer_address_state',
            'customer_address_country',
            'customer_address_postal_code',
        )
        merchant_payload = {field_name: getattr(self.transaction, field_name) for
                            field_name in field_names if getattr(self.transaction, field_name)}
        merchant_payload.update({
            'gateway_account': self.settings.code,
            # do not change gateway_name before asking!!!
            'gateway_name': self.settings.gateway.name,
            'reference_number': self.reference_number,
            'result': self.state
        })
        if self.gateway_response:
            merchant_payload['gateway_response'] = self.gateway_response
        if bool(self.message):
            merchant_payload['message'] = self.message
        logger.info("Merchant built payload %s" % merchant_payload)
        return merchant_payload

    @transition(field='state', source=PENDING, target=CANCELED)
    def reject(self, gateway_response=None):
        if not self.transaction.is_e_commerce():
            if not self.transaction.is_attempted():
                self.transaction.attempt()
        else:
            self.transaction.cancel()
        self.transaction.save()
        if gateway_response:
            self.gateway_response = gateway_response

    def _update_post_return(self, gateway_response):
        if not self.transaction.gateway_code and self.transaction.is_paid():
            self.transaction.gateway_code = self.settings.code
            self.transaction.save()

        if gateway_response is not None:
            self.gateway_response = gateway_response

    @transition(field='state', source=PENDING, target=SUCCESS)
    def acknowledge_payment(self, request, gateway_response):
        # can't test as knpay is not available and migs get failed
        # if not self.transaction.has_gateway():
        #     self.transaction.is_sandbox = self.settings.is_sandbox()
        self.transaction.acknowledge_payment(request, payment_attempt=self)
        self.transaction.is_sandbox = self.settings.is_sandbox()
        self.transaction.save()
        self._update_post_return(gateway_response)

    @transition(field='state', source=PENDING, target=FAILED)
    def fail(self, gateway_response):
        if not self.transaction.is_payment_request():
            self.transaction.failed()
            self.transaction.save()
        self._update_post_return(gateway_response)

    def can_be_errored(self):
        # if you have trouble with errored word, check link below
        # https://english.stackexchange.com/questions/244410/how-is-the-past-tense-of-error-spelt-in-british-english
        return self.state == self.PENDING

    @transition(field='state', source=PENDING, target=ERROR)
    def error(self, error_message, gateway_response=None):
        if self.transaction.is_payment_request():
            if not self.transaction.is_attempted():
                self.transaction.attempt()
        else:
            self.transaction.failed()
        self.transaction.save()
        self.message = error_message
        self._update_post_return(gateway_response)

    def is_knet(self):
        from .gateway import Gateway
        return self.settings.gateway.name == Gateway.KNET

    def is_kpay(self):
        from .gateway import Gateway
        return self.settings.gateway.name == Gateway.KPAY

    def is_burgan(self):
        from .gateway import Gateway
        return self.settings.gateway.name == Gateway.BURGAN

    def is_migs(self):
        from .gateway import Gateway
        return self.settings.gateway.name == Gateway.MIGS

    def is_cs(self):
        from .gateway import Gateway
        return self.settings.gateway.name == Gateway.CYBERSOURCE

    def is_paypal(self):
        from .gateway import Gateway
        return self.settings.gateway.name == Gateway.PAYPAL

    def is_mpgs(self):
        from .gateway import Gateway
        return self.settings.gateway.name == Gateway.MPGS


@receiver(fsm_signals.post_transition, sender=PaymentAttempt)
def restore_quantity(sender, **kwargs):
    """
    If catalogue payment gets failed, errored, canceled then this receiver
    function will be called and reserved quantity of product will be restored.
    :param sender: PaymentAttempt class
    :param kwargs: payment-attempt instance, name, source(state), target(state)
    """
    payment_attempt = kwargs['instance']
    payment_transaction = payment_attempt.transaction
    if not payment_transaction.is_catalogue():
        return

    state = kwargs['target']  # target state

    if state in [PaymentAttempt.CANCELED, PaymentAttempt.FAILED,
                 PaymentAttempt.ERROR, PaymentAttempt.PENDING]:
        try:
            reserved_id = payment_transaction.extra.get('reserved_id')
            reserved_stock = ReservedStock.objects.get(pk=reserved_id)
            reserved_stock.product.stock += reserved_stock.quantity
            reserved_stock.product.save()
            reserved_stock.delete()
        except KeyError:
            logger.exception("Reserved stock id doesn't exist in transaction-id "
                             "%s" % payment_transaction.pk)
        except ReservedStock.DoesNotExist:
            logger.exception("Reserved stock object does not exist with "
                             "transaction-id %s" % payment_transaction.pk)


def restore_catalogue_stocks():
    """
    Will delete all the reserved stocks which are older than 8 minutes to
    current time
    """
    try:
        reserved_stocks = ReservedStock.objects.all()
        for rstock in reserved_stocks:
            reserved_at = rstock.reserved_at
            time_delta = timezone.now() - reserved_at
            if time_delta.total_seconds() > 480:
                rstock.product.stock += rstock.quantity
                rstock.product.save()
                rstock.delete()
        print("unused reserved stock deleted")
    except Exception as e:
        logger.exception("Restore catalogue stock error: %s" % e)


if django_settings.CATALOGUE_STOCK_RESTORE:
    scheduler = django_rq.get_scheduler('default')

    scheduler.cron("*/10 * * * *",
                   func=restore_catalogue_stocks,
                   queue_name='default'
                   )
