# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django import template
from django.utils.translation import ugettext_lazy as _

from config.models import About

register = template.Library()


@register.assignment_tag
def knpay_get_current_version():
    about = About.get_solo()
    about_message = _('KNPay version - %s') % about.version
    return about_message
