# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django import template

from utils.funcs import prepare_payment_payload, base_payment_payload

register = template.Library()


@register.simple_tag
def prepare_payload(payment_attempt):
    payload = prepare_payment_payload(payment_attempt)
    return payload


@register.simple_tag
def base_prepare_payload(payment_attempt):
    payload = base_payment_payload(payment_attempt)
    return payload
