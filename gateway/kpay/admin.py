# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib import admin
from polymorphic.admin import PolymorphicChildModelAdmin
from .models import KPaySettings


@admin.register(KPaySettings)
class KPaySettingsAdmin(PolymorphicChildModelAdmin):
    base_model = KPaySettings
    prepopulated_fields = {'code': ('name',)}
    save_as = True
