# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

from django.core.urlresolvers import reverse
from django.db import models
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

from api.func import get_knpay_url
from utils.funcs import is_ascii
from utils.validators import FileValidator
from gateway.models import GatewaySettings
from extras.currency.data import currencies
from .client import KPayClient


logger = logging.getLogger('gateway')


def get_upload_path(instance, filename):
    filename, dot, extension = filename.rpartition('.')
    return 'gateway/kpay/{}/{}.{}'.format(instance.pk, slugify(filename), extension)


class KPaySettings(GatewaySettings):
    alias = models.CharField(_("Alias"), max_length=32,
                             help_text=_("Alias name if the resource file contains "
                                         "multiple aliases, if not leave it empty."))
    enable_inquiry = models.BooleanField(_("Enable inquiry?"), default=True,
                                         help_text=_("Enable callback check after "
                                                     "transaction stage ends."))
    resource = models.FileField(_("Resource file"), upload_to=get_upload_path,
                                validators=[FileValidator(allowed_extensions=['cgn'])])
    keystore = models.FileField(_("Keystore file"), upload_to=get_upload_path,
                                     validators=[FileValidator(allowed_extensions=['bin'])])

    class Meta:
        verbose_name = _("KPay")
        verbose_name_plural = _("KPay")
        permissions = (
            ('use_kpaysettings', _("Can use KPay")),
        )

    def callback_is_enabled(self):
        return self.enable_inquiry

    def get_field_mapping(self):
        return {
            'udf1': ['order_no'],
            'udf2': ['customer_email'],
            'udf3': ['customer_phone'],
            'udf4': ['customer_first_name', 'customer_last_name'],
        }

    def get_udf_payload(self, payment_attempt):
        mapping = self.get_field_mapping()
        payload = {}
        for udf, field_names in mapping.items():
            field_value = ' '.join([getattr(payment_attempt.transaction, field_name)
                                    for field_name in field_names])

            if len(field_value) > 1:
                if is_ascii(field_value):
                    if '+' in field_value:
                        field_value = field_value.replace('+', '')
                    payload[udf] = field_value
        return payload

    @staticmethod
    def create_payment_url(payment_attempt):
        client = KPayClient(payment_attempt)
        payment_url = client.connect()
        if payment_url is not None:
            return payment_url
        error_message = "Communication error between KNPay and gateway. " \
                        "Please report this issue to KNPay administrator."
        payment_attempt.error(error_message=error_message)
        payment_attempt.save()

    def get_language(self, payment_attempt=None):
        language = payment_attempt.transaction.language
        return 'ENG' if language == 'en' else 'ARA'

    def get_currency(self, payment_attempt=None):
        return currencies[self.currency_config.default_currency.code]

    def get_error_url(self, payment_attempt=None):
        # URL where KPay will POST error details if happens (extremely rare)
        return '{knpay_url}{path}'.format(knpay_url=get_knpay_url(),
                                          path=reverse('kpay:error', args=(payment_attempt.reference_number,)))

    def get_response_url(self, payment_attempt=None):
        # URL where KPay will POST payment details (no matter is success or failure)
        return '{knpay_url}{path}'.format(knpay_url=get_knpay_url(),
                                          path=reverse('kpay:response', args=(payment_attempt.reference_number,)))

