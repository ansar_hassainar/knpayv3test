# # -*- coding: utf-8 -*-

import binascii
import logging
import re
from urlparse import parse_qsl
import zipfile
from collections import OrderedDict

from django.utils.translation import ugettext_lazy as _

import requests
from Crypto.Cipher import DES3, AES
from Crypto.Util.Padding import pad
from cStringIO import StringIO
from xmltodict import parse

from gateway.client import BaseClient

logger = logging.getLogger('gateway')


class KPayClient(BaseClient):
    name = _("KPay")
    _parsed_resource = dict()

    def __init__(self, *args, **kwargs):
        super(KPayClient, self).__init__(*args, **kwargs)
        self.resource_file = self.settings.resource
        self.key = self._get_key_from_key_store()
        self._parse()

    def _get_key_from_key_store(self):
        import jks
        ks = jks.KeyStore.load(self.settings.keystore.path, 'password')
        sk = ks.secret_keys['pgkey']
        return "".join("{:02x}".format(b) for b in bytearray(sk.key)).decode('hex')

    def _decrypt(self, encrypted_str):
        cipher = DES3.new(str(self.key), DES3.MODE_ECB)
        return cipher.decrypt(encrypted_str)

    def _parse(self):
        # parse cgn
        with open(self.resource_file.path, 'rb') as cgn_file:
            encrypted_str = cgn_file.read()

        decrypted_str = self._decrypt(encrypted_str)
        memory_file = StringIO()
        memory_file.write(decrypted_str)

        try:
            zip_file = zipfile.ZipFile(memory_file)
        except zipfile.BadZipfile:
            raise zipfile.BadZipfile

        xml = self._decrypt(zip_file.read('%s.xml' % self.settings.alias))
        regex = re.compile('<*.?><terminal><id>(.*?)</id><password>(.*?)</password>'
                           '<resourceKey>(.*?)</resourceKey><webaddress>(.*?)</webaddress></terminal>')
        keys = regex.findall(xml)[0]
        # Parameters found inside the resource file
        resource_kwargs = ('id', 'password', 'resourceKey', 'webaddress')

        self._parsed_resource = dict(zip(resource_kwargs, keys))
        self._resource_key = self._parsed_resource['resourceKey']

    def _build_payload(self):
        # Don't change the order of the values!
        data = OrderedDict()

        data['amt'] = self.payment_attempt.get_amount()
        data['action'] = 1  # Payment action on KPay side
        data['responseURL'] = self.settings.get_response_url(payment_attempt=self.payment_attempt)
        data['errorURL'] = self.settings.get_error_url(self.payment_attempt)
        data['trackid'] = self.payment_attempt.reference_number

        # Add the UDF values to the data connection dictionary
        # Disallowed characters are substituted with '-'
        disallowed = ['~', '`', '!', '#', '$', '%', '^', '|', '\\', ':', '\'', '"', '/',
                      '<', '>', '(', ')', '{', '}', '[', '?', ';', '&', '*', '\]']
        for udf, value in self.settings.get_udf_payload(self.payment_attempt).items():
            data[udf] = re.sub('[%s]' % ''.join(disallowed), '-', value)

        data['currencycode'] = self.payment_attempt.settings.get_currency()
        data['langid'] = self.settings.get_language(payment_attempt=self.payment_attempt)
        data['id'] = self._parsed_resource['id']
        data['password'] = self._parsed_resource['password']
        logger.info("KPay prepared payload {}".format(data))
        return data

    def _get_cipher(self):
        # iv must be 16 bytes length, if some length issue, pad func can be used increase
        # the size
        iv = key = self._resource_key
        return AES.new(key, AES.MODE_CBC, iv)

    def _encrypt_aes(self, querystring):
        cipher = self._get_cipher()
        encrypted_buffer = cipher.encrypt(pad(querystring, 16))
        hex_str = binascii.hexlify(bytearray(encrypted_buffer))

        return hex_str.upper()

    def decrypt_aes(self, data):
        cipher_text = binascii.unhexlify(data)
        cipher = self._get_cipher()
        parsed_data = cipher.decrypt(cipher_text)
        return dict(parse_qsl(parsed_data))

    def connect(self):
        """
        Returns the URL for redirecting to KPay
        """
        payload = self._build_payload()
        querystring = ''.join(['%(key)s=%(val)s&' % {'key': key, 'val': val}
                               for key, val in payload.items()])

        trandata = self._encrypt_aes(querystring)

        # Don't change this! It's replica 100% from Java SDK
        url = "%(webaddress)s/PaymentHTTP.htm?" \
              "param=paymentInit&trandata=%(trandata)s&errorURL=%(errorURL)" \
              "s&responseURL=%(responseURL)s&tranportalId=%(tranportalId)s" % {
                  'webaddress': self._parsed_resource['webaddress'],
                  'trandata': trandata,
                  'errorURL': payload['errorURL'],
                  'responseURL': payload['responseURL'],
                  'tranportalId': self._parsed_resource['id']
              }
        logger.info("KPay prepared url %s" % url)
        return url

    def callback(self):
        """
        callback for transaction status
        """

        body = "<request><transid>%(transid)s</transid><amt>%(amt)s</amt>" \
               "<action>8</action><id>%(id)s</id><trackid>%(trackid)s</trackid><udf5>TrackID</udf5>" \
               "<password>%(password)s</password><errorURL></errorURL>" \
               "<responseURL></responseURL></request>" % {
                   'transid': self.payment_attempt.reference_number,
                   'amt': self.payment_attempt.get_amount(),
                   'id': self._parsed_resource['id'],
                   'trackid': self.payment_attempt.reference_number,
                   'password': self._parsed_resource['password'],
               }
        logger.info("Request body for callback attempt %s - %s" % (body, self.payment_attempt.reference_number))

        url = self._parsed_resource['webaddress'] + '/tranPipe.htm?param=tranInit'

        try:
            r = requests.post(url, data=body, headers={'Content-Type': 'application/xml'})
            logger.info("Response content for inquiring attempt %s - %s" % (
                self.payment_attempt.reference_number, r.content))
        except Exception as e:
            logger.exception("Error while inquiring attempt %s - %s" % (self.payment_attempt.reference_number, e))
            return False, {}

        try:
            if r.status_code == requests.codes.ok:
                gateway_response = dict(parse("<root>%s</root>" % r.content)['root'])
                return True, gateway_response
            else:
                return False, {}
        except Exception as e:
            logger.exception("Error in KPay response for "
                             "inquiring attempt %s - %s" % (self.payment_attempt.reference_number, e))
            return False, {}

