# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

from django.db import transaction
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic
from django.views.generic.detail import SingleObjectMixin
from django.utils.translation import ugettext as _

from braces.views import CsrfExemptMixin

from utils.views.mixins import PSPResponseMixin
from gateway.models import PaymentAttempt
from .client import KPayClient


logger = logging.getLogger('gateway')


class HandleResponseMixin(object):
    def handle_data(self, data):
        pass


class ResponseFromKPayView(CsrfExemptMixin,
                           SingleObjectMixin,
                           HandleResponseMixin,
                           generic.View):
    """
    Handle KPay response
    """
    queryset = PaymentAttempt.objects.pending()
    slug_url_kwarg = 'reference_number'
    slug_field = 'reference_number'

    def _update_state(self, data, decrypt=True):
        with transaction.atomic():
            url = self.handle_psp_response(data, decrypt=decrypt)
            return HttpResponseRedirect(url)

    def get_redirect_url(self):
        url = self.request.build_absolute_uri(reverse('kpay:redirect', args=(self.object.reference_number,)))
        # response_url = 'REDIRECT={}'.format(url)
        logger.info("REDIRECT URL %s for attempt %s" % (url, self.object.reference_number))
        return url

    def post(self, request, *args, **kwargs):
        payload = request.POST.copy()

        logger.info("KPay payload sent to response URL {}".format(payload))
        self.object = self.get_object()
        try:
            # don't change this ever, this is how value is being sent in raw body
            # DENIED+BY+RISK
            if payload.get('result') == 'DENIED BY RISK':
                # DENIED BY RISK case, when payload is sent decrypted
                return self._update_state(payload, decrypt=False)
            else:
                return self._update_state(payload.get('trandata', {}), decrypt=True)
        except Exception as e:
            logger.exception("Updating kpay payment attempt failed due to %s" % e)
            # refresh from db
            self.object = self.get_object()
            url = self.get_redirect_url()
            if self.object.settings.callback_is_enabled():

                self.object.data['initial_response'] = "Error: %s" % e
                self.object.save()
                return HttpResponseRedirect(url, 'text/plain')
            else:
                self.object.error(str(e))
                self.object.save()
                return HttpResponseRedirect(url)

    def handle_psp_response(self, trandata, decrypt=True):
        client = KPayClient(self.object)
        data = client.decrypt_aes(trandata) if decrypt else trandata

        url = self.get_redirect_url()

        if self.object.settings.callback_is_enabled():
            # don't take any actions on the transaction state.
            # it will be updated once callback is done, when user is returning
            # from gateway
            self.object.data['initial_response'] = data
            self.object.save()
            return url

        else:
            if data.get('result') == 'CAPTURED':
                self.object.acknowledge_payment(self.request, data)

            elif data.get('ErrorText') or data.get('Error'):
                error_message = data.get('ErrorText', data.get('Error', ''))
                self.object.error(error_message, gateway_response=data)

            elif data.get('result') == 'NOT CAPTURED':
                # payment failed due to insufficient funds, etc.
                self.object.fail(data)

            else:
                self.object.reject(data)

            self.object.save()

            return url


class ErrorResponseFromKPayView(CsrfExemptMixin,
                                PSPResponseMixin,
                                SingleObjectMixin,
                                generic.View):
    queryset = PaymentAttempt.objects.all()
    slug_url_kwarg = 'reference_number'
    slug_field = 'reference_number'
    http_method_names = ['get', 'post']

    def post(self, request, *args, **kwargs):
        data = request.POST.copy()
        logger.info("KPay payload sent to error URL {}".format(data))
        self.object = self.get_object()
        return self.handle_psp_response(data)

    def handle_psp_response(self, data):
        if self.object.can_be_errored():
            self.object.error("KPay Error", gateway_response=data)
        else:
            # we can't mark as error twice.
            logger.exception('Redirect to error KPay page %s' % self.object.reference_number)
            msg = 'Redirect to error KPay page'
            self.object.message += '\nNew error msg: %s - %s' % (msg, data)

        self.object.save()
        url = reverse('kpay:redirect', args=(self.object.reference_number,))
        return HttpResponseRedirect(url)


class RedirectFromKPayView(CsrfExemptMixin,
                           PSPResponseMixin,
                           SingleObjectMixin,
                           generic.View):
    """
    View for handling successful/unsuccessful redirects from KPay Gateway.

    For transactions created via API we are POSTing back to the customer
    server transaction details - variable data is holding it -. Server
    replies with an URL where the user shall be redirected. More
    details below on return description.

    # E-Commerce transactions:
        # Notify the disclosure_url with payment details as JSON
        # if merchant's server returns 200 status, redirect the customer to redirect_url
        # else redirect to KNPay landing page with payment details
    """
    queryset = PaymentAttempt.objects.all()
    slug_url_kwarg = 'reference_number'
    slug_field = 'reference_number'

    def handle_psp_response(self, data):
        attempt = self.object
        if not attempt.data.get('post_attempt_notifications_sent', False):
            if attempt.settings.callback_is_enabled():
                client = KPayClient(attempt)
                status, callback_payload = client.callback()

                attempt.data['callback_payload'] = callback_payload

                payload = attempt.data.get('initial_response', None)
                if payload is None or not isinstance(payload, dict):
                    payload = callback_payload

                if status:
                    if payload.get('result') in ['SUCCESS', 'CAPTURED']:
                        attempt.acknowledge_payment(self.request, payload)
                    elif 'ERROR' in payload.get('result', ''):
                        error_message = payload.get('error_code_tag')
                        attempt.error(error_message, gateway_response=payload)
                    elif 'NOT CAPTURED' in payload.get('result'):
                        # payment failed due to insufficient funds, etc.
                        attempt.fail(payload)
                    elif 'SUSPECT' in payload.get('result'):
                        error_message = _("Please verify your bank statement before retrying the payment.")
                        attempt.error(error_message, gateway_response=payload)
                    else:
                        attempt.reject(gateway_response=payload)
                else:
                    attempt.error("KPay Error", gateway_response=payload)
                    attempt.message += _("Please verify your bank statement before retrying the payment.")

                attempt.save()
            attempt.transaction.send_post_attempt_notifications(attempt)
        return self.redirect()
