# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib import admin
from polymorphic.admin import PolymorphicChildModelAdmin
from .models import CyberSourceSettings


@admin.register(CyberSourceSettings)
class CyberSourceSettingsAdmin(PolymorphicChildModelAdmin):
    base_model = CyberSourceSettings
    prepopulated_fields = {'code': ('name',)}
    save_as = True
