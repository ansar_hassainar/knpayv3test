# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import base64
import hashlib
import hmac
import time
import uuid

from django.db import models
from django.utils.translation import ugettext_lazy as _

from gateway.models import GatewaySettings
from .client import CyberSourceClient


class CyberSourceSettings(GatewaySettings):
    """
    CyberSource settings model
    """
    # management
    manual_submission = models.BooleanField(
        _("Manual submission?"), default=False,
        help_text=_("Define if payment form shall be submitted manually. "
                    "Useful for test purposes.")
    )
    # CONSTANTS
    production_url = 'https://secureacceptance.cybersource.com/pay'
    sandbox_url = 'https://testsecureacceptance.cybersource.com/pay'

    # FIELDS
    merchant_id = models.CharField(_("Merchant ID"), max_length=256)
    profile_id = models.CharField(_("Profile ID"), max_length=256)
    access_key = models.CharField(_("Access key"), max_length=256)
    secret_key = models.TextField(_("Secret key"), max_length=256)

    # MDD Fields
    merchant_name = models.CharField(_("Merchant Name"), default='',
                                     max_length=256)
    product_category = models.CharField(_("Product Category"), default='',
                                        max_length=100)

    def __unicode__(self):
        return '%s - %s' % (self.merchant_id, self.profile_id)

    class Meta:
        verbose_name = _("CyberSource")
        verbose_name_plural = _("CyberSource")
        permissions = (
            ('use_cybersourcesettings', _("Can use CyberSource")),
        )

    @property
    def payment_url(self):
        return getattr(self, "%s_url" % self.type)

    @staticmethod
    def create_payment_url(payment_attempt):
        client = CyberSourceClient(payment_attempt=payment_attempt)
        url = client.connect()
        return url

    def get_field_mapping(self):
        pass

    def get_currency(self, payment_attempt=None):
        pass

    def get_language(self, payment_attempt=None):
        if payment_attempt.transaction.language == 'en':
            return 'en-us'
        return 'ar-xn'

    @staticmethod
    def get_form_data(payment_attempt):
        config = payment_attempt.settings
        data = {
            'access_key': config.access_key,
            'profile_id': config.profile_id,
            'payment_method': 'card',
            'transaction_type': 'sale',
            'ignore_avs': 'true',
            'transaction_uuid': uuid.uuid4(),
            'signed_date_time': time.strftime('%Y-%m-%dT%H:%M:%SZ', time.gmtime()),
            'signed_field_names': 'access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,'
                                  'signed_date_time,locale,transaction_type,reference_number,amount,ignore_avs,'
                                  'currency,payment_method',
            'unsigned_field_names': 'signature,bill_to_forename,bill_to_surname,bill_to_email,bill_to_phone,'
                                    'bill_to_address_line1,bill_to_address_city,bill_to_address_state,'
                                    'bill_to_address_country,bill_to_address_postal_code,customer_ip_address,'
                                    'merchant_defined_data1,merchant_defined_data2,merchant_defined_data3,'
                                    'merchant_defined_data4,merchant_defined_data5,merchant_defined_data6,'
                                    'merchant_defined_data7,merchant_defined_data8,merchant_defined_data20',
            'amount': payment_attempt.get_amount(),
            'locale': config.get_language(payment_attempt),
            'reference_number': payment_attempt.reference_number,
            'currency': config.currency_config.default_currency.code,
            'bill_to_email': payment_attempt.transaction.customer_email,
            'bill_to_surname': payment_attempt.transaction.customer_last_name,
            'bill_to_forename': payment_attempt.transaction.customer_first_name,
            'bill_to_address_line1': payment_attempt.transaction.customer_address_line1,
            'bill_to_address_city': payment_attempt.transaction.customer_address_city,
            'bill_to_address_country': payment_attempt.transaction.customer_address_country,
            # mdd fields
            'merchant_defined_data1': 'WC',  # Web Channel Operation
            'merchant_defined_data2': config.merchant_name,
            'merchant_defined_data3': config.product_category,
            'merchant_defined_data4': payment_attempt.transaction.extra.get(
                'product_description', ''),  # Product Name
            'merchant_defined_data5': 'NO',  # Previous Customer
            'merchant_defined_data6': 'Merchant Delivery',  # Shipping Method
            'merchant_defined_data7': payment_attempt.transaction.extra.get(
                'items_sold', ''),  # Number of items sold
            'merchant_defined_data8': payment_attempt.transaction.extra.get(
                'shipping_country', 'KW'),  # Shipping country
            'merchant_defined_data20': 'NO',  # VIP customer
        }
        signature = config.sign(data)
        data['signature'] = signature
        return data

    def sign(self, payload):
        keys = payload['signed_field_names'].split(',')
        message = ','.join(['{key}={value}'.format(
            key=key, value=payload[key]) for key in keys])
        message = message.encode('utf-8')
        secret = self.secret_key.encode('utf-8')
        digested = hmac.new(secret, msg=message, digestmod=hashlib.sha256).digest()
        signature = base64.b64encode(digested).decode()
        return signature

    def get_reason(self, payload):
        code = payload.get('reason_code')
        return self.CYBERSOURCE_RESPONSES.get(code)

    def get_error_url(self, payment_attempt=None):
        """
        Not required here. Defined in CyberSource dashboard
        """

    def get_response_url(self, payment_attempt=None):
        """
        Not required here. Defined in CyberSource dashboard
        """

    def get_merchant_identifier(self, payment_attempt=None):
        pass

    CYBERSOURCE_RESPONSES = {
        '100': 'Successful transaction.',
        '101': 'The request is missing one or more required fields.',
        '102': 'One or more fields in the request contains invalid data.',
        '104': 'The merchantReferenceCode sent with this authorization '
               'request matches the merchantReferenceCode of'
               ' another authorization request that you sent in the last 15 minutes.',
        '110': 'Only a partial amount was approved.',
        '150': 'Error: General system failure. ',
        '151': 'Error: The request was received but there was a server timeout. '
               'This error does not include timeouts '
               'between the client and the server.',
        '152': 'Error: The request was received, but a service did not finish running in time.',
        '201': 'The issuing bank has questions about the request. '
               'You do not receive an authorization code in the reply '
               'message, but you might receive one verbally by calling the processor.',
        '202': 'Expired card. You might also receive this if the expiration date '
               'you provided does not match the date '
               'the issuing bank has on file.',
        '203': 'General decline of the card. No other information provided by the issuing bank.',
        '204': 'Insufficient funds in the account.',
        '205': 'Stolen or lost card.',
        '207': 'Issuing bank unavailable.',
        '208': 'Inactive card or card not authorized for card-not-present transactions.',
        '210': 'The card has reached the credit limit. ',
        '211': 'Invalid card verification number.',
        '221': 'The customer matched an entry on the processor\'s negative file.',
        '231': 'Invalid account number.',
        '232': 'The card type is not accepted by the payment processor.',
        '233': 'General decline by the processor.',
        '234': 'There is a problem with your CyberSource merchant configuration.',
        '235': 'The requested amount exceeds the originally authorized amount. Occurs, '
               'for example, if you try to capture an amount larger than the original '
               'authorization amount. This reason code only applies if you are '
               'processing a capture through the API.',
        '236': 'Processor Failure',
        '238': 'The authorization has already been captured. '
               'This reason code only applies if you are processing a capture '
               'through the API.',
        '239': 'The requested transaction amount must match the '
               'previous transaction amount. This reason code only applies '
               'if you are processing a capture or credit through the API.',
        '240': 'The card type sent is invalid or does not correlate with the credit card number.',
        '241': 'The request ID is invalid. This reason code only applies when you are '
               'processing a capture or credit through the API.',
        '242': 'You requested a capture through the API, but there is no corresponding, '
               'unused authorization record. Occurs if there was not a previously '
               'successful authorization request or if the previously successful authorization '
               'has already been used by another capture request. This reason code '
               'only applies when you are processing a capture through the API.',
        '243': 'The transaction has already been settled or reversed.',
        '246': 'The capture or credit is not voidable because the capture '
               'or credit information has already been submitted '
               'to your processor. Or, you requested a void for a '
               'type of transaction that cannot be voided. This reason '
               'code applies only if you are processing a void through the API.',
        '247': 'You requested a credit for a capture that was previously '
               'voided. This reason code applies only if you are '
               'processing a void through the API.',
        '250': 'Error: The request was received, but there was a timeout at the payment processor.',
        '520': 'The authorization request was approved by the issuing bank but '
               'declined by CyberSource based on your Smart '
               'Authorization settings.',
    }
