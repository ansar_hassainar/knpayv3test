# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging

from django.http import Http404
from django.views import generic
from django.views.generic.detail import SingleObjectMixin

from braces.views import CsrfExemptMixin
from ipware.ip import get_ip

from gateway.models import PaymentAttempt
from utils.views.mixins import PSPResponseMixin
from .forms import PaymentForm

logger = logging.getLogger('gateway')


class RedirectToCyberSourceView(SingleObjectMixin,
                                generic.FormView):
    """
    Fake native PSP redirection by rendering payment
    form and submit the customer to PSP
    """
    model = PaymentAttempt
    queryset = PaymentAttempt.objects.pending()
    form_class = PaymentForm
    slug_url_kwarg = 'reference_number'
    slug_field = 'reference_number'
    template_name = 'cs/redirect_to_cs.html'
    http_method_names = ['get']

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(RedirectToCyberSourceView, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(RedirectToCyberSourceView, self).get_form_kwargs()
        data = self.object.settings.get_form_data(self.object)
        data['customer_ip_address'] = get_ip(self.request)
        kwargs.update({'data': data})
        return kwargs

    def get_context_data(self, **kwargs):
        ctx = super(RedirectToCyberSourceView, self).get_context_data(**kwargs)
        ctx.update({
            'payment_url': self.object.settings.payment_url,
            'settings': self.object.settings
        })
        return ctx


class CyberSourceResponseMixin(CsrfExemptMixin,
                               PSPResponseMixin,
                               generic.View):
    """
    Handle POST requests from CyberSource
    """
    http_method_names = ['post']

    def get_object(self):
        try:
            return PaymentAttempt.objects.pending().get(reference_number=self.reference_number)
        except PaymentAttempt.DoesNotExist:
            logger.info("Attempt with ref no %s not found" % self.reference_number)
            raise Http404()

    def assess_decision(self, data, decision):
        raise NotImplementedError

    def handle_psp_response(self, data):
        expected_signature = self.object.settings.sign(data)
        if expected_signature != data.get('signature'):
            logger.info("Signing failed for ref no %s" % self.reference_number)
            raise Http404()

        decision = data.get('decision')
        self.assess_decision(data, decision)

        reason = self.object.settings.get_reason(data)
        logger.info("CS reason for ref no %s: %s" % (self.object.reference_number, reason))
        self.object.save()

        self.object.transaction.send_post_attempt_notifications(self.object)

        return self.redirect()

    def post(self, request, *args, **kwargs):
        data = request.POST.dict()
        self.reference_number = data.get('req_reference_number')
        self.object = self.get_object()
        logger.info("CyberSource payload sent to response URL (%s) %s" % (self.__class__.__name__, data))
        return self.handle_psp_response(data)


class CompleteView(CyberSourceResponseMixin):
    """
    Transaction Response Page
        Hosted by you
    """
    def assess_decision(self, data, decision):
        if decision == 'ACCEPT':
            self.object.acknowledge_payment(self.request, data)
        else:
            message = data.get('message', '')
            self.object.error(error_message=message)
            self.object.gateway_response = data


class CancelView(CyberSourceResponseMixin):
    """
    Custom Cancel Response Page
        Hosted by you
    """
    def assess_decision(self, data, decision):
        self.object.reject(data)
