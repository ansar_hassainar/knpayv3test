# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-03-13 06:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cybersource', '0004_auto_20171211_1705'),
    ]

    operations = [
        migrations.AddField(
            model_name='cybersourcesettings',
            name='merchant_name',
            field=models.CharField(default='', max_length=256, verbose_name='Merchant Name'),
        ),
    ]
