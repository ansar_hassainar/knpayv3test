# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django import forms


class PaymentForm(forms.Form):
    """
    Basic payment form which handles only required fields for
    Cybersource Secure Acceptance Web/Mobile.
    """
    access_key = forms.CharField()
    profile_id = forms.CharField()
    transaction_uuid = forms.CharField()
    signed_field_names = forms.CharField()
    unsigned_field_names = forms.CharField()
    signed_date_time = forms.CharField()
    signature = forms.CharField()
    ignore_avs = forms.CharField()
    locale = forms.CharField()
    transaction_type = forms.CharField()
    reference_number = forms.CharField()
    amount = forms.DecimalField()
    currency = forms.CharField()
    payment_method = forms.CharField()
    bill_to_forename = forms.CharField()
    bill_to_surname = forms.CharField()
    bill_to_email = forms.CharField()
    bill_to_phone = forms.CharField()
    bill_to_address_line1 = forms.CharField()
    bill_to_address_city = forms.CharField()
    bill_to_address_state = forms.CharField()
    bill_to_address_country = forms.CharField()
    bill_to_address_postal_code = forms.CharField()
    customer_ip_address = forms.CharField()
    merchant_defined_data1 = forms.CharField()
    merchant_defined_data2 = forms.CharField()
    merchant_defined_data3 = forms.CharField()
    merchant_defined_data4 = forms.CharField()
    merchant_defined_data5 = forms.CharField()
    merchant_defined_data6 = forms.CharField()
    merchant_defined_data7 = forms.CharField()
    merchant_defined_data8 = forms.CharField()
    merchant_defined_data20 = forms.CharField()

    def __init__(self, *args, **kwargs):
        super(PaymentForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].required = False

