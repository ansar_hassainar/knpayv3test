# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^rtcs/(?P<reference_number>[0-9A-Za-z_\-]+)/$',
        views.RedirectToCyberSourceView.as_view(), name='redirect_to_cs'),

    # CyberSource Customer Response Pages
    url(r'^complete/$', views.CompleteView.as_view(), name='complete'),
    url(r'^cancel/$', views.CancelView.as_view(), name='cancel'),
]
