# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib.sites.models import Site

from gateway.client import BaseClient


class CyberSourceClient(BaseClient):
    def connect(self):
        site = Site.objects.get_current()
        path = reverse('cs:redirect_to_cs', args=(self.payment_attempt.reference_number,))
        return '%s://%s%s' % (settings.PROTOCOL, site.domain, path)

    def _build_payload(self):
        pass
