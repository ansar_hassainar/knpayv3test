# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-02-15 09:44
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import django_extensions.db.fields
import django_fsm
import jsonfield.fields
import model_utils.fields
import utils.db.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('currency', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Gateway',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Modified')),
                ('is_active', models.BooleanField(db_index=True, default=True, help_text='Designates if this object is active, can be used and displayed on site.', verbose_name='Is active?')),
                ('name', models.CharField(choices=[('knet', 'Knet'), ('burgan', 'Burgan'), ('migs', 'MIGS'), ('cybersource', 'CyberSource')], help_text='Name of the gateway you want to register', max_length=32, unique=True, verbose_name='Name')),
                ('logo', models.ImageField(blank=True, help_text='Logo to be used in emails/confirmation pages/etc', null=True, upload_to='gateway/logos', verbose_name='Logo')),
            ],
            options={
                'verbose_name': 'Gateway',
                'verbose_name_plural': 'Gateways',
            },
        ),
        migrations.CreateModel(
            name='GatewaySettings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Modified')),
                ('is_active', models.BooleanField(db_index=True, default=True, help_text='Designates if this object is active, can be used and displayed on site.', verbose_name='Is active?')),
                ('type', models.CharField(choices=[('sandbox', 'Sandbox'), ('production', 'Production')], default='sandbox', max_length=32, verbose_name='Type')),
                ('name', models.CharField(help_text="Name to be displayed in dropdowns or anywhere else the settings are being shown. IE: 'Knet' or 'Knet demo'", max_length=16, verbose_name='Name')),
                ('code', django_extensions.db.fields.AutoSlugField(blank=True, editable=False, help_text='Code to identify the settings in the API/URLs/etc', max_length=16, populate_from='name', unique=True, verbose_name='Code')),
                ('display_gateway_response', models.BooleanField(default=False, help_text='Designates if the customer shall see the raw gateway response after payment stage completes. Some PSP policies are requiring this.', verbose_name='Display gateway response?')),
                ('currency_config', models.ForeignKey(help_text='Select currency exchange configuration for this account', on_delete=django.db.models.deletion.CASCADE, to='currency.ExchangeConfig', verbose_name='Currency configuration')),
                ('gateway', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='gateway.Gateway', verbose_name='Gateway')),
                ('polymorphic_ctype', models.ForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='polymorphic_gateway.gatewaysettings_set+', to='contenttypes.ContentType')),
            ],
            options={
                'verbose_name': 'Settings',
                'verbose_name_plural': 'Settings',
            },
        ),
        migrations.CreateModel(
            name='PaymentAttempt',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Modified')),
                ('state', django_fsm.FSMField(choices=[('pending', 'Pending'), ('success', 'Success'), ('canceled', 'Canceled'), ('error', 'Error')], db_index=True, default='pending', max_length=50, protected=True, verbose_name='State')),
                ('reference_number', utils.db.fields.NullCharField(db_index=True, max_length=64, unique=True, verbose_name='Reference number')),
                ('gateway_response', jsonfield.fields.JSONField(blank=True, default='{}', verbose_name='Gateway response')),
                ('state_changed_at', model_utils.fields.MonitorField(blank=True, default=django.utils.timezone.now, monitor='state')),
                ('fee', models.CharField(blank=True, default='0', max_length=120, verbose_name='fee')),
                ('amount', models.CharField(blank=True, default='0', max_length=120, verbose_name='amount')),
                ('total', models.CharField(blank=True, default='0', max_length=120, verbose_name='total')),
                ('disclosed_to_merchant', models.BooleanField(default=False, verbose_name='Disclosed to merchant?')),
                ('message', models.CharField(blank=True, default='', help_text='Error message in case of failed payment', max_length=200, verbose_name='Message')),
                ('disclosure_url_error', models.TextField(blank=True, default='', help_text='Logs the error reason of merchant disclosure url.', verbose_name='Disclosure URL error')),
                ('settings', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gateway.GatewaySettings', verbose_name='Gateway Settings')),
            ],
            options={
                'ordering': ('-created', '-modified'),
                'get_latest_by': 'created',
                'verbose_name': 'Payment attempt',
                'verbose_name_plural': 'Payment attempts',
            },
        ),
        migrations.CreateModel(
            name='PaymentTransaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Modified')),
                ('state', django_fsm.FSMField(choices=[('created', 'Created'), ('pending', 'Pending'), ('attempted', 'Attempted'), ('paid', 'Paid'), ('canceled', 'Canceled'), ('expired', 'Expired'), ('invalid', 'Invalid')], db_index=True, default='created', max_length=50, protected=True, verbose_name='State')),
                ('type', models.CharField(choices=[('e_commerce', 'E-Commerce'), ('payment_request', 'Payment request'), ('customer_payment', 'Customer payment'), ('third_party', 'Third party')], default='e_commerce', max_length=24, verbose_name='Type')),
                ('gateway_code', models.CharField(blank=True, db_index=True, default='', max_length=16, verbose_name='Gateway code')),
                ('amount', models.CharField(max_length=24, verbose_name='Amount')),
                ('language', models.CharField(choices=[(b'en', 'English'), (b'ar', 'Arabic')], default=b'en', max_length=2, verbose_name='Language')),
                ('currency_code', models.CharField(choices=[('DZD', 'DZD'), ('QAR', 'QAR'), ('BGN', 'BGN'), ('BMD', 'BMD'), ('PAB', 'PAB'), ('XBD', 'XBD'), ('XBA', 'XBA'), ('XBC', 'XBC'), ('XBB', 'XBB'), ('BWP', 'BWP'), ('TZS', 'TZS'), ('VND', 'VND'), ('KYD', 'KYD'), ('UAH', 'UAH'), ('AWG', 'AWG'), ('GIP', 'GIP'), ('BYR', 'BYR'), ('ALL', 'ALL'), ('XPD', 'XPD'), ('BYN', 'BYN'), ('DJF', 'DJF'), ('THB', 'THB'), ('BND', 'BND'), ('NIO', 'NIO'), ('LAK', 'LAK'), ('SYP', 'SYP'), ('MAD', 'MAD'), ('MZN', 'MZN'), ('PHP', 'PHP'), ('ZAR', 'ZAR'), ('NPR', 'NPR'), ('CRC', 'CRC'), ('AED', 'AED'), ('GBP', 'GBP'), ('HUF', 'HUF'), ('LSL', 'LSL'), ('XDR', 'XDR'), ('TTD', 'TTD'), ('MMK', 'MMK'), ('KPW', 'KPW'), ('ANG', 'ANG'), ('RWF', 'RWF'), ('NOK', 'NOK'), ('MXV', 'MXV'), ('MOP', 'MOP'), ('INR', 'INR'), ('MXN', 'MXN'), ('TJS', 'TJS'), ('COP', 'COP'), ('COU', 'COU'), ('HNL', 'HNL'), ('FJD', 'FJD'), ('ETB', 'ETB'), ('PEN', 'PEN'), ('BZD', 'BZD'), ('ILS', 'ILS'), ('DOP', 'DOP'), ('TWD', 'TWD'), ('MDL', 'MDL'), ('BSD', 'BSD'), ('MVR', 'MVR'), ('XTS', 'XTS'), ('AUD', 'AUD'), ('SRD', 'SRD'), ('KRW', 'KRW'), ('VEF', 'VEF'), ('ZMW', 'ZMW'), ('CDF', 'CDF'), ('RUB', 'RUB'), ('SBD', 'SBD'), ('PLN', 'PLN'), ('MKD', 'MKD'), ('TOP', 'TOP'), ('GNF', 'GNF'), ('WST', 'WST'), ('ERN', 'ERN'), ('BAM', 'BAM'), ('CAD', 'CAD'), ('CVE', 'CVE'), ('XXX', 'XXX'), ('PGK', 'PGK'), ('SOS', 'SOS'), ('STD', 'STD'), ('IRR', 'IRR'), ('XPF', 'XPF'), ('XOF', 'XOF'), ('NZD', 'NZD'), ('ARS', 'ARS'), ('RSD', 'RSD'), ('BHD', 'BHD'), ('SDG', 'SDG'), ('XAU', 'XAU'), ('NAD', 'NAD'), ('GHS', 'GHS'), ('EGP', 'EGP'), ('BOV', 'BOV'), ('TMT', 'TMT'), ('BOB', 'BOB'), ('DKK', 'DKK'), ('LBP', 'LBP'), ('AOA', 'AOA'), ('KHR', 'KHR'), ('MYR', 'MYR'), ('LYD', 'LYD'), ('JOD', 'JOD'), ('SAR', 'SAR'), ('XPT', 'XPT'), ('HKD', 'HKD'), ('CHE', 'CHE'), ('CHF', 'CHF'), ('SVC', 'SVC'), ('MRO', 'MRO'), ('HRK', 'HRK'), ('CHW', 'CHW'), ('XAF', 'XAF'), ('XAG', 'XAG'), ('VUV', 'VUV'), ('UYU', 'UYU'), ('UYI', 'UYI'), ('PYG', 'PYG'), ('ZWL', 'ZWL'), ('XSU', 'XSU'), ('NGN', 'NGN'), ('MWK', 'MWK'), ('LKR', 'LKR'), ('PKR', 'PKR'), ('SZL', 'SZL'), ('MNT', 'MNT'), ('AMD', 'AMD'), ('UGX', 'UGX'), ('JMD', 'JMD'), ('SCR', 'SCR'), ('SHP', 'SHP'), ('AFN', 'AFN'), ('TRY', 'TRY'), ('BDT', 'BDT'), ('HTG', 'HTG'), ('MGA', 'MGA'), ('YER', 'YER'), ('LRD', 'LRD'), ('XCD', 'XCD'), ('SSP', 'SSP'), ('CZK', 'CZK'), ('BTN', 'BTN'), ('MUR', 'MUR'), ('IDR', 'IDR'), ('ISK', 'ISK'), ('XUA', 'XUA'), ('SEK', 'SEK'), ('CUP', 'CUP'), ('CLF', 'CLF'), ('BBD', 'BBD'), ('KMF', 'KMF'), ('GMD', 'GMD'), ('GTQ', 'GTQ'), ('CUC', 'CUC'), ('GEL', 'GEL'), ('CLP', 'CLP'), ('EUR', 'EUR'), ('KZT', 'KZT'), ('OMR', 'OMR'), ('BRL', 'BRL'), ('KES', 'KES'), ('USD', 'USD'), ('AZN', 'AZN'), ('USN', 'USN'), ('IQD', 'IQD'), ('GYD', 'GYD'), ('KWD', 'KWD'), ('BIF', 'BIF'), ('SGD', 'SGD'), ('UZS', 'UZS'), ('CNY', 'CNY'), ('SLL', 'SLL'), ('TND', 'TND'), ('FKP', 'FKP'), ('KGS', 'KGS'), ('RON', 'RON'), ('JPY', 'JPY')], max_length=3, verbose_name='Currency code')),
                ('order_no', utils.db.fields.NullCharField(db_index=True, max_length=128, unique=True, verbose_name='Merchant order no')),
                ('bulk_id', models.CharField(blank=True, db_index=True, max_length=128, null=True, verbose_name='Bulk ID')),
                ('sms_payment_details', models.BooleanField(default=False, help_text='If checked, customer will be SMSed with payment details upon transaction stage ends.', verbose_name='SMS payment details')),
                ('email_payment_details', models.BooleanField(default=False, help_text='If checked, customer will be mailed with payment details upon transaction stage ends.', verbose_name='Email payment details')),
                ('sms_notification', models.BooleanField(default=False, help_text='If checked, customer will get a SMS alert to pay the transaction.', verbose_name='SMS notification')),
                ('email_notification', models.BooleanField(default=False, help_text='If checked, customer will get a SMS alert to pay the transaction.', verbose_name='Email notification')),
                ('disclosure_url', models.URLField(blank=True, default='', help_text='URL where to send payment result after customer ends the session on gateway page.', verbose_name='Disclosure URL')),
                ('redirect_url', models.URLField(blank=True, default='', help_text='URL where to redirect the user after payment', verbose_name='Redirect URL')),
                ('customer_first_name', models.CharField(blank=True, default='', max_length=64, verbose_name='Customer first name')),
                ('customer_last_name', models.CharField(blank=True, default='', max_length=64, verbose_name='Customer last name')),
                ('customer_email', models.CharField(blank=True, default='', max_length=128, verbose_name='Customer email')),
                ('customer_phone', models.CharField(blank=True, default='', max_length=16, verbose_name='Customer phone')),
                ('customer_address_line1', models.TextField(blank=True, default='', verbose_name='Customer address line 1')),
                ('customer_address_line2', models.TextField(blank=True, default='', verbose_name='Customer address line 2')),
                ('customer_address_city', models.CharField(blank=True, default='', max_length=40, verbose_name='Customer address city')),
                ('customer_address_state', models.CharField(blank=True, default='', max_length=40, verbose_name='Customer address state')),
                ('customer_address_country', models.CharField(blank=True, default='', max_length=40, verbose_name='Customer address country')),
                ('customer_address_postal_code', models.CharField(blank=True, default='', max_length=12, verbose_name='Customer address postal code')),
                ('extra', jsonfield.fields.JSONField(blank=True, default={}, verbose_name='Extra')),
                ('seen_at', models.DateTimeField(blank=True, help_text='Defines if the customer or any other guest user has opened the payment link.', null=True, verbose_name='Seen at')),
                ('state_changed_at', model_utils.fields.MonitorField(default=django.utils.timezone.now, monitor='state')),
                ('invalidation_reason', models.CharField(blank=True, default='', editable=False, max_length=255, verbose_name='Invalidation reason')),
                ('short_url', models.URLField(blank=True, help_text='pay.kn URL', verbose_name='Short url')),
                ('initiator', models.ForeignKey(blank=True, help_text='The user who created the transaction', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='transactions', to=settings.AUTH_USER_MODEL, verbose_name='initiator')),
            ],
            options={
                'ordering': ('-created', '-modified'),
                'get_latest_by': 'created',
                'verbose_name': 'Payment transaction',
                'verbose_name_plural': 'Payment transactions',
                'permissions': (('view_paymenttransaction', 'View Payment Transactions'), ('view_owned_paymenttransaction', 'View Owned Payment Transactions'), ('cancel_paymenttransaction', 'Cancel Payment Transactions')),
            },
        ),
        migrations.AddField(
            model_name='paymentattempt',
            name='transaction',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='attempts', to='gateway.PaymentTransaction'),
        ),
    ]
