# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2017-05-10 12:39
from __future__ import unicode_literals

from django.db import migrations
import django.utils.timezone
import model_utils.fields


class Migration(migrations.Migration):

    dependencies = [
        ('gateway', '0013_auto_20170508_1108'),
    ]

    operations = [
        migrations.AlterField(
            model_name='paymentattempt',
            name='state_changed_at',
            field=model_utils.fields.MonitorField(default=django.utils.timezone.now, monitor='state'),
        ),
        migrations.AlterField(
            model_name='paymenttransaction',
            name='state_changed_at',
            field=model_utils.fields.MonitorField(blank=True, default=django.utils.timezone.now, monitor='state'),
        ),
    ]
