# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-10-31 07:59
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gateway', '0039_auto_20181022_1838'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='gateway',
            options={'permissions': (('view_statistics', 'Can view dashboard statistics (Index)'), ('add_support', 'Can add support tickets')), 'verbose_name': 'Gateway', 'verbose_name_plural': 'Gateways'},
        ),
    ]
