# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^response/(?P<reference_number>[0-9A-Za-z_\-]+)/$',
        views.ResponseFromKnetView.as_view(), name='response'),
    url(r'^handler/(?P<reference_number>[0-9A-Za-z_\-]+)/$',
        views.RedirectFromKnetView.as_view(), name='redirect'),
    url(r'^error/(?P<reference_number>[0-9A-Za-z_\-]+)/$',
        views.ErrorResponseFromKnetView.as_view(), name='error'),
]
