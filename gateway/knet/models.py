# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _

from api.func import get_knpay_url
from utils.funcs import is_ascii
from utils.validators import FileValidator
from gateway.models import GatewaySettings
from extras.currency.data import currencies
from .client import KnetClient


logger = logging.getLogger('gateway')


class KnetSettings(GatewaySettings):
    alias = models.CharField(_("Alias"), max_length=32, default='', blank=True,
                             help_text=_("Alias name if the resource file contains "
                                         "multiple aliases, if not leave it empty."))
    file = models.FileField(_("Resource CGN file"), upload_to='gateway/knet/resource',
                            validators=[FileValidator(allowed_extensions=['cgn'])])

    class Meta:
        verbose_name = _("Knet")
        verbose_name_plural = _("Knet")
        permissions = (
            ('use_knetsettings', _("Can use Knet")),
        )

    def get_field_mapping(self):
        return {
            'udf1': ['order_no'],
            'udf2': ['customer_email'],
            'udf3': ['customer_first_name', 'customer_last_name'],
            'udf4': ['customer_phone']
        }

    def get_udf_payload(self, payment_attempt):
        mapping = self.get_field_mapping()
        payload = {}
        for udf, field_names in mapping.items():
            field_value = ' '.join([getattr(payment_attempt.transaction, field_name)
                                    for field_name in field_names])

            if len(field_value) > 1:
                if is_ascii(field_value):
                    payload[udf] = field_value
        return payload

    @staticmethod
    def create_payment_url(payment_attempt):
        client = KnetClient(payment_attempt)
        response = client.connect()
        if response is not None:
            response = response.split(':', 1)
            payment_id = response[0]
            payment_url = '%s?PaymentID=%s' % (response[1], payment_id)
            payment_attempt.data = {'payment_id': payment_id}
            payment_attempt.save()
            return payment_url
        error_message = "Communication error between KNPay and gateway. " \
                        "Please report this issue to KNPay administrator."
        payment_attempt.error(error_message=error_message)
        payment_attempt.save()

    def get_language(self, payment_attempt=None):
        language = payment_attempt.transaction.language
        return 'ENG' if language == 'en' else 'ARA'

    def get_currency(self, payment_attempt=None):
        return currencies[self.currency_config.default_currency.code]

    def get_error_url(self, payment_attempt=None):
        # URL where KNET will POST error details if happens (extremely rare)
        return '{knpay_url}{path}'.format(knpay_url=get_knpay_url(),
                                          path=reverse('knet:error', args=(payment_attempt.reference_number,)))

    def get_response_url(self, payment_attempt=None):
        # URL where KNET will POST payment details (no matter is success or failure)
        return '{knpay_url}{path}'.format(knpay_url=get_knpay_url(),
                                          path=reverse('knet:response', args=(payment_attempt.reference_number,)))

