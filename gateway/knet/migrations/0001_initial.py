# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-02-15 09:44
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import utils.validators


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('gateway', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='KnetSettings',
            fields=[
                ('gatewaysettings_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='gateway.GatewaySettings')),
                ('alias', models.CharField(blank=True, default='', help_text='Alias name if the resource file contains multiple aliases, if not leave it empty.', max_length=32, verbose_name='Alias')),
                ('file', models.FileField(upload_to='gateway/settings/resource', validators=[utils.validators.FileValidator(allowed_extensions=['cgn'])], verbose_name='Resource CGN file')),
            ],
            options={
                'verbose_name': 'Knet',
                'verbose_name_plural': 'Knet',
            },
            bases=('gateway.gatewaysettings',),
        ),
    ]
