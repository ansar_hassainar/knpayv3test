# # -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-

import itertools
import logging
import re
import zipfile
import xml.etree.ElementTree as ET
from cStringIO import StringIO

from django.utils.translation import ugettext_lazy as _

import requests

from gateway.client import BaseClient


logger = logging.getLogger('gateway')

# DO NOT TOUCH
V1 = 'v1'  # old resource.cgn file
V2 = 'v2'  # new resource.cgn file


class KnetClient(BaseClient):
    name = _("Knet")
    _action = 1  # Payment action on KNET side
    _parsed_resource = dict()

    # Key used to authenticate with KNET
    key = 'Those who profess to favour freedom and yet depreciate agitation '
    key += 'are men who want rain without thunder and lightning'

    def __init__(self, *args, **kwargs):
        super(KnetClient, self).__init__(*args, **kwargs)
        self.resource = self.settings.file
        self.resource_version = self._get_resource_version()
        getattr(self, '_parse_%s' % self.resource_version)()

    def _get_resource_version(self):
        if self.resource.size < 1000:
            return V1
        return V2

    def _zip(self, xor):
        # Create a temporary string file using the faster cStringIO module
        memory_file = StringIO()
        with open(self.resource.path, 'rb') as f:
            memory_file.write(xor(f.read()))
        try:
            return zipfile.ZipFile(memory_file)
        except zipfile.BadZipfile:
            raise zipfile.BadZipfile

    def _parse_v1(self):
        """
        Old style resource parsing
        """
        def xor(file_string):
            parsed = list()
            i = 0
            for val in file_string:
                if i == len(self.key):
                    i = 0
                parsed.append(chr(ord(val) ^ ord(self.key[i])))
                i += 1
            return ''.join(parsed)

        regex = re.compile('<*.?><terminal><id>(.*?)</id><password>(.*?)</password><webaddress>(.*?)</webaddress><port'
                           '>(.*?)</port><context>(.*?)</context></terminal>')

        zip_file = self._zip(xor)

        params = regex.findall(xor(zip_file.read(zip_file.filelist[0].filename)))[0]
        # Parameters found inside the resource file
        resource_kwargs = ('id', 'password', 'webaddress', 'port', 'context')
        self._parsed_resource = dict(zip(resource_kwargs, params))

    def _parse_v2(self):
        """
        New style resource parsing
        """
        def xor(file_string):
            key = itertools.cycle(self.key)
            return ''.join(chr(ord(k) ^ ord(v)) for (k, v) in itertools.izip(file_string, key))

        zip_file = self._zip(xor)

        alias_name = self.settings.alias or zip_file.filelist[0].filename.split('.')[0]
        xml = xor(zip_file.read("{}.xml".format(alias_name)))

        # Grab the data from the resource file
        xml = ET.fromstring(xml)
        # Parameters found inside the resource file
        resource_kwargs = ('id', 'password', 'passwordhash', 'webaddress', 'port', 'context',)
        for kwarg in resource_kwargs:
            self._parsed_resource[kwarg] = xml.find(kwarg).text

    def connect(self):
        """
        Performs a request to KNET banking service
        """
        try:
            psp_payload = self._build_payload()
            request_url = 'https://%(webaddress)s:%(port)s/%(context)s' % self._parsed_resource
            request_url += '/servlet/PaymentInitHTTPServlet'
            logger.info("KNET request URL {}".format(request_url))
            response = requests.post(request_url, psp_payload, timeout=25)
            logger.info("KNET response on url connect {}".format(response.text))
        except Exception as e:
            logger.exception("Error occurred while requesting payment url from KNET. {}".format(e))
            return None

        if 'ERROR' in response.text:
            logger.exception("Error in KNET response after requesting payment URL {}".format(response.text))
            return None
        else:
            logger.info("KNET response after requesting payment URL {}".format(response.text))
            return response.text

    def _build_payload(self):
        data = {
            'id': self._parsed_resource['id'],
            'action': self._action,
            'amt': self.payment_attempt.get_amount(),
            'trackid': self.payment_attempt.reference_number,
            'currencycode': self.payment_attempt.settings.get_currency(),
            'langid': self.settings.get_language(payment_attempt=self.payment_attempt),
            'errorURL': self.settings.get_error_url(self.payment_attempt),
            'responseURL': self.settings.get_response_url(payment_attempt=self.payment_attempt),
            'password': self._parsed_resource['password'],
        }

        if self.resource_version == V2:
            data['passwordhash'] = self._parsed_resource['passwordhash']

        # Add the UDF values to the data connection dictionary
        # Disallowed characters are substituted with '-'
        disallowed = ['~', '`', '!', '#', '$', '%', '^', '|', '\\', ':', '\'', '"', '/',
                      '<', '>', '(', ')', '{', '}', '[', '?', ';', '&', '*', '\]']
        for udf, value in self.settings.get_udf_payload(self.payment_attempt).items():
            data[udf] = re.sub('[%s]' % ''.join(disallowed), '-', value)
        logger.info("KNET prepared payload {}".format(data))
        return data
