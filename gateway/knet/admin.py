# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib import admin
from polymorphic.admin import PolymorphicChildModelAdmin
from .models import KnetSettings


@admin.register(KnetSettings)
class KnetSettingsAdmin(PolymorphicChildModelAdmin):
    base_model = KnetSettings
    prepopulated_fields = {'code': ('name',)}
    save_as = True
