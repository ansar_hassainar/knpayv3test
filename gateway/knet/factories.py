# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import factory

from gateway.factories import GatewayFactory
from .models import KnetSettings


class KnetSettingsFactory(factory.DjangoModelFactory):
    class Meta:
        model = KnetSettings

    name = 'knet-test'
    gateway = factory.SubFactory(GatewayFactory)
    file = factory.django.FileField(from_path='gateway/knet/test_data/resource_kwt.cgn')