# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

from django.db import transaction
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic
from django.views.generic.detail import SingleObjectMixin

from braces.views import CsrfExemptMixin

from utils.views.mixins import PSPResponseMixin
from gateway.models import PaymentAttempt


logger = logging.getLogger('gateway')


class ResponseFromKnetView(CsrfExemptMixin,
                           PSPResponseMixin,
                           SingleObjectMixin,
                           generic.View):
    """
    Handle KNET response
    """
    queryset = PaymentAttempt.objects.pending()
    slug_url_kwarg = 'reference_number'
    slug_field = 'reference_number'

    def post(self, request, *args, **kwargs):
        data = request.POST.copy()
        logger.info("KNET payload sent to response URL {}".format(data))
        self.object = self.get_object()
        data = self.request.POST.copy().dict()
        try:
            with transaction.atomic():
                url = self.handle_psp_response(data)
                return HttpResponse(url, 'text/plain')
        except Exception as e:
            logger.exception("Updating knet payment attempt failed due to %s" % e)
            # refresh from db
            self.object = self.get_object()
            self.object.error(str(e))
            self.object.save()
            return HttpResponse(status=400)

    def handle_psp_response(self, data):

        if data.get('result') == 'CAPTURED':
            self.object.acknowledge_payment(self.request, data)

        elif data.get('ErrorText') or data.get('Error'):
            error_message = data.get('ErrorText', data.get('Error', ''))
            self.object.error(error_message, gateway_response=data)

        elif data.get('result') == 'NOT CAPTURED':
            # payment failed due to insufficient funds, etc.
            self.object.fail(data)

        else:
            self.object.reject(data)

        self.object.save()

        url = 'REDIRECT={}'.format(
            self.request.build_absolute_uri(reverse('knet:redirect',
                                                    args=(self.object.reference_number,))))
        logger.info("%s" % url)
        return url


class ErrorResponseFromKnetView(CsrfExemptMixin,
                                PSPResponseMixin,
                                SingleObjectMixin,
                                generic.View):
    queryset = PaymentAttempt.objects.all()
    slug_url_kwarg = 'reference_number'
    slug_field = 'reference_number'
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        data = request.GET.copy()
        logger.info("KNET payload sent to error URL {}".format(data))
        self.object = self.get_object()
        data = self.request.GET.copy().dict()
        return self.handle_psp_response(data)

    def handle_psp_response(self, data):
        if self.object.can_be_errored():
            self.object.error("KNET Error", gateway_response=data)
        else:
            # we can't mark as error twice.
            logger.exception('Redirect to error knet page %s' % self.object.reference_number)
            msg = 'Redirect to error knet page'
            self.object.message += '\nNew error msg: %s - %s' % (msg, data)

        self.object.save()
        url = reverse('knet:redirect', args=(self.object.reference_number,))
        return HttpResponseRedirect(url)


class RedirectFromKnetView(CsrfExemptMixin,
                           PSPResponseMixin,
                           SingleObjectMixin,
                           generic.View):
    """
    View for handling successful/unsuccessful redirects from KNet Gateway.

    For transactions created via API we are POSTing back to the customer
    server transaction details - variable data is holding it -. Server
    replies with an URL where the user shall be redirected. More
    details below on return description.

    # E-Commerce transactions:
        # Notify the disclosure_url with payment details as JSON
        # if merchant's server returns 200 status, redirect the customer to redirect_url
        # else redirect to KNPay landing page with payment details
    """
    queryset = PaymentAttempt.objects.all()
    slug_url_kwarg = 'reference_number'
    slug_field = 'reference_number'

    def handle_psp_response(self, data):
        attempt = self.object
        if not attempt.data.get('post_attempt_notifications_sent', False):
            attempt.transaction.send_post_attempt_notifications(attempt)
        return self.redirect()
