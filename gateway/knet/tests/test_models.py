# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.core.urlresolvers import resolve
from django.core.validators import URLValidator
from django.test import TestCase

import pytest
from mixer.backend.django import mixer
from mock import patch

from utils.tests import knet_settings_factory

pytestmark = pytest.mark.django_db


class TestKnetSettings(TestCase):
    def setUp(self):
        self.settings = knet_settings_factory()

    @patch('gateway.knet.client.KnetClient.__init__', return_value=None)
    @patch('gateway.knet.client.KnetClient.connect', return_value=None)
    def test_create_payment_url_when_knet_down(self, mock1, mock2):
        attempt = mixer.blend('gateway.PaymentAttempt', settings=self.settings)
        url = self.settings.create_payment_url(attempt)
        assert url is None

    @patch('gateway.knet.client.KnetClient.__init__', return_value=None)
    @patch('gateway.knet.client.KnetClient.connect', return_value='6536968592062610:http://example.com/')
    def test_create_payment_url_when_knet_up(self, mock1, mock2):
        attempt = mixer.blend('gateway.PaymentAttempt', settings=self.settings)
        url = self.settings.create_payment_url(attempt)
        validator = URLValidator()
        assert validator(url) is None, "Should return a valid url"
