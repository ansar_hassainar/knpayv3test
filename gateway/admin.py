# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from functools import update_wrapper

from django import forms
from django.contrib import admin, messages
from django.utils.translation import ugettext_lazy as _, gettext_lazy
from django.conf.urls import url
from django.core.exceptions import PermissionDenied
from django.contrib.admin.utils import model_ngettext
from django.contrib.auth import get_permission_codename
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.db import router, transaction
from django.views.decorators.csrf import csrf_protect
from django.utils.decorators import method_decorator

from fsm_admin.mixins import FSMTransitionMixin
from polymorphic.admin import (
    PolymorphicParentModelAdmin, PolymorphicChildModelFilter)

from gateway.knet.models import KnetSettings
from gateway.kpay.models import KPaySettings
from gateway.migs.models import MIGSSettings
from gateway.burgan.models import BurganSettings
from gateway.cybersource.models import CyberSourceSettings
from gateway.mpgs.models import MPGSSettings
from gateway.pp.models import PayPalSettings
from utils.admin import PrettyJsonAdminMixin
from .forms import GatewayAdminForm
from .models import Gateway, PaymentTransaction, PaymentAttempt, \
    GatewaySettings, DeletedPaymentTransaction
from .views import AdminDiscloseTransaction


csrf_protect_m = method_decorator(csrf_protect)


class GatewayPolymorphicChildModelFilter(PolymorphicChildModelFilter):
    title = _("Gateway")


@admin.register(Gateway)
class GatewayAdmin(admin.ModelAdmin):
    form = GatewayAdminForm


@admin.register(GatewaySettings)
class GatewaySettingsAdmin(PolymorphicParentModelAdmin):
    """ The parent model admin """
    base_model = GatewaySettings
    child_models = (KnetSettings, BurganSettings, MIGSSettings,
                    CyberSourceSettings, MPGSSettings, PayPalSettings,
                    KPaySettings)
    list_filter = (GatewayPolymorphicChildModelFilter, 'type', 'is_active')
    list_display = ('__unicode__', 'is_active', 'name', 'code')
    list_editable = ('is_active', 'name', 'code',)


class PaymentAttemptForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(PaymentAttemptForm, self).__init__(*args, **kwargs)
        for field_name in self.fields:
            self.fields[field_name].disabled = True

    class Meta:
        model = PaymentAttempt
        fields = '__all__'


class PaymentAttemptInline(admin.StackedInline):
    form = PaymentAttemptForm
    model = PaymentAttempt
    extra = 0
    readonly_fields = ('data',)

    def has_add_permission(self, request):
        return False


def delete_selected(modeladmin, request, queryset):
    """
    Does not delete but updates the value of is_deleted to True
    So that these transactions will be available in deleted listing
    """
    # Check that the user has delete permission for the actual model
    if not modeladmin.has_delete_permission(request):
        raise PermissionDenied

    if queryset.paid().exists():
        if not modeladmin.has_delete_paid_permission(request):
            modeladmin.message_user(request, _("You are not having permission to delete paid transactions."), messages.ERROR)
            return

    n = queryset.count()

    # The user has already confirmed the deletion.
    # Do the deletion and return None to display the change list view again.
    if n:
        for obj in queryset:
            if obj.state == obj.PAID:
                obj.is_deleted = True
                obj.save()
            else:
                obj_display = str(obj)
                modeladmin.log_deletion(request, obj, obj_display)
                obj.delete()

        modeladmin.message_user(request, _("Successfully deleted %(count)d %(items)s.") % {
            "count": n, "items": model_ngettext(modeladmin.opts, n)
        }, messages.SUCCESS)


def restore_selected(modeladmin, request, queryset):
    """
    Will restore payment transactions back to original listing
    """
    n = queryset.count()

    queryset.update(is_deleted=False)

    modeladmin.message_user(request, _("Successfully restored %(count)d %(items)s.") % {
        "count": n, "items": model_ngettext(modeladmin.opts, n)
    }, messages.SUCCESS)


delete_selected.short_description = _(
    "Delete selected %(verbose_name_plural)s")
restore_selected.short_description = _(
    "Restore selected %(verbose_name_plural)s")


class PaymentTransactionAdminMixin(FSMTransitionMixin,
                                   PrettyJsonAdminMixin,
                                   admin.ModelAdmin):
    list_filter = ('state', 'type', 'created', 'modified',
                   'is_sandbox', 'gateway_code')
    list_display = ('__unicode__', 'id', 'encrypted_pk', 'order_no', 'gateway_code',
                    'state', 'amount', 'type', 'created', 'modified')
    readonly_fields = ('state', 'invalidation_reason', 'state_changed_at')
    search_fields = ('id', 'order_no', 'amount', 'customer_email',
                     'customer_first_name', 'customer_last_name',
                     'attempts__reference_number')
    json_field = 'extra'
    json_short_description = _("Extra")
    inlines = [PaymentAttemptInline]
    change_form_template = 'admin/gateway/paymenttransaction/change_form.html'

    def has_add_permission(self, request):
        return False

    def get_urls(self):
        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)
            wrapper.model_admin = self
            return update_wrapper(wrapper, view)

        urls = super(PaymentTransactionAdminMixin, self).get_urls()
        disclose_urls = [
            url(r'^disclose-transaction/(?P<pk>\d+)/$', wrap(
                AdminDiscloseTransaction.as_view()), name='disclose_transaction')
        ]
        return disclose_urls + urls

    def get_form(self, request, obj=None, **kwargs):
        form = super(PaymentTransactionAdminMixin, self).get_form(
            request, obj=obj, **kwargs)
        for field_name in form.base_fields:
            form.base_fields[field_name].disabled = True
        return form

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_add_another'] = False
        extra_context['show_save_and_continue'] = False
        extra_context['show_save'] = False
        return super(PaymentTransactionAdminMixin, self).change_view(request, object_id,
                                                                     form_url, extra_context=extra_context)


@admin.register(PaymentTransaction)
class PaymentTransactionAdmin(PaymentTransactionAdminMixin):
    actions = [delete_selected]

    def get_queryset(self, request):
        qs = super(PaymentTransactionAdmin, self).get_queryset(request)
        return qs.filter(is_deleted=False)

    def has_delete_paid_permission(self, request, obj=None):
        opts = self.opts
        codename = get_permission_codename('delete_paid', opts)
        return request.user.has_perm('%s.%s' % (opts.app_label, codename), obj)

    def delete_model(self, request, obj):
        """
        Given a model instance delete it if not state is PAID
        """
        if obj.state == obj.PAID:
            if not self.has_delete_paid_permission(request):
                raise PermissionDenied
            else:
                obj.is_deleted = True
                obj.save()
        else:
            obj.delete()

    @csrf_protect_m
    def delete_view(self, request, object_id, extra_context=None):
        with transaction.atomic(using=router.db_for_write(self.model)):
            obj = PaymentTransaction.objects.get(pk=object_id)
            if obj.state == obj.PAID:
                if not self.has_delete_paid_permission(request):
                    self.message_user(request, _("You are not having permission to delete paid transactions."), messages.ERROR)
                    redirect_url = reverse('admin:index', current_app=self.admin_site.name)
                    return HttpResponseRedirect(redirect_url) 
                else:
                    return self._delete_view(request, object_id, extra_context)
            else:
                return self._delete_view(request, object_id, extra_context)


@admin.register(DeletedPaymentTransaction)
class DeletedTransactionAdmin(PaymentTransactionAdminMixin):
    actions = [restore_selected]
