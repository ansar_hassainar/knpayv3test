# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import json
import tablib

from django.utils.translation import ugettext as _

from import_export import resources, fields

from .models import PaymentTransaction, GatewaySettings
from extras.dashboard.models import PaymentRequestFormConfig, CustomerPaymentFormConfig
from extras.dashboard.bulk.models import BulkConfig


class PaymentTransactionResource(resources.ModelResource):
    id = fields.Field(column_name=_("ID"), attribute='id')
    initiator = fields.Field(column_name=_("Initiator"), attribute='initiator')
    type = fields.Field(column_name=_("Type"), attribute='type')
    state = fields.Field(column_name=_("State"), attribute='state')
    gateway_code = fields.Field(column_name=_("Gateway_code"), attribute='gateway_code')
    amount = fields.Field(column_name=_('Amount'), attribute='amount')
    currency_code = fields.Field(column_name=_("Currency code"), attribute='currency_code')
    order_no = fields.Field(column_name=_("Order no."), attribute='order_no')
    bulk_id = fields.Field(column_name=_("Bulk ID"), attribute='bulk_id')
    customer_first_name = fields.Field(column_name=_("Customer first name"), attribute='customer_first_name')
    customer_last_name = fields.Field(column_name=_("Customer last name"), attribute='customer_last_name')
    customer_email = fields.Field(column_name=_("Customer email"), attribute='customer_email')
    customer_phone = fields.Field(column_name=_("Customer phone"), attribute='customer_phone')
    customer_address_line1 = fields.Field(
        column_name=_("Customer address line 1"), attribute='customer_address_line1')
    customer_address_line2 = fields.Field(
        column_name=_("Customer address line 1"), attribute='customer_address_line2')
    customer_address_city = fields.Field(
        column_name=_("Customer address city"), attribute='customer_address_city')
    customer_address_state = fields.Field(
        column_name=_("Customer address state"), attribute='customer_address_state')
    customer_address_country = fields.Field(
        column_name=_("Customer address country"), attribute='customer_address_country')
    customer_address_postal_code = fields.Field(
        column_name=_("Customer address postal code"), attribute='customer_address_postal_code')
    email_recipients = fields.Field(
        column_name=_("Email Recipients"), attribute='email_recipients')
    remarks = fields.Field(column_name=_("Remarks"), attribute='remarks')
    extra = fields.Field(column_name=_("Extra"), attribute='extra')
    gateway_payload = fields.Field(
        column_name=_("Gateway Response"), attribute='gateway_payload'
    )

    # custom
    gateway = fields.Field(column_name=_("Gateway"))
    reference_number = fields.Field(column_name=_("Reference number"))
    payment_date = fields.Field(column_name=_("Payment date"))

    def __init__(self, payment_type=None, *args, **kwargs):
        super(PaymentTransactionResource, self).__init__(*args, **kwargs)
        self.payment_type = payment_type
        self.customer_active_fields = self.get_customer_active_fields()

    class Meta:
        models = PaymentTransaction
        customer_fields = (
            'order_no',
            'customer_first_name',
            'customer_last_name',
            'customer_email',
            'customer_phone',
            'customer_address_line1',
            'customer_address_line2',
            'customer_address_city',
            'customer_address_state',
            'customer_address_country',
            'customer_address_postal_code',
            'email_recipients'
        )

        fields = (
            'id',
            'type',
            'state',
            'gateway',
            'gateway_code',
            'amount',
            'currency_code',
            'reference_number',
            'payment_date',
            'initiator'
        ) + customer_fields + ('extra',
                               'gateway_payload'
                               )

        export_order = fields

    def get_customer_active_fields(self):
        customer_active_fields = []

        if self.payment_type == PaymentTransaction.PAYMENT_REQUEST:
            # include bulk config + payment request form config fields
            customer_active_fields += ['initiator']

            request_form_fields = PaymentRequestFormConfig.get_solo().fields.active()
            fields = [field.get_name() for field in request_form_fields]

            bulkconfig_form_fields = BulkConfig.get_solo().fields.active()
            if bulkconfig_form_fields:
                for field in bulkconfig_form_fields:
                    if not field.get_name() in fields:
                        fields.append(field.get_name())

            customer_active_fields += [field for field in fields if field in self._meta.customer_fields]

        elif self.payment_type == PaymentTransaction.CUSTOMER_PAYMENT:
            form_active_fields = CustomerPaymentFormConfig.get_solo().fields.active()
            customer_active_fields += [field.get_name() for field in form_active_fields
                                       if field.get_name() in self._meta.customer_fields]
        elif self.payment_type == PaymentTransaction.E_COMMERCE:
            # include only initiator field for E-COMMERCE transactions
            customer_active_fields += ['initiator']
        else:
            pass

        return tuple(customer_active_fields)

    def get_export_order(self):
        order = (
            'type',
            'id',
            'state',
            'gateway',
            'gateway_code',
            'amount',
            'currency_code',
            'reference_number',
            'payment_date') + self.customer_active_fields + ('extra',
                                                             'gateway_payload'
                                                             )
        return order

    def dehydrate_type(self, obj):
        return obj.get_type_display()

    def dehydrate_state(self, obj):
        return obj.get_state_display()

    def dehydrate_gateway(self, obj):
        settings = GatewaySettings.objects.get_for_code(obj.gateway_code)
        if settings:
            return settings.gateway.get_name_display()
        return _("Not selected")

    def dehydrate_reference_number(self, obj):
        try:
            return obj.get_paid_attempt().reference_number
        except:
            return ''

    def dehydrate_payment_date(self, obj):
        try:
            return obj.get_paid_attempt().modified.strftime('%d-%m-%Y')
        except:
            return ''

    def dehydrate_gateway_payload(self, obj):
        try:
            return json.dumps(obj.get_paid_attempt().gateway_response)
        except (AttributeError, KeyError) as e:
            return json.dumps({})


class ExportResource(object):
    """
    Custom csv and xls export using tablib library
    """
    def __init__(self, queryset, fields, header_fields,
                 gateway_response_fields, extra_fields, file_format):
        self.queryset = queryset
        self.fields = fields + gateway_response_fields
        self.final_fields = header_fields + gateway_response_fields
        self.gateway_response_fields = gateway_response_fields
        self.extra_fields = extra_fields
        self.file_format = file_format
        self.data = tablib.Dataset()
        self.data.headers = self.final_fields

    def construct_dump(self):
        for obj in self.queryset:
            # It might not be paid all time
            # includes failed, canceled, error
            if obj.state == PaymentTransaction.PAID:
                paid_attempt = obj.get_paid_attempt()
            else:
                paid_attempt = obj.attempts.last() if obj.attempts.exists() else None

            fields_data = []
            for field in self.fields:

                if field in self.extra_fields:
                    fields_data.append(obj.extra.get(field, ''))
                    continue

                if field == 'initiator':
                    try:
                        fields_data.append(obj.initiator.username)
                    except AttributeError:
                        fields_data.append('')
                    continue

                if paid_attempt is not None:
                    if field in self.gateway_response_fields:
                        fields_data.append(paid_attempt.gateway_response.get(field, ''))
                        continue
                    if field == 'reference_number':
                        fields_data.append(paid_attempt.reference_number)
                        continue
                    if field == 'payment_date':
                        fields_data.append(
                            paid_attempt.state_changed_at.strftime('%d-%m-%Y'))
                        continue

                fields_data.append(getattr(obj, field, ''))
            self.data.append(fields_data)

    def dump(self):
        return self.data.export(self.file_format)
