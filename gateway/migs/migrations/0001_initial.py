# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-02-15 09:44
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('gateway', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='MIGSSettings',
            fields=[
                ('gatewaysettings_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='gateway.GatewaySettings')),
                ('secret_key', models.CharField(max_length=50, verbose_name='Secret key')),
                ('merchant_id', models.CharField(max_length=50, verbose_name='Merchant ID')),
                ('access_code', models.CharField(max_length=50, verbose_name='VPC access Code')),
                ('url', models.URLField(help_text='IE:https://migs.mastercard.com.au/vpcpay', verbose_name='VPC URL')),
            ],
            options={
                'verbose_name': 'MiGS',
                'verbose_name_plural': 'MiGS',
            },
            bases=('gateway.gatewaysettings',),
        ),
    ]
