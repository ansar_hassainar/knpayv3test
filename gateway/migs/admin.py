# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib import admin
from polymorphic.admin import PolymorphicChildModelAdmin
from .models import MIGSSettings


@admin.register(MIGSSettings)
class MIGSSettingsAdmin(PolymorphicChildModelAdmin):
    base_model = MIGSSettings
    prepopulated_fields = {'code': ('name',)}
    save_as = True
