# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^response/(?P<reference_number>[0-9A-Za-z_\-]+)/$',
        views.MIGSResponseView.as_view(), name='response'),
]
