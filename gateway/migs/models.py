# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging
import hashlib
import binascii
import hmac
from decimal import Decimal

from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _

from api.func import get_knpay_url
from gateway.models import GatewaySettings
from .client import MIGSClient


logger = logging.getLogger('gateway')


class MIGSSettings(GatewaySettings):
    secret_key = models.CharField(_("Secret key"), max_length=50)
    merchant_id = models.CharField(_("Merchant ID"), max_length=50)
    access_code = models.CharField(_("VPC access Code"), max_length=50)
    url = models.URLField(_("VPC URL"), help_text=_("IE:https://migs.mastercard.com.au/vpcpay"))

    class Meta:
        verbose_name = _("MiGS")
        verbose_name_plural = _("MiGS")
        permissions = (
            ('use_migssettings', _("Can use MiGS")),
        )

    @staticmethod
    def create_payment_url(payment_attempt):
        client = MIGSClient(payment_attempt)
        url = client.connect()
        return url

    def get_language(self, payment_attempt=None):
        return payment_attempt.transaction.language

    def get_currency(self, payment_attempt=None):
        return self.currency_config.default_currency.code

    def get_amount(self, payment_attempt=None):
        """
        MIGS doesn't allow decimals in the vpv_amount var,
        therefore, we need to cut them off
        1.000 KWD will become 1000
        1.00 USD will become 100
        2 decimals has to be multiplied by 100
        3 decimals has to be multiplied by 100
        and so on
        """
        multiplier = int('1%s' % (self.currency_config.default_currency.decimal_places * '0'))
        return int(Decimal(payment_attempt.get_amount()) * multiplier)

    def get_response_url(self, payment_attempt=None):
        return '{knpay_url}{path}'.format(knpay_url=get_knpay_url(),
                                          path=reverse('migs:response', args=(payment_attempt.reference_number,)))

    @staticmethod
    def get_response_description(code):
        return {
            '0': 'Transaction Successful',
            '?': 'Transaction status is unknown',
            '1': 'Unknown Error',
            '2': 'Bank Declined Transaction',
            '3': 'No Reply from Bank',
            '4': 'Expired Card',
            '5': 'Insufficient funds',
            '6': 'Error Communicating with Bank',
            '7': 'Payment Server System Error',
            '8': 'Transaction Type Not Supported',
            '9': 'Bank declined transaction (Do not contact Bank)',
            'A': 'Transaction Aborted',
            'C': 'Transaction Cancelled',
            'D': 'Deferred transaction has been received and is awaiting processing',
            'F': '3D Secure Authentication failed',
            'I': 'Card Security Code verification failed',
            'L': 'Shopping Transaction Locked (Please try the transaction again later)',
            'N': 'Cardholder is not enrolled in Authentication scheme',
            'P': 'Transaction has been received by the Payment Adaptor and is being processed',
            'R': 'Transaction was not processed - Reached limit of retry attempts allowed',
            'S': 'Duplicate SessionID (OrderInfo)',
            'T': 'Address Verification Failed',
            'U': 'Card Security Code Failed',
            'V': 'Address Verification and Card Security Code Failed',
        }.get(code, 'Unable to be determined')

    def hash_is_valid(self, migs_payload):
        logger.info('MIGS payload %s' % migs_payload)

        if migs_payload.get('vpc_SecureHash'):
            migs_hash = migs_payload.get('vpc_SecureHash')
            hash_data = self._get_hash_data(migs_payload, self.secret_key)

            return migs_hash.upper() == hash_data

    def _get_hash_data(self, migs_response, secure_secret):
        fields = migs_response
        sorted_fields = []
        for k in sorted(fields):
            if k.startswith('vpc_') or k.startswith('user_'):
                if fields[k] is not '' and k not in ['vpc_SecureHash', 'vpc_SecureHashType']:
                    sorted_fields.append((k,fields[k]))
        message = '&'.join(['%s=%s' % (k, v) for (k, v) in sorted_fields])
        digest = hmac.new(str(binascii.unhexlify(secure_secret)), message, digestmod=hashlib.sha256).hexdigest()

        return digest.upper()

    def get_merchant_identifier(self, payment_attempt=None):
        identifier = super(MIGSSettings, self).get_merchant_identifier(payment_attempt=payment_attempt)
        return identifier or 'MIblank%s' % payment_attempt.reference_number

