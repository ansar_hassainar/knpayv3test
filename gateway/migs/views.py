# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

from django.http import HttpResponse
from django.views import generic
from django.views.generic.detail import SingleObjectMixin

from utils.views.mixins import PSPResponseMixin
from ..models import PaymentAttempt

logger = logging.getLogger(__name__)


class MIGSResponseView(SingleObjectMixin,
                       PSPResponseMixin,
                       generic.View):
    queryset = PaymentAttempt.objects.pending()
    slug_url_kwarg = 'reference_number'
    slug_field = 'reference_number'

    def handle_psp_response(self, data):
        if self.object.settings.hash_is_valid(data):
            if data['vpc_Message'] == 'Approved':
                self.object.acknowledge_payment(self.request, data)
            else:
                self.object.reject(data)

            self.object.save()

            self.object.transaction.send_post_attempt_notifications(self.object)
        return self.redirect()