# -*- coding: utf-8 -*-

import binascii
import hashlib
import hmac
import logging
import urllib
from collections import OrderedDict

from gateway.client import BaseClient


logger = logging.getLogger('gateway')


class MIGSClient(BaseClient):
    _title = 'python VPC 3-Party'
    _command = 'pay'
    _version = '1'

    def connect(self):
        """
        An example of the start of a Transaction Request is:

        https://Virtual_Payment_Client_URL/vpcpay?vpc%5FVersion=1&vpc%5FLocale=en
        &vpc%5FCommand=pay&vpc%5FAccessCode=A8698556&vpc%5FMerchTxnRef=123&vpc%5F
        Merchant=TESTMERCHANT&vpc%5FOrderInfo=Example&vpc%5FAmount=100&vpc%5FRetu
        rnURL=http%3A%2F%2FMerchant_Web_URL%

        :return: url
        """
        querystring = urllib.urlencode(self._build_payload())
        url = '%s?%s' % (self.settings.url, querystring)
        logger.info('MiGS URL for %s %s' % (self.payment_attempt.reference_number, url))
        return url

    def _get_hash(self, kwargs):
        sorted_fields = [(k, kwargs[k]) for k in sorted(kwargs) if k.startswith('vpc_') and kwargs[k] is not '']
        message = '&'.join(['%s=%s' % (k, v) for (k, v) in sorted_fields])
        digest = hmac.new(str(binascii.unhexlify(self.settings.secret_key)),
                          message, digestmod=hashlib.sha256).hexdigest()
        return digest.upper()

    def _build_payload(self):
        kwargs = OrderedDict()
        # DO NOT CHANGE THE ORDER!!!!!!!!!
        kwargs['Title'] = self._title
        kwargs['vpc_AccessCode'] = self.settings.access_code
        kwargs['vpc_Amount'] = self.settings.get_amount(self.payment_attempt)
        kwargs['vpc_Command'] = self._command
        kwargs['vpc_Locale'] = self.settings.get_language(self.payment_attempt)
        kwargs['vpc_MerchTxnRef'] = self.payment_attempt.reference_number
        kwargs['vpc_Merchant'] = self.settings.merchant_id
        kwargs['vpc_OrderInfo'] = self.settings.get_merchant_identifier(self.payment_attempt)
        kwargs['vpc_ReturnURL'] = self.settings.get_response_url(self.payment_attempt)
        kwargs['vpc_Version'] = self._version
        # kwargs['vpc_Currency'] = self.settings.get_currency(self.payment_attempt)
        hash_ = self._get_hash(kwargs)
        kwargs.update({
            'vpc_SecureHash': hash_,
            'vpc_SecureHashType': 'SHA256'
        })
        logger.info('MiGS payload for %s %s' % (self.payment_attempt.reference_number, kwargs))
        return kwargs
