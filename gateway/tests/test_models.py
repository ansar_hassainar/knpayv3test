# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import json
from decimal import Decimal
from unittest import skip
from datetime import timedelta, datetime

from django.core.urlresolvers import reverse
from django.test.testcases import TestCase
from django.utils.timezone import now
from django.utils.translation import get_language, activate

import pytest
import requests
from mixer.backend.django import mixer
from mock import patch

from api.func import get_knpay_url
from utils.tests import knet_settings_factory, burgan_settings_factory, \
    cybersource_settings_factory, paypal_settings_factory, \
    migs_settings_factory, mpgs_settings_factory
from config.models import Config
from utils.funcs import shortify_url
from ..exceptions import FailedCondition
from ..models import PaymentTransaction, PaymentAttempt

pytestmark = pytest.mark.django_db


class TestPaymentTransaction(TestCase):
    def test_new_transaction_state(self):
        obj = mixer.blend(PaymentTransaction)
        assert obj.state == PaymentTransaction.CREATED, 'Each new transaction should be in CREATED state'

    def test_get_amount(self):
        obj = mixer.blend(PaymentTransaction, amount='245')
        assert type(obj.get_amount()) is Decimal, 'should return a Decimal instance'
        assert str(obj.get_amount()) == obj.amount, 'should return the saved amount'

    @skip('WIP')
    def test_encrypted_pk(self):
        obj = mixer.blend(PaymentTransaction)
        with patch('unfriendly.utils.utils', spec=False) as patcher:
            patcher.start()
            obj.encrypted_pk
            assert patcher.called, 'encrypt function should be called'

    def test_get_absolute_url(self):
        obj = mixer.blend(PaymentTransaction)
        assert obj.get_absolute_url() == reverse('dashboard:payment_request_detail', args=(obj.pk,)), 'should return the payment'

    @patch('utils.funcs.shortify_url')
    def test_get_payment_url(self, mock):
        obj = mixer.blend(PaymentTransaction, short_url='http://example.com/')
        assert obj.get_payment_url() == 'http://example.com/', 'if short url exists, it should be returned'

        obj = mixer.blend(PaymentTransaction, short_url='', type=PaymentTransaction.PAYMENT_REQUEST)
        dispatch_url = 'http://example.com%s' % reverse('gateway:dispatch', args=(obj.encrypted_pk,))

        def side_effect(*args, **kwargs):
            return dispatch_url

        mock.side_effect = side_effect

        assert obj.get_payment_url() == dispatch_url, 'if the transaction is payment request, ' \
                                                      'get_short_url should be called'

        obj = mixer.blend(PaymentTransaction, short_url='', type=PaymentTransaction.E_COMMERCE)
        dispatch_url = 'http://example.com%s' % reverse('gateway:dispatch', args=(obj.encrypted_pk,))

        assert obj.get_payment_url() == dispatch_url, 'if transaction is not payment request, it should ' \
                                                      'return by default the dispatch url without calling' \
                                                      'the shortify function'

    @patch('requests.post')
    def test_shortify(self, mock):
        obj = mixer.blend(PaymentTransaction, short_url='')

        def side_effect(*args, **kwargs):
            content = json.dumps({'shorturl': 'http://example.com'})
            response = requests.Response()
            response.status_code = requests.codes.ok
            response._content = content
            return response
        mock.side_effect = side_effect
        return_url = shortify_url('http://tmp.com')

        assert return_url == 'http://example.com', 'it should return the url from the http response'

    def test_get_payment_url_for_long_url(self):
        obj = mixer.blend(PaymentTransaction, short_url='')
        result = obj.get_payment_url()
        assert result == '{knpay_url}{path}'.format(knpay_url=get_knpay_url(),
                                                    path=reverse('gateway:dispatch', args=(obj.encrypted_pk,)))

    def test_get_payment_request_url(self):
        obj = mixer.blend(PaymentTransaction, language='ar')
        current_language = get_language()
        payment_request_url = reverse('ui:payment_request', args=(obj.encrypted_pk,))
        payment_request_url = payment_request_url.replace('en', 'ar')

        assert obj.get_payment_request_url() == payment_request_url, 'should return the ui payment ' \
                                                                     'request view url'

        new_language = get_language()

        assert current_language == new_language, 'the language should be switched back to user initial l' \
                                                 'language'

    @skip('WIP')
    def test_format_merchant_url(self):
        pass

    @skip('WIP')
    def test_get_attempt_url(self):
        pass

    def test_config(self):
        # Do not user Mixer because it's attempting to get Pause objects.
        # NO IDEA WHY.
        # config = mixer.blend('config.Config', secret_key='secret')
        obj = mixer.blend(PaymentTransaction)
        config = Config.get_solo()
        assert obj._config == config

    def test_get_merchant_redirect_url(self):
        obj = mixer.blend(PaymentTransaction, redirect_url='http://example.com')
        patcher = patch.object(PaymentTransaction, '_format_merchant_url', spec=False)
        patched = patcher.start()
        obj.get_merchant_redirect_url()
        assert patched.call_count == 1
        patched.assert_called_with('http://example.com')

    def test_has_gateway(self):
        obj = mixer.blend(PaymentTransaction)
        assert bool(obj.gateway_code) == obj.has_gateway(), 'should check against the existence of gateway field'

        obj = mixer.blend(PaymentTransaction, extra={"cart": 1})
        assert obj.extra == {'cart': 1}, 'should return dict'

    def test_is_paid(self):
        obj = mixer.blend(PaymentTransaction)
        obj.acknowledge_payment({'Test': 'test'}, False)
        assert obj.is_paid()

    @skip('WIP')
    def test_create_attempt(self):
        code = 'test-knet'
        settings = mixer.blend('knet.KnetSettings', code=code)
        obj = mixer.blend(PaymentTransaction)

        result = obj.create_attempt()
        assert '' == ''

    def test_is_e_commerce(self):
        obj = mixer.blend(PaymentTransaction, type=PaymentTransaction.E_COMMERCE)
        assert obj.is_e_commerce() is True

    def test_is_third_party(self):
        obj = mixer.blend(PaymentTransaction, type=PaymentTransaction.THIRD_PARTY)
        assert obj.is_third_party() is True

    def test_is_payment_request(self):
        obj = mixer.blend(PaymentTransaction, type=PaymentTransaction.PAYMENT_REQUEST)
        assert obj.is_payment_request() is True

    def test_icustomer_payment(self):
        obj = mixer.blend(PaymentTransaction, type=PaymentTransaction.CUSTOMER_PAYMENT)
        assert obj.is_customer_payment() is True

    def test_is_bulk(self):
        obj = mixer.blend(PaymentTransaction, bulk_id=1)
        assert obj.is_bulk() is True

    def test_is_pending(self):
        obj = mixer.blend(PaymentTransaction)
        obj.pending()
        obj.save()
        assert obj.is_pending() is True

    def test_is_canceled(self):
        obj = mixer.blend(PaymentTransaction)
        obj.cancel()
        obj.save()
        assert obj.is_canceled() is True

    def test_is_attempted(self):
        obj = mixer.blend(PaymentTransaction)
        obj.attempt()
        obj.save()
        assert obj.is_attempted() is True

    def test_can_be_cancelled(self):
        obj = mixer.blend(PaymentTransaction)
        obj.attempt()
        obj.save()
        assert obj.can_be_cancelled() is True

    def test_get_total_by_currency_code(self):
        obj = mixer.blend(PaymentTransaction, currency_code='KWD', amount=Decimal(12))
        assert PaymentTransaction.reports.all().get_total_by_currency_code('KWD') == Decimal(12)
        assert PaymentTransaction.reports.all().get_total_by_currency_code('USD') == None

    def test_get_currencies(self):
        mixer.blend(PaymentTransaction, currency_code='KWD', amount=Decimal(12))
        mixer.blend(PaymentTransaction, currency_code='USD', amount=Decimal(12))
        assert set(PaymentTransaction.reports.all().get_currencies()) == set(['KWD', 'USD'])

    def test_total_amount_by_gateway_code(self):
        mixer.blend(PaymentTransaction, currency_code='KWD', amount=Decimal(12), gateway_code='knet')
        mixer.blend(PaymentTransaction, currency_code='KWD', amount=Decimal(24), gateway_code='knet')
        mixer.blend(PaymentTransaction, currency_code='KWD', amount=Decimal(24), gateway_code='migs')
        assert PaymentTransaction.reports.all().get_total_amount_by_gateway_code('knet') == Decimal(36)
        assert PaymentTransaction.reports.all().get_total_amount_by_gateway_code('migs') == Decimal(24)

    def test_get_by_gateway_code(self):
        mixer.blend(PaymentTransaction, currency_code='KWD', amount=Decimal(12), gateway_code='knet')
        mixer.blend(PaymentTransaction, currency_code='KWD', amount=Decimal(24), gateway_code='knet')
        mixer.blend(PaymentTransaction, currency_code='KWD', amount=Decimal(24), gateway_code='migs')

        assert set(PaymentTransaction.reports.all().get_by_gateway_code('knet')) == set(PaymentTransaction.objects.filter(gateway_code='knet'))
        assert set(PaymentTransaction.reports.all().get_by_gateway_code('migs')) == set(PaymentTransaction.objects.filter(gateway_code='migs'))

    def test_get_within_interval(self):
        date = now()
        tomorrow = date + timedelta(days=1)
        past_date_start = date - timedelta(days=20)
        past_date_end = date - timedelta(days=13)

        mixer.blend(PaymentTransaction, currency_code='KWD', amount=Decimal(12), gateway_code='knet')
        mixer.blend(PaymentTransaction, currency_code='KWD', amount=Decimal(24), gateway_code='knet')
        mixer.blend(PaymentTransaction, currency_code='KWD', amount=Decimal(24), gateway_code='migs')

        assert PaymentTransaction.reports.all().get_within_interval(date, tomorrow).count() == 3
        assert PaymentTransaction.reports.all().get_within_interval(past_date_start, past_date_end).count() == 0

    def test_payable_conditions(self):
        assert PaymentTransaction.PAYABLE_CONDITIONS == ['meet_payable_states', 'gateway_is_available',
                                                         'currency_is_available',  'currency_exchange_is_available',
                                                         'not_expired']

    def test_meet_payable_states(self):
        obj = mixer.blend(PaymentTransaction)
        obj.cancel()

        try:
            obj._meet_payable_states()
        except FailedCondition:
            assert True

        obj = mixer.blend(PaymentTransaction)
        obj.invalidate('demo')

        try:
            obj._meet_payable_states()
        except FailedCondition:
            assert True

        obj = mixer.blend(PaymentTransaction)
        obj.expire()

        try:
            obj._meet_payable_states()
        except FailedCondition:
            assert True

        obj = mixer.blend(PaymentTransaction)
        assert obj._meet_payable_states() is None
        obj.pending()
        assert obj._meet_payable_states() is None
        obj.attempt()
        assert obj._meet_payable_states() is None

        obj.acknowledge_payment({'Test': 'test'}, False)

        try:
            obj._meet_payable_states()
        except FailedCondition:
            assert True

    def test_gateway_is_available(self):
        gateway = knet_settings_factory()
        obj = mixer.blend(PaymentTransaction, gateway_code=gateway.code)
        assert obj._gateway_is_available(gateway_code=gateway.code) is None
        gateway.delete()

        try:
            obj._gateway_is_available()
        except FailedCondition:
            assert True

    def test_currency_is_available(self):
        gateway = knet_settings_factory()
        currency = mixer.blend('currency.Currency', code='USD')
        gateway.currency_config.currencies.add(currency.pk)
        gateway.currency_config.save()

        obj = mixer.blend(PaymentTransaction, gateway_code=gateway.code, currency_code=currency.code)
        assert obj._currency_is_available(gateway_code=gateway.code) is None

        currency.delete()

        try:
            obj._currency_is_available(gateway_code=gateway.code)
        except FailedCondition:
            assert True

    def test_not_expired(self):
        obj = mixer.blend(PaymentTransaction)
        assert obj._not_expired() is None, 'if there is no config, transaction should be alive forever'
        config = Config.get_solo()
        config.transaction_expiration_time = ''
        config.save()
        assert obj._not_expired() is None, 'if transaction_expiration_time is null, transaction should be alive forever'

        obj.created = now() - timedelta(days=3), 'created 3 days ago'
        config.transaction_expiration_time = '72:00', 'hours'
        assert obj._not_expired() is None, 'a transaction created 3 days ago should be alive ' \
                                           'if expiration time is 3 days'

        obj.created = now() - timedelta(days=2), 'created 2 days ago'
        assert obj._not_expired() is None, 'a transaction created 2 days ago should be alive ' \
                                           'if expiration time is 3 days'

        obj.created = now() - timedelta(days=4), 'created 4 days ago'

        try:
            obj._not_expired()
        except FailedCondition:
            assert True, 'should be expired'

    def test_created_is_valid_for_payment(self):
        gateway_settings = knet_settings_factory()
        obj = mixer.blend(PaymentTransaction, gateway_code=gateway_settings.code, currency_code='KWD')
        assert obj.state == PaymentTransaction.CREATED
        obj.is_valid_for_payment() is True, 'when created, should valid for payment'
        assert obj.state == PaymentTransaction.CREATED, 'state should be not changed'

    def test_pending_is_valid_for_payment(self):
        gateway_settings = knet_settings_factory()
        obj = mixer.blend(PaymentTransaction, gateway_code=gateway_settings.code, currency_code='KWD')
        obj.pending()
        assert obj.is_valid_for_payment() is True
        assert obj.is_pending(), 'state should not change'

    def test_attempted_is_valid_for_payment(self):
        gateway_settings = knet_settings_factory()
        obj = mixer.blend(PaymentTransaction, gateway_code=gateway_settings.code, currency_code='KWD')
        obj.attempt()
        assert obj.is_valid_for_payment() is True
        assert obj.is_attempted(), 'state should not change'

    def test_paid_is_valid_for_payment(self):
        gateway_settings = knet_settings_factory()
        obj = mixer.blend(PaymentTransaction, gateway_code=gateway_settings.code, currency_code='KWD')
        obj.acknowledge_payment({'Test': 'test'}, False)
        assert obj.is_valid_for_payment() is False
        assert obj.is_paid(), 'state should not change'

    def test_canceled_is_valid_for_payment(self):
        gateway_settings = knet_settings_factory()
        obj = mixer.blend(PaymentTransaction, gateway_code=gateway_settings.code, currency_code='KWD')
        obj.cancel()
        assert obj.is_valid_for_payment() is False
        assert obj.is_canceled(), 'state should not change'

    def test_expired_is_valid_for_payment(self):
        gateway_settings = knet_settings_factory()
        obj = mixer.blend(PaymentTransaction, gateway_code=gateway_settings.code, currency_code='KWD')
        obj.expire()
        assert obj.is_valid_for_payment() is False
        assert obj.is_expired(), 'state should not change'

    def test_invalid_is_valid_for_payment(self):
        gateway_settings = knet_settings_factory()
        obj = mixer.blend(PaymentTransaction, gateway_code=gateway_settings.code, currency_code='KWD')
        obj.invalidate('really')
        assert obj.is_valid_for_payment() is False
        assert obj.is_invalid(), 'state should not change'

    def test_pending_with_expired_date_is_valid_for_payment(self):
        gateway_settings = knet_settings_factory()
        obj = mixer.blend(PaymentTransaction, gateway_code=gateway_settings.code, currency_code='KWD')
        obj.pending()
        config = Config.get_solo()
        config.transaction_expiration_time = '24:00'
        config.save()
        obj.created = now() - timedelta(days=2)

        assert obj.is_pending() is True, 'is in payable state'
        assert obj.is_valid_for_payment() is False, 'expired transaction should not be payable'
        assert obj.is_expired() is True, 'should be marked as expired'

    def test_invalid_gateway_is_valid_for_payment(self):
        gateway_settings = knet_settings_factory()
        obj = mixer.blend(PaymentTransaction, gateway_code=gateway_settings.code, currency_code='KWD')
        obj.pending()
        gateway_settings.delete()
        assert obj.is_valid_for_payment() is False, 'expired transaction should not be payable'
        assert obj.is_invalid() is True, 'should be marked as expired'
        assert 'Gateway' in obj.invalidation_reason, 'Gateway should be the failure reason'

    def test_invalid_currency_is_valid_for_payment(self):
        gateway_settings = knet_settings_factory()
        obj = mixer.blend(PaymentTransaction, gateway_code=gateway_settings.code, currency_code='USD')
        obj.pending()
        assert obj.is_valid_for_payment() is False, 'expired transaction should not be payable'
        assert obj.is_invalid() is True, 'should be marked as expired'
        assert 'Currency' in obj.invalidation_reason, 'Currency should be the failure reason'

    def test_kwarg_invalid_gateway_is_valid_for_payment(self):
        gateway_settings = knet_settings_factory()
        obj = mixer.blend(PaymentTransaction, gateway_code='', currency_code='USD')
        obj.pending()
        assert obj.is_valid_for_payment(gateway_code='lalaband') is False, 'gateway does not exists'
        assert obj.is_pending(), 'transaction shouldn\'t be invalidated for checking if they are payable ' \
                                 'with other gateways'


class TestPaymentAttempt(TestCase):
    def test_pending(self):
        gateway_settings = knet_settings_factory()
        tran = mixer.blend(PaymentTransaction, gateway_code=gateway_settings.code,
                           amount=Decimal(12), currency_code='KWD')
        tran.create_attempt(gateway_code=gateway_settings.code)
        tran.create_attempt(gateway_code=gateway_settings.code)
        tran.create_attempt(gateway_code=gateway_settings.code)
        obj = PaymentAttempt.objects.first()
        obj.reject()
        obj.save()
        assert PaymentAttempt.objects.pending().count() == 2

    def test_create_with_amount(self):
        gateway_settings = knet_settings_factory()
        tran = mixer.blend(PaymentTransaction, gateway_code=gateway_settings.code,
                           amount=Decimal(12), currency_code='KWD')
        PaymentAttempt.objects.create_with_amount(gateway_settings, Decimal(12), tran)
        obj = PaymentAttempt.objects.first()
        assert obj.total == '12.000'
        assert obj.transaction == tran
        assert obj.settings == gateway_settings
        assert obj.fee == '0.000'
        assert obj.amount == '12.000'

    def test_is_knet(self):
        from gateway.models import Gateway
        gateway_settings = knet_settings_factory()
        tran = mixer.blend(PaymentTransaction, gateway_code=gateway_settings.code,
                           amount=Decimal(12), currency_code='KWD')
        PaymentAttempt.objects.create_with_amount(gateway_settings, Decimal(12), tran)
        obj = PaymentAttempt.objects.first()
        assert obj.settings.gateway.name == Gateway.KNET

    def test_is_burgan(self):
        from gateway.models import Gateway
        gateway_settings = burgan_settings_factory()
        tran = mixer.blend(PaymentTransaction, gateway_code=gateway_settings.code,
                           amount=Decimal(12), currency_code='KWD')
        PaymentAttempt.objects.create_with_amount(gateway_settings, Decimal(12), tran)
        obj = PaymentAttempt.objects.first()
        assert obj.settings.gateway.name == Gateway.BURGAN

    def test_is_cs(self):
        from gateway.models import Gateway
        gateway_settings = cybersource_settings_factory()
        tran = mixer.blend(PaymentTransaction, gateway_code=gateway_settings.code,
                           amount=Decimal(12), currency_code='KWD')
        PaymentAttempt.objects.create_with_amount(gateway_settings, Decimal(12), tran)
        obj = PaymentAttempt.objects.first()
        assert obj.settings.gateway.name == Gateway.CYBERSOURCE

    def test_is_paypal(self):
        from gateway.models import Gateway
        gateway_settings = paypal_settings_factory()
        tran = mixer.blend(PaymentTransaction, gateway_code=gateway_settings.code,
                           amount=Decimal(12), currency_code='KWD')
        PaymentAttempt.objects.create_with_amount(gateway_settings, Decimal(12), tran)
        obj = PaymentAttempt.objects.first()
        assert obj.settings.gateway.name == Gateway.PAYPAL

    def test_is_migs(self):
        from gateway.models import Gateway
        gateway_settings = migs_settings_factory()
        tran = mixer.blend(PaymentTransaction, gateway_code=gateway_settings.code,
                           amount=Decimal(12), currency_code='KWD')
        PaymentAttempt.objects.create_with_amount(gateway_settings, Decimal(12), tran)
        obj = PaymentAttempt.objects.first()
        assert obj.settings.gateway.name == Gateway.MIGS

    def test_is_mpgs(self):
        from gateway.models import Gateway
        gateway_settings = mpgs_settings_factory()
        tran = mixer.blend(PaymentTransaction, gateway_code=gateway_settings.code,
                           amount=Decimal(12), currency_code='KWD')
        PaymentAttempt.objects.create_with_amount(gateway_settings, Decimal(12), tran)
        obj = PaymentAttempt.objects.first()
        assert obj.settings.gateway.name == Gateway.MPGS

    def test_is_cancelled(self):
        gateway_settings = knet_settings_factory()
        tran = mixer.blend(PaymentTransaction, amount=Decimal(12),
                          gateway_code=gateway_settings.code,
                          currency_code='KWD')
        tran.create_attempt(gateway_code=gateway_settings.code)
        obj = PaymentAttempt.objects.first()
        obj.reject()
        obj.save()
        assert obj.state == obj.CANCELED

    def test_is_success(self):
        gateway_settings = knet_settings_factory()
        tran = mixer.blend(PaymentTransaction, amount=Decimal(12),
                          gateway_code=gateway_settings.code,
                          currency_code='KWD')
        tran.create_attempt(gateway_code=gateway_settings.code)
        obj = PaymentAttempt.objects.first()
        obj.acknowledge_payment({'Test': 'test'}, False)
        obj.save()
        assert obj.state == obj.SUCCESS

    def test_is_failed(self):
        gateway_settings = knet_settings_factory()
        tran = mixer.blend(PaymentTransaction, amount=Decimal(12),
                          gateway_code=gateway_settings.code,
                          currency_code='KWD')
        tran.create_attempt(gateway_code=gateway_settings.code)
        obj = PaymentAttempt.objects.first()
        obj.fail({'Test': 'test'}, False)
        obj.save()
        assert obj.state == obj.FAILED

    def test_is_error(self):
        gateway_settings = knet_settings_factory()
        tran = mixer.blend(PaymentTransaction, amount=Decimal(12),
                          gateway_code=gateway_settings.code,
                          currency_code='KWD')
        tran.create_attempt(gateway_code=gateway_settings.code)
        obj = PaymentAttempt.objects.first()
        obj.error('test error')
        obj.save()
        assert obj.state == obj.ERROR

    def test_can_be_errored(self):
        gateway_settings = knet_settings_factory()
        tran = mixer.blend(PaymentTransaction, amount=Decimal(12),
                           gateway_code=gateway_settings.code,
                           currency_code='KWD')
        tran.create_attempt(gateway_code=gateway_settings.code)
        obj = PaymentAttempt.objects.first()
        assert obj.can_be_errored() is True, 'Pending transaction could be cancelled'

    def test_english_detail_url(self):
        gateway_settings = knet_settings_factory()
        tran = mixer.blend(PaymentTransaction, amount=Decimal(12),
                           gateway_code=gateway_settings.code,
                           currency_code='KWD', language='en')
        tran.create_attempt(gateway_code=gateway_settings.code)
        obj = PaymentAttempt.objects.first()
        activate('ar')
        url = obj.get_details_url()
        assert get_language() == 'en'
        assert url == '/en/pos/attempt-detail/'+obj.reference_number+'/'

    def test_arabic_detail_url(self):
        gateway_settings = knet_settings_factory()
        tran = mixer.blend(PaymentTransaction, amount=Decimal(12),
                           gateway_code=gateway_settings.code,
                           currency_code='KWD', language='ar')

        tran.create_attempt(gateway_code=gateway_settings.code)
        obj = PaymentAttempt.objects.first()
        activate('en')
        url = obj.get_details_url()
        assert get_language() == 'ar'
        assert url == '/ar/pos/attempt-detail/'+obj.reference_number+'/'

