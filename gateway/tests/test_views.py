
from __future__ import unicode_literals

from django.core.urlresolvers import resolve
from django.core.validators import URLValidator
from django.test import TestCase

from decimal import Decimal
import pytest
from mixer.backend.django import mixer
from mock import patch

from utils.tests import knet_settings_factory
from ..models import PaymentTransaction, PaymentAttempt

pytestmark = pytest.mark.django_db


class TestRedirectToPSPView(TestCase):

    def setUp(self):
        self.settings = knet_settings_factory()

    @patch('gateway.knet.client.KnetClient.__init__', return_value=None)
    @patch('gateway.knet.client.KnetClient.connect', return_value=None)
    def test_redirect_to_psp_when_it_is_down(self, moc1, moc2):
        transaction = mixer.blend(
            PaymentTransaction, gateway_code=self.settings.code, amount=Decimal(12), currency_code='KWD')
        response = self.client.get(
            '/gw/redirect-to-psp/%s' % transaction.encrypted_pk, follow=True)
        transaction = PaymentTransaction.objects.get(id =transaction.id )
        assert transaction.state == PaymentTransaction.FAILED , 'PaymentTransaction should be Failed'
        assert response.status_code == 200 , 'View should response with 200'

    @patch('gateway.knet.client.KnetClient.__init__', return_value=None)
    @patch('gateway.knet.client.KnetClient.connect', return_value='6536968592062610:http://example.com/example')
    def test_redirect_to_psp_when_it_is_connected(self, moc1, moc2):
        transaction = mixer.blend(
            PaymentTransaction, gateway_code=self.settings.code, amount=Decimal(12), currency_code='KWD')
        response = self.client.get(
            '/gw/redirect-to-psp/%s' % transaction.encrypted_pk, follow=True)
        transaction = PaymentTransaction.objects.get(id=transaction.id)
        attemp = transaction.get_latest_attempt()

        assert attemp.data == {
            'payment_id': '6536968592062610'}, 'attemp payment id should be updated'
        assert response.status_code == 404, 'return 404 as the redirect link not avilable in test '