# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _

from api.func import get_knpay_url
from gateway.models import GatewaySettings


class MPGSSettings(GatewaySettings):
    """
    MPGS settings model
    """
    HIDE = 'HIDE'
    READ_ONLY = 'READ_ONLY'
    BILLING_ADDRESS_CHOICES = (
        (HIDE, _("Hide")),
        ('MANDATORY', _("Mandatory")),
        ('OPTIONAL', _("OPTIONAL")),
        (READ_ONLY, _("READ_ONLY"))
    )
    merchant_id = models.CharField(_("Merchant ID"), max_length=256)
    password = models.CharField(_("Password"), max_length=256)
    billing_address = models.CharField(_("Billing address"), choices=BILLING_ADDRESS_CHOICES,
                                       default=HIDE, max_length=24,
                                       help_text=_("Indicates if you require the payer to provide "
                                                   "their billing address during the payment interaction"))

    def __unicode__(self):
        return self.merchant_id

    def has_to_show_address(self):
        return self.billing_address != self.HIDE

    class Meta:
        verbose_name = _("MPGS")
        verbose_name_plural = _("MPGS")
        permissions = (
            ('use_mpgssettings', _("Can use MPGS")),
        )

    def get_type(self):
        return 'test' if self.type == 'sandbox' else 'ap'

    @property
    def payment_url(self):
        return 'https://%s-gateway.mastercard.com/' \
               'api/rest/version/40/merchant/%s/session' % (self.get_type(), self.merchant_id)

    @staticmethod
    def create_payment_url(payment_attempt):
        from .client import MPGSClient
        client = MPGSClient(payment_attempt=payment_attempt)
        url = client.connect()
        return url

    def get_field_mapping(self):
        pass

    def get_currency(self, payment_attempt=None):
        return payment_attempt.transaction.currency_code

    def get_language(self, payment_attempt=None):
        return payment_attempt.transaction.language

    def get_error_url(self, payment_attempt=None):
        return '{knpay_url}{path}'.format(knpay_url=get_knpay_url(),
                                          path=reverse('mpgs:cancel', args=(payment_attempt.reference_number,)))

    def get_response_url(self, payment_attempt=None):
        return '{knpay_url}{path}'.format(knpay_url=get_knpay_url(),
                                          path=reverse('mpgs:response', args=(payment_attempt.reference_number,)))
