# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib import admin
from polymorphic.admin import PolymorphicChildModelAdmin
from .models import MPGSSettings


@admin.register(MPGSSettings)
class MPGSSettingsAdmin(PolymorphicChildModelAdmin):
    base_model = MPGSSettings
    prepopulated_fields = {'code': ('name',)}
    save_as = True
