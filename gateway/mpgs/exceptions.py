# -*- coding: utf-8 -*-

from __future__ import unicode_literals


class UnableToCreateSession(Exception):
    def __init__(self, message):
        self.message = message
