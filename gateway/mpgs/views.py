# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.views import generic
from django.views.generic.detail import SingleObjectMixin

from utils.views.mixins import PSPResponseMixin, PSPRedirectMixin
from gateway.models import PaymentAttempt
from .client import MPGSClient
from .exceptions import UnableToCreateSession


class PaymentView(PSPRedirectMixin, generic.DetailView):
    """
    Payment page, where user is paying
    """
    queryset = PaymentAttempt.objects.pending()
    slug_url_kwarg = 'reference_number'
    slug_field = 'reference_number'
    http_method_names = ['get']
    template_name = 'mpgs/payment.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        client = MPGSClient(self.object)

        try:
            session_id = client.create_session()
        except UnableToCreateSession as e:
            self.object.error(e.message)
            self.object.save()
            return self.redirect()

        context = self.get_context_data(
            object=self.object,
            session_id=session_id,
        )
        return self.render_to_response(context)


class ResponseView(SingleObjectMixin,
                   PSPResponseMixin,
                   generic.View):
    queryset = PaymentAttempt.objects.pending()
    slug_url_kwarg = 'reference_number'
    slug_field = 'reference_number'

    def handle_psp_response(self, data):
        if self.object.data.get('success_indicator', 0) == data.get('resultIndicator', 1):
            self.object.acknowledge_payment(self.request, data)
        else:
            self.object.fail(data)
        self.object.save()

        self.object.transaction.send_post_attempt_notifications(self.object)

        return self.redirect()


class CancelView(PSPResponseMixin,
                 SingleObjectMixin,
                 generic.View):
    queryset = PaymentAttempt.objects.pending()
    slug_url_kwarg = 'reference_number'
    slug_field = 'reference_number'

    def handle_psp_response(self, data):
        self.object.reject(data)
        self.object.save()
        return self.redirect()
