# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging
from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib.sites.models import Site
import requests
from gateway.client import BaseClient
from utils.funcs import format_country_code
from .exceptions import UnableToCreateSession
from .models import MPGSSettings


logger = logging.getLogger('gateway')


class MPGSClient(BaseClient):
    """
    MasterCard Payment Gateway API Reference: Operations - Version 41, Protocol: REST-JSON
    """

    def get_auth(self):
        return 'merchant.%s' % self.settings.merchant_id, self.settings.password

    def connect(self):
        site = Site.objects.get_current()
        path = reverse('mpgs:pay', args=(self.payment_attempt.reference_number,))
        return '%s://%s%s' % (settings.PROTOCOL, site.domain, path)

    def create_session(self):
        config = self.payment_attempt.settings
        payload = self._build_payload()
        logger.info('MPGS payload: %s for attempt [%s]' % (payload, self.payment_attempt.reference_number))
        response = requests.post(
            config.payment_url,
            json=payload,
            auth=self.get_auth()
        )
        logger.info('MPGS response: %s for attempt [%s]' % (response.content, self.payment_attempt.reference_number))
        if response.status_code in [200, 201]:
            content = response.json()
            session_id = content['session']['id']
            self.payment_attempt.data = dict(
                success_indicator=content['successIndicator'],
                session_id=session_id
            )
            self.payment_attempt.save()

            return content['session']['id']
        else:
            raise UnableToCreateSession(response.content)

    def _get_order(self):
        payment_attempt = self.payment_attempt
        config = payment_attempt.settings
        order = {
            'order': {
                'id': payment_attempt.reference_number,
                'amount': str(self.payment_attempt.get_amount()),
                'currency': config.currency_config.default_currency.code,
                # 'subMerchant': 'KNPay'
            }
        }
        if payment_attempt.transaction.order_no:
            order['order']['reference'] = config.get_merchant_identifier(payment_attempt)
        # if payment_attempt.transaction.extra:
        #     order['custom'] = payment_attempt.transaction.extra
        return order

    def _map(self, mapping):
        payment_transaction = self.payment_attempt.transaction
        return {value: getattr(payment_transaction, key) for key, value in mapping.items()
                if getattr(payment_transaction, key)}

    def _get_customer(self):
        mapping = {
            'customer_email': 'email',
            'customer_first_name': 'firstName',
            'customer_last_name': 'lastName',
            'customer_phone': 'mobilePhone'
        }
        customer = self._map(mapping)
        return {'customer': customer} if bool(customer) else None

    def _get_interaction(self):
        payment_attempt = self.payment_attempt
        config = payment_attempt.settings
        interaction = {
            'interaction': {
                'locale': payment_attempt.transaction.language,
                'displayControl': {
                    'billingAddress': config.billing_address
                },
                'returnUrl': config.get_response_url(self.payment_attempt),
                'cancelUrl': config.get_error_url(self.payment_attempt),
            }
        }
        if payment_attempt.transaction.customer_email:
            interaction['interaction']['displayControl']['customerEmail'] = MPGSSettings.READ_ONLY
        return interaction

    def _get_billing(self):
        payment_attempt = self.payment_attempt
        config = payment_attempt.settings
        if config.has_to_show_address():
            mapping = {
                'customer_address_line1': 'street',
                'customer_address_line2': 'street2',
                'customer_address_city': 'city',
                'customer_address_state': 'stateProvince',
                'customer_address_country': 'country',
                'customer_address_postal_code': 'postcodeZip',
            }
            address = self._map(mapping)
            try:
                address['customer_address_country'] = format_country_code(address['customer_address_country'], 'alpha_3')
            except KeyError:
                pass
            return {'billing': {'address': address}}

    def _build_payload(self):
        parameters = ['order', 'customer', 'interaction', 'billing']
        payload = {'apiOperation': 'CREATE_CHECKOUT_SESSION'}
        for param in parameters:
            val = getattr(self, '_get_%s' % param)()
            if val is not None:
                payload.update(val)
        return payload
