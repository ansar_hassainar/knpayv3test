# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^pay/(?P<reference_number>[0-9A-Za-z_\-]+)/$',
        views.PaymentView.as_view(), name='pay'),
    url(r'^response/(?P<reference_number>[0-9A-Za-z_\-]+)/$',
        views.ResponseView.as_view(), name='response'),
    url(r'^cancel/(?P<reference_number>[0-9A-Za-z_\-]+)/$',
        views.CancelView.as_view(), name='cancel'),
]
