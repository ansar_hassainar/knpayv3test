# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class GatewayConfig(AppConfig):
    name = 'gateway'
    verbose_name = _("Gateway")

    def ready(self):
        from actstream import registry
        registry.register(self.get_model('PaymentTransaction'))
        # registry.register(self.get_model('PaymentAtteo'))
