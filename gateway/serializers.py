# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from collections import OrderedDict
from decimal import Decimal

from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _, force_text as _ft
from phonenumber_field.validators import validate_international_phonenumber
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.fields import SkipField
from rest_framework.utils import html
from rest_framework.validators import UniqueValidator

from config.models import ECommerceConfig
from extras.currency.models import Currency, CurrencyExchange
from extras.dashboard.models import PaymentRequestFormConfig, Field
from gateway.models import GatewaySettings
from utils.funcs import prepare_payment_payload
from utils.rest.validators import AlphaNumericValidator, EmailListValidator
from .models import PaymentTransaction

# do not change the order
inbound_common_fields = [
    'gateway_code',
    'currency_code',
    'language',
    'amount',
    'order_no',
    'disclosure_url',
    'redirect_url',
    'customer_first_name',
    'customer_last_name',
    'customer_email',
    'customer_phone',
    'customer_address_line1',
    'customer_address_line2',
    'customer_address_city',
    'customer_address_state',
    'customer_address_country',
    'customer_address_postal_code',
    'extra',
    'email_payment_details',
    'sms_payment_details',
    'sms_notification',
    'sms_payment_details',
    'email_notification',
    'push_notification',
    'email_payment_details',
    'email_recipients',
    'attachment'
]

fetch_fields = [
    'transaction_id',
    'gateway_code',
    'currency_code',
    'amount',
    # 'fee',
    'amount',
    # 'total',
    'state',
    'transaction_created',
    'transaction_paid_at',
    'type'
]

# These are app specific extra fields only for backend operations.
# Merchant don't need to see these details
HIDDEN_EXTRA_FIELDS = [
    'registration_id'
]


class BaseTransactionSerializer(serializers.ModelSerializer):
    """
    Transaction model serializer
    """
    default_error_messages = {
        'required': _("This field may not be blank."),
        'email_required': _("Sending email requires Email field to be filled."),
        'phone_required': _("Sending SMS requires Phone field to be filled."),
    }
    amount = serializers.FloatField()
    language = serializers.ChoiceField(
        choices=settings.LANGUAGES, default=settings.LANGUAGE_CODE)
    customer_address_postal_code = serializers.CharField(
        validators=[AlphaNumericValidator()], required=False, max_length=12)
    sms_notification = serializers.BooleanField(default=False, required=False)
    sms_payment_details = serializers.BooleanField(default=False, required=False)
    extra = serializers.DictField(allow_null=True, required=False,
                                  child=serializers.CharField(allow_blank=True),)
    email_recipients = serializers.CharField(allow_blank=True,
                                             validators=[EmailListValidator()],
                                             required=False)

    def __init__(self, *args, **kwargs):
        self.fields['currency_code'] = serializers.ChoiceField(choices=Currency.objects.choices())
        self.gateway_settings = None
        super(BaseTransactionSerializer, self).__init__(*args, **kwargs)

    class Meta:
        model = PaymentTransaction
        fields = '__all__'

    def _get_gateway_settings(self):
        gateway_code = self.initial_data.get('gateway_code')
        try:
            return GatewaySettings.objects.get_for_code(gateway_code)
        except GatewaySettings.DoesNotExist:
            return None

    def validate_currency_code(self, value):
        self.gateway_settings = self._get_gateway_settings()
        if self.gateway_settings is not None:
            config = self.gateway_settings.currency_config  # currency config
            if not config.is_currency_code_accepted(value):
                raise serializers.ValidationError(
                    _("%s currency is not configured to be accepted by"
                      " %s.") % (value, self.gateway_settings.code))

            if (config.default_currency.code != value) and config.work_as == config.LOCAL:
                if not CurrencyExchange.objects.filter(from_code=value, to_code=config.default_currency).exists():
                    raise serializers.ValidationError(_("Rate %s to %s is not defined. Contact an "
                                                        "administrator to define it.") % (
                                                      value, config.default_currency))
        return value

    def _get_currency(self):
        if self.instance:
            currency_code = self.instance.currency_code
        else:
            currency_code = self.initial_data.get('currency_code')
        return Currency.objects.get(code=currency_code)

    def get_amount(self, validated_data):
        # validate amount against the currency_code
        currency = self._get_currency()
        if self.instance:
            amount = self.instance.amount
        else:
            amount = validated_data['amount']

        amount_field = serializers.DecimalField(
            max_digits=16,
            decimal_places=currency.decimal_places,
            coerce_to_string=True,
            min_value=Decimal('0.01')
        )
        try:
            amount_field.run_validation(amount)
        except Exception as e:
            raise serializers.ValidationError({'amount': e.detail})

        return amount_field.to_representation(amount)

    def validate_email_payment_details(self, value):
        email = bool(self.initial_data.get('customer_email'))
        if value and not email:
            raise serializers.ValidationError(self.default_error_messages['email_required'])
        return value

    def validate_email_notification(self, value):
        email = bool(self.initial_data.get('customer_email'))
        if value and not email:
            raise serializers.ValidationError(self.default_error_messages['email_required'])
        return value

    def validate_sms_notification(self, value):
        customer_phone = bool(self.initial_data.get('customer_phone'))
        if value and not customer_phone:
            raise serializers.ValidationError(self.default_error_messages['phone_required'])
        return value

    def validate_sms_payment_details(self, value):
        customer_phone = bool(self.initial_data.get('customer_phone'))
        if value and not customer_phone:
            raise serializers.ValidationError(self.default_error_messages['phone_required'])
        return value

    def validate(self, attrs):
        validated_data = super(BaseTransactionSerializer, self).validate(attrs)
        validated_data['amount'] = self.get_amount(validated_data)
        if self.gateway_settings is not None:
            validated_data['is_sandbox'] = self.gateway_settings.is_sandbox()

        return validated_data

    def create(self, validated_data):
        instance = super(BaseTransactionSerializer, self).create(validated_data)
        instance.send_payment_request_notifications(sms=instance.sms_notification,
                                                    email=instance.email_notification,
                                                    push=instance.push_notification)
        return instance


class ValidationSerializerMixin(serializers.Serializer):
    def _get_merchant_url(self, url, value):
        return value or getattr(self.config, url)

    def validate_redirect_url(self, value):
        return self._get_merchant_url('redirect_url', value)

    def validate_disclosure_url(self, value):
        return self._get_merchant_url('disclosure_url', value)


class ECommerceTransactionSerializer(ValidationSerializerMixin, BaseTransactionSerializer):
    """
    Public API
    """
    type = serializers.CharField(required=False, default=PaymentTransaction.E_COMMERCE, read_only=True)
    email_notification = serializers.BooleanField(default=False, required=False)
    email_payment_details = serializers.BooleanField(default=False, required=False)
    order_no = serializers.CharField(required=True, max_length=128,
                                     validators=[UniqueValidator(
                                         queryset=PaymentTransaction.objects.all())]
                                     )
    disclosure_url = serializers.URLField()
    redirect_url = serializers.URLField()

    def __init__(self, *args, **kwargs):
        self.config = ECommerceConfig.get_solo()
        kwargs.pop('fields', None)
        super(ECommerceTransactionSerializer, self).__init__(*args, **kwargs)
        self.fields['gateway_code'] = serializers.ChoiceField(
            choices=GatewaySettings.objects.choices(), required=True)
        self.fields['redirect_url'].required = not bool(self.config.redirect_url)
        self.fields['disclosure_url'].required = not bool(self.config.disclosure_url)

    class Meta(BaseTransactionSerializer.Meta):
        fields = inbound_common_fields + ['type']


class PrivateTransactionSerializer(BaseTransactionSerializer):
    order_no = serializers.CharField(required=False, max_length=128,
                                     validators=[UniqueValidator(
                                         queryset=PaymentTransaction.objects.all())]
                                     )
    email_notification = serializers.BooleanField(default=True, required=False)
    email_payment_details = serializers.BooleanField(default=True, required=False)
    type = serializers.ChoiceField(choices=PaymentTransaction.TYPE_CHOICES)
    bulk_id = serializers.CharField(required=False)
    initiator = serializers.PrimaryKeyRelatedField(queryset=get_user_model().objects.all(), required=False)

    class Meta(BaseTransactionSerializer.Meta):
        fields = inbound_common_fields + ['type', 'bulk_id', 'initiator']

    def __init__(self, *args, **kwargs):
        kwargs.pop('fields', None)
        self.fields['gateway_code'] = serializers.ChoiceField(
            choices=GatewaySettings.objects.choices(blank=True),
            required=False)
        super(PrivateTransactionSerializer, self).__init__(*args, **kwargs)


class ShopifyTransactionSerializer(BaseTransactionSerializer):
    type = serializers.CharField(required=False, default=PaymentTransaction.THIRD_PARTY)
    email_payment_details = serializers.BooleanField(required=False, default=True)

    class Meta(BaseTransactionSerializer.Meta):
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        kwargs.pop('fields', None)
        self.fields['gateway_code'] = serializers.ChoiceField(
            choices=GatewaySettings.objects.choices(blank=True),
            required=False)
        super(ShopifyTransactionSerializer, self).__init__(*args, **kwargs)


class BulkSaveListSerializerMixin(object):
    def create(self, validated_data):
        """
        Two fixes are applied here.
        1> many=True save
        2> Extra data save with custom fields
        3> Default email by user
        """
        payment_req_config = PaymentRequestFormConfig.get_solo()
        value = []
        for data in validated_data:
            extra = {}

            if 'customer_email' not in data or not data['customer_email']:
                data['customer_email'] = payment_req_config.default_email
                extra['has_default_email'] = True

            # In order to allow blank customer_email, in fields had to set
            # default False, now changing to True
            data['email_notification'] = True
            data['email_payment_details'] = True

            extra_fields = PaymentRequestFormConfig.get_solo().fields.active().values_list(
                'name', flat=True)

            for extra_field in extra_fields:
                if data.get(extra_field) is not None:
                    extra[extra_field] = data.get(extra_field)
                    del data[extra_field]
            if extra:
                data['extra'] = extra
            value.append(self.child.Meta.model.objects.create(**data))
        return value


class BulkListSerializer(BulkSaveListSerializerMixin, serializers.ListSerializer):
    """
    Issue: 5345
    https://github.com/encode/django-rest-framework/issues/5345
    Fix found in rejected PR as below.
    https://github.com/daggaz/django-rest-framework/commit/d0b85b28b78f7fe97cfb0a90046788b649ab5326#diff-80f43677c94659fbd60c8687cce68eafR634
    """
    def to_internal_value(self, data):
        """
        List of dicts of native values <- List of dicts of primitive datatypes.
        """
        if html.is_html_input(data):
            data = html.parse_html_list(data)

        if not isinstance(data, list):
            message = self.error_messages['not_a_list'].format(
                input_type=type(data).__name__
            )
            raise ValidationError({
                settings.NON_FIELD_ERRORS_KEY: [message]
            }, code='not_a_list')

        if not self.allow_empty and len(data) == 0:
            if self.parent and self.partial:
                raise SkipField()

            message = self.error_messages['empty']
            raise ValidationError({
                settings.NON_FIELD_ERRORS_KEY: [message]
            }, code='empty')

        ret = []
        errors = []

        for item in data:
            # added to fix initial_data for many=True
            self.child.initial_data = item
            try:
                validated = self.child.run_validation(item)
            except ValidationError as exc:
                errors.append(exc.detail)
            else:
                ret.append(validated)
                errors.append({})

        if any(errors):
            raise ValidationError(errors)

        return ret


class DashboardTransactionSerializer(ValidationSerializerMixin, BaseTransactionSerializer):
    """
    Serializer for dashboard endpoint
    """
    type = serializers.CharField(required=False,
                                 default=PaymentTransaction.PAYMENT_REQUEST, read_only=True)
    customer_email = serializers.EmailField(required=False, allow_blank=True)
    email_notification = serializers.BooleanField(default=False, required=False)
    email_payment_details = serializers.BooleanField(default=False, required=False)
    order_no = serializers.CharField(required=False, max_length=128,
                                     validators=[UniqueValidator(
                                         queryset=PaymentTransaction.objects.all())]
                                     )
    disclosure_url = serializers.URLField(required=False)
    redirect_url = serializers.URLField(required=False)
    initiator = serializers.PrimaryKeyRelatedField(queryset=get_user_model().objects.all(), required=True)
    initiator_name = serializers.SerializerMethodField(read_only=True)
    amount_string = serializers.SerializerMethodField(read_only=True)
    can_cancel = serializers.SerializerMethodField(read_only=True)
    details_fields = serializers.SerializerMethodField(read_only=True)
    notifications = serializers.SerializerMethodField(read_only=True)
    qr_code = serializers.ImageField(read_only=True)
    attachment_short_url = serializers.URLField(read_only=True)
    share_message = serializers.SerializerMethodField(read_only=True)

    def __init__(self, *args, **kwargs):
        kwargs.pop('fields', None)
        self.config = PaymentRequestFormConfig.get_solo()
        self.fields['gateway_code'] = serializers.ChoiceField(
            choices=GatewaySettings.objects.choices(blank=True),
            required=False)
        for field in self.config.fields.active():
            self.add_defined_field(field.get_name(), label=field.get_label(),
                                   required=field.required, field=field)
        super(DashboardTransactionSerializer, self).__init__(*args, **kwargs)

    def add_defined_field(self, name, label=None, initial=None, required=False,
                          field=None):
        field_klass = serializers.CharField
        self.fields[name] = field_klass(label=label or name, initial=initial,
                                        required=required, write_only=True)

    def get_initiator_name(self, obj):
        if obj.initiator:
            return obj.initiator.get_full_name()
        return "-"

    def get_amount_string(self, obj):
        return "{}".format(obj.amount)

    def get_can_cancel(self, obj):
        return obj.can_be_cancelled()

    def get_details_fields(self, obj):
        try:
            if obj.is_attempted():
                payload = prepare_payment_payload(obj.get_latest_attempt())
                return payload
        except:
            pass
        return self.get_details_screen(obj)

    def get_details_screen(self, obj):
        data = OrderedDict()

        data['Payment URL'] = obj.get_payment_url()
        data['Amount'] = obj.amount
        data['Gateway'] = obj.gateway_code
        data['Currency'] = obj.currency_code
        data['Language'] = obj.language

        extra = obj.extra
        for key, value in extra.items():
            if key in HIDDEN_EXTRA_FIELDS:
                continue
            field = Field.objects.get_for_name(key, self.config)
            label = field.get_label() if field is not None else key
            data[_ft(label)] = value

        for field in PaymentTransaction.CUSTOMER_FIELDS:
            if field == 'attachment':
                continue
            val = getattr(obj, field)
            if val:
                data[_ft(PaymentTransaction._meta.get_field(field).verbose_name)] = val
        return data

    def get_notifications(self, obj):
        data = {
            'email': obj.email_notification,
            'sms': obj.sms_notification,
            'push': obj.push_notification
        }
        return data

    def get_share_message(self, obj):
        return obj.get_whatsapp_template() or ''

    def create(self, validated_data):
        # To save extra data
        data = validated_data
        extra = {}
        payment_req_config = PaymentRequestFormConfig.get_solo()

        # default-email functionality
        # customer may not provide customer_email key or it's value
        if 'customer_email' not in data or not data['customer_email']:
            data['customer_email'] = payment_req_config.default_email
            extra['has_default_email'] = True
        # In order to allow blank customer_email, in fields had to set
        # default False, now changing to True
        data['email_notification'] = True
        data['email_payment_details'] = True

        extra_fields = payment_req_config.fields.active().values_list(
            'name', flat=True)

        for extra_field in extra_fields:
            if data.get(extra_field) is not None:
                extra[extra_field] = data.get(extra_field)
                del data[extra_field]
        if extra:
            data['extra'] = extra
        instance = super(DashboardTransactionSerializer, self).create(validated_data=data)
        return instance

    class Meta:
        model = PaymentTransaction
        fields = inbound_common_fields + ['type', 'initiator', 'state',
                                          'id', 'initiator_name',
                                          'amount_string', 'can_cancel',
                                          'details_fields',
                                          'notifications', 'qr_code',
                                          'share_message',
                                          'attachment_short_url']
        read_only_fields = ('id', )
        list_serializer_class = BulkListSerializer


class AppDashboardSerializer(DashboardTransactionSerializer):

    push_notification = serializers.BooleanField(default=True, required=False)

    def validate_customer_phone(self, customer_phone):
        if customer_phone and not customer_phone.startswith('+'):
            customer_phone = "+%s" % customer_phone
        validate_international_phonenumber(customer_phone)
        return customer_phone


class ReportSerializer(serializers.ModelSerializer):
    transaction_id = serializers.CharField(source='id')
    amount = serializers.CharField(source='paid_attempt_amount')
    fee = serializers.CharField(source='paid_attempt_fee')
    total = serializers.SerializerMethodField()
    transaction_created = serializers.DateTimeField(source='created')
    parent_gateway_code = serializers.SerializerMethodField()

    def get_total(self, obj):
        return obj.paid_attempt_total or obj.amount

    class Meta:
        model = PaymentTransaction
        fields = ('transaction_id', 'gateway_code', 'currency_code', 'amount',
                  'fee', 'total', 'state', 'transaction_created',
                  'type', 'state_changed_at', 'parent_gateway_code')

    def get_parent_gateway_code(self, obj):
        gateway_settings = obj.gateway_settings
        parent_gateway_code = gateway_settings.gateway.name if gateway_settings else ""
        return parent_gateway_code

class DiscloseToMerchantSerializer(serializers.Serializer):
    order_no = serializers.CharField(required=True, max_length=128)
    disclosure_url = serializers.URLField(required=False)

    def validate_order_no(self, value):
        try:
            self.transaction = PaymentTransaction.objects.get(order_no=value)
            return value
        except PaymentTransaction.DoesNotExist:
            raise serializers.ValidationError(_("Order number does not exist."))

    def validate(self, attrs):
        transaction = self.transaction
        if not transaction.has_payment_attempts():
            raise serializers.ValidationError(_("No attempts for this order."))
        return attrs
