# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.
from gateway.models import GatewaySettings
from .client import TwoCheckoutClient


class TwoCheckoutSettings(GatewaySettings):
    merchant_id = models.CharField(_("Merchant ID"), max_length=256)
    secret_key = models.TextField(_("Secret key"), max_length=256)

    def __unicode__(self):
        return self.merchant_id

    class Meta:
        verbose_name = _("TwoCheckout")
        verbose_name_plural = _("TwoCheckout")
        permissions = (
            ('use_twocheckoutsettings', _("Can use TwoCheckout")),
        )