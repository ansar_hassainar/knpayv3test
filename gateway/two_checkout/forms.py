# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django import forms


class PaymentForm(forms.Form):
    """
    TwoCheckoutHostPaymentForm
    """

    sid = forms.CharField(widget=forms.HiddenInput())

    mode = forms.CharField(widget=forms.HiddenInput())
    li_0_type = forms.CharField(widget=forms.HiddenInput())
    li_0_name = forms.CharField(widget=forms.HiddenInput())
    li_0_quantity = forms.CharField(widget=forms.HiddenInput())
    li_0_price = forms.CharField(widget=forms.HiddenInput())
    li_0_tangible = forms.CharField(widget=forms.HiddenInput())
    currency = forms.CharField(widget=forms.HiddenInput())
    name = forms.CharField(widget=forms.HiddenInput())
    street_address = forms.CharField(widget=forms.HiddenInput())
    city = forms.CharField(widget=forms.HiddenInput())
    state = forms.CharField(widget=forms.HiddenInput())
    zip = forms.CharField(widget=forms.HiddenInput())
    country = forms.CharField(widget=forms.HiddenInput())
    email = forms.CharField(widget=forms.HiddenInput())
    phone= forms.CharField(widget=forms.HiddenInput())
    phone_extension = forms.CharField(widget=forms.HiddenInput())
    x_receipt_link_url = forms.CharField(widget=forms.HiddenInput())
    purchase_step = forms.CharField(widget=forms.HiddenInput())
    transaction_pk = forms.CharField(widget=forms.HiddenInput())
