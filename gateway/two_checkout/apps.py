from __future__ import unicode_literals

from django.apps import AppConfig


class TwoCheckoutConfig(AppConfig):
    name = 'two_checkout'
