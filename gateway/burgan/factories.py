# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import factory

from gateway.factories import GatewayFactory
from .models import BurganSettings


class BurganSettingsFactory(factory.DjangoModelFactory):
    class Meta:
        model = BurganSettings

    name = 'burgan-test'
    gateway = factory.SubFactory(GatewayFactory)
    file = factory.django.FileField(from_path='gateway/burgan/test_data/resource_kwt.cgn')