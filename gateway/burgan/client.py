# # -*- coding: utf-8 -*-

import itertools
import logging
import re
import zipfile
import xml.etree.ElementTree as ET
from cStringIO import StringIO

from django.utils.translation import ugettext_lazy as _

import requests

from gateway.client import BaseClient


logger = logging.getLogger('gateway')


class BurganClient(BaseClient):
    name = _("Burgan")
    _action = 1  # Payment action on Burgan side
    _parsed_resource = dict()

    # Parameters found inside the resource file
    _resource_kwargs = ('id', 'password', 'passwordhash', 'webaddress', 'port', 'context',)

    def __init__(self, *args, **kwargs):
        super(BurganClient, self).__init__(*args, **kwargs)
        self._parse_resource()

    def _xor(self, file_string):
        # Key used to authenticate with Burgan
        key = 'Those who profess to favour freedom and yet depreciate agitation '
        key += 'are men who want rain without thunder and lightning'
        key = itertools.cycle(key)
        return ''.join(chr(ord(k) ^ ord(v)) for (k, v) in itertools.izip(file_string, key))

    def _parse_resource(self):
        # Create a temporary string file using the faster cStringIO module
        memory_file = StringIO()
        with open(self.settings.file.path, 'rb') as f:
            memory_file.write(self._xor(f.read()))

        # The resource is an XML file inside a ZIP archive
        try:
            xml = zipfile.ZipFile(memory_file)
        except zipfile.BadZipfile:
            raise zipfile.BadZipfile

        alias_name = self.settings.alias or xml.filelist[0].filename.split('.')[0]
        xml = self._xor(xml.read("{}.xml".format(alias_name)))

        # Grab the data from the resource file
        xml = ET.fromstring(xml)
        for kwarg in self._resource_kwargs:
            self._parsed_resource[kwarg] = xml.find(kwarg).text

    def connect(self):
        """
        Performs a request to Burgan banking service
        """
        try:
            psp_payload = self._build_payload()
            request_url = 'https://%(webaddress)s:%(port)s/%(context)s' % self._parsed_resource
            request_url += '/servlet/PaymentInitHTTPServlet'
            logger.info("Burgan request URL {}".format(request_url))
            response = requests.post(request_url, psp_payload)
            logger.info("Burgan response on url connect {}".format(response.text))
        except Exception as e:
            logger.exception("Error occurred while requesting payment url from Burgan. {}".format(e))
            return None

        if 'ERROR' in response.text:
            logger.exception("Error in Burgan response after requesting payment URL {}".format(response.text))
            return None
        else:
            logger.info("Burgan response after requesting payment URL {}".format(response.text))
            return response.text

    def _build_payload(self):
        data = {
            'id': self._parsed_resource['id'],
            'action': self._action,
            'amt': self.payment_attempt.get_amount(),
            'trackid': self.payment_attempt.reference_number,
            'currencycode': self.payment_attempt.settings.get_currency(self.payment_attempt),
            'langid': self.settings.get_language(self.payment_attempt),
            'errorURL': self.settings.get_error_url(self.payment_attempt),
            'responseURL': self.settings.get_response_url(self.payment_attempt),
            'password': self._parsed_resource['password'],
            'passwordhash': self._parsed_resource['passwordhash']
        }

        # Add the UDF values to the data connection dictionary
        # Disallowed characters are substituted with '-'
        disallowed = ['~', '`', '!', '#', '$', '%', '^', '|', '\\', ':', '\'', '"', '/']
        for key, value in self.settings.field_mapping.items():
            data[value] = re.sub('[%s]' % ''.join(disallowed), '-', getattr(self.payment_attempt, value, ''))
        logger.info("Burgan prepared payload {}".format(data))
        return data
