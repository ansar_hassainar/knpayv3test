from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class BurganConfig(AppConfig):
    name = 'burgan'
    verbose_name = _("Burgan")
