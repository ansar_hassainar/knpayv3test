# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib import admin
from polymorphic.admin import PolymorphicChildModelAdmin
from .models import BurganSettings


@admin.register(BurganSettings)
class BurganSettingsAdmin(PolymorphicChildModelAdmin):
    base_model = BurganSettings
    prepopulated_fields = {'code': ('name',)}
    save_as = True
