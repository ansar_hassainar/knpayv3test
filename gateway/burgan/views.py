# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic
from django.views.generic.detail import SingleObjectMixin

from braces.views import CsrfExemptMixin

from utils.views.mixins import PSPResponseMixin
from gateway.models import PaymentAttempt


logger = logging.getLogger('gateway')


class ResponseFromBurganView(CsrfExemptMixin,
                           PSPResponseMixin,
                           SingleObjectMixin,
                           generic.View):
    """
    Handle KNET response
    """
    queryset = PaymentAttempt.objects.pending()

    def post(self, request, *args, **kwargs):
        data = request.POST.copy()
        logger.info("Burgan payload sent to response URL {}".format(data))
        self.object = self.get_object()
        data = self.request.POST.copy().dict()
        return self.handle_psp_response(data)

    def handle_psp_response(self, data):

        if data['result'] == 'CAPTURED':
            self.object.acknowledge_payment(self.request, data)
        else:
            self.object.reject(data)
        self.object.save()

        url = 'REDIRECT={}'.format(
            self.request.build_absolute_uri(reverse('burgan:redirect', args=(self.object.pk,))))
        logger.info("Burgan REDIRECT URL %s" % url)
        return HttpResponse(url, 'text/plain')


class ErrorResponseFromBurganView(CsrfExemptMixin,
                                PSPResponseMixin,
                                SingleObjectMixin,
                                generic.View):
    queryset = PaymentAttempt.objects.pending()
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        data = request.GET.copy()
        logger.info("Burgan payload sent to response URL {}".format(data))
        self.object = self.get_object()
        data = self.request.GET.copy().dict()
        return self.handle_psp_response(data)

    def handle_psp_response(self, data):
        self.object.reject(data)
        self.object.save()

        url = self.request.build_absolute_uri(reverse('burgan:redirect', args=(self.object.pk,)))
        logger.info("Burgan REDIRECT URL %s" % url)
        return HttpResponseRedirect(url)


class RedirectFromBurganView(CsrfExemptMixin,
                           PSPResponseMixin,
                           SingleObjectMixin,
                           generic.View):
    """
    View for handling successful/unsuccessful redirects from Burgan Gateway.

    For transactions created via API we are POSTing back to the customer
    server transaction details - variable data is holding it -. Server
    replies with an URL where the user shall be redirected. More
    details below on return description.

    # E-Commerce transactions:
        # Notify the disclosure_url with payment details as JSON
        # if merchant's server returns 200 status, redirect the customer to redirect_url
        # else redirect to KNPay landing page with payment details
    """
    queryset = PaymentAttempt.objects.all()

    def handle_psp_response(self, data):
        return self.redirect()