# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^response/(?P<pk>\d+)/$', views.ResponseFromBurganView.as_view(), name='response'),
    url(r'^handler/(?P<pk>\d+)/$', views.RedirectFromBurganView.as_view(), name='redirect'),
    url(r'^error/(?P<pk>\d+)/$', views.ErrorResponseFromBurganView.as_view(), name='error'),
]
