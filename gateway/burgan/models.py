# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _

from api.func import get_knpay_url
from utils.validators import FileValidator
from gateway.models import GatewaySettings
from extras.currency.data import currencies
from .client import BurganClient

logger = logging.getLogger('gateway')


class BurganSettings(GatewaySettings):
    alias = models.CharField(_("Alias"), max_length=32, default='', blank=True,
                             help_text=_("Alias name if the resource file contains "
                                         "multiple aliases, if not leave it empty."))
    file = models.FileField(_("Resource CGN file"), upload_to='gateway/burgan/resource',
                            validators=[FileValidator(allowed_extensions=['cgn'])])

    class Meta:
        verbose_name = _("Burgan")
        verbose_name_plural = _("Burgan")
        permissions = (
            ('use_burgansettings', _("Can use Burgan")),
        )

    def get_field_mapping(self):
        return {
            'udf1': ['order_no'],
            'udf2': ['customer_email'],
            'udf3': ['customer_first_name', 'customer_last_name'],
            'udf4': ['customer_phone']
        }

    @staticmethod
    def create_payment_url(payment_attempt):
        client = BurganClient(payment_attempt)
        response = client.connect()
        if response is not None:
            response = response.split(':', 1)
            payment_id = response[0]
            payment_url = response[1] + '?PaymentID=' + payment_id
            payment_attempt.extra = {'payment_id': payment_id}
            return payment_url
        error_message = "Communication error between KNPay and gateway. " \
                        "Please report this issue to KNPay administrator."
        payment_attempt.error(error_message=error_message)
        payment_attempt.save()

    def get_language(self, payment_attempt=None):
        language = payment_attempt.transaction.language
        return 'ENG' if language == 'en' else 'ARA'

    def get_currency(self, payment_attempt=None):
        return int(currencies[payment_attempt.settings.currency_config.default_currency.code])

    def get_error_url(self, payment_attempt=None):
        # URL where Burgan will POST error details if happens (extremely rare)
        return '{knpay_url}{path}'.format(knpay_url=get_knpay_url(),
                                          path=reverse('burgan:error', args=(payment_attempt.pk,)))

    def get_response_url(self, payment_attempt=None):
        # URL where Burgan will POST payment details (no matter is success or failure)
        return '{knpay_url}{path}'.format(knpay_url=get_knpay_url(),
                                          path=reverse('burgan:response', args=(payment_attempt.pk,)))

