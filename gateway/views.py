# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views import generic
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django_fsm import TransitionNotAllowed

from utils.views.mixins import EncryptedPKMixin
from .models import PaymentTransaction


class DispatchView(EncryptedPKMixin, generic.View):
    """
    Validate if the payment transaction is available to redirect.
    If available, initialize the payment attempt else
    or return to error/invalid url
    """
    model = PaymentTransaction
    queryset = PaymentTransaction.objects.all()

    def get(self, request, *args, **kwargs):
        self.payment_transaction = self.get_object()

        # TODO: refactor attempt, because some states cannot be reverted to attempt state.
        if not self.payment_transaction.is_attempted():
            self.payment_transaction.pending()
            self.payment_transaction.save()

        return getattr(self, "handle_%s_transaction" % self.payment_transaction.type)()

    def handle_e_commerce_transaction(self):
        """
        E_COMMERCE PAYMENT (created through api)

        Redirect customer to redirect to psp page
        where will take care of necessary logic.
        """
        url = reverse('gateway:redirect_to_psp', args=(
            self.payment_transaction.encrypted_pk,))
        return self.redirect(url)

    def handle_payment_request_transaction(self):
        """
        PAYMENT REQUEST (dashboard/bulk)

        Redirect the customer to the payment details page
        where he can submit the payment form
        """
        return self.redirect(self.payment_transaction.get_payment_request_url())

    def handle_customer_payment_transaction(self):
        """
        CUSTOMER PAYMENT (created by himself)

        Create payment attempt and redirect customer to it
        """
        return self.redirect(self.payment_transaction.get_attempt_url())

    def handle_third_party_transaction(self):
        """
        THIRD PARTY (Shopify as of now)

        Create payment attempt and redirect customer to it
        """
        return self.redirect(self.payment_transaction.get_attempt_url())

    def handle_catalogue_transaction(self):
        """
        CATALOGUE CUSTOMER PAYMENT (created by himself)

        Create payment attempt and redirect customer to it
        """
        return self.redirect(self.payment_transaction.get_attempt_url())

    @staticmethod
    def redirect(url):
        return HttpResponseRedirect(url)


class RedirectToPSPView(EncryptedPKMixin, generic.View):
    # TODO: write more test cases for this logic
    """
    Redirect to PSP page
    Logic:

    1. if transaction is valid for payment, create a payment attempt
        a. if attempt is valid redirect customer to it
        b. if invalid, disclose information to merchant url
            - if merchant url replies 200, redirect customer back to merchant
            - else, redirect to internal failure page
    2. if transaction is not valid for payment:
        a. check if information was disclosed to merchant
            - if it was disclosed, check if the merchant reply was 200
                - if it was 200, redirect customer back to merchant page
                - if it wasn't, redirect to internal failure page
        b. if information wasn't disclosed to merchant
            - disclose it to merchant and
                - if merchant response is 200, redirect the customer back to merchant
                - else:
                    - check if was paid
    """
    queryset = PaymentTransaction.objects.e_commerce()

    def get(self, request, *args, **kwargs):
        payment_transaction = self.get_object()

        if payment_transaction.is_valid_for_payment():
            # get the attempt url
            # if e24PaymentPipe PSP or likewise, will return direct gateway page
            # else, will return an internal page which will submit the the form via js
            url = payment_transaction.get_attempt_url()
            if url:
                return HttpResponseRedirect(url)
            else:
                # means there was an error while connecting to psp
                # send the error details to the merchant and redirect the customer to
                # merchant error url
                attempt = payment_transaction.get_latest_attempt()
                # if failure, invalidate the transaction
                try:
                    payment_transaction.invalidate(attempt.message)
                    payment_transaction.save()
                except TransitionNotAllowed:
                    # transaction is already failed in case. KNPay couldn't call PSP (PSP down)
                    pass
                return self._disclose_to_merchant_and_redirect(payment_transaction)
        else:
            # check if error result was disclosed to the merchant
            if payment_transaction.was_disclosed_to_merchant():
                # if there was no error on disclosure
                # redirect back to merchant
                if payment_transaction.can_be_redirected_to_merchant():
                    return HttpResponseRedirect(payment_transaction.get_merchant_redirect_url())

                else:
                    return HttpResponseRedirect(payment_transaction.get_latest_attempt().get_details_url())
            else:
                return self._disclose_to_merchant_and_redirect(payment_transaction)

    @staticmethod
    def _disclose_to_merchant_and_redirect(payment_transaction):
        """
        check if payment_transaction has payment attempts
        if not, means it's invalid, so create one attempt and errorize it
        else, get the latest attempt
        """
        if payment_transaction.has_payment_attempts():
            payment_attempt = payment_transaction.get_latest_attempt()
        else:
            payment_attempt = payment_transaction.create_attempt()
            payment_attempt.error(payment_transaction.invalidation_reason)
            payment_attempt.save()
        payment_attempt.disclose_to_merchant()
        if payment_transaction.can_be_redirected_to_merchant():
            # redirect the customer back to the merchant
            url = payment_transaction.get_merchant_redirect_url()
        else:
            url = payment_attempt.get_details_url()
        return HttpResponseRedirect(url)


class AdminDiscloseTransaction(generic.DetailView):
    model = PaymentTransaction

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.state == self.object.PAID:
            disclosed = self.object.disclose_to_merchant()
            if disclosed:
                messages.success(request,
                                 _('Transaction details disclosed successfully to %s') % self.object.disclosure_url)
            else:
                messages.error(request,
                               _('Transaction details were not disclosed to %s. Please check disclose url error.') % self.object.disclosure_url)

        return HttpResponseRedirect(reverse('admin:gateway_paymenttransaction_change', args=(self.object.id,)))