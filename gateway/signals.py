# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.dispatch import Signal

payment_acknowledged = Signal(providing_args=["instance", "request"])
