from django.utils.translation import ugettext_lazy as _

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Field, Submit, Div, HTML, Reset


class PaymentTransactionFilterFormHelper(FormHelper):
    form_show_labels = False
    # disable_csrf = True
    form_class = 'transaction_filter_form'
    form_method = 'GET'
    layout = Layout(
            Row(
                Div( 
                    Row(
                        Div(
                            Div(
                                Div(
                                    HTML("{% load i18n %}<label>{% trans 'Created' %}</label>"),
                                    css_class='col-sm-2'
                                    ),
                                Div(
                                    Field('created_0', placeholder=_('From')),
                                    css_class='col-sm-5',
                                    ),
                                Div(
                                    Field('created_1', placeholder=_('To')),
                                    css_class='col-sm-5',
                                    ),
                                css_class='form-group row'
                                ),
                            css_class='col-lg-12'
                            ),
                       
                        ),
                    css_class='col-lg-8',
                    ),
                
                ),
            Row(
                Div( 
                    Row(
                        Div(
                            Div(
                                Div(
                                    HTML("{% load i18n %}<label>{% trans 'Modified' %}</label>"),
                                    css_class='col-sm-2'
                                    ),
                                Div(
                                    Field('modified_0', placeholder=_('From')),
                                    css_class='col-sm-5',
                                    ),
                                Div(
                                    Field('modified_1', placeholder=_('To')),
                                    css_class='col-sm-5',
                                    ),
                                css_class='form-group row'
                                ),
                            css_class='col-lg-12'
                            ),
                       
                        ),
                    css_class='col-lg-8',
                    ),
                
                ),
            Row(
                Div( 
                    Row(
                        Div(
                            Div(
                                Div(
                                    HTML("{% load i18n %}<label>{% trans 'Status' %}</label>"),
                                    css_class='col-sm-2'
                                    ),
                                Div(
                                    Field('state'),
                                    css_class='col-sm-4',
                                    ),
                                Div(
                                    HTML("{% load i18n %}<label>{% trans 'Gateway' %}</label>"),
                                    css_class='col-sm-2'
                                    ),
                                Div(
                                    Field('gateway_code'),
                                    css_class='col-sm-4',
                                    ),
                                css_class='form-group row'
                                ),
                            css_class='col-lg-12'
                            ),
                       
                        ),
                    css_class='col-lg-8',
                    ),
                
                ),
            Row(
                Div( 
                    Row(
                        Div(
                            Div(
                                Div(
                                    HTML("{% load i18n %}<label>{% trans 'Created by' %}</label>"),
                                    css_class='col-sm-2'
                                    ),
                                Div(
                                    Field('initiator'),
                                    css_class='col-sm-10',
                                    ),
                                Div(
                                    # Read description on extras/dashboard/bulk/helpers.py line 18
                                    HTML("{% load i18n %}<label>{% trans 'Keyword' %}</label>" + "".format({'keyword': _('Keyword')})),
                                    css_class='col-sm-2'
                                    ),
                                Div(
                                    Field('q', placeholder=_('Search in: order no, bulk id or customer information')),
                                    css_class='col-sm-8',
                                    ),
                                css_class='form-group row'
                                ),
                            css_class='col-lg-12'
                            ),
                       
                        ),
                    css_class='col-lg-8',
                    ),
                
                ),
            Row(
                Div( 
                    Row(
                        Div(
                            Div(
                                Div(
                                    HTML("<label>%s</label>" % ("Seen")) ,
                                    css_class='col-sm-2'
                                    ),
                                Div(
                                    Field('seen'),
                                    css_class='col-sm-4',
                                    ),
                                Div(
                                    HTML("<label>%s</label>" % ("Email Seen")) ,
                                    css_class='col-sm-2'
                                    ),
                                Div(
                                    Field('email_seen'),
                                    css_class='col-sm-4',
                                    ),
                                css_class='form-group row'
                                ),
                            css_class='col-lg-12'
                            ),
                       
                        ),
                    css_class='col-lg-8',
                    ),
                
                ),
            Row(
                Div( 
                    Row(
                        Div(
                            Div(
                                Div(
                                    Reset('reset', _('Clear')),
                                    Submit('submit', _('Submit')),
                                    css_class='col-sm-6'
                                    ),
                                css_class='form-group row'
                                ),
                            css_class='col-lg-12'
                            ),
                       
                        ),
                    css_class='col-lg-8',
                    ),
                
                ),
            )


class AllPaymentTransactionFilterFormHelper(FormHelper):
    form_show_labels = False
    form_class = 'transaction_filter_form'
    form_method = 'GET'
    layout = Layout(
        Row(
            Div(
                Row(
                    Div(
                        Div(
                            Div(
                                HTML(
                                    "{% load i18n %}<label>{% trans 'Status' %}</label>"),
                                css_class='col-sm-2'
                            ),
                            Div(
                                Field('state'),
                                css_class='col-sm-4',
                            ),
                            Div(
                                HTML(
                                    "{% load i18n %}<label>{% trans 'Gateway' %}</label>"),
                                css_class='col-sm-2'
                            ),
                            Div(
                                Field('gateway_code'),
                                css_class='col-sm-4',
                            ),
                            css_class='form-group row'
                        ),
                        css_class='col-lg-12'
                    ),

                ),
                css_class='col-lg-8',
            ),

        ),
        Row(
            Div(
                Row(
                    Div(
                        Div(
                            Div(
                                HTML(
                                    "{% load i18n %}<label>{% trans 'Type' %}</label>"),
                                css_class='col-sm-2'
                            ),
                            Div(
                                Field('type'),
                                css_class='col-sm-4',
                            ),
                            Div(
                                HTML(
                                    "{% load i18n %}<label>{% trans 'Created by' %}</label>"),
                                css_class='col-sm-2'
                            ),
                            Div(
                                Field('initiator'),
                                css_class='col-sm-4',
                            ),
                            css_class='form-group row'
                        ),
                        css_class='col-lg-12'
                    ),

                ),
                css_class='col-lg-8',
            ),

        ),
        Row(
            Div(
                Row(
                    Div(
                        Div(
                            Div(
                                # Read description on extras/dashboard/bulk/helpers.py line 18
                                HTML(
                                    "{% load i18n %}<label>{% trans 'Keyword' %}</label>" + "".format(
                                        {'keyword': _('Keyword')})),
                                css_class='col-sm-2'
                            ),
                            Div(
                                Field('q', placeholder=_(
                                    'Search in: order no, bulk id or customer information')),
                                css_class='col-sm-8',
                            ),
                            css_class='form-group row'
                        ),
                        css_class='col-lg-12'
                    ),

                ),
                css_class='col-lg-8',
            ),

        ),
        Row(
            Div(
                Row(
                    Div(
                        Div(
                            Div(
                                Reset('reset', _('Clear')),
                                Submit('submit', _('Submit')),
                                css_class='col-sm-6'
                            ),
                            css_class='form-group row'
                        ),
                        css_class='col-lg-12'
                    ),

                ),
                css_class='col-lg-8',
            ),

        ),
    )