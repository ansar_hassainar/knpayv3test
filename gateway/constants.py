# -*- coding: utf-8 -*-

from __future__ import unicode_literals


from django.utils.translation import ugettext_lazy as _


E_COMMERCE = 'e_commerce'
PAYMENT_REQUEST = 'payment_request'
CUSTOMER_PAYMENT = 'customer_payment'
THIRD_PARTY = 'third_party'
CATALOGUE = 'catalogue'
TYPE_CHOICES = (
    # request from API, usually e-commerce
    (E_COMMERCE, _("E-Commerce")),
    # Merchant requests for a payment to somebody
    (PAYMENT_REQUEST, _("Payment request")),
    # Merchant market and sells single product
    (CATALOGUE, _("Catalogue Purchase")),
    # customer pays by himself an amount to the merchant
    (CUSTOMER_PAYMENT, _("Customer payment")),
    # Similar to e-commerce, but from third party providers
    # through plugins. IE: Shopify and Air Cairo
    (THIRD_PARTY, _("Third party"))
)
